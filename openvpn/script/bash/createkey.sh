#!/bin/bash
client=$1
keydir=/etc/openvpn/easy-rsa/2.0
finalkeydir=/home/zodiacvpn/openvpn/keys/users
if [ x$client = x ]; then
    echo "Usage: $0 clientname"
    exit 1 
fi

if [ ! -e keys/$client.key ]; then
    echo "Generating keys..."
    cd $keydir
    . $keydir/vars
    export KEY_COUNTRY=$3
    export KEY_PROVINCE=$4
    export KEY_CITY=""
    export KEY_ORG="ZodiacVPN"
    export KEY_EMAIL=$2
    . $keydir/pkitool $client
    echo "...keys generated." 
fi

mkdir $finalkeydir/$client
cp $keydir/keys/$client* $finalkeydir/$client/
chown -R apache:apache $finalkeydir/$client
