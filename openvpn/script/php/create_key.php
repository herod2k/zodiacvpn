<?php
class openvpnkey{
	var $db_link=null;
	var $db=null;
	var $db_host='localhost';
	var $db_user='zodiacvpn';
	var $db_password='Y5eVBCv6YQJpLaZ4';
	var $db_name='zodiacvpn';
	var $bash_dir='/home/zodiacvpn/openvpn/script/bash/';
	var $user_keys_dir='/home/zodiacvpn/openvpn/keys/users/';

	private function connect_db(){
			$this->db_link = mysql_connect($this->db_host, $this->db_user, $this->db_password);
			if (!$this->db_link) {
					die('Could not connect: ' . mysql_error());
			}
			$this->db=mysql_select_db($this->db_name, $this->db_link);

	}

	private function disconnect_db(){
			mysql_close($this->db_link);
	}

	private function create_key($username,$email,$ip){
		$country=@geoip_country_name_by_name($ip);
                $country_code=@geoip_country_code_by_name($ip);
		$cmd=$this->bash_dir.'createkey.sh "'.$username.'" "'.$email.'" "'.$country_code.'" "'.$country.'"';
		exec($cmd);
		return true;
	}	
	
	private function create_password_file($username,$password){
		$fp=fopen($this->user_keys_dir.$username.'/'.$username.'.dat','w');
		fwrite($fp,$username."\n");	
		fwrite($fp,$password."\n");
		fclose($fp);	
	}
	private function createRandomPassword() { 
		$chars = "abcdefghijkmnopqrstuvwxyz023456789"; 
		srand((double)microtime()*1000000); 
		$i = 0; 
		$pass = '' ; 
		while ($i <= 7) { 
			$num = rand() % 33; 
			$tmp = substr($chars, $num, 1); 
			$pass = $pass . $tmp; 
			$i++; 
		} 
		return $pass; 

	}
	
	private function add_to_db($username,$password,$wp_user_id,$ip){
		$this->connect_db();
		$sql="INSERT INTO user(username, password, wp_user_id, valid, ip) VALUES('".$username."', ENCRYPT('".$password."'),'".$wp_user_id."',DATE_ADD(CURRENT_TIMESTAMP,INTERVAL 2 DAY),'".$ip."');";	
		mysql_query($sql,$this->db_link);
		
		$this->disconnect_db();
	}
	
	function create($username,$email,$wp_user_id,$ip){
		$this->create_key($username,$email,$ip);
		$password=$this->createRandomPassword();
		$this->create_password_file($username,$password);	
		$this->add_to_db($username,$password,$wp_user_id,$ip);
	}

}
if (count($argv)==5){
	$obj=new openvpnkey();
	$obj->create($argv[1],$argv[2],$argv[3],$argv[4]);
} else {
	echo "ERROR! The correct use is:"."\n";
	echo "php -f ".__FILE__." username email id ip"."\n";
}
?>
