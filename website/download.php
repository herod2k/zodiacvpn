<?php
error_reporting(E_ALL);
ini_set('display_errors', '1');

class download_vpn{
	var $vpn_id=1;
	var $os='windows';
	var $tmp_dir=null;
	var $username=null;

	function __construct($os,$id,$username){
		$this->setOs($os);
		$this->setVpnId($id);
		$this->setUserName($username);
		$this->createTmpDir();
		$this->copyGenericFiles();
		$this->copySpecificFiles();
		$this->changeConfValues();
		$this->renameConfFile();
		$this->osSpecificCommands();
		$this->compressDir();
	}

	private function setOs($os){
		$this->os=$os;
	}

	private function setVpnId($id){
                $this->vpn_id=$id;
	}
	
	private function setUserName($username){
                $this->username=$username;
        }
	
	private function generateRandomString($length = 10, $letters = '1234567890qwertyuiopasdfghjklzxcvbnm') { 
		$s = ''; 
		$lettersLength = strlen($letters)-1; 
		for($i = 0 ; $i < $length ; $i++) { 
			$s .= $letters[rand(0,$lettersLength)]; 
		} 
		return $s; 
	}

	private function createTmpDir(){
		$this->tmp_dir='zvpn_'.$this->generateRandomString(15);
		mkdir('/tmp/'.$this->tmp_dir, 0700);
        }

	private function copyGenericFiles(){
		copy ('../openvpn/keys/generic/zodiacvpn.crt','/tmp/'.$this->tmp_dir.'/zodiacvpn.crt');
                copy ('../openvpn/keys/generic/zodiacvpn.key','/tmp/'.$this->tmp_dir.'/zodiacvpn.key');
                exec ('cp ../openvpn/keys/generic/'.$this->os.'/* /tmp/'.$this->tmp_dir.'/');
	}

	private function copySpecificFiles(){
		copy ('../openvpn/keys/users/'.$this->getVpnCrtName().'/'.$this->getVpnCrtName().'.crt','/tmp/'.$this->tmp_dir.'/'.$this->getVpnCrtName().'.crt');
		copy ('../openvpn/keys/users/'.$this->getVpnCrtName().'/'.$this->getVpnCrtName().'.key','/tmp/'.$this->tmp_dir.'/'.$this->getVpnCrtName().'.key');
		copy ('../openvpn/keys/users/'.$this->getVpnCrtName().'/'.$this->getVpnCrtName().'.dat','/tmp/'.$this->tmp_dir.'/'.$this->getVpnCrtName().'.dat');
	}
	
	private function getVpnCrtName(){
		return $this->username.'_c'.$this->vpn_id;
	}
	
	private function changeConfValues(){
		exec ("sed -i \"s|%NOMECRT%|".$this->getVpnCrtName()."|g\" /tmp/".$this->tmp_dir."/client.ovpn");
	}

	private function renameConfFile(){
		rename ("/tmp/".$this->tmp_dir."/client.ovpn","/tmp/".$this->tmp_dir."/".$this->getVpnCrtName().".ovpn");
	}

	private function osSpecificCommands(){
		switch($this->os){
			case 'linux':
				rename ("/tmp/".$this->tmp_dir."/".$this->getVpnCrtName().".ovpn", "/tmp/".$this->tmp_dir."/".$this->getVpnCrtName().".conf");
			break;
			case 'apple':
				rename ("/tmp/".$this->tmp_dir."/".$this->getVpnCrtName().".dat", "/tmp/".$this->tmp_dir."/".$this->getVpnCrtName()."_password.txt");
			break;
		}
	}
		
	private function compressDir(){
		switch($this->os){
                        case 'apple':
			case 'windows':
                                exec ("zip -9 -j /tmp/".$this->tmp_dir.".zip /tmp/".$this->tmp_dir."/*");
				exec ("rm -Rf /tmp/".$this->tmp_dir."/");
                        	header('Content-Type: application/octet-stream');
				header('Content-Disposition: attachment; filename="'.$this->getVpnCrtName().'.zip"'); 
				header('Content-Transfer-Encoding: binary');
				readfile("/tmp/".$this->tmp_dir.".zip");
				unlink("/tmp/".$this->tmp_dir.".zip");
			break;
			case 'linux':
				exec ("cd /tmp/".$this->tmp_dir."; tar -pczf /tmp/".$this->tmp_dir.".tar.gz *");
                                exec ("rm -Rf /tmp/".$this->tmp_dir."/");
                                header('Content-Type: application/octet-stream');
                                header('Content-Disposition: attachment; filename="'.$this->getVpnCrtName().'.tar.gz"');
                                header('Content-Transfer-Encoding: binary');
                                readfile("/tmp/".$this->tmp_dir.".tar.gz");
                                unlink("/tmp/".$this->tmp_dir.".tar.gz");
			break;
                }
	}

	
}

include_once('wp-load.php' );
if (get_current_user_id()>0){
	if (!empty($_GET['format']) && !empty($_GET['id_vpn']) && $_GET['id_vpn']>0){
		$user_info = get_userdata(get_current_user_id());
		$obj = new download_vpn($_GET['format'],$_GET['id_vpn'],$user_info->user_login);
		die();
	}
} 
	header('location: http://www.zodiacvpn.com');
	exit();
?>
