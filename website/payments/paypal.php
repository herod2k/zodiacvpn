<?php
function log_vpn($post){
	$fp=fopen('/tmp/zodiacvpn.log','a');
	fwrite($fp,date('Y-m-d H:i:s').'  LOG  ############'."\n");
	fwrite($fp,print_r($post,true));
	fwrite($fp,print_r($_SERVER,true));
	fclose($fp);
}

function check_txnid($tnxid){
	global $wpdb;
	$valid_txnid = true;
	$sql = "SELECT * FROM `paypal` WHERE txnid = '".$tnxid."'";
	$payments=$wpdb->get_results($sql,ARRAY_A);
	if (count($payments)>0){
		return false;
	} else {
		return true;
	}
}

function insert_payment_db($certificate,$id_paypal,$wp_user_id,$value,$currency,$days){
	global $wpdb;
	$data['type']='paypal';
	$data['certificate']=$certificate;
	$data['id_payment']=$id_paypal;
	$data['wp_user_id']=$wp_user_id;
	$data['value']=$value;
	$data['currency']=$currency;
	$data['days']=$days;
	$wpdb->insert('payments',$data);
	return $wpdb->insert_id;
}

function get_new_certificate_name($wp_username,$wp_user_id){
	global $wpdb;
	$sql="SELECT * FROM `user` WHERE `wp_user_id`='".$wp_user_id."'";
	$certificates=$wpdb->get_results($sql,ARRAY_A);
	$n=count($certificates)+1;
	return $wp_username.'_c'.$n;
}
	
function generate_new_vpn_certificate($wp_username,$wp_user_id,$wp_user_ip){
	$user_info = get_userdata($wp_user_id);
	$new_certificate_name=get_new_certificate_name($wp_username,$wp_user_id);
	$cmq='../../openvpn/script/bash/create '.$new_certificate_name.' '.$user_info->user_email.' '.$wp_user_id.' '.$wp_user_ip;
	exec($cmq);
	return $new_certificate_name;
}

function insert_payment_data($data,$post){
	global $wpdb;
	#userdata
	$username=explode('_',$data['custom'][1]);
	$username=$username[0];
	$user=get_user_by('slug', $username);
	#pricedata
	
	$sql="SELECT * FROM prices WHERE code='".$data['custom'][0]."'";
	$prices=$wpdb->get_results($sql,ARRAY_A);
	#datainsert
	$sqldata['user_id']=$data['custom'][2];
	$sqldata['username']=$data['custom'][1];
	$sqldata['wp_user_id']=$user->ID;
	$sqldata['price_id']=$prices[0]['id'];
	$sqldata['item_name']=$data['item_name'];
	$sqldata['payment_status']=$data['payment_status'];
	$sqldata['payment_amount']=$data['payment_amount'];
	$sqldata['payment_currency']=$data['payment_currency'];
	$sqldata['txn_id']=$data['txn_id'];
	$sqldata['receiver_email']=$data['receiver_email'];
	$sqldata['payer_email']=$data['payer_email'];
	$sqldata['custom']=$post['custom'];
	$sqldata['post_data']= json_encode($post);
	$sqldata['ip']= $data['custom'][3];
	#query
	$wpdb->insert('paypal',$sqldata);
	if ($wpdb->insert_id>0)
	return $wpdb->insert_id;
}

function validate_payment_data($zodiacvpn_price_code,$zodiacvpn_vpn,$zodiacvpn_vpn_id){
	global $wpdb;
	if ($zodiacvpn_vpn!='new'){
		$username=explode('_',$zodiacvpn_vpn);
		$username=$username[0];
		$user=get_user_by('slug', $username);
		if ($user->ID>0){
			$sql="SELECT * FROM user where id='".$zodiacvpn_vpn_id."' AND username='".$zodiacvpn_vpn."' AND wp_user_id='".$user->ID."'";
			$users=$wpdb->get_results($sql,ARRAY_A);
			if (count($users)==1){
				$sql="SELECT * FROM prices WHERE code='".$zodiacvpn_price_code."'";
				$prices=$wpdb->get_results($sql,ARRAY_A);
				if (count($prices)==1){
					return true;
				} else {
					return false;
				}
			} else {
				return false;
			}
		} else {
			return false;
		}
	} else {
		return true;
	}
}

function get_days_by_code($code){
	global $wpdb;
	$mylink = $wpdb->get_row("SELECT * FROM `prices` WHERE code='".$code."' LIMIT 1", ARRAY_A);
	return $mylink['days'];
}

function extend_days_vpn($zodiacvpn_price_code,$zodiacvpn_vpn){
	global $wpdb;
	$days=get_days_by_code($code);
	$sql="SELECT * FROM user where username='".$zodiacvpn_vpn."'";
	$data=$wpdb->get_row($sql,ARRAY_A);
	$sqldata=array();
	$date_valid=strtotime($data['valid']);
	$date_actual=time();
	$days=get_days_by_code($zodiacvpn_price_code);
	$date['valid']=($date_valid>$date_actual)?date('Y-m-d H:i:s',$date_valid+($days * 24 * 60 * 60)):date('Y-m-d H:i:s',$date_actual+($days * 24 * 60 * 60));
	$sqldata['valid']=$date['valid'];
	$sqldata['active']=1;
	$sqldata['enabled']=1;
	$wpdb->update('user',$sqldata,array('username' => $zodiacvpn_vpn));	
}

log_vpn($_POST);
if (isset($_POST["txn_id"]) && isset($_POST["txn_type"])){
	#prepare data conexions and constants
	ini_set('sendmail_from', 'support@zodiacvpn.com');
	$header_email = "From: ZodiacVPN PayPal Module <support@zodiacvpn.com>\r\n";
	include_once('../wp-load.php' );
	
	$req['cmd'] = '=_notify-validate';
	foreach ($_POST as $key => $value) {
		$value = urlencode(stripslashes($value));
		$value = preg_replace('/(.*[^%^0^D])(%0A)(.*)/i','${1}%0D%0A${3}',$value);// IPN fix
		$req[$key]=$value;
	}
	$req=implode('&',$req);
	
	$data['item_name']=$_POST['item_name'];
	$data['item_number']=$_POST['item_number'];
	$data['payment_status']= $_POST['payment_status'];
	$data['payment_amount']=$_POST['mc_gross'];
	$data['payment_currency']=$_POST['mc_currency'];
	$data['txn_id']=$_POST['txn_id'];
	$data['receiver_email']=$_POST['receiver_email'];
	$data['payer_email']=$_POST['payer_email'];
	$data['custom']=explode('|',$_POST['custom']);
	
	 // post back to PayPal system to validate
	$header = "POST /cgi-bin/webscr HTTP/1.0\r\n";
	$header .= "Host: www.sandbox.paypal.com\r\n";
	$header .= "Content-Type: application/x-www-form-urlencoded\r\n";
	$header .= "Content-Length: " . strlen($req) . "\r\n\r\n";
	$fp = fsockopen ('ssl://www.sandbox.paypal.com', 443, $errno, $errstr, 30);
	if (!$fp) {
		mail ("support@zodiacvpn.com","Error Socket PayPal",$errstr."(".$errno.")<br />"."\n"."Dati ricevuti: ".print_r($_POST,true)."<br/>"."\n",$header_email);
	} else {
		fputs ($fp, $header.$req);
		$res = fgets ($fp, 1024);
		if (strcmp (trim($res), "HTTP/1.1 200 OK") == 0) {
			if (check_txnid($data['txn_id'])){
				if (validate_payment_data($data['custom'][0],$data['custom'][1],$data['custom'][2])){
					if ($data['custom'][1]=='new'){
						$zodiacvpn_vpn=generate_new_vpn_certificate($data['custom'][4],$data['custom'][5],$data['custom'][3]);
					}
					$paypal_id=insert_payment_data($data,$_POST);
					if ($paypal_id>0){
						if ($data['custom'][1]=='new'){
							$body_mail="E' stato generato un nuovo certificato: ".$zodiacvpn_vpn.' pagato '.$data['payment_amount'].$data['payment_currency']." assegnato all'utente ".$data['custom'][4];
						} else {
							$zodiacvpn_vpn=$data['custom'][1];
							$body_mail="E' stato rinnovato il certificato: ".$zodiacvpn_vpn.' pagato '.$data['payment_amount'].$data['payment_currency']." assegnato all'utente ".$data['custom'][4];
						}
						$zodiacvpn_price_code=$data['custom'][0];
						$result=insert_payment_db($zodiacvpn_vpn,$paypal_id,$data['custom'][5],$data['payment_amount'],$data['payment_currency'],get_days_by_code($data['custom'][0]));
						if ($result>0){
							extend_days_vpn($zodiacvpn_price_code,$zodiacvpn_vpn);
							mail ("payments@zodiacvpn.com","Payment PayPal",$body_mail."\n",$header_email);
						} else {
							mail ("support@zodiacvpn.com","Error Payment DB PayPal","Errore nell'inserimento nel DB nella tabella payments. CONTROLLARE."."<br/>"."\n"."Dati ricevuti: ".print_r($_POST,true)."<br/>"."\n",$header_email);
						}
					} else {
						mail ("support@zodiacvpn.com","Error Payment DB PayPal","Errore nell'inserimento nel DB nella tabella paypal. CONTROLLARE."."<br/>"."\n"."Dati ricevuti: ".print_r($_POST,true)."<br/>"."\n",$header_email);
					}
				} else {
					mail ("support@zodiacvpn.com","Error Payment Data PayPal","I valori del pagamento non combaciano tra di loro: prezzo, user id, wp_user_id etc etc. CONTROLLARE."."<br/>"."\n"."Dati ricevuti: ".print_r($_POST,true)."<br/>"."\n",$header_email);
				}
			} else {
				mail ("support@zodiacvpn.com","Error Payment id PayPal","Valore Txn_id duplicato! Qualcuno sta cercando di riutilizzare i dati di acquisto."."<br/>"."\n"."Dati ricevuti: ".print_r($_POST,true)."<br/>"."\n",$header_email);
			}
		} else {
			mail ("support@zodiacvpn.com","Error Verification PayPal","Risposta Verification Paypal: ".$res."\n\n"."Paypal Request: ".$header.$req."\n\n"."Dati ricevuti: ".print_r($_POST,true)."\n\n",$header_email);
		}	
	}
	fclose($fp);
} else {
	mail ("support@zodiacvpn.com","Error POST PayPal","Errore, stanno inviando male i dati del POST, possibile tentativo di hack della pagina paypal.php."."<br/>"."\n"."Dati della richiesta: POST=".print_r($_POST,true)."<br/>"."\n"." SERVER=".print_r($_POST,true));
	header('location: /');
}

?>
