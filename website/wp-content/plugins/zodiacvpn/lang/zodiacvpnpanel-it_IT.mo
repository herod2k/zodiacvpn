��    #      4  /   L           	               %     C     O     ]     e  :   j  <   �  E   �  ;   (  :   d  =   �     �     �               #     4     D     M  *   l  !   �     �  .   �  5   �  Q   4     �     �     �     �     �     �  �  �     �     �     �     �     �     	  	   	     	  <   $	  >   a	  L   �	  V   �	  <   D
  ?   �
     �
     �
     �
               )  	   8     B  B   a  '   �  #   �  *   �  <     n   X     �     �     �      �                              	          #   "                   !                      
                                                                                   Active until Amount Buy Now Buy a New VPN Certificate for Certificate Complete Name Created Date Download ZodiacVPN Certificate number %id_vpn% for Android Download ZodiacVPN Certificate number %id_vpn% for Apple OSX Download ZodiacVPN Certificate number %id_vpn% for GNU/Linux and Unix Download ZodiacVPN Certificate number %id_vpn% for Other OS Download ZodiacVPN Certificate number %id_vpn% for Windows Download ZodiacVPN Certificate number %id_vpn% for iPhone iOS Download it for Existents VPN Certificates New VPN Certificate Number Payments history Registered From Remaning Renew This VPN Certificate for The transaction has an error retry please. The transaction is gone perfectly There are no payments This Certificate has expired, you can renew it This Certificate is going to expire, you can renew it To install it you can read <a href="/how-to/"><span>our how-to section</span></a> Type User Informations VPN Certificate Number VPN Certificates Informations days e-Mail address Project-Id-Version: ZodiacVPN Panel
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2012-08-14 16:51+0800
PO-Revision-Date: 
Last-Translator: Alessandro <alessandro.liguori@djherod.com>
Language-Team: Zodiac Software <webmaster@zodiacvpn.com>
Language: it_IT
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-KeywordsList: __;_e
X-Poedit-Basepath: /srv/http/zodiacvpn/website/wp-content/plugins
X-Poedit-SearchPath-0: zodiacvpn
 Attivo fino al Valore Compra Compra un nuovo certificato VPN Certificato Nome completo Creato il Data Scarica il certificato ZodiacVPN numero %id_vpn% per Android Scarica il certificato ZodiacVPN numero %id_vpn% per Apple OSX Scarica il certificato ZodiacVPN numero %id_vpn% per iPhone GNU/Linux e Unix Scarica il certificato ZodiacVPN numero %id_vpn% per tutti gli altri sistemi operativi Scarica il certificato ZodiacVPN numero %id_vpn% per Windows Scarica il certificato ZodiacVPN numero %id_vpn% per iPhone iOS Scarica il certificato VPN per Certificati VPN esistenti Nuovo certificato VPN Numero Storico pagamenti Registrato dal Rimangono Rinnova questo certificato VPN E' avvenuto un errore durante la transazione, per favore riprovare La transazione è avvenuta con successo Non sono stati effettuati pagamenti Il certificato è scaduto, può rinnovarlo Il certificato sta per scadere, le consigliamo di rinnovarlo Per installare i nostri certificati può leggere <a href="/it/guide/"><span>la nostra sezione guide</span></a> Tipo Informazioni utente Certificato VPN numero Informazioni sui certificati VPN giorni Indirizzo email 