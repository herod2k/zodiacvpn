<?php
/***
Plugin Name: zodiacvpnpanel
Plugin URI: http://www.zodiacvpn.com
Description: Replace magics {zodiacvpnpanel} with zodiacvpn certificate panel
Version: 1.0.0
Author: Alessandro Liguori, alessandro.liguori@djherod.com
Author URI: http://djherod.com
Min WP Version: 2.5.0
Max WP Version: 3.4.2
***/
load_plugin_textdomain( 'zodiacvpnpanel', false, dirname( plugin_basename( __FILE__ ) ) . '/lang/' );

function _convert_currency_symbol($currency){
	$symbols=array();
	$symbols['USD']='$';
	$symbols['HKD']='HK$';
	$symbols['EUR']='&euro;';
	return $symbols[$currency];
}

function _date_diff($d1,$d2){
	$time_remaining=round(($d1-$d2)/86400);
	return $time_remaining;
}

function _get_vpn_prices(){
	global $wpdb;
	$sql="SELECT * FROM `prices` WHERE active=1";
	return $wpdb->get_results($sql,ARRAY_A);
}

function _get_price_by_code($code){
	global $wpdb;
	$sql="SELECT value FROM `prices` WHERE active=1 AND code='".$code."'";
	$res=$wpdb->get_results($sql,ARRAY_A);
	return $res[0]['value'];
}

function _get_days_by_code($code){
	global $wpdb;
	$mylink = $wpdb->get_row("SELECT * FROM `prices` WHERE code='".$code."' LIMIT 1", ARRAY_A);
	return $mylink['days'];
}

function _get_certificates_by_wpuid($wpuid){
	global $wpdb;
	$sql="SELECT * FROM `user` WHERE wp_user_id=".$wpuid;
	return $wpdb->get_results($sql,ARRAY_A);
}

function _get_vpn_paypal_panel($user_id,$cert_username='new',$id=''){
	$user_info = get_userdata($user_id);
	$vpnprices=_get_vpn_prices();
	$html='<div class="vpn_certificate_PayPal">';
	$html.='<form action="" method="post">';
	if ($cert_username!='new') {
		$html.=__('Renew This VPN Certificate for','zodiacvpnpanel').':';
	} else {
		$html.=__('Buy a New VPN Certificate for','zodiacvpnpanel').':';
	}
	$html.='<select name="zodiacvpn_price_code">';
	foreach ($vpnprices as $price){
	$html.='<option value="'.$price['code'].'">'.$price['days'].' '.__('days','zodiacvpnpanel').' - '.$price['value']._convert_currency_symbol($price['currency']).'</option>';
	}
	$html.='</select>';
	$html.='<input type="hidden" name="zodiacvpn_payment_type" value="paypal">';
	$html.='<input type="hidden" name="zodiacvpn_vpn_id" value="'.$id.'">';
	$html.='<input type="hidden" name="zodiacvpn_vpn" value="'.$cert_username.'">';
	$html.='<input type="hidden" name="zodiacvpn_action" value="payment">';
	$html.='<input type="hidden" name="zodiacvpn_user" value="'.$user_info->user_login.'|'.$user_info->ID.'">';
	$html.='<input type="hidden" name="module" value="zodiacvpn">';
	$html.='<input type="submit" value="'.__('Buy Now','zodiacvpnpanel').'">';
	$html.='</form>';
	$html.='</div>';
	return $html;
}

function _get_vpn_download_row($id_vpn=1){
	$html='<div class="vpn_download_row">';
        $html.='<div class="vpn_download_description">'.__('Download it for','zodiacvpnpanel').':</div>';
        $html.='<div class="vpn_download_logos">';
        $html.='<a href="/download.php?id_vpn='.$id_vpn.'&format=windows"><div class="windows vpn_so_logo" title="'.str_replace('%id_vpn%',$id_vpn,__('Download ZodiacVPN Certificate number %id_vpn% for Windows','zodiacvpnpanel')).'"></div></a>';
        $html.='<a href="/download.php?id_vpn='.$id_vpn.'&format=apple"><div class="apple vpn_so_logo" title="'.str_replace('%id_vpn%',$id_vpn,__('Download ZodiacVPN Certificate number %id_vpn% for Apple OSX','zodiacvpnpanel')).'"></div></a>';
        $html.='<a href="/download.php?id_vpn='.$id_vpn.'&format=linux"><div class="linux vpn_so_logo" title="'.str_replace('%id_vpn%',$id_vpn,__('Download ZodiacVPN Certificate number %id_vpn% for GNU/Linux and Unix','zodiacvpnpanel')).'"></div></a>';
        //$html.='<a href="/download.php?id_vpn='.$id_vpn.'&format=iphone"><div class="iphone vpn_so_logo" title="'.str_replace('%id_vpn%',$id_vpn,__('Download ZodiacVPN Certificate number %id_vpn% for iPhone iOS','zodiacvpnpanel')).'"></div></a>';
        //$html.='<a href="/download.php?id_vpn='.$id_vpn.'&format=android"><div class="android vpn_so_logo" title="'.str_replace('%id_vpn%',$id_vpn,__('Download ZodiacVPN Certificate number %id_vpn% for Android','zodiacvpnpanel')).'"></div></a>';
        //$html.='<a href="/download.php?id_vpn='.$id_vpn.'&format=others"><div class="others vpn_so_logo" title="'.str_replace('%id_vpn%',$id_vpn,__('Download ZodiacVPN Certificate number %id_vpn% for Other OS','zodiacvpnpanel')).'"></div></a>';
	
	$html.='</div>';
	$html.='<div class="vpn_download_howto">'.__('To install it you can read <a href="/how-to/"><span>our how-to section</span></a>','zodiacvpnpanel').'</div>';
	$html.='</div>';
	return $html;
}

function _get_the_panel_vpndata($user_id){
	$certificates = _get_certificates_by_wpuid($user_id);
	$html='<div id="panel_vpndata_container">';
	$html.='<div id="logo_certificate"></div><h2>'.__('VPN Certificates Informations','zodiacvpnpanel').'</h2>';
	$html.='<div id="panel_existent_certificate">';
	$html.='<div id="panel_vpn_message"><strong>'.__('You can\'t use a VPN certificate on differents devices at the same time, if you need to browse internet with differents devices at the same time you have to buy more than one VPN certificate','zodiacvpnpanel').'</strong></div>';
	$html.='<h3>'.__('Existents VPN Certificates','zodiacvpnpanel').'</h3>';
	$html.='<div id="panel_list_certificate">';
	foreach($certificates as $cert){
		$status=($cert['active']==1)?"active":"inactive";
		$name=explode('_',$cert['username']);
		$num_cert_key=count($name)-1;
		$num_cert=substr($name[$num_cert_key],1,strlen($name[$num_cert_key]));
		$html.='<div class="vpn_certificate_row '.$status.'">'; 
		$html.='<div class="vpn_certificate_name">'.__('VPN Certificate Number','zodiacvpnpanel').': '.$num_cert.'</div>';
		$date_start=strtotime($cert['creation']);
		$date_end=strtotime($cert['valid']);
		$now=time();
		$remaing_days=_date_diff($date_end,$now);
		$html.='<div class="vpn_certificate_date_relase"><span>'.__('Created','zodiacvpnpanel').':</span> '.date_i18n('l, j F Y - H:i', $date_start).'</div>';
		$html.='<div class="vpn_certificate_date_relase"><span>'.__('Active until','zodiacvpnpanel').':</span> '.date_i18n('l, j F Y - H:i', $date_end).'</div>';
		if ($remaing_days>=7){
			$html.='<div class="vpn_certificate_date_remaining green_vpn"><span>'.__('Remaning','zodiacvpnpanel').':</span><strong> '.$remaing_days.' '._n('day','days',$remaing_days,'zodiacvpnpanel').'</strong></div>';
		} else if ($remaing_days>0 && $remaing_days<7){
			$html.='<div class="vpn_certificate_date_remaining yellow_vpn"><span>'.__('Remaning','zodiacvpnpanel').':</span><strong> '.$remaing_days.' '._n('day','days',$remaing_days,'zodiacvpnpanel').' '.__('This Certificate is going to expire, you can renew it','zodiacvpnpanel').'</strong></div>';
		} else if ($remaing_days<1){
			$html.='<div class="vpn_certificate_date_remaining red_vpn"><span>'.__('Remaning','zodiacvpnpanel').':</span> 0 '._n('day','days',2,'zodiacvpnpanel').' - <strong>'.__('This Certificate has expired, you can renew it','zodiacvpnpanel').'</strong></div>';
		}
		$html.=_get_vpn_download_row($num_cert);
		$html.=_get_vpn_paypal_panel($user_id,$cert['username'],$cert['id']);
		$html.='</div>';
	}
	$html.='</div>';
	$html.='</div>';
	$html.='<div id="new_vpn_certificate">';
	$html.='<h3>'.__('New VPN Certificate','zodiacvpnpanel').'</h3>';
	$html.=_get_vpn_paypal_panel($user_id);
	$html.='</div>';
	$html.='</div>';
	return $html;
}

function _get_the_panel_userdata($user_id,$content=''){
	$user_info = get_userdata($user_id);
	$html='<div id="panel_userdata_container">';
	$html.='<div id="logo_user"></div><h2>'.__('User Informations','zodiacvpnpanel').'</h2>';
	$html.=$content;
	$html.='<div class="user_logout">'.__('<a href="/login/?a=logout">Logout</a>','zodiacvpnpanel').'</div>';
	$html.='<div class="user_label">'.__('Complete Name','zodiacvpnpanel').':</div>';
	$html.='<div class="user_data">'.$user_info->first_name.' '.$user_info->last_name.'</div>';
	$html.='<div class="user_label">'.__('e-Mail address','zodiacvpnpanel').':</div>';
	$html.='<div class="user_data">'.$user_info->user_email.'</div>';
	$html.='<div class="user_label">'.__('Registered From','zodiacvpnpanel').':</div>';
	$date=strtotime($user_info->user_registered);
	$html.='<div class="user_data">'.date_i18n('l, j F Y', $date).'</div>';
	$html.='</div>';
	return $html;
}

function _get_the_header_vpn_message($get){
	$html='';
	if (!empty($get['r'])){
		switch ($get['r']){
			case 'okvpn':
				$vpn_class_message='class="vpn_header_message green_vpn"';
				$vpn_message=__('The transaction is gone perfectly','zodiacvpnpanel');
			break;
			case 'kovpn':
				$vpn_class_message='class="vpn_header_message red_vpn"';
				$vpn_message=__('The transaction has an error retry please.','zodiacvpnpanel');
			break;
		}
		$html.='<div id="zodiacvpn_message" '.$vpn_class_message.'>';
		$html.=$vpn_message;
		$html.='</div>';
	}
	return $html;
}

function _get_payments($user_id){
	global $wpdb;
	$sql="SELECT * FROM `payments` where `wp_user_id`='".$user_id."' ORDER BY `date` DESC";
	return $wpdb->get_results($sql,ARRAY_A);
}

function _get_the_panel_payments($user_id){
	$payments=_get_payments($user_id);
	$html='<div id="zodiacvpn_payment_container">';
	$html.='<div id="logo_credits"></div>';
	$html.='<h2>'.__('Payments history','zodiacvpnpanel').'</h2>';
	if (count($payments)>0){
		
		$html.='<table>';
		$html.='<thead>';
		$html.='<tr>';
		$html.='<th>'.__('Number','zodiacvpnpanel').'</th>';
		$html.='<th>'.__('Type','zodiacvpnpanel').'</th>';
		$html.='<th>'.__('Date','zodiacvpnpanel').'</th>';
		$html.='<th>'.__('Certificate','zodiacvpnpanel').'</th>';
		$html.='<th>'.ucfirst(__('days','zodiacvpnpanel')).'</th>';
		$html.='<th>'.__('Amount','zodiacvpnpanel').'</th>';
		$html.='</tr>';
		$html.='</thead>';
		$html.='<tbody>';
		$total=0;
		$total_days=0;
		foreach ($payments as $payment){
			$tmp=explode('_',$payment['certificate']);
			$tmp_key=count($tmp)-1;
			$html.='<tr>';
			$html.='<td>'.$payment['id'].'</td>';
			$html.='<td>'.$payment['type'].'</td>';
			$html.='<td>'.$payment['date'].'</td>';
			$html.='<td> '.__('Certificate','zodiacvpnpanel').' '.substr($tmp[$tmp_key],1,strlen($tmp[$tmp_key])).'</td>';
			$html.='<td>'.$payment['days'].'</td>';
			$html.='<td>'.$payment['value']._convert_currency_symbol($payment['currency']).'</td>';
			$html.='</tr>';
			$total += $payment['value'];
			$total_days += $payment['days'];
		}
	
		$html.='</tbody>';
		$html.='<tfoot>';
		$html.='<tr>';
		$html.='<td></td><td></td><td></td>';
		$html.='<td>'.__('Total','zodiacvpnpanel').'</td>';
		$html.='<td>'.$total_days.'</td>';
		$html.='<td>'.$total._convert_currency_symbol($payment['currency']).'</td>';
		$html.='</tr>';
		$html.='</tfoot>';
		$html.='</table>';
	} else {
		$html.='<h3>'.__('There are no payments','zodiacvpnpanel').'</h3>';
	}
	$html.='</div>';
	
	return $html;
}

function _get_the_panel($user_id,$content=''){
	global $_GET;
	$user_info = get_userdata($user_id);
	$html='<div id="panel_container">';
	$html.=_get_the_header_vpn_message($_GET);
	$html.=_get_the_panel_userdata($user_id,$content);
	$html.=_get_the_panel_vpndata($user_id);
	$html.=_get_the_panel_payments($user_id);
	$html.='</div>';
	return $html;
}

function _get_back_button(){
	$html='<div id="vpn_button">';
	$html.='<span>'.__('Go back to <a href="/members-area/">members area</a>','zodiacvpnpanel').'</span>';
	$html.='</div>';
	return $html;
}

function zodiacvpnpanel_replace_magic($content){
	global $_GET,$_POST;
	if (get_current_user_id()>0){
		if (($matches = preg_match_all('/\{zodiacvpnpanel\}/', $content, $aResult)) !== false) {
			if (!empty($aResult[0]) && empty($_GET) && empty($_POST)){
				$content = str_replace('{zodiacvpnpanel}','',$content);
				$content = _get_the_panel(get_current_user_id(),$content);
			} elseif(!empty($_POST)) {
				$content .= _get_back_button();
			} elseif(!empty($_GET['r'])){
				$content = _get_the_header_vpn_message($_GET);
				$content .= _get_back_button();	
			}
		}
		
		
	} 
	$content = str_replace('{zodiacvpnpanel}','',$content);
	return $content;
}
add_filter('the_content', 'zodiacvpnpanel_replace_magic');

function _create_query_string($data){
	$strings=array();
	foreach ($data as $k=>$v){
		$strings[]=$k.'='.urlencode($v);
	}
	return implode('&',$strings);
}

function _get_language(){
	global $_SERVER;
	$url=$_SERVER["REQUEST_URI"];
	$url=explode('/',$url);
	$lang=(strlen($url[1])==2)?$url[1]:'en';
	return $lang;
}

function _go_to_paypal($post){
	global $_SERVER;
	$lang=_get_language();
	if ($post['zodiacvpn_vpn']=='new'){
		$item_name=sprintf(__('New Zodiac VPN certificate: %d days','zodiacvpnpanel'),_get_days_by_code($post['zodiacvpn_price_code']));
	} else {
		$tmp=explode('_',$post['zodiacvpn_vpn']);
		$tmp_key=count($tmp)-1;
		$item_name=sprintf(__('Renew Zodiac VPN cerficate number %d: %d days','zodiacvpnpanel'),substr($tmp[$tmp_key],1,strlen($tmp[$tmp_key])),_get_days_by_code($post['zodiacvpn_price_code']));
	}
	$urlbase='https://www.sandbox.paypal.com/cgi-bin/webscr'; #paypal sandbox
	$user_id=get_current_user_id();
	$user_info = get_userdata($user_id);
	$data=array();
	$data['cmd']="_xclick";
	$data['business']="alessa_1342409332_biz@djherod.com";
	$data['currency_code']="USD";
	$data['lc']=$lang;
	$data['bn']="PP-BuyNowBF:btn_buynow_LG.gif:NonHostedGuest";
	$data['item_name']=$item_name;
	$data['first_name']=$user_info->first_name;
	$data['last_name']=$user_info->last_name;
	$data['payer_email']=$user_info->user_email;
	$data['item_number']=$post['zodiacvpn_vpn_id'];
	$data['amount']=_get_price_by_code($post['zodiacvpn_price_code']);
	if ($lang=='en'){
		$data['return']='http://www.zodiacvpn.com/members-area/?r=okvpn';
		$data['cancel_return']='http://www.zodiacvpn.com/members-area/?r=kovpn';
	} else {
		$data['return']='http://www.zodiacvpn.com/'.$lang.'/members-area/?r=okvpn';
        	$data['cancel_return']='http://www.zodiacvpn.com/'.$lang.'/members-area/?r=kovpn';
	}
	$data['notify_url']='http://www.zodiacvpn.com/payments/paypal.php';
	$data['custom']=$post['zodiacvpn_price_code']."|".$post['zodiacvpn_vpn']."|".$post['zodiacvpn_vpn_id']."|".$_SERVER["REMOTE_ADDR"]."|".$post['zodiacvpn_user'];
	header('location: '.$urlbase.'?'._create_query_string($data));
	die();
}

function zodiacvpn_payment_gateway(){
	global $_POST;
    	if (!empty($_POST) && $_POST['module']=='zodiacvpn'){
		if ($_POST['zodiacvpn_payment_type']=='paypal'){
			_go_to_paypal($_POST);
		}
	}
}
add_action('pre_get_posts', 'zodiacvpn_payment_gateway');
?>
