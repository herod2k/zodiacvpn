<?php
/*
Plugin Name: Themes High Latest Posts
Plugin URI: http://www.themeshigh.com/
Description: Displays latests posts blog at a widget location by loading via ajax.
Author: Chang Master
Version: 1
Author URI: http://www.themeshigh.com/
*/

 add_action( 'widgets_init', 'latestposts_widget_init' );

 
 function latestposts_widget_init() {
	register_widget( 'Latestposts_widget' );
}


class Latestposts_widget extends WP_Widget {
	
	function Latestposts_widget(){
		$params1 = array( 'classname' => 'latestposts_widget', 'description' => esc_html__('Shows the latest posts via ajax', 'Current') );
		$params2 = array( 'width' => 150, 'height' => 350, 'id_base' => 'latestposts_widget' );
		$this->WP_Widget( 'latestposts_widget', esc_html__('Current Latest Posts', 'Current'), $params1, $params2 );
		
	}


	function update( $new_object, $old_object ) {
		$object = $old_object;

		$object['title'] = strip_tags( $new_object['title'] );
		$object['description'] =$new_object['description'];
		$object['categories'] = $new_object['categories'];
		$object['sortable'] = $new_object['sortable'];
		$object['itemsperpage'] = $new_object['itemsperpage'];
		$object['itemsperrow'] = $new_object['itemsperrow'];		
		$object['title_style'] = $new_object['title_style'];
		$object['author'] = $new_object['author'];
		$object['lightbox'] = $new_object['lightbox'];		
		$object['readmore'] = $new_object['readmore'];
		$object['readmore_text'] = $new_object['readmore_text'];		
		$object['thumbnails'] = $new_object['thumbnails'];
		$object['aspect'] = $new_object['aspect'];
		$object['navpos'] = $new_object['navpos'];
		$object['navcolor'] = $new_object['navcolor'];		
		return $object;
	}


	function widget( $params, $object ) {
		global $Current_options;
		extract( $params );
			
		$title = apply_filters('widget_title', $object['title'] );
		$description = absint( $object['description'] );
		$categories = $object['categories'];
		$sortable = isset( $object['sortable'] ) ? $object['sortable'] : false;
		$itemsperpage = absint( $object['itemsperpage'] );
		$itemsperrow = absint( $object['itemsperrow'] );
		$title_style = absint( $object['title_style'] );
		$author = isset( $object['author'] ) ? $object['author'] : false;
		$lightbox = isset( $object['lightbox'] ) ? $object['lightbox'] : false;		
		$readmore = isset( $object['readmore'] ) ? $object['readmore'] : false;
		$readmore_text = $object['readmore_text'];		
		$thumbnails = isset( $object['thumbnails'] ) ? $object['thumbnails'] : false;
		$aspect = (isset( $object['aspect'])  && is_numeric($object['aspect']) ) ? $object['aspect'] : $Current_options['image_aspect'];
		$navpos = isset( $object['navpos'] ) ? $object['navpos'] : false;
		$navcolor = isset( $object['navcolor'] ) ? $object['navcolor'] : false;
		
		echo $before_widget;
		?>
		<div class="plugin_latestposts_container<?php echo $navcolor?"":" desaturate";?>">
       <?php if( $title ) { ?>
       <div class="<?php echo ((sizeof($categories) < 2 || !$sortable) && !$navpos && $itemsperrow>1)?"alpha grid_".(6*$itemsperrow-6):""; ?>"><?php echo $before_title . $title . $after_title;?></div>
       <?php 
	   } 
	   if(!$navpos) {
	  	 	if(sizeof($categories) > 1 && $sortable) { ?>
       <div class="plugin_latestposts_sort_all grid_<?php echo 6*$itemsperrow-6;?> alpha"></div>
       		<?php } ?>
        <div class="navigation_all grid_6 omega" style="float:right;"></div>
        <?php } ?>
        
        <br class="clear" />
        	<div class="contents"></div>
	  <?php
	  if($navpos) { ?>
      <br class="clear" />
      <div class="bottomnavcontainer">
      <?php
	  	 	if(sizeof($categories) > 1 && $sortable) { ?>
       <div class="plugin_latestposts_sort_all grid_<?php echo 6*$itemsperrow-6;?> alpha"></div>
       		<?php } ?>
        <div class="navigation_all grid_6 omega" style="float:right;"></div>
        </div>
        <?php } ?>            
            <form class="plugin_latestposts_form">
            	<input type="hidden" name="categories" id="categories" value="<?php echo implode("|", $categories);?>" />
            	<input type="hidden" name="itemsperpage" id="itemsperpage" value="<?php echo $itemsperpage;?>" />
            	<input type="hidden" name="itemsperrow" id="itemsperrow" value="<?php echo $itemsperrow;?>" />
            	<input type="hidden" name="aspect" id="aspect" value="<?php echo $aspect;?>" /> 
                <input type="hidden" name="title_style" id="title_style" value="<?php echo $title_style;?>" />                
            	<input type="hidden" name="currentcategory" id="currentcategory" value="" />
            	<input type="hidden" name="description" id="description" value="<?php echo $description;?>" /> 
 				<input type="hidden" name="author" id="author" value="<?php echo $author;?>" />
 				<input type="hidden" name="readmore" id="readmore" value="<?php echo $readmore;?>" />
 				<input type="hidden" name="readmore_text" id="readmore_text" value="<?php echo $readmore_text;?>" />
                <input type="hidden" name="sortable" id="sortable" value="<?php echo $sortable;?>" />    
                <input type="hidden" name="thumbnails" id="thumbnails" value="<?php echo $thumbnails;?>" />
                <input type="hidden" name="lightbox" id="lightbox" value="<?php echo $lightbox;?>" />                
                                                                
            	<input type="hidden" name="paged" id="paged" value="" />                
            	<input type="hidden" name="action" id="action" value="plugin_latestposts_get" />

            </form>
        </div>
		<?php
		echo $after_widget;
	}	





	function form( $object ) {
		global $Current_options;
		
		$failsafe_values = array( 
			'title' => esc_html__('Recent Posts', 'Current'), 
			'categories' => '',
			'sortable' => false,
			'lightbox' => false,			
			'description' => 0,
			'itemsperpage' => 4,
			'itemsperrow' => 4,
			'title_style' => 1,
			'author' => false,
			'readmore' => false,
			'readmore_text' => esc_html__('Read more', 'Current'),
			'thumbnails' => true,
			'aspect' => $Current_options['image_aspect'],
			'navpos' => false,
			'navcolor' => true			
		);
		
		$object = wp_parse_args( (array) $object, $failsafe_values ); ?>

			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php esc_html_e('Title:', 'Current'); ?></label>
			<input id="<?php echo $this->get_field_id( 'title' ); ?>" type="text" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo $object['title']; ?>"/><br />


			<label for="<?php echo $this->get_field_id( 'categories' ); ?>"><?php esc_html_e('Categories:', 'Current'); ?></label><br />

            <?php $cats_array=get_categories(); ?>
            
			<select id="simple_blog_cats" multiple="multiple" name="<?php echo $this->get_field_name( 'categories' );?>[]">
			<?php 
					foreach ($cats_array as $cat_obj) {
						$cur_cat_ID = $cat_obj->term_id; ?>
						<option value="<?php echo $cur_cat_ID;?>" <?php echo is_array($object['categories'])?(in_array($cur_cat_ID, $object['categories'])?"selected='selected'":""):"";?>><?php echo $cat_obj->name; ?></option>
    <?php			} ?>
			</select>

			<br />
            
			<label for="<?php echo $this->get_field_id( 'sortable' ); ?>">
			    <input class="checkbox" type="checkbox" <?php checked( $object['sortable'], true ); ?> id="<?php echo $this->get_field_id( 'sortable' ); ?>" name="<?php echo $this->get_field_name( 'sortable' ); ?>" value="1" <?php checked('1', $object['sortable']); ?> />
			    <span style="font-size:0.9em"><?php esc_html_e('Show category sorting options in case of multiple categories', 'Current'); ?></span>
			</label>
		<br />            

		<label for="<?php echo $this->get_field_id( 'description' ); ?>"><?php esc_html_e('Post description:', 'Current'); ?></label>
			<select name="<?php echo $this->get_field_name( 'description' ); ?>"  id="<?php echo $this->get_field_id( 'title_style' ); ?>" >
                        <option value="0"<?php echo ($object['description']=='0')?' selected="selected"':''?>><?php esc_html_e('Do not display', 'Current'); ?></option>
                        <option value="1"<?php echo ($object['description']=='1')?' selected="selected"':''?>><?php esc_html_e('Post content', 'Current'); ?></option>
                        <option value="2"<?php echo ($object['description']=='2')?' selected="selected"':''?>><?php esc_html_e('Post excerpt', 'Current'); ?></option>
                        </select>
			<br />

			<label for="<?php echo $this->get_field_id( 'itemsperpage' ); ?>"><?php esc_html_e('Items per page', 'Current'); ?>:</label>
			<input id="<?php echo $this->get_field_id( 'itemsperpage' ); ?>" type="text" name="<?php echo $this->get_field_name( 'itemsperpage' ); ?>" value="<?php echo $object['itemsperpage']; ?>" size="2" maxlength="2" />
			<br />			


			<label for="<?php echo $this->get_field_id( 'itemsperrow' ); ?>"><?php esc_html_e('Columns per row', 'Current'); ?>:</label>
			<input id="<?php echo $this->get_field_id( 'itemsperrow' ); ?>" type="text" name="<?php echo $this->get_field_name( 'itemsperrow' ); ?>" value="<?php echo $object['itemsperrow']; ?>" size="2" maxlength="2" />
			<br />				
			
			
		<label for="<?php echo $this->get_field_id( 'title_style' ); ?>"><?php esc_html_e('Post title style:', 'Current'); ?></label>
			<select name="<?php echo $this->get_field_name( 'title_style' ); ?>"  id="<?php echo $this->get_field_id( 'title_style' ); ?>" >
                        <option value="0"<?php echo ($object['title_style']=='0')?' selected="selected"':''?>><?php esc_html_e('Do not display', 'Current'); ?></option>
                        <option value="1"<?php echo ($object['title_style']=='1')?' selected="selected"':''?>><?php esc_html_e('Display normal', 'Current'); ?></option>
                        <option value="2"<?php echo ($object['title_style']=='2')?' selected="selected"':''?>><?php esc_html_e('Link to the post', 'Current'); ?></option>
                        </select>
            
			<br />
            
			<label for="<?php echo $this->get_field_id( 'author' ); ?>">
			    <input class="checkbox" type="checkbox" <?php checked( $object['author'], true ); ?> id="<?php echo $this->get_field_id( 'author' ); ?>" name="<?php echo $this->get_field_name( 'author' ); ?>" value="1" <?php checked('1', $object['author']); ?> />
			    <?php esc_html_e('Show author and post date', 'Current'); ?>
			</label>
		<br />

			<label for="<?php echo $this->get_field_id( 'readmore' ); ?>">
			    <input class="checkbox" type="checkbox" <?php checked( $object['readmore'], true ); ?> id="<?php echo $this->get_field_id( 'readmore' ); ?>" name="<?php echo $this->get_field_name( 'readmore' ); ?>" value="1" <?php checked('1', $object['readmore']); ?> />
			    <?php esc_html_e('Show read more link ', 'Current'); ?>
			</label>
		<br />
			<label for="<?php echo $this->get_field_id( 'readmore_text' ); ?>">
			   <?php esc_html_e('Read more text', 'Current'); ?><input id="<?php echo $this->get_field_id( 'readmore_text' ); ?>" type="text" name="<?php echo $this->get_field_name( 'readmore_text' ); ?>" value="<?php echo $object['readmore_text']; ?>" />
			    
			</label>
		<br />        
		
			<label for="<?php echo $this->get_field_id( 'thumbnails' ); ?>">
			    <input class="checkbox" type="checkbox" <?php checked( $object['thumbnails'], true ); ?> id="<?php echo $this->get_field_id( 'thumbnails' ); ?>" name="<?php echo $this->get_field_name( 'thumbnails' ); ?>" value="1" <?php checked('1', $object['thumbnails']); ?> />
			    <?php esc_html_e('Show thumbnails', 'Current'); ?>
			</label><br />
            
			<label for="<?php echo $this->get_field_id( 'lightbox' ); ?>">
			    <input class="checkbox" type="checkbox" <?php checked( $object['lightbox'], true ); ?> id="<?php echo $this->get_field_id( 'lightbox' ); ?>" name="<?php echo $this->get_field_name( 'lightbox' ); ?>" value="1" <?php checked('1', $object['lightbox']); ?> />
			    <span class="legend"><?php esc_html_e('Have lightbox open up to display post media when clicked upon the thumbnails', 'Current'); ?></span>
			</label><br />
            
			<label for="<?php echo $this->get_field_id( 'navpos' ); ?>">
			    <input class="checkbox" type="checkbox" <?php checked( $object['navpos'], true ); ?> id="<?php echo $this->get_field_id( 'navpos' ); ?>" name="<?php echo $this->get_field_name( 'navpos' ); ?>" value="1" <?php checked('1', $object['navpos']); ?> />
			    <?php esc_html_e('Navigation at bottom', 'Current'); ?>
			</label><br />    
                               
			<?php
            /*<label for="<?php echo $this->get_field_id( 'navcolor' ); ?>">
			    <input class="checkbox" type="checkbox" <?php checked( $object['navcolor'], true ); ?> id="<?php echo $this->get_field_id( 'navcolor' ); ?>" name="<?php echo $this->get_field_name( 'navcolor' ); ?>" value="1" <?php checked('1', $object['navcolor']); ?> />
			    <?php esc_html_e('Buttons use theme style', 'Current'); ?>
			</label><br />   
            */?>
			<label for="<?php echo $this->get_field_id( 'aspect' ); ?>">
			   <?php esc_html_e('Aspect ratio', 'Current'); ?><input id="<?php echo $this->get_field_id( 'aspect' ); ?>" type="text" name="<?php echo $this->get_field_name( 'aspect' ); ?>" value="<?php echo $object['aspect']; ?>" />
			    
			</label> 
                        
<?php
	}


}

?>