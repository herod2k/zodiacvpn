<?php 

$root = dirname(dirname(dirname(dirname(dirname(__FILE__)))));

if ( file_exists( $root.'/wp-load.php' ) ) {
    require_once( $root.'/wp-load.php' );
} elseif ( file_exists( $root.'/wp-config.php' ) ) {
    require_once( $root.'/wp-config.php' );
}
header("Content-type: text/css");

global $mocktail_slider_array;
global $isfrontpage;
global $post;
if($_REQUEST['post']=='fp')
	$isfrontpage=true;
else {
	$isfrontpage=false;
	$post->ID = $_REQUEST['post'];	
}
foreach($mocktail_slider_array as $args) {
	if(willrender_mocktail_slider($args)) {
		if($isfrontpage)
			$slider=md5($Current_options['mocktail_slider_frontpage']);
		else
			$slider = md5(get_post_meta($post->ID, $args['id'].'_val', true));
?>
@charset "utf-8";

div#mocktail_slider_wrap {
	position:relative;
    height: <?php echo $Current_options['mocktail_'.$slider.'_height'] ;?>px;    
    }
div#mocktail_slider_bg {
    height: 100%;
    width: 100%;
    position:absolute;
    z-index:0;
}
div#mocktail_slider_container {
	position:relative;
    height: <?php echo $Current_options['mocktail_'.$slider.'_height'] ;?>px;
    z-index:999;
}

ul#jcycleslider > li {
	position:relative; 
    display:block; 
    background-image:none;
    padding-left:0 !important;
}
ul#jcycleslider li div.slide_component2, 
ul#jcycleslider li div.slide_component3 {
	position:absolute;
    top:0;
    left:0;
}
ul#jcycleslider > li { 
	margin-left:0;
}

<?php

	$numslides =sizeof(explode( '|', $Current_options['mocktail_'.$slider] ));
?>
ul#jcycleslider, ul#jcycleslider>li, .slide_component1, .slide_component2, .slide_component3, ul#jcycleslider>li img {width:100% !important;}

ul#jcycleslider {
	height:100%;
}

@media only screen and (min-width: 960px) {

    ul#jcycleslider>li .slide_component3 {
    	font-size:1em;
        }
}

@media only screen and (min-width: 768px) and (max-width: 959px) {
	div#mocktail_slider_container, div#mocktail_slider_wrap {
     	height: <?php echo ($Current_options['mocktail_'.$slider.'_height']/960)*768 ;?>px   
    }
   

    ul#jcycleslider>li .slide_component3 {
    	font-size:0.95em;
        }
    
}

@media only screen and (min-width: 480px) and (max-width: 767px) {
	div#mocktail_slider_container, div#mocktail_slider_wrap {
     	height: <?php echo ($Current_options['mocktail_'.$slider.'_height']/960)*480 ;?>px       
    }


    ul#jcycleslider>li .slide_component3 {
    	font-size:0.7em;
        }       
    
}

@media only screen and (max-width: 479px) {
	div#mocktail_slider_container, div#mocktail_slider_wrap {
     	height: <?php echo ($Current_options['mocktail_'.$slider.'_height']/960)*320 ;?>px       
    }

    
     ul#jcycleslider>li .slide_component3 {
    	font-size:0.5em;
        }   
}


<?php }
}?>