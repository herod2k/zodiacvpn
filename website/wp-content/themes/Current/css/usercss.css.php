<?php 

$root = dirname(dirname(dirname(dirname(dirname(__FILE__)))));

if ( file_exists( $root.'/wp-load.php' ) ) {
    require_once( $root.'/wp-load.php' );
} elseif ( file_exists( $root.'/wp-config.php' ) ) {
    require_once( $root.'/wp-config.php' );
}
global $post;
if($_REQUEST['post'] && $_REQUEST['post']!='')
	$post->ID = $_REQUEST['post'];

header("Content-type: text/css");

    ?>

.sf-menu li li,
.sf-menu li li li,
input, select, textarea,
dt.gallery-icon > a  {
        background-color:#<?php echo $Current_options['theme_color'];?>;
    }


    div#right_sidebar li, ul.menu li,
    div#left_sidebar li {
        margin-top:<?php echo $Current_options['line_height']/2;?>em;
        padding-bottom:<?php echo $Current_options['line_height']/2;?>em;
    }
    div#left_sidebar ul.children, div#right_sidebar ul.children, ul.menu ul.children {
		padding-top:<?php echo $Current_options['line_height']/2;?>em;
        margin-bottom:-<?php echo $Current_options['line_height']/2;?>em;
	}
    div.form_row, p.form-submit {
        height: <?php echo $Current_options['line_height']*3;?>em;
        margin-bottom: <?php echo $Current_options['line_height'];?>em;
        margin-top: 0;
    }
    div.form_row.textarea {
        height: <?php echo $Current_options['line_height']*8;?>em;
    }
    div.form_row.recaptcha {
        height: <?php echo $Current_options['line_height']*7;?>em;
    }
    textarea {
        height: <?php echo $Current_options['line_height']*6;?>em;
    }
    

    div.blogpostmetainfo {
            margin-top: <?php echo $Current_options['line_height'];?>em;
            margin-bottom: <?php echo $Current_options['line_height'];?>em;    
        }    
    

    
div.twitter_list ul#twitter_update_list {
	line-height:<?php echo $Current_options['line_height'];?>;
	margin-top:<?php echo $Current_options['line_height'];?>em;      
}
div.twitter_list ul#twitter_update_list li {
	line-height:<?php echo $Current_options['line_height'];?>;
	margin-bottom:<?php echo $Current_options['line_height'];?>em;  
}
div.tagcloud a {
	margin-bottom:<?php echo $Current_options['line_height'];?>em;
    }	
    
div.plugin_latestposts_container div.contents div.latestpostitem {   
    margin-bottom:<?php echo $Current_options['line_height'];?>em;
    }
    

div.page_section > div > div.container_24, div.latestpostitem div.item_thumbnail {
	background-color:#<?php echo $Current_options['theme_color'];?>;
}

div#left_sidebar > div, div#right_sidebar > div {
        margin-top: <?php echo $Current_options['line_height'];?>em;
        margin-bottom: <?php echo 2*$Current_options['line_height'];?>em;
    }

div#page_content > div > div > div, div#page_footer > div > div > div {
    margin-top: <?php echo $Current_options['line_height'];?>em;
    margin-bottom: <?php echo $Current_options['line_height'];?>em;
}

.custombuttonwrap a,
.custombuttonwrap a span,
.custombuttonwrap a:visited,
.custombuttonwrap a span:visited,
.custombuttonwrap a:hover,
.custombuttonwrap a:hover span
 {
    background-color:#<?php echo $Current_options['theme_color'];?>;
}


.inputbutton, 
input[type="submit"], 
button,
.inputbutton:hover, 
input[type="submit"]:hover, 
button:hover,
div.metahoverdisplay {
    background-color:#<?php echo $Current_options['theme_color'];?>;
}

.th_widget > h1,
.th_widget > h2,
.th_widget > h3,
.th_widget > h4,
.th_widget > h5,
.th_widget > h6,
.widget_title > h1,
.widget_title > h2,
.widget_title > h3,
.widget_title > h4,
.widget_title > h5,
.widget_title > h6,
.widgetheading {
	color:#<?php echo $Current_options['headings_color'];?>;
}

.ui-widget-content {
	padding-top:  <?php echo $Current_options['line_height']/2;?>em;
	padding-bottom:  <?php echo $Current_options['line_height']/2;?>em; 
}
.ui-tabs-nav {
    padding-bottom:   <?php echo $Current_options['line_height'];?>em;
    }
div#map, div.thumb, dt.gallery-icon {
    margin-bottom:   <?php echo $Current_options['line_height'];?>em;
}
div.blogpostmetaicons > a {
	height: <?php echo $Current_options['line_height'];?>em;
    width: <?php echo $Current_options['line_height'];?>em;
}


div.metahoverdisplay {
	top: <?php echo $Current_options['line_height']*1.5;?>em;
    }
    
input#recaptcha_response_field {color : #<?php echo $Current_options['css_tag_body_color_color'];?>;}