<?php 

$root = dirname(dirname(dirname(dirname(dirname(__FILE__)))));

if ( file_exists( $root.'/wp-load.php' ) ) {
    require_once( $root.'/wp-load.php' );
} elseif ( file_exists( $root.'/wp-config.php' ) ) {
    require_once( $root.'/wp-config.php' );
}
global $post, $h1cof, $h2cof, $h3cof, $h4cof, $h5cof, $h6cof, $leading;
if($_REQUEST['post'] && $_REQUEST['post']!='')
	$post->ID = $_REQUEST['post'];

header("Content-type: text/css");


dynamiccss_general_settings( array ('name' => 'Layout Settings','type' => 'general_settings','function' => 'settings','width' => '6','css,id,header_top,min-height,px' => 'Header height','css,id,navigation-menu,margin-top,px' => 'Main menu top margin','css,id,inset,margin-top,px' => 'Inset top margin','param,image_aspect' => 'Image aspect ratio','param,video_aspect' => 'Video aspect ratio','id' => 'layout_settings','row' => '0','column' => '0'));


dynamiccss_google_webfonts( array ('name' => 'Fonts Management','type' => 'google_webfonts','function' => 'settings','width' => '6','id' => 'fonts_management','row' => '0','column' => '1'));


dynamiccss_simple_background( array ('name' => 'page bg','type' => 'simple_background','function' => 'settings','width' => '6','id' => 'page_bg','row' => '0','column' => '2'));


dynamiccss_shortcode_button( array ('name' => 'shortcode button','type' => 'shortcode_button','function' => 'shortcode','width' => '6','id' => 'shortcode_button','row' => '0','column' => '3'));


dynamiccss_shortcode_tabsaccordion( array ('name' => 'shortcode tabs accordion','type' => 'shortcode_tabsaccordion','function' => 'shortcode','width' => '6','id' => 'shortcode_tabs_accordion','row' => '1','column' => '0'));


dynamiccss_shortcode_msgbox( array ('name' => 'shortcode msgbox','type' => 'shortcode_msgbox','function' => 'shortcode','width' => '6','id' => 'shortcode_msgbox','row' => '1','column' => '2'));


dynamiccss_shortcode_divider( array ('name' => 'shortcode divider','type' => 'shortcode_divider','function' => 'shortcode','width' => '6','id' => 'shortcode_divider','row' => '1','column' => '3'));


dynamiccss_plugin_latestposts( array ('name' => 'Latest Posts','type' => 'plugin_latestposts','function' => 'plugin','width' => '6','id' => 'latest_posts','row' => '2','column' => '0'));


dynamiccss_shortcode_quotesandcaps( array ('name' => 'Shortcode quotes and caps','type' => 'shortcode_quotesandcaps','function' => 'shortcode','width' => '6','id' => 'shortcode_quotes_and_caps','row' => '2','column' => '2'));


dynamiccss_shortcode_list( array ('name' => 'Shortcode List Style','type' => 'shortcode_list','function' => 'shortcode','width' => '6','id' => 'shortcode_list_style','row' => '2','column' => '3'));


dynamiccss_general_settings( array ('name' => 'Content Settings','type' => 'general_settings','function' => 'settings','width' => '6','param,excerpt_length' => 'Excerpt Length (words)','param,comments_posts,checkbox' => 'Display comments on posts','param,comments_pages,checkbox' => 'Display comments on pages','id' => 'content_settings','row' => '3','column' => '0'));


dynamiccss_general_settings( array ('name' => 'General Colors Configuration','type' => 'general_settings','function' => 'settings','width' => '6','param,theme_color,color' => 'Theme color','css,tag,body,color,color' => 'General text color','param,headings_color,color' => 'Headings color','css,tag,a#a:visited,color,color' => 'Links color','css,tag,a:hover,color,color' => 'Links hover color','css,tag,h1.pagetitle,color,color' => 'Page title color','id' => 'general_colors_configuration','row' => '3','column' => '1'));


dynamiccss_general_settings( array ('name' => 'Main Menu Colors','type' => 'general_settings','function' => 'settings','width' => '6','css,tag,ul.sf-menu>li>a#ul.sf-menu>li>a:visited,color,color' => 'Menu links color','css,tag,ul.sf-menu>li>a:hover,color,color' => 'Menu hover color','css,tag,ul.sf-menu ul.sub-menu a#ul.sf-menu ul.sub-menu a:visited,color,color' => 'Sub-menu links color','css,tag,ul.sf-menu ul.sub-menu a:hover,color,color' => 'Sub-menu hover color','id' => 'main_menu_colors','row' => '3','column' => '2'));


dynamiccss_simple_logo( array ('name' => 'Simple logo','type' => 'simple_logo','function' => 'logo','width' => '4','id' => 'simple_logo','row' => '7','column' => '0'));


dynamiccss_simple_portfolio( array ('name' => 'Simple Portfolio','type' => 'simple_portfolio','function' => 'page','image_frame' => 'image_frame','width' => '12','id' => 'simple_portfolio','row' => '20','column' => '1'));


function dynamiccss_general_settings($args = array()) { 

	global $Current_options;
	$cssids = array();
	$cssclasses = array();
	$csstags = array();	
	foreach($args as $key => $value) {
		if(strpos($key, ",")>0) {
			$exploded = explode(",", $key);
			if($exploded[0]=="css") {
				$unit=(isset($exploded[4])?$exploded[4]:"");
				$css = $exploded[3]." : ".(($unit=="color")?"#":"").$Current_options[ str_replace(" ", "_", str_replace(">","_", str_replace(":","_",str_replace(".","_",str_replace("#", "_", str_replace(",", "_", $key))))))].(($unit!="color")?$unit:"").";";
				$exploded[2]=str_replace("#", ",\n", $exploded[2]);
				
				if($exploded[1]=="id")
					$cssids[$exploded[2]] = isset($cssids[$exploded[2]])?$cssids[$exploded[2]]."\n".$css:$css;
			
				if($exploded[1]=="class")
					$cssclasses[$exploded[2]] = isset($cssclasses[$exploded[2]])?$cssclasses[$exploded[2]]."\n".$css:$css;
					
				if($exploded[1]=="tag")
					$csstags[$exploded[2]] = isset($csstags[$exploded[2]])?$csstags[$exploded[2]]."\n".$css:$css;	
				
			}
			
		}
	}
	
	foreach($cssids as $key => $val)
		echo "#".$key." {\n	".$val."\n}\n";
		
	foreach($cssclasses as $key => $val)
		echo "#".$key." {\n	".$val."\n}\n";	
		
	foreach($csstags as $key => $val)
		echo $key." {\n	".$val."\n}\n";			
	

 }
 

function dynamiccss_google_webfonts($args = array()) { 

	global $Current_options, $h1cof, $h2cof, $h3cof, $h4cof, $h5cof, $h6cof;
	$cssids = array();

		$font_family_style="normal";
		$splitted=explode(":",$Current_options['font_family']);
		$font_family=str_replace("+", " ",$splitted[0]);
		if(isset($splitted[1]))
			$splitted=explode("00",$splitted[1]);
		$font_family_weight=($splitted[0] != "")?$splitted[0]."00":"normal";
		if(isset($splitted[1]))
			$font_family_style=strtolower(($splitted[1] != "")?$splitted[1]:"normal");
		
		$splitted=explode(":",$Current_options['title_headings_font_family']);
		$title_headings_font_family=str_replace("+", " ",$splitted[0]);
		if(isset($splitted[1]))
		$splitted=explode("00",$splitted[1]);
		$title_headings_font_family_weight=($splitted[0] != "")?$splitted[0]."00":"normal";
		if(isset($splitted[1]))
		$title_headings_font_family_style=strtolower(($splitted[1] != "")?$splitted[1]:"normal");
		
		$splitted=explode(":",$Current_options['top_nav_font_family']);
		$top_nav_font_family=str_replace("+", " ",$splitted[0]);
		if(isset($splitted[1]))
		$splitted=explode("00",$splitted[1]);
		$top_nav_font_family_weight=($splitted[0] != "")?$splitted[0]."00":"normal";
		if(isset($splitted[1]))
		$top_nav_font_family_style=strtolower(($splitted[1] != "")?$splitted[1]:"normal");
		?>
		body { font-family:<?php echo $font_family; ?>; line-height:<?php echo $Current_options['line_height'];?>; font-size:<?php echo $Current_options['font_size']; ?>px; font-weight:<?php echo $font_family_weight; ?>; font-style:<?php echo $font_family_style; ?>;}
        
        <?php $leading=$Current_options['font_size']*$Current_options['line_height']; 

        $h1cof=2 * $Current_options['heading_font_size_coefficient'];
        $h2cof=1.6 * $Current_options['heading_font_size_coefficient'];
        $h3cof=1.2 * $Current_options['heading_font_size_coefficient'];
        $h4cof=1 * $Current_options['heading_font_size_coefficient'];
        $h5cof=0.89 * $Current_options['heading_font_size_coefficient'];
        $h6cof=0.7 * $Current_options['heading_font_size_coefficient'];
        ?>
        
        h1 {font-size:<?php echo $h1cof; ?>em !important; }
        h2 {font-size:<?php echo $h2cof; ?>em !important; }
        h3 {font-size:<?php echo $h3cof; ?>em !important; }
        h4 {font-size:<?php echo $h4cof; ?>em !important; }
        h5 {font-size:<?php echo $h5cof; ?>em !important; }
        h6 {font-size:<?php echo $h6cof; ?>em !important; }  
        
        h1, h2, h3, h4, h5, h6, #slogan { font-family:<?php echo $title_headings_font_family; ?>; font-weight:<?php echo $title_headings_font_family_weight; ?>; <?php echo isset($title_headings_font_family_style)?"font-style:".$title_headings_font_family_style.";":""; ?> }
        
        <?php if(isset($Current_options['heading_font_transform']) && $Current_options['heading_font_transform']!="none") { ?>
.th_widget > h1,
.th_widget > h2,
.th_widget > h3,
.th_widget > h4,
.th_widget > h5,
.th_widget > h6,
.widget_title > h1,
.widget_title > h2,
.widget_title > h3,
.widget_title > h4,
.widget_title > h5,
.widget_title > h6,
h1.pagetitle {
        text-transform: <?php echo $Current_options['heading_font_transform'];?>;
        }
  <?php } ?>      
        h1 {line-height:<?php echo $leading/($h1cof*$Current_options['font_size']);?>;
            margin-top:<?php echo $leading/($h1cof*$Current_options['font_size']);?>em;
            margin-bottom:<?php echo $leading/($h1cof*$Current_options['font_size']);?>em;}
        
        h2 {line-height:<?php echo $leading/($h2cof*$Current_options['font_size']);?>;
            margin-top:<?php echo $leading/($h2cof*$Current_options['font_size']);?>em;
            margin-bottom:<?php echo $leading/($h2cof*$Current_options['font_size']);?>em;}
         
        h3 {line-height:<?php echo $leading/($h3cof*$Current_options['font_size']);?>;
            margin-top:<?php echo $leading/($h3cof*$Current_options['font_size']);?>em;
            margin-bottom:<?php echo $leading/($h3cof*$Current_options['font_size']);?>em;}
        
        h4 {line-height:<?php echo $leading/($h4cof*$Current_options['font_size']);?>;
            margin-top:<?php echo $leading/($h4cof*$Current_options['font_size']);?>em;
            margin-bottom:<?php echo $leading/($h4cof*$Current_options['font_size']);?>em;}
            
        h5 {line-height:<?php echo $leading/($h5cof*$Current_options['font_size']);?>;
            margin-top:<?php echo $leading/($h5cof*$Current_options['font_size']);?>em;
            margin-bottom:<?php echo $leading/($h5cof*$Current_options['font_size']);?>em;}
            
        h6 {line-height:<?php echo $leading/($h6cof*$Current_options['font_size']);?>;
            margin-top:<?php echo $leading/($h6cof*$Current_options['font_size']);?>em;
            margin-bottom:<?php echo $leading/($h6cof*$Current_options['font_size']);?>em;} 
            
        p{
        	margin: <?php echo $Current_options['line_height'];?>em 0;
        }
        
  
        small {
        	font-size:0.85em;
        	line-height:<?php echo $leading/(0.85*$Current_options['font_size']);?>;
            margin-top:<?php echo $leading/(0.85*$Current_options['font_size']);?>em;
            margin-bottom:<?php echo $leading/(0.85*$Current_options['font_size']);?>em;
        }

       ul.sf-menu a, ul.sf-menu a span { font-family:<?php echo $top_nav_font_family; ?>; font-weight:<?php echo $top_nav_font_family_weight; ?>; 		
	   <?php echo isset($top_nav_font_family_style)?"font-style:".$top_nav_font_family_style.";":""; ?> font-size:<?php echo $Current_options['top_nav_font_size']; ?>px;<?php echo (isset($Current_options['top_nav_font_transform']) && $Current_options['top_nav_font_transform']!="none")?"text-transform:".$Current_options['top_nav_font_transform'].";":""; ?>}
		.sf-menu li li{font-size:<?php echo $Current_options['top_nav_sub_font_size'];?>em}
        
        
        
.sf-menu ul {

	width:			<?php echo (12/$Current_options['top_nav_sub_font_size'])+4;?>em !important;
}

ul.sf-menu li li:hover ul,
ul.sf-menu li li.sfHover ul {
	left:			<?php echo 12/$Current_options['top_nav_sub_font_size'];?>em !important;
}

ul.sf-menu li li li:hover ul,
ul.sf-menu li li li.sfHover ul {
	left:			<?php echo 12/$Current_options['top_nav_sub_font_size'];?>em !important;

}
		<?php }
 

function dynamiccss_simple_background($args = array()) { 

	global $Current_options, $simple_background_array, $post;

	foreach($simple_background_array as $simple_background_item) { ?>
		body {<?php if (isset($Current_options[$simple_background_item['id'].'_img']) && $Current_options[$simple_background_item['id'].'_img'] != '') { ?>background-image: url("<?php echo $Current_options[$simple_background_item['id'].'_img']; ?>"); background-repeat: <?php echo $Current_options[$simple_background_item['id'].'_img_repeat']; ?>; background-position: <?php echo $Current_options[$simple_background_item['id'].'_img_position_horizontal']." ".$Current_options[$simple_background_item['id'].'_img_position_vertical']; ?>;<?php } ?> background-color:#<?php echo $Current_options[$simple_background_item['id'].'_color']; ?>;
        <?php if(isset($Current_options[$simple_background_item['id'].'_fixed']) && $Current_options[$simple_background_item['id'].'_fixed']=="yes")
			echo "background-attachment:fixed;\n";?>
        
        }		
        
        body {<?php if (get_post_meta($post->ID, $simple_background_item['id'].'_img', true) != '') { ?>background-image: url("<?php echo get_post_meta($post->ID, $simple_background_item['id'].'_img', true); ?>"); background-repeat: <?php echo get_post_meta($post->ID, $simple_background_item['id'].'_img_repeat', true); ?>; background-position: <?php echo get_post_meta($post->ID, $simple_background_item['id'].'_img_position_horizontal', true); 
 echo get_post_meta($post->ID, $simple_background_item['id'].'_img_position_vertical', true); ?>;<?php } 
 if (get_post_meta($post->ID, $simple_background_item['id'].'_color', true) != '') { ?>background-color:#<?php echo get_post_meta($post->ID, $simple_background_item['id'].'_color', true).";";


		
		} ?>
     
        }
<?php
	}
	

	

 }
 

function dynamiccss_shortcode_button($args = array()) { 

	global $Current_options;
	$leading = $Current_options['font_size']*$Current_options['line_height'];
?>
div.custombuttonwrap.xl {
    padding-top:<?php echo ((($Current_options['font_size']*$Current_options['line_height'])-(52%($Current_options['font_size']*$Current_options['line_height'])))/$Current_options['font_size']/2);?>em;
    padding-bottom:<?php echo ((($Current_options['font_size']*$Current_options['line_height'])-(52%($Current_options['font_size']*$Current_options['line_height'])))/$Current_options['font_size']/2);?>em;

    margin-top:<?php echo 1/ceil(52/$leading);?>em;
    margin-bottom:<?php echo 1/ceil(52/$leading);?>em;        
}

div.custombuttonwrap.l {padding-top:<?php echo ((($Current_options['font_size']*$Current_options['line_height'])-(36%($Current_options['font_size']*$Current_options['line_height'])))/$Current_options['font_size']/2);?>em;padding-bottom:<?php echo ((($Current_options['font_size']*$Current_options['line_height'])-(36%($Current_options['font_size']*$Current_options['line_height'])))/$Current_options['font_size']/2);?>em;

    margin-top:<?php echo 1/ceil(36/$leading);?>em;
    margin-bottom:<?php echo 1/ceil(36/$leading);?>em;    
}


div.custombuttonwrap.m {
	padding-top:<?php echo ((($Current_options['font_size']*$Current_options['line_height'])-(27%($Current_options['font_size']*$Current_options['line_height'])))/$Current_options['font_size']/2);?>em;
    padding-bottom:<?php echo ((($Current_options['font_size']*$Current_options['line_height'])-(27%($Current_options['font_size']*$Current_options['line_height'])))/$Current_options['font_size']/2);?>em;
	
    margin-top:<?php echo 1/ceil(27/$leading);?>em;
    margin-bottom:<?php echo 1/ceil(27/$leading);?>em;    
    
	
    }
    

div.custombuttonwrap.s {padding-top:<?php echo ((($Current_options['font_size']*$Current_options['line_height'])-(22%($Current_options['font_size']*$Current_options['line_height'])))/$Current_options['font_size']/2);?>em;padding-bottom:<?php echo ((($Current_options['font_size']*$Current_options['line_height'])-(22%($Current_options['font_size']*$Current_options['line_height'])))/$Current_options['font_size']/2);?>em;

    margin-top:<?php echo 1/ceil(22/$leading);?>em;
    margin-bottom:<?php echo 1/ceil(22/$leading);?>em;    
}
		<?php }
 

function dynamiccss_shortcode_tabsaccordion($args = array()) { 

	global $Current_options, $h4cof;
?>
.ui-widget-header {
	line-height: <?php echo $Current_options['line_height'];?>;
}
.ui-widget-header li {
	padding-top: <?php echo $Current_options['line_height']/4;?>em !important;
	padding-bottom: <?php echo $Current_options['line_height']/4;?>em !important;    
}
h4.ui-accordion-header {
	line-height:<?php echo (($Current_options['font_size']*$Current_options['line_height'])-2)/($h4cof*$Current_options['font_size']);?>;
    padding-top:<?php echo (($Current_options['font_size']*$Current_options['line_height'])+2)/($h4cof*$Current_options['font_size']+2)/4;?>em;
    padding-bottom:<?php echo (($Current_options['font_size']*$Current_options['line_height'])+2)/($h4cof*$Current_options['font_size']+2)/4;?>em;    
    } 
		<?php }
 

function dynamiccss_shortcode_msgbox($args = array()) { 

	global $Current_options;
?>
.custom .msgboxpadding, .info .msgbox-icon, .success .msgbox-icon, .warning .msgbox-icon, .error .msgbox-icon { 
		padding-top: <?php echo $Current_options['line_height'];?>em !important;
			padding-bottom: <?php echo $Current_options['line_height'];?>em !important;
 }	
 .msgboxwrap{
	margin-top: <?php echo $Current_options['line_height'];?>em;
    margin-bottom: <?php echo $Current_options['line_height'];?>em;  
 }
		<?php }
 

function dynamiccss_shortcode_divider($args = array()) { 

	global $Current_options;
?>
div.divider_generic_wrap {
	margin:<?php echo $Current_options["line_height"];?>em 0;
}
		<?php }
 

function dynamiccss_plugin_latestposts($args = array()) { 

	global $Current_options;
?>
div.plugin_latestposts_container div.bottomnavcontainer {
	margin-top:<?php echo $Current_options['line_height'];?>em;  
    }
div.plugin_latestposts_container div.plugin_latestposts_sort_all,
div.plugin_latestposts_container div.navigation_all {
    margin-bottom:<?php echo $Current_options['line_height'];?>em;
}
		<?php }
 

function dynamiccss_shortcode_quotesandcaps($args = array()) { 

	global $Current_options;
?>

blockquote{
	line-height:<?php echo $Current_options['line_height']/1.33;?>;
    	margin-top:<?php echo $Current_options['line_height']/1.33;?>em;
        	margin-bottom:<?php echo $Current_options['line_height']/1.33;?>em;
}

		<?php }
 

function dynamiccss_shortcode_list($args = array()) { 

	global $Current_options;
?>
ul.list_style1, ul.list_style2, ul.list_style3, ul.list_style4, ul.list_style5, ul.list_style6, ul.list_style7, ul.list_style8, ul.list_style9, ul.list_style10, ul.list_style11 {
	line-height:<?php echo $Current_options['line_height'];?>;
	margin-top:<?php echo $Current_options['line_height'];?>em;      
}
ul.list_style1 li, ul.list_style2 li, ul.list_style3 li, ul.list_style4 li, ul.list_style5 li, ul.list_style6 li, ul.list_style7 li, ul.list_style8 li, ul.list_style9 li, ul.list_style10 li, ul.list_style11 li {
	line-height:<?php echo $Current_options['line_height'];?>;
          
}
ul.list_style1.big li, ul.list_style2.big li, ul.list_style3.big li, ul.list_style4.big li, ul.list_style5.big li, ul.list_style6.big li, ul.list_style7.big li, ul.list_style8.big li, ul.list_style9.big li, ul.list_style10.big li, ul.list_style11.big li {
	margin-bottom:<?php echo $Current_options['line_height'];?>em;  
    background-position:0 50%;          
}
		<?php }
 

function dynamiccss_simple_logo($args = array()) { 

	global $Current_options;
	
	$logo_img_url = (isset( $Current_options['simple_logo_url'] ) && $Current_options['simple_logo_url']!="") ? $Current_options['simple_logo_url'] : get_template_directory_uri().'/images/logo.png';	

?>
#simple_logo_abs #simple_logo {
	padding-top:<?php echo $Current_options['simple_logo_top']; ?>px; 
    margin-left:<?php echo $Current_options['simple_logo_left']; ?>px;
}
#simple_logo_abs h1 a, 
#simple_logo_abs .site-name a { 
    background:transparent url( <?php echo esc_url($logo_img_url); ?> ) no-repeat 0 100%; 
    width:<?php echo $Current_options['simple_logo_width']; ?>px; 
    height:<?php echo $Current_options['simple_logo_height']; ?>px; 
}
div#slogan.simple_logo_slogan {
	margin-left:<?php echo $Current_options['simple_logo_slogan_left']; ?>px; 
    margin-top:<?php echo $Current_options['simple_logo_slogan_top']; ?>px; 
    font-size:<?php echo $Current_options['simple_logo_slogan_font_size'];?>px;
}

		<?php }
 

function dynamiccss_simple_portfolio($args = array()) { 

	global $Current_options;
?>
 div.portfolioitem {
	margin-bottom: <?php echo $Current_options['line_height'];?>em;
 }
		<?php }
 ?>
