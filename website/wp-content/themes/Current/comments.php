<?php
/**
 * @package WordPress
 * @subpackage WP-Magic
 */

// Do not delete these lines

global $availcols;

	if (!empty($_SERVER['SCRIPT_FILENAME']) && 'comments.php' == basename($_SERVER['SCRIPT_FILENAME']))
		die ('Please do not load this page directly. Thanks!');

	if ( post_password_required() ) { ?>
		<p class="nocomments"><?php esc_html_e('This post is password protected. Enter the password to view comments.', 'Current'); ?></p>
	<?php
		return;
	}

 if ( have_comments() ) : ?>
	<h5 id="comments"><?php comments_number( __( 'No Responses', 'Current' ), __( '1 Comment', 'Current' ), __( '% Comments', 'Current' ) );?></h5>

	<br class="clear" />

	<ol class="commentlist">
	    <?php wp_list_comments('type=comment&callback=Current_comment'); ?>
	</ol>

	<br class="clear" />

<?php // pagination
if(function_exists('wp_commentnavi')) :
	    wp_commentnavi();
	else : ?>
	    <div class="navigation">
		    <div class="alignleft"><?php previous_comments_link() ?></div>
		    <div class="alignright"><?php next_comments_link() ?></div>
	    </div>
<?php	endif; 
 else : // this is displayed if there are no comments so far 
 if ( comments_open() ) : ?>
		<!-- If comments are open, but there are no comments. -->
		<h5 class="nocomments"><?php esc_html_e('No comments yet.', 'Current'); ?></h5>
	 <?php else : // comments are closed ?>
		<!-- If comments are closed. -->
		<h5 class="nocomments"><?php esc_html_e('Comments are closed.', 'Current'); ?></h5>

	<?php endif; 
 endif; 
  if ( comments_open() ) :
if($availcols >= 18)
	$onethird=$availcols/3;
else
	$onethird = false;
	
	
$fields=array(
	'author' => '<div class="form_row '.($onethird?'grid_'.$onethird.' alpha':'').'"><label for="author"><small>'.__('Name', 'Current').($req?__('(required)', 'Current'):"").'</small></label><br />
<input class="inputbox" type="text" name="author" id="author" value="'.esc_attr($comment_author).'" size="22" tabindex="1" '.($req?"aria-required='true'":'').' />
</div>',
	'email'  => '<div class="form_row '.($onethird?'grid_'.$onethird:'').'"><label for="email"><small>'.__('Mail', 'Current').($req?__('(required)', 'Current'):"").'</small></label><br />
<input class="inputbox" type="text" name="email" id="email" value="'.esc_attr($comment_author_email).'" size="22" tabindex="2" '.($req?"aria-required='true'":'').' />
</div>',
	'url'    => '<div class="form_row '.($onethird?'grid_'.$onethird.' omega':'').'"><label for="url"><small>'.__('Website', 'Current').'</small></label><br />
<input class="inputbox" type="text" name="url" id="url" value="'.esc_attr($comment_author_url).'" size="22" tabindex="3" />
</div>'
);

$defaults = array(
	'fields'               => apply_filters( 'comment_form_default_fields', $fields ),
	'comment_field'        => '<label for="comment"><small>'.__('Comment', 'Current').'</small></label>
<div class="form_row textarea"><textarea class="inputtextarea" name="comment" id="comment" rows="10" tabindex="4" style="width:100%;"></textarea></div>',
	'must_log_in'          =>  sprintf( __('<p>You must be <a href="%s">logged in</a> to post a comment.</p>', 'Current' ), wp_login_url( get_permalink()) ),
	'logged_in_as'         => '<p>'.sprintf( __('Logged in as <a href="%1$s/wp-admin/profile.php">%2$s</a>.', 'Current' ), get_option('siteurl'), $user_identity).'<a href="'.wp_logout_url(get_permalink()).'" title="'.__('Log out of this account', 'Current').'"> '.__('Log out', 'Current').'</a></p>',
	'comment_notes_before' => '',
	'comment_notes_after'  => '',
	'id_form'              => 'commentform',
	'id_submit'            => 'submit',
	'title_reply'          => __( 'Leave a Reply', 'Current' ),
	'title_reply_to'       => __( 'Leave a Reply to %s', 'Current' ),
	'cancel_reply_link'    => __( 'Cancel reply', 'Current' ),
	'label_submit'         => __( 'Post Comment', 'Current' ),
);

comment_form($defaults);

endif; ?>


