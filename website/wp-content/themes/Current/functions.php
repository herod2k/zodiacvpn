<?php 
/**
 * @package WordPress
 * @subpackage Current
 */

add_filter( 'show_admin_bar', '__return_false' );
global $global_blocks;
$global_blocks = array(
	array (
		array (
			'name' => 'Layout Settings',
			'type' => 'general_settings',
			'function' => 'settings',
			'width' => '6',
			'css,id,header_top,min-height,px' => 'Header height',
			'css,id,navigation-menu,margin-top,px' => 'Main menu top margin',
			'css,id,inset,margin-top,px' => 'Inset top margin',
			'param,image_aspect' => 'Image aspect ratio',
			'param,video_aspect' => 'Video aspect ratio',
			'id' => 'layout_settings',
			'row' => '0',
			'column' => '0'
		),
		array (
			'name' => 'Fonts Management',
			'type' => 'google_webfonts',
			'function' => 'settings',
			'width' => '6',
			'id' => 'fonts_management',
			'row' => '0',
			'column' => '1'
		),
		array (
			'name' => 'page bg',
			'type' => 'simple_background',
			'function' => 'settings',
			'width' => '6',
			'id' => 'page_bg',
			'row' => '0',
			'column' => '2'
		),
		array (
			'name' => 'shortcode button',
			'type' => 'shortcode_button',
			'function' => 'shortcode',
			'width' => '6',
			'id' => 'shortcode_button',
			'row' => '0',
			'column' => '3'
		),
	),
	array (
		array (
			'name' => 'shortcode tabs accordion',
			'type' => 'shortcode_tabsaccordion',
			'function' => 'shortcode',
			'width' => '6',
			'id' => 'shortcode_tabs_accordion',
			'row' => '1',
			'column' => '0'
		),
		array (
			'name' => 'prettyphoto lightbox',
			'type' => 'prettyphoto_lightbox',
			'function' => 'lightbox',
			'width' => '6',
			'id' => 'prettyphoto_lightbox',
			'row' => '1',
			'column' => '1'
		),
		array (
			'name' => 'shortcode msgbox',
			'type' => 'shortcode_msgbox',
			'function' => 'shortcode',
			'width' => '6',
			'id' => 'shortcode_msgbox',
			'row' => '1',
			'column' => '2'
		),
		array (
			'name' => 'shortcode divider',
			'type' => 'shortcode_divider',
			'function' => 'shortcode',
			'width' => '6',
			'id' => 'shortcode_divider',
			'row' => '1',
			'column' => '3'
		),
	),
	array (
		array (
			'name' => 'Latest Posts',
			'type' => 'plugin_latestposts',
			'function' => 'plugin',
			'width' => '6',
			'id' => 'latest_posts',
			'row' => '2',
			'column' => '0'
		),
		array (
			'name' => 'Shortcode Columns',
			'type' => 'shortcode_columns',
			'function' => 'shortcode',
			'width' => '6',
			'id' => 'shortcode_columns',
			'row' => '2',
			'column' => '1'
		),
		array (
			'name' => 'Shortcode quotes and caps',
			'type' => 'shortcode_quotesandcaps',
			'function' => 'shortcode',
			'width' => '6',
			'id' => 'shortcode_quotes_and_caps',
			'row' => '2',
			'column' => '2'
		),
		array (
			'name' => 'Shortcode List Style',
			'type' => 'shortcode_list',
			'function' => 'shortcode',
			'width' => '6',
			'id' => 'shortcode_list_style',
			'row' => '2',
			'column' => '3'
		),
	),
	array (
		array (
			'name' => 'Content Settings',
			'type' => 'general_settings',
			'function' => 'settings',
			'width' => '6',
			'param,excerpt_length' => 'Excerpt Length (words)',
			'param,comments_posts,checkbox' => 'Display comments on posts',
			'param,comments_pages,checkbox' => 'Display comments on pages',
			'id' => 'content_settings',
			'row' => '3',
			'column' => '0'
		),
		array (
			'name' => 'General Colors Configuration',
			'type' => 'general_settings',
			'function' => 'settings',
			'width' => '6',
			'param,theme_color,color' => 'Theme color',
			'css,tag,body,color,color' => 'General text color',
			'param,headings_color,color' => 'Headings color',
			'css,tag,a#a:visited,color,color' => 'Links color',
			'css,tag,a:hover,color,color' => 'Links hover color',
			'css,tag,h1.pagetitle,color,color' => 'Page title color',
			'id' => 'general_colors_configuration',
			'row' => '3',
			'column' => '1'
		),
		array (
			'name' => 'Main Menu Colors',
			'type' => 'general_settings',
			'function' => 'settings',
			'width' => '6',
			'css,tag,ul.sf-menu>li>a#ul.sf-menu>li>a:visited,color,color' => 'Menu links color',
			'css,tag,ul.sf-menu>li>a:hover,color,color' => 'Menu hover color',
			'css,tag,ul.sf-menu ul.sub-menu a#ul.sf-menu ul.sub-menu a:visited,color,color' => 'Sub-menu links color',
			'css,tag,ul.sf-menu ul.sub-menu a:hover,color,color' => 'Sub-menu hover color',
			'id' => 'main_menu_colors',
			'row' => '3',
			'column' => '2'
		),
		array (
			'name' => 'Custom CSS for widgets',
			'type' => 'feature_customcss',
			'function' => 'widgetfield',
			'width' => '6',
			'id' => 'custom_css_for_widgets',
			'row' => '3',
			'column' => '3'
		),
	),
	array (
		array (
			'name' => 'Google Analytics',
			'type' => 'google_analytics',
			'function' => 'analytics',
			'width' => '6',
			'id' => 'google_analytics',
			'row' => '4',
			'column' => '0'
		),
		array (
			'name' => 'Shortcode Rhythmize',
			'type' => 'shortcode_rhythmize',
			'function' => 'shortcode',
			'width' => '6',
			'id' => 'shortcode_rhythmize',
			'row' => '4',
			'column' => '1'
		),
	),
	array (
		array (
			'name' => 'Footer block 1',
			'type' => 'simple_widget_location',
			'function' => 'widget_location',
			'titlestyle' => 'h5',
			'width' => '6',
			'id' => 'footer_block_1',
			'row' => '5',
			'column' => '0'
		),
		array (
			'name' => 'Footer block 2',
			'type' => 'simple_widget_location',
			'function' => 'widget_location',
			'titlestyle' => 'h5',
			'width' => '6',
			'id' => 'footer_block_2',
			'row' => '5',
			'column' => '1'
		),
		array (
			'name' => 'Footer block 3',
			'type' => 'simple_widget_location',
			'function' => 'widget_location',
			'titlestyle' => 'h5',
			'width' => '6',
			'id' => 'footer_block_3',
			'row' => '5',
			'column' => '2'
		),
		array (
			'name' => 'Footer block 4',
			'type' => 'simple_widget_location',
			'function' => 'widget_location',
			'titlestyle' => 'h5',
			'width' => '6',
			'id' => 'footer_block_4',
			'row' => '5',
			'column' => '3'
		),
	),
	array (
		array (
			'name' => 'Simple Footer',
			'type' => 'simple_footer',
			'function' => 'footer',
			'width' => '24',
			'id' => 'simple_footer',
			'row' => '6',
			'column' => '0'
		),
	),
	array (
		array (
			'name' => 'Simple logo',
			'type' => 'simple_logo',
			'function' => 'logo',
			'width' => '4',
			'id' => 'simple_logo',
			'row' => '7',
			'column' => '0'
		),
		array (
			'name' => 'Main Menu',
			'type' => 'simple_menu',
			'function' => 'menu',
			'width' => '15',
			'id' => 'main_menu',
			'row' => '7',
			'column' => '1'
		),
		array (
			'name' => 'Inset',
			'type' => 'simple_widget_location',
			'function' => 'widget_location',
			'width' => '5',
			'id' => 'inset',
			'row' => '7',
			'column' => '2'
		),
	),
	array (
		array (
			'name' => 'Mocktail Slider',
			'type' => 'mocktail_slider',
			'function' => 'slider',
			'width' => '24',
			'id' => 'mocktail_slider',
			'row' => '8',
			'column' => '0'
		),
	),
	array (
		array (
			'name' => 'Simple Pagetitle',
			'type' => 'simple_pagetitle',
			'function' => 'title',
			'width' => '24',
			'id' => 'simple_pagetitle',
			'row' => '9',
			'column' => '0'
		),
	),
	array (
		array (
			'name' => 'Left Sidebar',
			'type' => 'simple_sidebar',
			'function' => 'sidebar',
			'width' => '6',
			'id' => 'left_sidebar',
			'row' => '10',
			'column' => '0'
		),
		array (
			'name' => 'Simple 404',
			'type' => 'simple_404',
			'function' => 'page',
			'width' => '12',
			'id' => 'simple_404',
			'row' => '10',
			'column' => '1'
		),
		array (
			'name' => 'Right Sidebar',
			'type' => 'simple_sidebar',
			'function' => 'sidebar',
			'width' => '6',
			'id' => 'right_sidebar',
			'row' => '10',
			'column' => '2'
		),
	),
	array (
		array (
			'name' => 'Left Sidebar',
			'type' => 'simple_sidebar',
			'function' => 'sidebar',
			'width' => '6',
			'id' => 'left_sidebar',
			'row' => '11',
			'column' => '0'
		),
		array (
			'name' => 'Simple Archive',
			'type' => 'simple_archive',
			'function' => 'page',
			'width' => '12',
			'id' => 'simple_archive',
			'row' => '11',
			'column' => '1'
		),
		array (
			'name' => 'Right Sidebar',
			'type' => 'simple_sidebar',
			'function' => 'sidebar',
			'width' => '6',
			'id' => 'right_sidebar',
			'row' => '11',
			'column' => '2'
		),
	),
	array (
		array (
			'name' => 'Left Sidebar',
			'type' => 'simple_sidebar',
			'function' => 'sidebar',
			'width' => '6',
			'id' => 'left_sidebar',
			'row' => '12',
			'column' => '0'
		),
		array (
			'name' => 'Simple Blog',
			'type' => 'simple_blog',
			'function' => 'page',
			'width' => '12',
			'id' => 'simple_blog',
			'row' => '12',
			'column' => '1'
		),
		array (
			'name' => 'Right Sidebar',
			'type' => 'simple_sidebar',
			'function' => 'sidebar',
			'width' => '6',
			'id' => 'right_sidebar',
			'row' => '12',
			'column' => '2'
		),
	),
	array (
		array (
			'name' => 'Left Sidebar',
			'type' => 'simple_sidebar',
			'function' => 'sidebar',
			'width' => '6',
			'id' => 'left_sidebar',
			'row' => '13',
			'column' => '0'
		),
		array (
			'name' => 'Simple Contact',
			'type' => 'simple_contact',
			'function' => 'page',
			'width' => '12',
			'id' => 'simple_contact',
			'row' => '13',
			'column' => '1'
		),
		array (
			'name' => 'Right Sidebar',
			'type' => 'simple_sidebar',
			'function' => 'sidebar',
			'width' => '6',
			'id' => 'right_sidebar',
			'row' => '13',
			'column' => '2'
		),
	),
	array (
		array (
			'name' => 'Front page block A1',
			'type' => 'simple_widget_location',
			'function' => 'widget_location',
			'width' => '6',
			'id' => 'front_page_block_a1',
			'row' => '14',
			'column' => '0'
		),
		array (
			'name' => 'Front page block A2',
			'type' => 'simple_widget_location',
			'function' => 'widget_location',
			'width' => '6',
			'id' => 'front_page_block_a2',
			'row' => '14',
			'column' => '1'
		),
		array (
			'name' => 'Front page block A3',
			'type' => 'simple_widget_location',
			'function' => 'widget_location',
			'width' => '6',
			'id' => 'front_page_block_a3',
			'row' => '14',
			'column' => '2'
		),
		array (
			'name' => 'Front page block A4',
			'type' => 'simple_widget_location',
			'function' => 'widget_location',
			'width' => '6',
			'id' => 'front_page_block_a4',
			'row' => '14',
			'column' => '3'
		),
	),
	array (
		array (
			'name' => 'Front page block B1',
			'type' => 'simple_widget_location',
			'function' => 'widget_location',
			'width' => '6',
			'id' => 'front_page_block_b1',
			'row' => '15',
			'column' => '0'
		),
		array (
			'name' => 'Front page block B2',
			'type' => 'simple_widget_location',
			'function' => 'widget_location',
			'width' => '6',
			'id' => 'front_page_block_b2',
			'row' => '15',
			'column' => '1'
		),
		array (
			'name' => 'Front page block B3',
			'type' => 'simple_widget_location',
			'function' => 'widget_location',
			'width' => '6',
			'id' => 'front_page_block_b3',
			'row' => '15',
			'column' => '2'
		),
		array (
			'name' => 'Front page block B4',
			'type' => 'simple_widget_location',
			'function' => 'widget_location',
			'width' => '6',
			'id' => 'front_page_block_b4',
			'row' => '15',
			'column' => '3'
		),
	),
	array (
		array (
			'name' => 'Front page block C1',
			'type' => 'simple_widget_location',
			'function' => 'widget_location',
			'width' => '6',
			'id' => 'front_page_block_c1',
			'row' => '16',
			'column' => '0'
		),
		array (
			'name' => 'Front page block C2',
			'type' => 'simple_widget_location',
			'function' => 'widget_location',
			'width' => '6',
			'id' => 'front_page_block_c2',
			'row' => '16',
			'column' => '1'
		),
		array (
			'name' => 'Front page block C3',
			'type' => 'simple_widget_location',
			'function' => 'widget_location',
			'width' => '6',
			'id' => 'front_page_block_c3',
			'row' => '16',
			'column' => '2'
		),
		array (
			'name' => 'Front page block C4',
			'type' => 'simple_widget_location',
			'function' => 'widget_location',
			'width' => '6',
			'id' => 'front_page_block_c4',
			'row' => '16',
			'column' => '3'
		),
	),
	array (
		array (
			'name' => 'Left Sidebar',
			'type' => 'simple_sidebar',
			'function' => 'sidebar',
			'width' => '6',
			'id' => 'left_sidebar',
			'row' => '17',
			'column' => '0'
		),
		array (
			'name' => 'Simple Page',
			'type' => 'simple_page',
			'function' => 'page',
			'width' => '12',
			'id' => 'simple_page',
			'row' => '17',
			'column' => '1'
		),
		array (
			'name' => 'Right Sidebar',
			'type' => 'simple_sidebar',
			'function' => 'sidebar',
			'width' => '6',
			'id' => 'right_sidebar',
			'row' => '17',
			'column' => '2'
		),
	),
	array (
		array (
			'name' => 'Left Sidebar',
			'type' => 'simple_sidebar',
			'function' => 'sidebar',
			'width' => '6',
			'id' => 'left_sidebar',
			'row' => '18',
			'column' => '0'
		),
		array (
			'name' => 'Simple Page',
			'type' => 'simple_page',
			'function' => 'page',
			'width' => '12',
			'id' => 'simple_page',
			'row' => '18',
			'column' => '1'
		),
		array (
			'name' => 'Right Sidebar',
			'type' => 'simple_sidebar',
			'function' => 'sidebar',
			'width' => '6',
			'id' => 'right_sidebar',
			'row' => '18',
			'column' => '2'
		),
	),
	array (
		array (
			'name' => 'Left Sidebar',
			'type' => 'simple_sidebar',
			'function' => 'sidebar',
			'width' => '6',
			'id' => 'left_sidebar',
			'row' => '19',
			'column' => '0'
		),
		array (
			'name' => 'Simple Page',
			'type' => 'simple_page',
			'function' => 'page',
			'width' => '12',
			'id' => 'simple_page',
			'row' => '19',
			'column' => '1'
		),
		array (
			'name' => 'Right Sidebar',
			'type' => 'simple_sidebar',
			'function' => 'sidebar',
			'width' => '6',
			'id' => 'right_sidebar',
			'row' => '19',
			'column' => '2'
		),
	),
	array (
		array (
			'name' => 'Left Sidebar',
			'type' => 'simple_sidebar',
			'function' => 'sidebar',
			'width' => '6',
			'id' => 'left_sidebar',
			'row' => '20',
			'column' => '0'
		),
		array (
			'name' => 'Simple Portfolio',
			'type' => 'simple_portfolio',
			'function' => 'page',
			'image_frame' => 'image_frame',
			'width' => '12',
			'id' => 'simple_portfolio',
			'row' => '20',
			'column' => '1'
		),
		array (
			'name' => 'Right Sidebar',
			'type' => 'simple_sidebar',
			'function' => 'sidebar',
			'width' => '6',
			'id' => 'right_sidebar',
			'row' => '20',
			'column' => '2'
		),
	),
	array (
		array (
			'name' => 'Left Sidebar',
			'type' => 'simple_sidebar',
			'function' => 'sidebar',
			'width' => '6',
			'id' => 'left_sidebar',
			'row' => '21',
			'column' => '0'
		),
		array (
			'name' => 'Simple Search',
			'type' => 'simple_search',
			'function' => 'page',
			'width' => '12',
			'id' => 'simple_search',
			'row' => '21',
			'column' => '1'
		),
		array (
			'name' => 'Right Sidebar',
			'type' => 'simple_sidebar',
			'function' => 'sidebar',
			'width' => '6',
			'id' => 'right_sidebar',
			'row' => '21',
			'column' => '2'
		),
	),
	array (
		array (
			'name' => 'Left Sidebar',
			'type' => 'simple_sidebar',
			'function' => 'sidebar',
			'width' => '6',
			'id' => 'left_sidebar',
			'row' => '22',
			'column' => '0'
		),
		array (
			'name' => 'Simple Single',
			'type' => 'simple_single',
			'function' => 'page',
			'width' => '12',
			'id' => 'simple_single',
			'row' => '22',
			'column' => '1'
		),
		array (
			'name' => 'Right Sidebar',
			'type' => 'simple_sidebar',
			'function' => 'sidebar',
			'width' => '6',
			'id' => 'right_sidebar',
			'row' => '22',
			'column' => '2'
		),
	),
	array (
		array (
			'name' => 'Left Sidebar',
			'type' => 'simple_sidebar',
			'function' => 'sidebar',
			'width' => '6',
			'id' => 'left_sidebar',
			'row' => '23',
			'column' => '0'
		),
		array (
			'name' => 'Simple Sitemap',
			'type' => 'simple_sitemap',
			'function' => 'page',
			'width' => '12',
			'id' => 'simple_sitemap',
			'row' => '23',
			'column' => '1'
		),
		array (
			'name' => 'Right Sidebar',
			'type' => 'simple_sidebar',
			'function' => 'sidebar',
			'width' => '6',
			'id' => 'right_sidebar',
			'row' => '23',
			'column' => '2'
		),
	)
);


global $global_ids;
$global_ids = array('undefined','undefined','undefined','undefined','undefined','footer_block,generic','footer_bar,generic','header_top,generic','slide_show,generic','page_title,generic','undefined','undefined','undefined','undefined','front_blockA,generic','front_blockB,generic','front_blockC,generic','undefined','undefined','undefined','undefined','undefined','undefined','undefined'
);


global $global_js;
$global_js = array(
	'general_settings' => array(''),
	'google_webfonts' => array(''),
	'simple_background' => array(''),
	'shortcode_button' => array(''),
	'shortcode_tabsaccordion' => array('_jquery-ui-core.wps','_jquery-ui-tabs.wps','_jquery-ui-accordion-min.js'),
	'prettyphoto_lightbox' => array('jquery.prettyPhoto.js','prettyphotoload.js.php'),
	'shortcode_msgbox' => array(''),
	'shortcode_divider' => array(''),
	'plugin_latestposts' => array(''),
	'shortcode_columns' => array(''),
	'shortcode_quotesandcaps' => array(''),
	'shortcode_list' => array(''),
	'feature_customcss' => array(''),
	'google_analytics' => array(''),
	'shortcode_rhythmize' => array(''),
	'simple_widget_location' => array(''),
	'simple_footer' => array(''),
	'simple_logo' => array(''),
	'simple_menu' => array('superfish.js','jquery.mobilemenu.js','simple_menu_init.js'),
	'mocktail_slider' => array('jquery.cycle.all.js','jquery.easing.1.3.js','mocktail_init.js.php'),
	'simple_pagetitle' => array(''),
	'simple_sidebar' => array(''),
	'simple_404' => array(''),
	'simple_archive' => array(''),
	'simple_blog' => array(''),
	'simple_contact' => array('_http%3a%2f%2fmaps.google.com%2fmaps%2fapi%2fjs%3fsensor%3dfalse.lnk','_jquery.gmap.min.js'),
	'simple_page' => array(''),
	'simple_portfolio' => array(''),
	'simple_search' => array(''),
	'simple_single' => array(''),
	'simple_sitemap' => array(''),
);


global $global_css;
$global_css = array(
	'general_settings' => array(''),
	'google_webfonts' => array(''),
	'simple_background' => array(''),
	'shortcode_button' => array(''),
	'shortcode_tabsaccordion' => array('jquery-ui-1.8.17.custom.css'),
	'prettyphoto_lightbox' => array('prettyPhoto.css'),
	'shortcode_msgbox' => array(''),
	'shortcode_divider' => array(''),
	'plugin_latestposts' => array(''),
	'shortcode_columns' => array(''),
	'shortcode_quotesandcaps' => array(''),
	'shortcode_list' => array(''),
	'feature_customcss' => array(''),
	'google_analytics' => array(''),
	'shortcode_rhythmize' => array(''),
	'simple_widget_location' => array(''),
	'simple_footer' => array(''),
	'simple_logo' => array(''),
	'simple_menu' => array('simple_menu.css'),
	'mocktail_slider' => array('mocktail_slider.css.php'),
	'simple_pagetitle' => array(''),
	'simple_sidebar' => array(''),
	'simple_404' => array(''),
	'simple_archive' => array(''),
	'simple_blog' => array(''),
	'simple_contact' => array(''),
	'simple_page' => array(''),
	'simple_portfolio' => array(''),
	'simple_search' => array(''),
	'simple_single' => array(''),
	'simple_sitemap' => array(''),
);


global $global_scripts;
$global_scripts = array(
	'general_settings' => array(''),
	'google_webfonts' => array(''),
	'simple_background' => array(''),
	'shortcode_button' => array(''),
	'shortcode_tabsaccordion' => array(''),
	'prettyphoto_lightbox' => array(''),
	'shortcode_msgbox' => array(''),
	'shortcode_divider' => array(''),
	'plugin_latestposts' => array('plugin_latestposts.php'),
	'shortcode_columns' => array(''),
	'shortcode_quotesandcaps' => array(''),
	'shortcode_list' => array(''),
	'feature_customcss' => array(''),
	'google_analytics' => array(''),
	'shortcode_rhythmize' => array(''),
	'simple_widget_location' => array(''),
	'simple_footer' => array(''),
	'simple_logo' => array(''),
	'simple_menu' => array(''),
	'mocktail_slider' => array(''),
	'simple_pagetitle' => array(''),
	'simple_sidebar' => array(''),
	'simple_404' => array(''),
	'simple_archive' => array(''),
	'simple_blog' => array(''),
	'simple_contact' => array('recaptchalib.php'),
	'simple_page' => array(''),
	'simple_portfolio' => array(''),
	'simple_search' => array('ylsy_search_excerpt.php'),
	'simple_single' => array(''),
	'simple_sitemap' => array(''),
);



	if (!function_exists ('add_action')) {
			header('Status: 403 Forbidden');
			header('HTTP/1.1 403 Forbidden');
			exit();
	}
	
	global $Current_options, $post, $isfrontpage;

	if (function_exists('load_theme_textdomain')) {
		load_theme_textdomain('Current', get_template_directory().'/language');
	}
	


	$Current_options  = get_option('Current_options');	

	if(isset($Current_options['reset_to_defaults']) && $Current_options['reset_to_defaults'] == 'yes' ) {
		delete_option( "Current_options");
		$Current_options  = false;
	}

	if (!$Current_options) {
		include_once("theme_default_config.php");
	   add_option( "Current_options", $theme_default_config);
	   $Current_options  = get_option('Current_options');
	   
	}
	global $content_width;	
	if ( ! isset( $content_width ) ) $content_width = 960;
	
	 $Current_options["site_width"] = 960;
	 $Current_options["gutter_width"] = 40;

	if ( function_exists( 'add_theme_support' ) ) { 
		add_theme_support( 'post-thumbnails' ); 
	}


	add_filter('widget_text', 'do_shortcode');
	if ( function_exists('automatic-feed-links') ) {
		add_theme_support( 'automatic-feed-links' );
	}
	if ( function_exists('wp_nav_menu') ) {
		add_theme_support( 'nav-menus' );
		register_nav_menus( array(
		'primary' => esc_html__( 'Primary Navigation', 'Current' ),
		) );
	}
	
	function th_excerpt_length($length) {
		global $Current_options;	
		return $Current_options['excerpt_length'];
	}

	if(isset($Current_options['excerpt_length']))
		add_filter('excerpt_length', 'th_excerpt_length');


	function load_th_styles() {
		global $global_css, $global_blocks, $post;
		if( !is_admin() ){	
			wp_enqueue_style('style_reset', get_template_directory_uri() . '/css/reset.css', false, '1.0', 'screen');

			wp_enqueue_style('grid960', get_template_directory_uri() . '/css/960gs.css', false, '1.0', 'only screen and (min-width: 960px)');
			wp_enqueue_style('grid768', get_template_directory_uri() . '/css/768gs.css', false, '1.0', 'only screen and (min-width: 768px) and (max-width: 959px)');
			wp_enqueue_style('grid480', get_template_directory_uri() . '/css/480gs.css', false, '1.0', 'only screen and (min-width: 480px) and (max-width: 767px)');
			wp_enqueue_style('smallestgs', get_template_directory_uri() . '/css/smallestgs.css', false, '1.0', 'only screen and (max-width: 479px)');

			
			wp_enqueue_style('frontcss', get_template_directory_uri() . '/css/frontcss.css', false, '1.0', 'screen');
			wp_enqueue_style('style_base', get_template_directory_uri() . '/css/style.css', false, '1.0', 'screen');
			wp_enqueue_style('dynamiccss', get_template_directory_uri() . '/css/dynamiccss.css.php?post='.$post->ID, false, '1.0', 'screen');
			wp_enqueue_style('usercss', get_template_directory_uri() . '/css/usercss.css.php?post='.$post->ID, false, '1.0', 'screen');
			wp_enqueue_style('theme_css', get_template_directory_uri() . '/style.css', '1.0', 'screen');			
			wp_enqueue_style('uniform_css', get_template_directory_uri() . '/css/uniform.default.css', '1.5', 'screen');			
			
			$temparray=array();
			
			foreach($global_blocks as $row)
				foreach($row as $column)
					if(call_user_func("willrender_".$column['type'], $column))
						$temparray[$column['type']] = true;
			
			foreach($temparray as $key => $val)
				foreach($global_css[$key] as $css)
					wp_enqueue_style(str_replace(".", "_", $css), get_template_directory_uri() . '/css/'.$css.((strtolower(substr($css, strlen($css)-3, 3))=="php")?"?post=".(is_front_page()?'fp':$post->ID):""), false, null, 'screen');	
					
					
			wp_enqueue_style('ie7-style',   get_template_directory_uri() . '/css/ie7.css');
			global $wp_styles;
			$wp_styles->add_data( 'ie7-style', 'conditional', 'lte IE 7' );		
	
		}

	}
	
	function load_th_scripts() {
		global $global_js, $global_blocks, $post, $wp_scripts;		
		if( !is_admin() ){
			wp_enqueue_script('validate_lib', get_template_directory_uri()."/js/jquery.validate.min.js", array('jquery'), '1.6', false);
			wp_enqueue_script('frontjs', get_template_directory_uri()."/js/frontjs.js", array('jquery'), '1.0', false);
			wp_enqueue_script('respondjs', get_template_directory_uri()."/js/respond.min.js", array('jquery'), '1.0', false);
			wp_enqueue_script('uniformjs', get_template_directory_uri()."/js/jquery.uniform.min.js", array('jquery'), '1.5', false);


			
			$temparray=array();
			foreach($global_blocks as $row)
				foreach($row as $column)
					if(call_user_func("willrender_".$column['type'], $column))
						$temparray[$column['type']] = true;
			
			foreach($temparray as $key => $val)
				foreach($global_js[$key] as $css) {
					$infooter=false;
					if(substr($css, 0, 1)=="_") {
						$infooter=true;
						$css = substr($css, 1, strlen($css)-1);
					}

					if(strtolower(substr($css, strlen($css)-3, 3))=="wps") {
						wp_enqueue_script(substr($css, 0, strlen($css)-4), false, array('jquery'), false, $infooter);
					
					}
					elseif(strtolower(substr($css, strlen($css)-3, 3))=="lnk") {
						wp_enqueue_script(substr($css, 0, strlen($css)-4), urldecode(substr($css, 0, strlen($css)-4)), array('jquery'), false, $infooter);
					}
					else
						wp_enqueue_script(str_replace(".", "_", $css), get_template_directory_uri() . '/js/'.$css.((strtolower(substr($css, strlen($css)-3, 3))=="php")?"?post=".(is_front_page()?'fp':($post?$post->ID:"")):""), array('jquery'), '1.0', $infooter);
				}
							
			wp_enqueue_script('userscript', get_template_directory_uri()."/js/script.js", array('jquery'), null, false);
			
			


			global $global_blocks;
		
			
				$temparray = array();
				
				foreach($global_blocks as $row)
					foreach($row as $column)
						if(call_user_func("willrender_".$column['type'], $column))
							$temparray[$column['type']] = true;
						 
				foreach($temparray as $key => $val) {
					@include_once("includes/".$key.".php");
				}
			
			
		}
	}

add_action("init", "admin_side_style_scripts");

function admin_side_style_scripts() {
	if(is_admin()) {
		wp_enqueue_style('adminstyle', get_template_directory_uri() . '/css/adminstyle.css', false, '1.0', 'screen');
		//load color picker scripts
		wp_enqueue_style('th-colorpicker-style', get_template_directory_uri().'/css/colorpicker.css', false, '1.0', 'screen');
		wp_register_script('th-colorpicker', get_template_directory_uri().'/js/colorpicker.js', array('jquery'), '1.0.0', false );
	
		wp_enqueue_script('th-colorpicker');
		
		wp_register_script('admin-scripts', get_template_directory_uri().'/js/adminjs.js', array('jquery'), '1.0', true);
		
		wp_enqueue_script('admin-scripts');	
		
	}
}

	add_action('wp_print_styles', 'load_th_styles');
	add_action('wp_print_scripts', 'load_th_scripts');		
	

	if(is_admin()) {
			
			global $global_blocks;
		
			$temparray = array();
			
			foreach($global_blocks as $row)
				foreach($row as $column)
					$temparray[$column['type']] = true;
					 
			foreach($temparray as $key => $val) {
				@include_once("admin/".$key.".php");
			}			
			
		}

	function th_sidebar($index = '') {
		$sidebar = "";
		ob_start();
			if ( function_exists('dynamic_sidebar') && dynamic_sidebar( $index ) )
		$sidebar = ob_get_clean();
		return $sidebar;
	}
	
	function th_render_row($row = array()) {
		global $global_ids;	
		$i=0;
		$rownum=false;
		ob_start();
		foreach($row as $column) {
			$column['id'] = isset($column['id'])?$column['id']:str_replace(' ','_',$column['name']);
			$rownum = $column['row'];
			if(call_user_func("willrender_".$column['type'], $column)) {
				echo "<div id='".$column['id']."' class='grid_".calc_width($i, $row)."'>";
				call_user_func("front_".$column['type'], $column);
				echo "</div>";
			}
			$i+=1;
		}
		$content=ob_get_contents();
		ob_end_clean();
		
		if($content!="") {
			$theid=false;
			$theclass=false;
			if(isset($global_ids[$rownum]) && $global_ids[$rownum]!='' && $global_ids[$rownum]!='undefined') {
				$idclass=explode(",", $global_ids[$rownum]);
				if(isset($idclass[0]) && $idclass[0]!="")
					$theid=$idclass[0];
				if(isset($idclass[1]) && $idclass[1]!="")
					$theclass=$idclass[1];					
			}
			echo "<div id='wrap_".($theid?$theid:"")."'><div class='container_24".($theclass?" ".$theclass:"")."' ".($theid?" id='".$theid."'":"")." >".$content."<br class='clear' /></div></div>";
		}
	}
	
	
	function th_render_footer($row = array()) {
		foreach($row as $column) {
			$column['id'] = isset($column['id'])?$column['id']:str_replace(' ','_',$column['name']);
			if(call_user_func("willrender_".$column['type'], $column)) {
				call_user_func("footer_".$column['type'], $column);
			}
		}
	}
	
	function th_render_header($row = array()) {
		foreach($row as $column) {
			$column['id'] = isset($column['id'])?$column['id']:str_replace(' ','_',$column['name']);
			if(call_user_func("willrender_".$column['type'], $column)) {
				call_user_func("header_".$column['type'], $column);
			}
		}
	}
        
	function calc_width($i, $row) {
		$width = $row[$i]['width'];
		
		$prevwidth = 0;
		for($j=0; !($j>=$i); $j++) {
			$prevwidth+=$row[$j]['width'];
			if(call_user_func("willrender_".$row[$j]['type'], $row[$j])) {
				$prevwidth = 0;
				break;
			}
		}
		
		$width+=$prevwidth;
		
	
		for($i=$i+1; !($i>=sizeof($row)); $i++) {
			if(call_user_func("willrender_".$row[$i]['type'], $row[$i]))
				break;
			else
				$width+=$row[$i]['width'];
		}
		return $width;				
	}
	
	
	add_action( 'admin_init', 'Current_options_init' );
	add_action( 'admin_menu', 'Current_options_add_page' ); 
			
	function Current_options_init(){
		register_setting( 'Current_settings', 'Current_options', 'sanitize_options');
	} 
	
	function Current_options_add_page() {
		global $th_options_hook;
		$th_options_hook = add_theme_page( __( 'Theme Options', 'Current' ), __( 'Theme Options', 'Current' ), 'edit_theme_options', 'Current_settings_page', 'Current_options_do_page' );
		add_action('load-'.$th_options_hook, 'th_load_options');
	}
	
	function Current_options_do_page() {
		global $th_options_hook;
		?>
        <div class="wrap" style="padding-top:50px;">
			<div id="poststuff" class="metabox-holder">
                <div id="post-body" class="has-sidebar">
                    <div id="post-body-content" class="has-sidebar-content">
                    
		    <h2><?php echo __('Current theme Options page', 'Current'); ?></h2>
<?php 
		    $theme_home_directory = substr(strrchr( TEMPLATEPATH, "/" ), 1 );
		    if ( $theme_home_directory != 'Current' ) {
			echo '<div id="message" class="error"><p>The current theme home directory ( "'.$theme_home_directory.'" ) <strong>is not</strong> the correct one!</p>
				<p>Pease refer to <a href="http://wiki.envato.com/buying/support-buying/solving-broken-theme-issues/" target="_blank">this guide</a> or refer to the theme documentation for information on installation.</p></div>';
		    }                    
                 ?>   
                    
        	<form method="post" action="options.php">
			<?php
            	settings_fields( 'Current_settings' );
				$options = get_option('Current_options');
				
				wp_nonce_field('closedpostboxes', 'closedpostboxesnonce', false );

				wp_nonce_field('meta-box-order', 'meta-box-order-nonce', false ); 				
				
				do_meta_boxes($th_options_hook, 'normal', $options);
			
			
			?>
            <fieldset style="margin:2px 0 0;"><legend class="screen-reader-text"><span><?php esc_attr_e('Reset to defaults', 'Current') ?></span></legend>
						<label for="reset_to_defaults">
						    <input name="Current_options[reset_to_defaults]" type="checkbox" id="reset_to_defaults" value="yes" />
						    <?php esc_attr_e('Reset to defaults', 'Current') ?>
						</label>
			 </fieldset>
             <p><input class="button-primary" type="submit" name="submit" value="<?php esc_attr_e('Save Changes', 'Current') ?>" /></p>
            </form>
            		</div>
            	</div>
            </div>
                    
        </div>
        <script type="text/javascript">
		    //<![CDATA[
		    jQuery(document).ready( function($) {
			    // close postboxes that should be closed
			    $('.if-js-closed').removeClass('if-js-closed').addClass('closed');
			    // postboxes setup
			    postboxes.add_postbox_toggles('<?php echo $th_options_hook; ?>');
		    });
		    //]]>
		</script>
        <?php
	}
	
	function sanitize_options( $options ) {
		foreach($options as $key => $option)
			$options[$key] = str_replace('"', "'", $option);
		
$options =  sanitize_google_webfonts($options);

$options =  sanitize_mocktail_slider($options);

		return $options;
	}
	
		
	function display_save_changes_button($order = 1) {
		echo ('<div class="params_div'.$order.'"><div class="submit" style="float:right; clear:both;">
						<input type="hidden" id="Current_submit" value="1" name="Current_submit"/>
						<input class="button-primary" type="submit" name="submit" value="'.esc_attr__('Save Changes', 'Current').'" />
						</div><br class="clear" /></div>');
	}
	
	add_action( 'load-post.php', 'th_post_video_mbox_setup' );
	add_action( 'load-post-new.php', 'th_post_video_mbox_setup' );
	add_action('save_post', 'save_th_post_video_mbox');
	
	
	function th_post_video_mbox_setup() {
		add_action( 'add_meta_boxes', 'add_th_post_video_mbox_setup' );
	}
			
	function add_th_post_video_mbox_setup() {

		add_meta_box(
			'th-post-video-mbox',			// Unique ID
			esc_html__( 'Featured video', 'Current' ),		// Title
			'th_post_video_mbox',		// Callback function
			'',					// Admin page (or post type)
			'side',					// Context
			'core'					// Priority
		);
	}
	
	function th_post_video_mbox( $object, $box ) {
		global $post, $Current_options;
		?>
        <div class="params_div1">
        	<label for="th_post_video_src" style="float:left; width:100px;"><?php esc_html_e('Video Source', 'Current'); ?>:<br />
            	<select id="th_post_video_src" name="th_post_video_src">
            		<option value="vimeo" <?php if(get_post_meta($post->ID, 'th_post_video_src', true)=="vimeo") echo "selected='selected'"; ?>>vimeo</option>
            		<option value="youtube" <?php if(get_post_meta($post->ID, 'th_post_video_src', true)=="youtube") echo "selected='selected'"; ?>>youtube</option>
            		<option value="quicktime" <?php if(get_post_meta($post->ID, 'th_post_video_src', true)=="quicktime") echo "selected='selected'"; ?>>quicktime</option>
            		<option value="flash" <?php if(get_post_meta($post->ID, 'th_post_video_src', true)=="flash") echo "selected='selected'"; ?>>flash</option>                                                            
            	</select>
            </label>
            
            <label for="th_post_video_resource" id="th_post_video_resource_label" style="float:left;"><span></span>:<br />
            	<input id="th_post_video_resource" type="text" name="th_post_video_resource" value="<?php echo get_post_meta($post->ID, 'th_post_video_resource', true);?>" />
            </label>
            <br style="clear:both;" />
            
            <label for="th_post_video_width" style="float:left; width:100px;"><?php esc_html_e('Width', 'Current'); ?>:<br />
            	<input id="th_post_video_width" type="text" name="th_post_video_width" value="<?php echo get_post_meta($post->ID, 'th_post_video_width', true)?get_post_meta($post->ID, 'th_post_video_width', true):"0";?>" style="width:50px;" />
            </label>
            
            <label for="th_post_video_height" style="float:left;"><?php esc_html_e('Height', 'Current'); ?>:<br />
            	<input id="th_post_video_height" type="text" name="th_post_video_height" value="<?php echo get_post_meta($post->ID, 'th_post_video_height', true)?get_post_meta($post->ID, 'th_post_video_height', true):"0";?>" style="width:50px;" />
            </label>
            
            <br style="clear:both;" />
            <span class="legend"><?php _e('if the width or the height is set to zero, the dimensions of the video player are auto determined.', 'Current'); ?></span>
            
            <br />
<label for="th_post_video_aspect" ><?php esc_html_e('Aspect ratio', 'Current'); ?>: <input id="th_post_video_aspect" type="text" name="th_post_video_aspect" value="<?php echo get_post_meta($post->ID, 'th_post_video_aspect', true)?get_post_meta($post->ID, 'th_post_video_aspect', true):$Current_options['video_aspect'];?>" style="width:50px;" /> : 1
            </label>
            <br style="clear:both;" />
            <span class="legend"><?php _e('You can optionally specify your own video aspect ratio to fine tune the display', 'Current'); ?></span>
            <script type="text/javascript">
		    //<![CDATA[
		    jQuery(document).ready( function($) {
			    // close postboxes that should be closed
			    $('#th_post_video_src').change(function() {
					var theselected = $(this).find("option:selected").val();
					if(theselected == "vimeo" || theselected == "youtube")
						$('#th_post_video_resource_label > span').html('<?php esc_html_e('Video ID', 'Current'); ?>');
					else
						$('#th_post_video_resource_label > span').html('<?php esc_html_e('Video URL', 'Current'); ?>');					
				});
				
				var theselected = $('#th_post_video_src').find("option:selected").val();
				if(theselected == "vimeo" || theselected == "youtube")
					$('#th_post_video_resource_label > span').html('<?php esc_html_e('Video ID', 'Current'); ?>');
				else
					$('#th_post_video_resource_label > span').html('<?php esc_html_e('Video URL', 'Current'); ?>');
		
		    });
		    //]]>
		</script>
            
        </div>
        
        <?php
	}
	
	function save_th_post_video_mbox($post_id) {
			global $Current_options;
			$th_post_video = false;
			$width = 0;
			$height = 0;
			$aspect = $Current_options['video_aspect'];
			if(isset($_POST['th_post_video_width']) && is_numeric($_POST['th_post_video_width']))
				$width = $_POST['th_post_video_width'];
				
			if(isset($_POST['th_post_video_height']) && is_numeric($_POST['th_post_video_height']))
				$height = $_POST['th_post_video_height'];
				
			if(isset($_POST['th_post_video_aspect']) && is_numeric($_POST['th_post_video_aspect'])) {
				$aspect = $_POST['th_post_video_aspect'];
				if($aspect == 0)
					$aspect = $Current_options['video_aspect'];
			}
				
			if (isset($_POST['th_post_video_resource']) && $_POST['th_post_video_resource']!="") {

               	update_post_meta($post_id, 'th_post_video_resource', mysql_real_escape_string(trim($_POST['th_post_video_resource'])));

				switch($_POST['th_post_video_src']) {
					case "vimeo":
						$th_post_video =  "http://www.vimeo.com/".trim($_POST['th_post_video_resource']);
					break;
					case "youtube":
						$th_post_video =  "http://www.youtube.com/watch?v=".trim($_POST['th_post_video_resource']);
					break;
					case "quicktime":
					case "flash":
						$th_post_video = trim($_POST['th_post_video_resource']);
				}
			}
			else
				delete_post_meta($post_id, 'th_post_video_resource'); 
				
			if($th_post_video)
				update_post_meta($post_id, 'th_post_video', mysql_real_escape_string($th_post_video).(($height>0 && $width>0)?"?width=".$width."&height=".$height:""));
			else
				delete_post_meta($post_id, 'th_post_video'); 
			
			if(isset($_POST['th_post_video_src']))
				update_post_meta($post_id, 'th_post_video_src', $_POST['th_post_video_src']);

			update_post_meta($post_id, 'th_post_video_width', $width);
			update_post_meta($post_id, 'th_post_video_height', $height);
			update_post_meta($post_id, 'th_post_video_aspect', $aspect);
	
	}
	
	

	function th_imagethumb($width='260', $height=false, $lightbox=false) {

		global $Current_options, $post;
		
		$output = '';

		$rel_attr = ' rel="wp-prettyPhoto"';

		if(has_post_thumbnail()) {
			$blog_item_preview=wp_get_attachment_url( get_post_thumbnail_id() );
		}
	  	
		if ( isset($blog_item_preview) && $blog_item_preview ) {

			if($lightbox)
				$preview_item = $blog_item_preview;
			else
			   $preview_item = get_permalink(); 

			
			$output .= '<img class="blogitemimg" src="'.get_bloginfo("template_directory").'/scripts/timthumb.php?src='.$blog_item_preview.'&amp;w='.$width.($height?"&amp;h=".$height:"").'&amp;zc=1&amp;q=90" alt="'.get_the_title().'" style="width:100%;" />';
			
			$output = '<a class="thumblink" href="'.$preview_item.'" title="'.get_the_title().'" '.($lightbox?$rel_attr:"").' >'.$output.'<span class="thumblinkoverlay"></span></a>';
			
	    }
		echo $output;

	}
	
	function th_postvideo() {
		global $post;
		$output = '';
		
		$width = ((get_post_meta($post->ID, 'th_post_video_width', true)>0)?get_post_meta($post->ID, 'th_post_video_width', true):'100%');
		$height = ((get_post_meta($post->ID, 'th_post_video_height', true)>0)?get_post_meta($post->ID, 'th_post_video_height', true):'100%');
		$resource = get_post_meta($post->ID, 'th_post_video_resource', true);
		
		switch(get_post_meta($post->ID, 'th_post_video_src', true)) {
			case "vimeo":
				$output = '<iframe style="position:absolute" src="http://player.vimeo.com/video/'.$resource.'?title=0&amp;byline=0&amp;portrait=0&amp;color=b3b1b1" width="'.$width.'" height="'.$height.'" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>';
			break;
			case "youtube":
				$output = '<iframe style="position:absolute" width="'.$width.'" height="'.$height.'" src="http://www.youtube.com/embed/'.$resource.'?rel=0" frameborder="0" allowfullscreen></iframe>';
			break;
			case "quicktime":
				$output = '<object style="position:absolute" classid="clsid:02BF25D5-8C17-4B23-BC80-D3488ABDDC6B" codebase="http://www.apple.com/qtactivex/qtplugin.cab" height="'.$height.'" width="'.$width.'"><param name="src" value="'.$resource.'"><param name="type" value="video/quicktime"><embed src="'.$resource.'" height="'.$height.'" width="'.$width.'" type="video/quicktime" pluginspage="http://www.apple.com/quicktime/download/"></embed></object>';
			break;
			case "flash":
				$output = '<object style="position:absolute" classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="'.$width.'" height="'.$height.'"><param name="wmode" value="transparent" /><param name="allowfullscreen" value="true" /><param name="allowscriptaccess" value="always" /><param name="movie" value="'.$resource.'" /><embed src="'.$resource.'" type="application/x-shockwave-flash" allowfullscreen="true" allowscriptaccess="always" width="'.$width.'" height="'.$height.'" wmode="transparent"></embed></object>';
			
		}
		
		echo $output;
	}

// Comment template
function Current_comment( $comment, $args, $depth ) {
     $GLOBALS['comment'] = $comment;
   $template_dir_url = get_template_directory_uri(); ?>

   <li <?php comment_class(); ?> id="li-comment-<?php comment_ID() ?>">
     <div id="comment-<?php comment_ID(); ?>">
	<div class="commentdabba">
	    <div class="avatarcontainer">
<?php		echo get_avatar( $comment, $size='52', $default="{$template_dir_url}/images/noavatar.jpg" ); ?>
	    </div>
	    <div class="commentmetadata">
		<p class="authorname"><?php comment_author_link() ?></p>
<?php		    printf(__('<span class="time">%1$s</span> on <a href="#comment-%2$s" title="">%3$s</a>', 'Current'), get_comment_time('g:i a'), get_comment_ID(), get_comment_date('F j, Y') );
		    edit_comment_link(esc_html__('edit', 'Current'),'&nbsp;&nbsp;',''); ?>
	    </div>
	</div>

	<div class="commentbox">
    
<?php	    if ($comment->comment_approved == '0') : ?>
		<em><?php esc_html_e('Your comment is awaiting moderation.', 'Current') ?></em>
		<br />
<?php	    endif; 
	    comment_text() ?>
	    <div class="reply">
<?php		comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
	    </div>
        
        
	</div>


     </div>
<?php
}


add_filter( 'the_password_form', 'custom_password_form' );

function custom_password_form() {
	global $post;
	$label = 'pwbox-'.( empty( $post->ID ) ? rand() : $post->ID );
	$o = '<form class="protected-post-form" action="' . get_option('siteurl') . '/wp-pass.php" method="post"><p>
	' . __( "This post is password protected. To view it please enter your password below", 'Current' ) . '</p>
	<div class="form_row"><label for="' . $label . '">' . __( "Password", 'Current' ) . ': </label><br /><input name="post_password" id="' . $label . '" type="password" size="20" class="inputbox" style="width:200px;" /></div><div class="form_row"><input type="submit" name="Submit" value="' . esc_attr__( "Submit" ) . '" /></div>
	</form>
	';
	return $o;
}


back_general_settings(array ('name' => 'Layout Settings','type' => 'general_settings','function' => 'settings','width' => '6','css,id,header_top,min-height,px' => 'Header height','css,id,navigation-menu,margin-top,px' => 'Main menu top margin','css,id,inset,margin-top,px' => 'Inset top margin','param,image_aspect' => 'Image aspect ratio','param,video_aspect' => 'Video aspect ratio','id' => 'layout_settings','row' => '0','column' => '0'));


back_google_webfonts(array ('name' => 'Fonts Management','type' => 'google_webfonts','function' => 'settings','width' => '6','id' => 'fonts_management','row' => '0','column' => '1'));


back_simple_background(array ('name' => 'page bg','type' => 'simple_background','function' => 'settings','width' => '6','id' => 'page_bg','row' => '0','column' => '2'));


back_general_settings(array ('name' => 'Content Settings','type' => 'general_settings','function' => 'settings','width' => '6','param,excerpt_length' => 'Excerpt Length (words)','param,comments_posts,checkbox' => 'Display comments on posts','param,comments_pages,checkbox' => 'Display comments on pages','id' => 'content_settings','row' => '3','column' => '0'));


back_general_settings(array ('name' => 'General Colors Configuration','type' => 'general_settings','function' => 'settings','width' => '6','param,theme_color,color' => 'Theme color','css,tag,body,color,color' => 'General text color','param,headings_color,color' => 'Headings color','css,tag,a#a:visited,color,color' => 'Links color','css,tag,a:hover,color,color' => 'Links hover color','css,tag,h1.pagetitle,color,color' => 'Page title color','id' => 'general_colors_configuration','row' => '3','column' => '1'));


back_general_settings(array ('name' => 'Main Menu Colors','type' => 'general_settings','function' => 'settings','width' => '6','css,tag,ul.sf-menu>li>a#ul.sf-menu>li>a:visited,color,color' => 'Menu links color','css,tag,ul.sf-menu>li>a:hover,color,color' => 'Menu hover color','css,tag,ul.sf-menu ul.sub-menu a#ul.sf-menu ul.sub-menu a:visited,color,color' => 'Sub-menu links color','css,tag,ul.sf-menu ul.sub-menu a:hover,color,color' => 'Sub-menu hover color','id' => 'main_menu_colors','row' => '3','column' => '2'));


back_mocktail_slider(array ('name' => 'Mocktail Slider','type' => 'mocktail_slider','function' => 'slider','width' => '24','id' => 'mocktail_slider','row' => '8','column' => '0'));


back_simple_pagetitle(array ('name' => 'Simple Pagetitle','type' => 'simple_pagetitle','function' => 'title','width' => '24','id' => 'simple_pagetitle','row' => '9','column' => '0'));


back_simple_sidebar(array ('name' => 'Left Sidebar','type' => 'simple_sidebar','function' => 'sidebar','width' => '6','id' => 'left_sidebar','row' => '10','column' => '0'));


back_simple_sidebar(array ('name' => 'Right Sidebar','type' => 'simple_sidebar','function' => 'sidebar','width' => '6','id' => 'right_sidebar','row' => '10','column' => '2'));


back_simple_sidebar(array ('name' => 'Left Sidebar','type' => 'simple_sidebar','function' => 'sidebar','width' => '6','id' => 'left_sidebar','row' => '11','column' => '0'));


back_simple_sidebar(array ('name' => 'Right Sidebar','type' => 'simple_sidebar','function' => 'sidebar','width' => '6','id' => 'right_sidebar','row' => '11','column' => '2'));


back_simple_sidebar(array ('name' => 'Left Sidebar','type' => 'simple_sidebar','function' => 'sidebar','width' => '6','id' => 'left_sidebar','row' => '12','column' => '0'));


back_simple_blog(array ('name' => 'Simple Blog','type' => 'simple_blog','function' => 'page','width' => '12','id' => 'simple_blog','row' => '12','column' => '1'));


back_simple_sidebar(array ('name' => 'Right Sidebar','type' => 'simple_sidebar','function' => 'sidebar','width' => '6','id' => 'right_sidebar','row' => '12','column' => '2'));


back_simple_sidebar(array ('name' => 'Left Sidebar','type' => 'simple_sidebar','function' => 'sidebar','width' => '6','id' => 'left_sidebar','row' => '13','column' => '0'));


back_simple_contact(array ('name' => 'Simple Contact','type' => 'simple_contact','function' => 'page','width' => '12','id' => 'simple_contact','row' => '13','column' => '1'));


back_simple_sidebar(array ('name' => 'Right Sidebar','type' => 'simple_sidebar','function' => 'sidebar','width' => '6','id' => 'right_sidebar','row' => '13','column' => '2'));


back_simple_sidebar(array ('name' => 'Left Sidebar','type' => 'simple_sidebar','function' => 'sidebar','width' => '6','id' => 'left_sidebar','row' => '17','column' => '0'));


back_simple_sidebar(array ('name' => 'Right Sidebar','type' => 'simple_sidebar','function' => 'sidebar','width' => '6','id' => 'right_sidebar','row' => '17','column' => '2'));


back_simple_sidebar(array ('name' => 'Left Sidebar','type' => 'simple_sidebar','function' => 'sidebar','width' => '6','id' => 'left_sidebar','row' => '18','column' => '0'));


back_simple_sidebar(array ('name' => 'Right Sidebar','type' => 'simple_sidebar','function' => 'sidebar','width' => '6','id' => 'right_sidebar','row' => '18','column' => '2'));


back_simple_sidebar(array ('name' => 'Left Sidebar','type' => 'simple_sidebar','function' => 'sidebar','width' => '6','id' => 'left_sidebar','row' => '19','column' => '0'));


back_simple_sidebar(array ('name' => 'Right Sidebar','type' => 'simple_sidebar','function' => 'sidebar','width' => '6','id' => 'right_sidebar','row' => '19','column' => '2'));


back_simple_sidebar(array ('name' => 'Left Sidebar','type' => 'simple_sidebar','function' => 'sidebar','width' => '6','id' => 'left_sidebar','row' => '20','column' => '0'));


back_simple_portfolio(array ('name' => 'Simple Portfolio','type' => 'simple_portfolio','function' => 'page','image_frame' => 'image_frame','width' => '12','id' => 'simple_portfolio','row' => '20','column' => '1'));


back_simple_sidebar(array ('name' => 'Right Sidebar','type' => 'simple_sidebar','function' => 'sidebar','width' => '6','id' => 'right_sidebar','row' => '20','column' => '2'));


back_simple_sidebar(array ('name' => 'Left Sidebar','type' => 'simple_sidebar','function' => 'sidebar','width' => '6','id' => 'left_sidebar','row' => '21','column' => '0'));


back_simple_sidebar(array ('name' => 'Right Sidebar','type' => 'simple_sidebar','function' => 'sidebar','width' => '6','id' => 'right_sidebar','row' => '21','column' => '2'));


back_simple_sidebar(array ('name' => 'Left Sidebar','type' => 'simple_sidebar','function' => 'sidebar','width' => '6','id' => 'left_sidebar','row' => '22','column' => '0'));


back_simple_sidebar(array ('name' => 'Right Sidebar','type' => 'simple_sidebar','function' => 'sidebar','width' => '6','id' => 'right_sidebar','row' => '22','column' => '2'));


back_simple_sidebar(array ('name' => 'Left Sidebar','type' => 'simple_sidebar','function' => 'sidebar','width' => '6','id' => 'left_sidebar','row' => '23','column' => '0'));


back_simple_sitemap(array ('name' => 'Simple Sitemap','type' => 'simple_sitemap','function' => 'page','width' => '12','id' => 'simple_sitemap','row' => '23','column' => '1'));


back_simple_sidebar(array ('name' => 'Right Sidebar','type' => 'simple_sidebar','function' => 'sidebar','width' => '6','id' => 'right_sidebar','row' => '23','column' => '2'));


function willrender_general_settings($args = array()) {
				return false;
			}


function back_general_settings($args = array()) {
				global $general_settings_array;
				$args['id'] = isset($args['id'])?$args['id']:str_replace(' ','_',$args['name']);
				$general_settings_array[$args['id']]=$args;
			}
 

add_action( 'init', 'load_google_web_fonts' );

function load_google_web_fonts() {
	global $Current_options;
	if(!is_admin() && isset($Current_options['google_fonts_string']) && $Current_options['google_fonts_string']!='')
		wp_enqueue_style('google_webfonts', 'http://fonts.googleapis.com/css?family='.$Current_options['google_fonts_string'].'&subset=latin,cyrillic,greek,khmer,vietnamese', false, '1.0', 'screen');
}


function willrender_google_webfonts($args = array()) {
				return false;
			}


function back_google_webfonts($args = array()) {
				global $google_webfonts_array;
				$args['id'] = isset($args['id'])?$args['id']:str_replace(' ','_',$args['name']);
				$google_webfonts_array[$args['id']]=$args;
			}
 

function sanitize_google_webfonts($options) { 

	global $Current_options, $google_webfonts;
	
	
	$google_web_fonts_array = array_intersect(array( $options['font_family'],
							  $options['top_nav_font_family'],
							  $options['title_headings_font_family'] ), $google_webfonts);
	$options['google_fonts_string'] = implode( '|', array_unique($google_web_fonts_array) );



 return $options;
			}


function willrender_simple_background($args = array()) {
				return false;
			}


function back_simple_background($args = array()) {
				global $simple_background_array;
				$args['id'] = isset($args['id'])?$args['id']:str_replace(' ','_',$args['name']);
				$simple_background_array[$args['id']]=$args;
			}
 

	function button_function( $params ) {
    extract(shortcode_atts(array('text' => esc_html__('Read more...', 'Current'),
	    'title' => '',
		'corners' => '',
	    'url' => '#',
	    'size' => 'm',
	    'bgcolor' => '',
	    'color' => '',
	    'align' => '',
	    'target' => '',
		'bevel' => 'yes',
		'lightbox' => '0',
		'class' => '',
    ), $params));
    $target = ($target == '_blank') ? ' target="_blank"' : '';
    if($align!='')
		$align = ' btn'.$align;	
	if($lightbox!=0)
		$lightbox=' rel="wp-prettyPhoto[]"';
	else
		$lightbox=false;
	$shapeclass="";
	if( $bevel == 'yes' )
		$shapeclass=' bevel';
	
	if( $corners == 'round' )
		$shapeclass=' round';
	elseif( $corners == 'cornered' )
		$shapeclass=' cornered';
		
		
    $html = '<div class="custombuttonwrap '.strtolower($size).$align.' '.$class.'">
                <a '.($lightbox?$lightbox:"").' class="'.strtolower($size).' custombutton'.$shapeclass.'" href="'.$url.'" title="'.$title.'"'.$target.'><span style="'.(($bgcolor!='')?' background-color:'.$bgcolor.' !important;':'').(($color!='')?' color:'.$color.' !important;':'').'">'.do_shortcode($text).'</span></a></div>
	     ';
    return $html;
}
add_shortcode('button', 'button_function');



function willrender_shortcode_button($args = array()) {
				return false;
			}



	// Short code for Tab(s)
	function tabs_function( $params, $content = null ) {
		global $single_tab_array;
		$single_tab_array = array(); // clear the array
	

		$tabs_nav = '<div class="tabs">';
		$tabs_nav .= '<ul>';
		do_shortcode($content);
		$tabs_content = "";
		foreach ($single_tab_array as $tab => $tab_attr_array) {
		$random_id = rand(1000,2000);
		$tabs_nav .= '<li><a href="#tab'.$random_id.'"><h4>'.$tab_attr_array['title'].'</h4></a></li>';
		$tabs_content .= '<div id="tab'.$random_id.'">'.$tab_attr_array['content'].'</div>';
		}
		$tabs_nav .= '</ul>';
		$tabs_output = $tabs_nav . $tabs_content;
		$tabs_output .= '</div>';
		return $tabs_output;
	}
	
	add_shortcode('tabs', 'tabs_function');
	
	function tab_function( $params, $content = null ) {
		extract(shortcode_atts(array(
			'title'      => '',
		), $params));
		global $single_tab_array;
		$single_tab_array[] = array('title' => $title, 'content' => trim(do_shortcode($content)));
		return $single_tab_array;
	}
	add_shortcode('tab', 'tab_function');

	function accordion_function( $params, $content = null ) {
    global $single_tab_array;
    $single_tab_array = array(); 

 
    $html = '<div class="accordion">';

    do_shortcode($content);
    foreach ($single_tab_array as $tab => $tab_attr_array) {
	$html .= '<div>';
	$html .= '<h4><a href="#">'.$tab_attr_array['title'].'</a></h4>';
	$html .= '<div>'.$tab_attr_array['content'].'</div>';
    $html.= '</div>';
    }

    $html.= '</div>';
    return $html;
	}
	add_shortcode('accordion', 'accordion_function');



function willrender_shortcode_tabsaccordion($args = array()) {
				return true;
			}


function willrender_prettyphoto_lightbox($args = array()) {
					global $Current_options;
					if(isset( $Current_options['prettyphoto']) && $Current_options['prettyphoto']=="yes")
						return true;
					else
						return false;
			}


function msgbox_function( $params, $content = null ) {
    extract(shortcode_atts(array(
	    'type' => '',
	    'align' => 'left',
	    'bgcolor2' => '',
	    'bgcolor' => '',
	    'border_size' => '1',
		'border_color' => '#BBBBBB',
	    'width' => 'auto',
	    'color' => '#333333',
    ), $params));
	
    if ($type == '') {
	if ($align == 'center') {
	    $margin_left = $margin_right = 'auto !important';
	} elseif ($align == 'right') {
	    $margin_left = 'auto !important';
	    $margin_right = '0 !important';
	} else {
	    $margin_left = $margin_right = '0 !important';
	}
	if(strpos($width, '%')===false && strpos($width, 'auto')===false)
		$width.='px';
	
	$bgcss=(($bgcolor!='')?(($bgcolor2=='')?'background: '.$bgcolor.';':'background: '.$bgcolor.'; background: -moz-linear-gradient(top,  '.$bgcolor2.' 0%, '.$bgcolor.' 100%); background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,'.$bgcolor2.'), color-stop(100%,'.$bgcolor.')); background: -webkit-linear-gradient(top,  '.$bgcolor2.' 0%,'.$bgcolor.' 100%); background: -o-linear-gradient(top,  '.$bgcolor2.' 0%,'.$bgcolor.' 100%); background: -ms-linear-gradient(top,  '.$bgcolor2.' 0%,'.$bgcolor.' 100%); background: linear-gradient(top,  '.$bgcolor2.' 0%,'.$bgcolor.' 100%);'." filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='".$bgcolor2."', endColorstr='".$bgcolor."',GradientType=0 );"):"");
	
	$html = '<div class="msgboxwrap"><div class="custom" style="'.$bgcss.' margin-left:'.$margin_left.';
			   margin-right:'.$margin_right.';
			   border:'.$border_size.'px solid '.$border_color.';'.(($border_size>1)?"margin-top:-".$border_size."px; margin-bottom:-".$border_size."px;":"").'
			   width:'.$width.';
			   color:'.$color.';"><div class="msgboxpadding">'.do_shortcode($content).'</div></div></div>';
    } else {
	$html = '<div class="msgboxwrap"><div class="'.$type.'"><div class="msgbox-icon">'.do_shortcode($content).'</div></div></div>';
    }
    return $html;
}
add_shortcode('msgbox', 'msgbox_function');



function willrender_shortcode_msgbox($args = array()) {
				return false;
			}


function divider_generic( $params ) {
	global $Current_options;
	$leading=$Current_options['font_size']*$Current_options['line_height'];
	
	extract(shortcode_atts(array(
	    'color' => '#dbdbdb',
	    'height' => '1',
		'margin' => $leading,
		'opacity' => '',
		'backtotop' => '',
    ), $params));
	if($height>$leading){
		$height=$leading;
		$ceil=0;
		$floor=0;
	}
	else{
	$ceil=((($margin-$height)/2)>0?(($margin-$height)/$Current_options['font_size']/2):0);
	$floor=((($margin-$height)/2)>0?(($margin-$height)/$Current_options['font_size']/2):0);
	}
	if($opacity!='')
		$opacity = " opacity:".$opacity.";";
	else
		$opacity=false;
    return '<div class="divider_generic_wrap" style="padding:'.$ceil.'em 0 '.$floor.'em;"><div class="divider_generic'.(($backtotop!='')?' totop':'').'" style="height:'.$height.'px; background-color:'.$color.';'.($opacity?$opacity:"").'">'.(($backtotop!='')?'<a style="bottom:'.$height.'px" href="#top" title="'.esc_html__('Top of Page', 'Current').'">'.esc_html__('Back to Top', 'Current').'</a>':'').'</div></div>';
}
add_shortcode('divider', 'divider_generic');


function willrender_shortcode_divider($args = array()) {
				return false;
			}



	include ('scripts/plugin_latestposts.php');
	add_action('wp_ajax_plugin_latestposts_get', 'plugin_latestposts_get');
	add_action('wp_ajax_nopriv_plugin_latestposts_get', 'plugin_latestposts_get');
	
if(isset($_REQUEST['action']))	
	do_action( 'wp_ajax_nopriv_' . $_REQUEST['action'] );
if(isset($_POST['action']))	 
	do_action( 'wp_ajax_' . $_POST['action'] );
	
	function plugin_latestposts_get() {
		global $post, $global_blocks, $Current_options;
		$currentpfid = $_POST['categories'];
		$portfolio_items_per_page = $_POST['itemsperpage'];
		$colsperpage = $_POST['itemsperrow'];
		$currentcategoryid = (isset($_POST['currentcategory']) && $_POST['currentcategory']!='')?$_POST['currentcategory']:false;
		$sortable = (isset($_POST['sortable']) && $_POST['sortable']!='')?$_POST['sortable']:false;
		$aspect = $_POST['aspect'];
		$title_style = $_POST['title_style'];
		$description = $_POST['description'];
		$author = (isset($_POST['author']) && $_POST['author']!='')?$_POST['author']:false;	
		$lightbox = (isset($_POST['lightbox']) && $_POST['lightbox']!='')?$_POST['lightbox']:false;			
		$thumbnails = (isset($_POST['thumbnails']) && $_POST['thumbnails']!='')?$_POST['thumbnails']:false;			
		$readmore = (isset($_POST['readmore']) && $_POST['readmore']!='')?$_POST['readmore']:false;	
		$readmore_text = $_POST['readmore_text'];
		$font_size = $Current_options['font_size'];
		$line_height = $Current_options['line_height'];
		$site_width = $Current_options['site_width'];
		$gutter_width = $Current_options['gutter_width'];

if($sortable && strpos($currentpfid, "|")) { ?>
<div class="plugin_latestposts_sort">
<?php				$sortcats = array();
					$sortcats[]=do_shortcode("[button size='s' url='#category=0' text='".__("All")."' align='left']");
					foreach(explode("|", $currentpfid) as $categoryitem) { 
							$sortcats[]=do_shortcode("[button size='s' url='#category=".$categoryitem."' text='".get_cat_name($categoryitem)."' align='left']");

 					} 
					echo implode("  ", $sortcats);
					
					?>
</div>
<br class="clear" />                              

<?php }

	    if( $currentpfid ) :

		if ( !$currentcategoryid )
				$currentcategoryid = $currentpfid;
		
		
	$counter = 0;
	

	if(!get_query_var('paged') && isset($_POST['paged']) && $_POST['paged']!='')
		$paged=$_POST['paged'];
	else  $paged = 1;
	
	

	
	
	query_posts( array('cat' => str_replace("|", ",", $currentcategoryid), 'posts_per_page' => $portfolio_items_per_page, 'paged' => $paged));

		if (have_posts()) : 
  
		$counter=0;
		while (have_posts()) : the_post();
		$counter++;
		$endclass="";
        if($counter%$colsperpage==0)
			$endclass="omega";
        if(($counter-1)%$colsperpage==0)
			$endclass="alpha"			
		?>
		<div class="grid_6 <?php echo $endclass;?>">
        	<div class="latestpostitem" id="latestpostitem<?php echo $counter;?>">
        <?php
		if($thumbnails && get_post_meta($post->ID, '_thumbnail_id', true)) {
			 $thumbwidth=($site_width-($gutter_width*$colsperpage))/$colsperpage;
	
			if($thumbwidth<ceil($site_width/2))
				$thumbwidth = ceil($site_width/2);
				$thumbheight=(int)($thumbwidth/$aspect);
	
			echo "<div class='item_thumbnail'>";
			echo plugin_latestposts_imagethumb($thumbwidth, $thumbheight, false, $lightbox);
			echo "</div>";
		}

		$item_description="";

		if($title_style>0){
					$title="<h5 class='item_title'>".(($title_style==2)?"<a title='".get_the_title()."' href='".get_permalink()."'>":"");
					$title.=(get_the_title()?get_the_title():"&nbsp;");
					$title.=(($title_style==2)?"</a>":"")."</h5>";
					$item_description.= $title;
				}

		if($author && $author==1){
			$item_description.= "<small>".sprintf( __('By %1$s on %2$s ', 'Current'), get_the_author_link(), get_the_date() )."</small>";
			}

		if($description>0){
					$item_description.= ($description==1)?get_the_content():"<p>".get_the_excerpt()."</p>";
				}				
		
		if($readmore && $readmore==1) {
			$item_description.= do_shortcode('[button '.(($readmore_text!='')?'text="'.$readmore_text.'"':"") .' title="'.get_the_title().'" url="'.get_permalink().'"]')."<br  class=\"clear\" />";
			
		}
		
		if($item_description!="")
			echo "<div class=\"item_description\">".$item_description."</div>";
		
		?>
        </div>
        </div>
		<?php        
        if($endclass=="omega")		
			echo "<br class=\"clear\"/>";	
	 	endwhile; 
echo "<br class=\"clear\"/>";
		?>
		    <div class="navigation">
			    <?php if($paged>1) { ?>
                <div class="custombuttonwrap s alignleft"><a href="#paged=<?php echo $paged-1;?>" id="directionleft"><span>&lt;</span></a></div>
                <?php }
				wp_reset_query();
				query_posts( array('cat' => str_replace("|", ",", $currentcategoryid), 'posts_per_page' => $portfolio_items_per_page, 'paged' => $paged+1));

		if (have_posts()) { ?>
				<div class="custombuttonwrap s alignright"><a href="#paged=<?php echo $paged+1;?>" id="directionright"><span>&gt;</span></a></div>
		<?php } ?>                
		    </div>

<?php
	    else : ?>
		    <p></p><div class="warning">
				<div class="msgbox-icon"><?php _e("A Category needs to be assigned in the Portfolio section for this page in order to have the portfolio item posts from that category displayed here. Please check the Current setting's Portfolio Section.", 'wpavatar'); ?></div>
            </div>
            
<?php
	    endif;
	endif;

			die();
	}

	function plugin_latestposts_imagethumb($width='260', $height=false, $grey = false, $lightbox = false) {

		global $Current_options, $post;

		
		$output = '';


		if(get_post_meta($post->ID, '_thumbnail_id', true)) {
			$portfolio_item_preview=wp_get_attachment_url( get_post_meta($post->ID, '_thumbnail_id', true) );
		}
	  	
		$post_media = get_post_meta($post->ID, 'th_post_video', true);

	    if ( isset($portfolio_item_preview) && $portfolio_item_preview ) {

	 			$preview_item = get_permalink(); 
				
				if($lightbox)
					if($post_media)
						$preview_item=$post_media;
					else
						$preview_item=$portfolio_item_preview;
			
			$leading =  $Current_options["line_height"]*$Current_options["font_size"];
			
			if($grey)
				$output = '<img src="'.get_bloginfo("template_directory").'/scripts/timthumb.php?src='.$portfolio_item_preview.'&amp;w='.$width.($height?"&amp;h=".$height:"").'&amp;zc=1&amp;q=90'.(!$height?"&amp;leading=".$leading:"").'" alt="'.get_the_title().'" style="width:100%;" />';
			
				$output .= '<img class="portfolioitemimg'.($grey?' greyhover':'').'" src="'.get_bloginfo("template_directory").'/scripts/timthumb.php?src='.$portfolio_item_preview.'&amp;w='.$width.($height?"&amp;h=".$height:"").'&amp;zc=1&amp;q=90'.(!$height?"&amp;leading=".$leading:"").($grey?'&amp;f=2':'').'" alt="'.get_the_title().'" style="width:100%;" />';
			
			
			$output = '<a class="thumblink'.((isset($post_media) && $post_media!="")?' movie':'').'" href="'.$preview_item.'" title="'.get_the_title().'" '.($lightbox?'rel="wp-prettyPhoto[latestposts]"':'').'>'.$output.'<span class="thumblinkoverlay"></span></a>';
			
	    }



	echo $output;

}



function willrender_plugin_latestposts($args = array()) {
				return true;
			}



function one_fourth_function( $params, $content = null ) {

    return '<div class="one_fourth">'.do_shortcode($content).'</div>';
}
add_shortcode('one_fourth', 'one_fourth_function');


function one_fourth_last_function( $params, $content = null ) {
    return '<div class="one_fourth last_column">'.do_shortcode($content).'</div><br class="clear" />';
}
add_shortcode('one_fourth_last', 'one_fourth_last_function');

function one_third_function( $params, $content = null ) {
    return '<div class="one_third">'.do_shortcode($content).'</div>';
}
add_shortcode('one_third', 'one_third_function');


function one_third_last_function( $params, $content = null ) {
    return '<div class="one_third last_column">'.do_shortcode($content).'</div><br class="clear" />';
}
add_shortcode('one_third_last', 'one_third_last_function');

function one_half_function( $params, $content = null ) {
    return '<div class="one_half">'.do_shortcode($content).'</div>';
}
add_shortcode('one_half', 'one_half_function');


function one_half_last_function( $params, $content = null ) {
    return '<div class="one_half last_column">'.do_shortcode($content).'</div><br class="clear" />';
}
add_shortcode('one_half_last', 'one_half_last_function');

function two_third_function( $params, $content = null ) {
    return '<div class="two_third">'.do_shortcode($content).'</div>';
}
add_shortcode('two_third', 'two_third_function');


function two_third_last_function( $params, $content = null ) {
    return '<div class="two_third last_column">'.do_shortcode($content).'</div><br class="clear" />';
}
add_shortcode('two_third_last', 'two_third_last_function');

function three_fourth_function( $params, $content = null ) {
    return '<div class="three_fourth">'.do_shortcode($content).'</div>';
}
add_shortcode('three_fourth', 'three_fourth_function');


function three_fourth_last_function( $params, $content = null ) {
    return '<div class="three_fourth last_column">'.do_shortcode($content).'</div><br class="clear" />';
}
add_shortcode('three_fourth_last', 'three_fourth_last_function');


function willrender_shortcode_columns($args = array()) {
				return false;
			}



// Short code for dropcaps

function dropcap_function( $params, $content = null ) {
    return '<span class="dropcap">'.$content.'</span>';
}
add_shortcode('dropcap', 'dropcap_function');

//shortcodes for pullquote, blockquote

function pullquote_function( $params, $content = null ) {
    extract(shortcode_atts(array(
	    'align' => 'left',
    ), $params));
    $align = ($align == 'right') ? 'right' : 'left';
    return '<blockquote class="'.$align.'">'.do_shortcode($content).'</blockquote>';
}
add_shortcode('pullquote', 'pullquote_function');

function blockquote_function( $params, $content = null ) {
    return '<blockquote>'.do_shortcode($content).'</blockquote>';
}
add_shortcode('blockquote', 'blockquote_function');


function willrender_shortcode_quotesandcaps($args = array()) {
				return false;
			}


function list_style_function( $params, $content = null ) {
    extract(shortcode_atts(array(
	    'style' => '1',
		'big' => '0',
    ), $params));
    $content = str_replace('<ul>', '<ul class="'.(($big>0)?"big":"").' list_style'.$style.'">', do_shortcode($content));
    return $content;
}
add_shortcode('list', 'list_style_function');


function willrender_shortcode_list($args = array()) {
				return false;
			}


global $widgetcss_options;
if((!$widgetcss_options = get_option('widget_css')) || !is_array($widgetcss_options) ) $widgetcss_options = array();


function filter_widget( $params ) {
	global $widgetcss_options;
	

		  if(isset($widgetcss_options[$params[0]['widget_id']]))
		  	$params[0]['before_widget'] = str_replace( 'th_widget', 'th_widget '.$widgetcss_options[$params[0]['widget_id']], $params[0]['before_widget'] );
	      return $params;

}
add_filter('dynamic_sidebar_params','filter_widget');



function willrender_feature_customcss($args = array()) {
				return true;
			}


add_action('wp_footer', 'google_analytics');
 
function google_analytics() { 
	global $Current_options;
	if(isset($Current_options['google_analytics']) && $Current_options['google_analytics']!="")
		echo $Current_options['google_analytics'];
}


function willrender_google_analytics($args = array()) {
				return true;
			}


function rhythmize_function($params, $content = null ) {
	
global $Current_options;
	$leading=$Current_options['font_size']*$Current_options['line_height'];

	
	extract(shortcode_atts(array(
			'height'      => '0',
			'lines'		=> '0',
			'align'		=> 'top',
		), $params));	
	
	if($height>0) {
		if($lines>($height/$leading))
			$html='<div class="rhythmize" style="padding-'.((strtolower($align)=='top')?'bottom':'top').':'.($lines*$leading-$height)/$Current_options['font_size'].'em;">'.do_shortcode($content).'</div>';	
		else
			$html='<div class="rhythmize" style="padding-'.((strtolower($align)=='top')?'bottom':'top').':'.($leading-($height%$leading))/$Current_options['font_size'].'em;">'.do_shortcode($content).'</div>';	
	}
	else
		$html=do_shortcode($content);
		
	return $html;
}

add_shortcode('rhythmize', 'rhythmize_function');


function willrender_shortcode_rhythmize($args = array()) {
				return false;
			}


function reg_simple_widget_location($widgets = array()) {
					foreach($widgets as $args) {
						$args['name'] = isset($args['name'])?$args['name']:'';
						$args['id'] = isset($args['id'])?$args['id']:str_replace(' ','_',strtolower($args['name']));
						$args['desc'] = isset($args['desc'])?$args['desc']:'';	
						$args['titlestyle'] = isset($args['titlestyle'])?$args['titlestyle']:'h3';	
						
						register_sidebar(array(
						'name' => esc_html__($args['name'], 'Current'),
						'id' => $args['id'],
						'description' => esc_html__($args['desc'], 'Current'),
						'before_widget' => '<div class="th_widget">',
						'after_widget' => '</div>',
						'before_title' => '<'.$args['titlestyle'].'>',
						'after_title' => '</'.$args['titlestyle'].'>',
						));
					}
				}


function willrender_simple_widget_location($args = array()) {
				$args['id'] = isset($args['id'])?$args['id']:str_replace(' ','_',$args['name']);			
				return is_active_sidebar($args['id']);
			}


function willrender_simple_footer($args = array()) {
			global $Current_options;
			if((isset($Current_options['simple_footer_cp']) && $Current_options['simple_footer_cp'] == 'yes') || 
			(isset($Current_options['simple_footer_wp']) && $Current_options['simple_footer_wp'] == 'yes') || 
			(isset($Current_options['simple_footer_entries']) && $Current_options['simple_footer_entries'] == 'yes') || 
			(isset($Current_options['simple_footer_comments']) && $Current_options['simple_footer_comments'] == 'yes') || 
			(isset($Current_options['simple_footer_tc']) && $Current_options['simple_footer_tc'] == 'yes') || 
			(isset($Current_options['simple_footer_top']) && $Current_options['simple_footer_top'] == 'yes'))
				return true;
			else
				return false;
			}


function willrender_simple_logo($args = array()) {
				return true;
			}

 function th_navigation($args = array()) {
				
				if ( function_exists( 'wp_nav_menu' ) ) {
					wp_nav_menu( array( 'container_class' => 'navigation-menu',
								'container_id' => 'navigation-menu',
								'menu_class' => 'sf-menu',
								'link_before'=> '<span>',
								'link_after' => '</span>',
								'theme_location' => 'primary',
								'fallback_cb' => 'Current_nav_fallback' 
								)
							);
				}
				else
					Current_nav_fallback();
			}
			
		function Current_nav_fallback() {
		
				global $Current_options;
				$menu_html = '<div id="navigation-menu" class="navigation-menu">';
				$menu_html .= '<ul class="sf-menu">';
				$menu_html .= is_front_page() ? "<li class='current_page_item'>" : "<li>";
				$menu_html .= '<a href="'.get_bloginfo('url').'"><span>'.esc_html__('Home', 'Current').'</span></a></li>';
				$menu_html .= wp_list_pages('depth=5&title_li=0&sort_column=menu_order&link_before=<span>&link_after=</span>'.(isset($Current_options['excluded_pages_from_menu'])?'&exclude='.$Current_options['excluded_pages_from_menu']:'').'&echo=0');
				$menu_html .= '</ul>';
				$menu_html .= '</div>';
				echo $menu_html;
			}

function willrender_simple_menu($args = array()) {
				return true;
			}


function willrender_mocktail_slider($args = array()) {
				global $post, $Current_options, $isfrontpage;
				$args['id'] = isset($args['id'])?$args['id']:str_replace(' ','_',$args['name']);
				$slider="none";
				if(is_front_page() || $isfrontpage)
					$slider=$Current_options['mocktail_slider_frontpage'];
				elseif(is_archive() || is_search() || is_404())
					$slider=$Current_options['mocktail_slider_archive'];
				elseif($post)
					$slider = get_post_meta($post->ID, $args['id'].'_val', true);
				if(!$slider || $slider == "none")
					return false;
				else
					return true;
			}


function back_mocktail_slider($args = array()) {
		global $mocktail_slider_array;
		$args['id'] = isset($args['id'])?$args['id']:str_replace(' ','_',$args['name']);
		$mocktail_slider_array[$args['id']]=$args;
	}


function sanitize_mocktail_slider($options) { 

	global $Current_options;
	
	$sliders = explode("|", $options['mocktail_slider_list']);
	
	foreach($sliders as $slider) {
		if($slider != $options['mocktail_slider_current']) {
			$compare = "mocktail_".md5($slider);
			foreach($Current_options as $key => $val) {
				if($compare == substr($key, 0, strlen($compare)))
				$options[$key] = $val;
			}
		}
	}
		



 return $options;
			}


function willrender_simple_pagetitle($args = array()) {
				global $Current_options;
				if(isset($Current_options['simple_pagetitle_enable']) && $Current_options['simple_pagetitle_enable'] == 'yes' && (!is_front_page() || isset($args["frontpage"])))
					return true;
				else
					return false;
			}


function back_simple_pagetitle($args = array()) {
				global $simple_pagetitle_array;
				$args['id'] = strtolower(isset($args['id'])?$args['id']:str_replace(' ','_',$args['name']));
				$simple_pagetitle_array[$args['id']]=$args;
			}

 
			 if(isset($Current_options['simple_sidebar_list']))
			 	$temp_simple_sidebars = explode("|", $Current_options['simple_sidebar_list']);
			else
				$temp_simple_sidebars = array();
				
			array_push($temp_simple_sidebars, "Default sidebar");
			
			foreach($temp_simple_sidebars as $item) {
				$args = array();
				$args['name'] = $item;
				$args['id'] = "simple_sidebar_".str_replace(" ", "_", strtolower($item));
				$args['desc'] = __('Sidebar', 'Current')." ".$item;	
				$args['titlestyle'] = 'h3';	
				register_sidebar(array(
				'name' => esc_html__($args['name'], 'Current'),
				'id' => $args['id'],
				'description' => esc_html__($args['desc'], 'Current'),
				'before_widget' => '<div class="th_widget">',
				'after_widget' => '</div>',
				'before_title' => '<div class="widget_title"><'.$args['titlestyle'].'>',
				'after_title' => '</'.$args['titlestyle'].'></div>',
				));
				
			} 

function willrender_simple_sidebar($args = array()) {
				global $post, $Current_options;
				$args['id'] = isset($args['id'])?$args['id']:str_replace(' ','_',$args['name']);
				$sidebar="none";
				if(is_archive() || is_search() || is_404())
					$sidebar = $Current_options['simple_sidebar_'.$args['id'].'_archive'];
				elseif($post)
					$sidebar = get_post_meta($post->ID, $args['id'].'_val', true);
					
				if($sidebar == "none")
					return false;
				else
					return is_active_sidebar("simple_sidebar_".str_replace(" ", "_", strtolower($sidebar)));
			}


function back_simple_sidebar($args = array()) {
				global $simple_sidebar_array;
				$args['id'] = strtolower(isset($args['id'])?$args['id']:str_replace(' ','_',$args['name']));
				$simple_sidebar_array[$args['id']]=$args;
			}


function willrender_simple_404($args = array()) {
				return true;
			}


function willrender_simple_archive($args = array()) {
				return true;
			}


function willrender_simple_blog($args = array()) {
				return true;
			}


function back_simple_blog($args = array()) {
				global $simple_blog_array;
				$args['id'] = strtolower(isset($args['id'])?$args['id']:str_replace(' ','_',$args['name']));
				$simple_blog_array[$args['id']]=$args;
			}


function willrender_simple_contact($args = array()) {
				return true;
			}


function back_simple_contact($args = array()) {
				global $simple_contact_array;
				$args['id'] = isset($args['id'])?$args['id']:str_replace(' ','_',$args['name']);
				$simple_contact_array[$args['id']]=$args;
			}


function willrender_simple_page($args = array()) {
				return true;
			}


	function simple_portfolio_imagethumb($width='260', $height=false, $grey = false) {

		global $Current_options, $post;
		$rel_attr = ' rel="wp-prettyPhoto[portfolio]"';
		$output = '';


		if(has_post_thumbnail()) {
			$portfolio_item_preview=wp_get_attachment_url( get_post_thumbnail_id() );
		}
	  	
		$post_media = get_post_meta($post->ID, 'th_post_video', true);

	    if ( isset($portfolio_item_preview) && $portfolio_item_preview ) {

			if(isset($post_media) && $post_media!="")
				$preview_item = $post_media;
			else
	 			$preview_item = $portfolio_item_preview; 
			
			$leading =  $Current_options["line_height"]*$Current_options["font_size"];
			
			if($grey)
				$output = '<img src="'.get_bloginfo("template_directory").'/scripts/timthumb.php?src='.$portfolio_item_preview.'&amp;w='.$width.($height?"&amp;h=".$height:"").'&amp;zc=1&amp;q=90'.(!$height?"&amp;leading=".$leading:"").'" alt="'.get_the_title().'" style="width:100%;" />';
			
				$output .= '<img class="portfolioitemimg'.($grey?' greyhover':'').'" src="'.get_bloginfo("template_directory").'/scripts/timthumb.php?src='.$portfolio_item_preview.'&amp;w='.$width.($height?"&amp;h=".$height:"").'&amp;zc=1&amp;q=90'.(!$height?"&amp;leading=".$leading:"").($grey?'&amp;f=2':'').'" alt="'.get_the_title().'" style="width:100%;" />';
			
			
			$output = '<a class="thumblink'.((isset($post_media) && $post_media!="")?' movie':'').'" '.$rel_attr.' href="'.$preview_item.'" title="'.get_the_title().'">'.$output.'<span class="thumblinkoverlay"></span></a>';
			
	    }



	echo $output;

}


function willrender_simple_portfolio($args = array()) {
				return true;
			}


function back_simple_portfolio($args = array()) {
				global $simple_portfolio_array;
				$args['id'] = isset($args['id'])?$args['id']:str_replace(' ','_',$args['name']);
				$simple_portfolio_array[$args['id']]=$args;
			}


function willrender_simple_search($args = array()) {
				return true;
			}


function willrender_simple_single($args = array()) {
				return true;
			}


function willrender_simple_sitemap($args = array()) {
				return true;
			}


function back_simple_sitemap($args = array()) {
				global $simple_sitemap_array;
				$args['id'] = strtolower(isset($args['id'])?$args['id']:str_replace(' ','_',$args['name']));
				$simple_sitemap_array[$args['id']]=$args;
			}


function languages_list_footer(){
	$languages = icl_get_languages('skip_missing=0&orderby=code');
	if(!empty($languages)){
		echo '<div id="footer_language_list"><ul>';
		foreach($languages as $l){
			echo '<li>';
			if($l['country_flag_url']){
				if(!$l['active']) echo '<a href="'.$l['url'].'">';
				echo '<img src="'.$l['country_flag_url'].'" height="12" alt="'.$l['language_code'].'" width="18" />';
				if(!$l['active']) echo '</a>';
			}
			if(!$l['active']) echo '<a href="'.$l['url'].'">';
			echo icl_disp_language($l['native_name']);
			if(!$l['active']) echo '</a>';
			echo '</li>';
		}
		echo '</ul></div>';
	}
}

function th_load_options() {
		wp_enqueue_style('Current_admin_css', get_template_directory_uri() . '/css/admincss.css', false, '1.0', 'screen');
		wp_enqueue_style (  'wp-jquery-ui-dialog');

		wp_enqueue_style('thickbox');
		wp_enqueue_script('jquery-ui-resizable');
		wp_enqueue_script('thickbox');
		wp_enqueue_script('media-upload');
		
		wp_enqueue_script('common');
		wp_enqueue_script('wp-lists');
		wp_enqueue_script('postbox');
		
	
		global $th_options_hook;

add_meta_box('cpanel_general_settings', __('General settings', 'Current'), 'render_cpanel_general_settings', $th_options_hook, 'normal', 'core');

add_meta_box('cpanel_google_webfonts', __('Font settings', 'Current'), 'render_cpanel_google_webfonts', $th_options_hook, 'normal', 'core');

add_meta_box('cpanel_simple_background', __('Background settings', 'Current'), 'render_cpanel_simple_background', $th_options_hook, 'normal', 'core');

add_meta_box('cpanel_prettyphoto_lightbox', __('prettyPhoto Lightbox', 'Current'), 'render_cpanel_prettyphoto_lightbox', $th_options_hook, 'normal', 'core');

add_meta_box('cpanel_google_analytics', __('Site Analytics', 'Current'), 'render_cpanel_google_analytics', $th_options_hook, 'normal', 'core');

add_meta_box('cpanel_simple_footer', __('Footer management', 'Current'), 'render_cpanel_simple_footer', $th_options_hook, 'normal', 'core');

add_meta_box('cpanel_simple_logo', __('Logo management', 'Current'), 'render_cpanel_simple_logo', $th_options_hook, 'normal', 'core');

add_meta_box('cpanel_mocktail_slider', __('Sliders management', 'Current'), 'render_cpanel_mocktail_slider', $th_options_hook, 'normal', 'core');

add_meta_box('cpanel_simple_pagetitle', __('Page title management', 'Current'), 'render_cpanel_simple_pagetitle', $th_options_hook, 'normal', 'core');

add_meta_box('cpanel_simple_sidebar', __('Sidebars management', 'Current'), 'render_cpanel_simple_sidebar', $th_options_hook, 'normal', 'core');

add_meta_box('cpanel_simple_archive', __('General page and archive settings', 'Current'), 'render_cpanel_simple_archive', $th_options_hook, 'normal', 'core');
}


reg_simple_widget_location( array (
	array ('name' => 'Footer block 1',
		'type' => 'simple_widget_location',
		'function' => 'widget_location',
		'titlestyle' => 'h5',
		'width' => '6'),
	array ('name' => 'Footer block 2',
		'type' => 'simple_widget_location',
		'function' => 'widget_location',
		'titlestyle' => 'h5',
		'width' => '6'),
	array ('name' => 'Footer block 3',
		'type' => 'simple_widget_location',
		'function' => 'widget_location',
		'titlestyle' => 'h5',
		'width' => '6'),
	array ('name' => 'Footer block 4',
		'type' => 'simple_widget_location',
		'function' => 'widget_location',
		'titlestyle' => 'h5',
		'width' => '6'),
	array ('name' => 'Inset',
		'type' => 'simple_widget_location',
		'function' => 'widget_location',
		'width' => '5'),
	array ('name' => 'Front page block A1',
		'type' => 'simple_widget_location',
		'function' => 'widget_location',
		'width' => '6'),
	array ('name' => 'Front page block A2',
		'type' => 'simple_widget_location',
		'function' => 'widget_location',
		'width' => '6'),
	array ('name' => 'Front page block A3',
		'type' => 'simple_widget_location',
		'function' => 'widget_location',
		'width' => '6'),
	array ('name' => 'Front page block A4',
		'type' => 'simple_widget_location',
		'function' => 'widget_location',
		'width' => '6'),
	array ('name' => 'Front page block B1',
		'type' => 'simple_widget_location',
		'function' => 'widget_location',
		'width' => '6'),
	array ('name' => 'Front page block B2',
		'type' => 'simple_widget_location',
		'function' => 'widget_location',
		'width' => '6'),
	array ('name' => 'Front page block B3',
		'type' => 'simple_widget_location',
		'function' => 'widget_location',
		'width' => '6'),
	array ('name' => 'Front page block B4',
		'type' => 'simple_widget_location',
		'function' => 'widget_location',
		'width' => '6'),
	array ('name' => 'Front page block C1',
		'type' => 'simple_widget_location',
		'function' => 'widget_location',
		'width' => '6'),
	array ('name' => 'Front page block C2',
		'type' => 'simple_widget_location',
		'function' => 'widget_location',
		'width' => '6'),
	array ('name' => 'Front page block C3',
		'type' => 'simple_widget_location',
		'function' => 'widget_location',
		'width' => '6'),
	array ('name' => 'Front page block C4',
		'type' => 'simple_widget_location',
		'function' => 'widget_location',
		'width' => '6'),));
?>
