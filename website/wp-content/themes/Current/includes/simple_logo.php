
<?php
function front_simple_logo($args = array()) { ?>
			<div id="simple_logo_abs">
    <div id="simple_logo">
    <?php                   if( is_front_page() ) { ?>
                                <h1><a href="<?php echo home_url(); ?>"><?php bloginfo('name'); ?></a></h1>
    <?php                  } else { ?>
                                <div class="site-name"><a href="<?php echo home_url(); ?>"><?php bloginfo('name'); ?></a></div>
    <?php                   } ?>
    </div>
    <div id="slogan" class="simple_logo_slogan"><?php bloginfo('description'); ?></div>
</div>
			<?php }
?>
