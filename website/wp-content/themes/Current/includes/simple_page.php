
<?php
function front_simple_page($args = array()) { 

global $post, $global_blocks, $Current_options, $availcols;
$mcurrentpage = $post->ID;
$font_size = $Current_options['font_size'];
$line_height = $Current_options['line_height'];
$site_width = $Current_options['site_width'];
$gutter_width = $Current_options['gutter_width'];
$postmetainfodisplay = false;
 
 
		if (have_posts())
					while (have_posts())
						the_post();

				$availcols = calc_width($args['column'], $global_blocks[$args['row']]);
				$availwidth = $availcols*$site_width/24;

   
?>      
        
		    <div <?php post_class() ?> id="post-<?php the_ID(); ?>">
            
            
			<div class="currentitem">

           <?php 

			if(get_post_meta($post->ID, 'th_post_video', true)) {
				$thumbwidth = get_post_meta($post->ID, 'th_post_video_width', true);
				$thumbheight = get_post_meta($post->ID, 'th_post_video_height', true);
				
				if($thumbwidth>0 && $thumbheight>0)
					$videowrap = "class='postvideowrap' style='height:".$thumbheight."px; width:".$thumbwidth."px; position: relative;margin:auto;'";					
				else
					$videowrap = "class='postvideowrap grid_".$availcols." alpha omega' style='height: 0;padding-bottom: ".(100/get_post_meta($post->ID, 'th_post_video_aspect', true))."%;position: relative;'";
				
				echo "<div ".$videowrap.">";
				th_postvideo();
				echo "</div> <br class=\"clear\" />";
			}
			elseif(has_post_thumbnail()) {
				echo "<div class='thumb'>";				
				th_imagethumb(($availwidth-$gutter_width), false, true);
				echo "</div>";
			}
			


				the_content();
			wp_link_pages(array('before' => '<p><strong>'.__('Pages','Current').':</strong> ', 'after' => '</p>', 'next_or_number' => __('number','Current'))); 
 ?>
         

			</div>
		    </div>
            
<?php
	if(isset($Current_options['comments_pages']) && $Current_options['comments_pages']=="yes")
		comments_template();

 }
?>
