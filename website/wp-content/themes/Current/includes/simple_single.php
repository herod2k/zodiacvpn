
<?php
function front_simple_single($args = array()) { 

global $post, $global_blocks, $Current_options, $availcols;
$mcurrentpage = $post->ID;
$font_size = $Current_options['font_size'];
$line_height = $Current_options['line_height'];
$site_width = $Current_options['site_width'];
$gutter_width = $Current_options['gutter_width'];
$postmetainfodisplay = false;
 
 
		if (have_posts())
					while (have_posts())
						the_post();

				$availcols = calc_width($args['column'], $global_blocks[$args['row']]);
				$availwidth = $availcols*$site_width/24;

 $postmetainfodisplay = false;
 
if($Current_options['simple_archive_author'] == 'yes' || $Current_options['simple_archive_category'] == 'yes'  || (comments_open() && $Current_options['simple_archive_comments']=="yes") || $Current_options['simple_archive_tags'] == 'yes') {
	ob_start();
?>

                            <div class="postmetadatawrap">   

                                   <?php  $divid = false;                                 
                                    
									if($Current_options['simple_archive_author'] == 'yes') {
                                           echo "<div class='blogpostauthor".(!$divid?" alpha":"")."'><strong>".__('Author', 'Current').":</strong> ".get_the_author_link()."</div>";
											echo "<div class='blogpostdate'><strong>".__('Posted', 'Current').":</strong> ".get_the_date()."</div>";
											$divid=true;
									}
									
									if($Current_options['simple_archive_category'] == 'yes') {
                                     		echo"<div class='blogpostcategory".(!$divid?" alpha":"")."'>";
											the_category(', ');
											echo "</div>";
											$divid=true;
									}
                                
									if(isset($Current_options['comments_posts']) && $Current_options['comments_posts']=="yes" && comments_open() && $Current_options['simple_archive_comments']=="yes") {
											
											echo "<div class='blogpostcomments".(!$divid?" alpha":"")."'>";
											comments_popup_link( __( 'Leave a comment', 'Current' ), __( '1 Comment', 'Current' ), __( '% Comments', 'Current' ) );
											echo "</div>";
											$divid=true;
									}
																	
									if($Current_options['simple_archive_tags'] == 'yes'){
											echo "<div class='blogposttags".(!$divid?" alpha":"")."'>";
											the_tags(__('Tags: ', 'Current'), ', ', '');
											echo "</div>";
									}
									
									?>
                              </div>

<?php
$postmetainfodisplay=ob_get_contents();
  ob_end_clean();
}
   
?>      
        
		    <div <?php post_class() ?> id="post-<?php the_ID(); ?>">
            
            
			<div class="currentitem">

           <?php 

			if(get_post_meta($post->ID, 'th_post_video', true)) {
				$thumbwidth = get_post_meta($post->ID, 'th_post_video_width', true);
				$thumbheight = get_post_meta($post->ID, 'th_post_video_height', true);
				
				if($thumbwidth>0 && $thumbheight>0)
					$videowrap = "class='postvideowrap' style='height:".$thumbheight."px; width:".$thumbwidth."px; position: relative;margin:auto;'";					
				else
					$videowrap = "class='postvideowrap grid_".$availcols." alpha omega' style='height: 0;padding-bottom: ".(100/get_post_meta($post->ID, 'th_post_video_aspect', true))."%;position: relative;'";
				
				echo "<div ".$videowrap.">";
				th_postvideo();
				echo "</div> <br class=\"clear\" />";
			}
			elseif(has_post_thumbnail()) {
				echo "<div class='thumb'>";
				th_imagethumb(($availwidth-$gutter_width), false, true);
				echo "</div>";
			}
			?>
                  
             
			<div class="blogposttitle">
             <h3>&nbsp;</h3>
             </div>
			 <?php
			 if($postmetainfodisplay) {?>
				 <div class="blogpostmetainfo">
				 
			<?php	echo $postmetainfodisplay;	?>
            	</div>
			 <?php }
			
			
				if($postmetainfodisplay) echo "<div class=\"blogpostcontent\">";
			


				the_content();
		   			wp_link_pages(array('before' => '<p><strong>'.__('Pages','Current').':</strong> ', 'after' => '</p>', 'next_or_number' => __('number','Current'))); 
			if($postmetainfodisplay) echo "</div> <br class=\"clear\" />";

 ?>
         

			</div>
		    </div>
<?php
	if(isset($Current_options['comments_posts']) && $Current_options['comments_posts']=="yes")
		comments_template();
	
 }
?>
