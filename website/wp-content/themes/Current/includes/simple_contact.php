
<?php
function front_simple_contact($args = array()) { 

global $post, $global_blocks, $Current_options;
$mcurrentpage = $post->ID;
$font_size = $Current_options['font_size'];
$line_height = $Current_options['line_height'];
$site_width = $Current_options['site_width'];
$gutter_width = $Current_options['gutter_width'];



				$availcols = calc_width($args['column'], $global_blocks[$args['row']]);
				$availwidth = $availcols*$site_width/24;
				
				
if ( get_post_meta($mcurrentpage, $args['id'].'_recaptcha', true) == 'yes' ) {

    if ( !function_exists('_recaptcha_qsencode') ) {
	require_once(dirname(dirname(__FILE__)).'/scripts/recaptchalib.php');
    }

    $publickey = get_post_meta($mcurrentpage, $args['id'].'_recaptcha_pbkey', true); 
    $privatekey = get_post_meta($mcurrentpage, $args['id'].'_recaptcha_prkey', true);
    $response = null;
    $captcha_error = null;
    if( isset($_POST['submit']) ) {
	$response = recaptcha_check_answer ($privatekey,
		    $_SERVER["REMOTE_ADDR"],
		    $_POST["recaptcha_challenge_field"],
		    $_POST["recaptcha_response_field"]
		);
	if ( !$response->is_valid ) {
	    $captcha_error = $response->error;
	}
    }
}


if( isset($_POST['submit']) ) {

    $contact_name = strip_tags(trim(stripslashes($_POST['contact_name'])));
    $contact_email = trim($_POST['contact_email']);
    $contact_phone = trim($_POST["contact_phone"]);

    $contact_message = strip_tags(trim(stripslashes($_POST['contact_message'])));


    if( $contact_name  == '' && get_post_meta($mcurrentpage, $args['id'].'_name_req', true) == 'yes' ) { 
	$contact_nameError = __('This field is required', 'Current');
    } 

    if( $contact_email == '' ) {
	$contact_emailError = __('This field is required', 'Current');
    } else if( !is_email( $contact_email ) ) {
	$contact_emailError = __('Please enter a valid email address', 'Current');
    }

	if( $contact_phone  == '' && get_post_meta($mcurrentpage, $args['id'].'_phone_req', true) == 'yes' ) { 
	$contact_phoneError = __('Please enter a phone number', 'Current');
    } 

	if( $contact_phone != '' && get_post_meta($mcurrentpage, $args['id'].'_phone_us', true) == 'yes' ) {

    $pattern = '/^((([0-9]{1})*[- .(]*([0-9]{3})[- .)]*[0-9]{3}[- .]*[0-9]{4})+)*$/';
    if(!preg_match( $pattern, $contact_phone ))
		$contact_phoneError = __('Please specify a valid phone number', 'Current');
	
	}


    if( $contact_message == '' ) {
		$contact_messageError = __('This field is required', 'Current');
    }

    if( !isset($contact_nameError) && !isset($contact_emailError) && !isset($contact_messageError) && !isset($captcha_error) ) {

	$contact_subject = sprintf(__('Message from %s', 'Current'), get_option('blogname') );
	$contact_contents = __("Sender's name: ", 'Current') . $contact_name . "\r\n" .
			    __('E-mail: ', 'Current') . $contact_email . "\r\n" .
			    $contact_phone ."\r\n" .
			    __('Message: ', 'Current') . $contact_message . " \r\n";

	$contact_header = "From: $contact_name <".$contact_email.">\r\n";
	$contact_header .= "Reply-To: $contact_email\r\n";
	$contact_header .= "Return-Path: $contact_email\r\n";

	$contact_success = ( @wp_mail( get_post_meta($mcurrentpage, $args['id'].'_receivers', true), $contact_subject, $contact_contents, $contact_header ) ) ? true : false;

	$contact_name = $contact_email = $contact_phone = $contact_message = '';
    }
}


 
if (have_posts()) : while (have_posts()) : the_post();


				$availcols = calc_width($args['column'], $global_blocks[$args['row']]);
				$availwidth = $availcols*$site_width/24;


if(get_post_meta($post->ID, 'th_post_video', true)) {
				$thumbwidth = get_post_meta($post->ID, 'th_post_video_width', true);
				$thumbheight = get_post_meta($post->ID, 'th_post_video_height', true);
				
				if($thumbwidth>0 && $thumbheight>0)
					$videowrap = "class='postvideowrap' style='height:".$thumbheight."px; width:".$thumbwidth."px; position: relative;margin:auto;'";					
				else
					$videowrap = "class='postvideowrap grid_".$availcols." alpha omega' style='height: 0;padding-bottom: ".(100/get_post_meta($post->ID, 'th_post_video_aspect', true))."%;position: relative;'";
				
				echo "<div class='thumb'><div ".$videowrap.">";
				th_postvideo();
				echo "</div> <br class=\"clear\" /></div>";
			}
			elseif(has_post_thumbnail()) {
				echo "<div class='thumb'>";
				th_imagethumb(($availwidth-$gutter_width), false, true);
				echo "</div>";
			}


		    the_content();
						wp_link_pages(array('before' => '<p><strong>'.__('Pages','Current').':</strong> ', 'after' => '</p>', 'next_or_number' => __('number','Current'))); 
		endwhile; endif; 				
	

 if(get_post_meta($post->ID, $args['id'].'_google_map', true)!='') { ?>
		<div id="map" style='height: 0;padding-bottom:<?php echo (100/get_post_meta($post->ID, $args['id'].'_google_map_aspect', true))."%";?>;position: relative;'><?php esc_html_e('Map Loading...', 'Current'); ?></div>
             <script type="text/javascript">
		    //<![CDATA[
		    jQuery(document).ready( function($) {
				var map = $('#map');
			
					if( map.length ) {
			
						map.gMap({
							address: '<?php echo get_post_meta($post->ID, $args['id'].'_google_map', true); ?>',
							zoom: <?php echo get_post_meta($post->ID, $args['id'].'_google_map_zoom', true); ?>,
							markers: [
								{ 'address' : '<?php echo get_post_meta($post->ID, $args['id'].'_google_map', true); ?>' }
							]
						});
			
					}
			});
		    //]]>
		</script>
        
	<?php } 

	$nextclass=false;
	if(get_post_meta($post->ID, $args['id'].'_address', true)!='') { ?>
     <div class="grid_6 alpha">
        
            <?php echo get_post_meta($post->ID, $args['id'].'_address', true);?>
            
        </div>
    
    <?php
		$nextclass='class="grid_'.($availcols-6).' omega"';
	} ?> 
    
    <div <?php echo $nextclass; ?>>
    
    <?php
	if( isset($contact_nameError) || isset($contact_emailError) || isset($contact_messageError) || isset($captcha_error) ) {
		
		echo do_shortcode('[msgbox type="error"]'.__('There is an error in the contact submission. Please correct it and submit again.', 'Current').(isset($captcha_error)?"<br />".__('The image verification string is incorrect. Please try again.', 'Current'):'').'[/msgbox]');

	}
	elseif(isset($contact_success) && $contact_success){
		echo do_shortcode('[msgbox type="success"]'.__('Thanks. We have received your message.', 'Current').'[/msgbox]');
	}
	elseif(isset($_POST['submit'])) {
		echo do_shortcode('[msgbox type="warning"]'.__('The message could not be sent.', 'Current').'[/msgbox]');
	}
	
 $formitems = array(); 
 if(get_post_meta($mcurrentpage, $args['id'].'_name', true)=="yes") { 
		ob_start();
		?>
        <div class="form_row textbox left">
		    <label for="contact_name"><?php esc_html_e('Name', 'Current'); 
 if(get_post_meta($mcurrentpage, $args['id'].'_name_req', true)=="yes") {?> <em> (<?php esc_html_e('Required', 'Current'); ?></em>)<?php }?>:</label><br />
		    <input id="contact_name" name="contact_name" value="<?php echo isset($contact_name)?esc_attr($contact_name):""; ?>" class="inputbox <?php if(get_post_meta($mcurrentpage, $args['id'].'_name_req', true)=="yes") echo "required";
 if(isset($contact_nameError)) echo "formerror";?>" />
        <?php if(isset($contact_nameError)) { ?>
        	<label for="contact_name" generated="true" class="formerror"><?php echo $contact_nameError;?></label>
        <?php } ?>
		</div>
        <?php 
			$formitems[]=ob_get_clean();
		} 
 if(get_post_meta($mcurrentpage, $args['id'].'_phone', true)=="yes") { 
			ob_start();
		?>
        <div class="form_row textbox left">		    
		    <label for="contact_phone"><?php esc_html_e('Phone', 'Current'); 
 if(get_post_meta($mcurrentpage, $args['id'].'_phone_req', true)=="yes") {?> <em> (<?php esc_html_e('Required', 'Current'); ?></em>)<?php }?>:</label><br />
		    <input id="contact_phone" name="contact_phone" value="<?php echo isset($contact_phone)?esc_attr($contact_phone):""; ?>" class="inputbox <?php if(get_post_meta($mcurrentpage, $args['id'].'_phone_req', true)=="yes") echo "required";
 if(get_post_meta($mcurrentpage, $args['id'].'_phone_us', true)=="yes") echo "phoneUS";
 if(isset($contact_phoneError)) echo "formerror";?>" />
        <?php if(isset($contact_phoneError)) { ?>
        	<label for="contact_phone" generated="true" class="formerror"><?php echo $contact_phoneError;?></label>
        <?php } ?>            
		</div>
		<?php 
		$formitems[]=ob_get_clean();
		}
		ob_start();
		?>
        <div class="form_row textbox">
           <label for="contact_email"><?php esc_html_e('E-Mail', 'Current'); ?> <em> (<?php esc_html_e('Required', 'Current'); ?></em>):</label><br />
		    <input id="contact_email" name="contact_email" value="<?php echo isset($contact_email)?esc_attr($contact_email):""; ?>" class="inputbox required email <?php if(isset($contact_emailError)) echo "formerror";?>" />
        <?php if(isset($contact_emailError)) { ?>
        	<label for="contact_email" generated="true" class="formerror"><?php echo $contact_emailError;?></label>
        <?php } ?>            
		</div>
        <?php 
		$formitems[]=ob_get_clean();
		?>
        
        <form id="simple_contact_form"  method="post" action="<?php echo the_permalink(); ?>">
        <fieldset>
        <?php

			$formitemcount=0;
		foreach($formitems as $formitem) {
			if($formitemcount==0)
				$formitem = str_replace("class=\"form_row", "class=\"form_row alpha", $formitem);
			if($formitemcount==(sizeof($formitems)-1))	
				$formitem = str_replace("class=\"form_row", "class=\"form_row omega", $formitem);
				
			echo $formitem = str_replace("class=\"form_row", "class=\"form_row grid_".($availcols-6)/sizeof($formitems), $formitem);;
			$formitemcount+=1;
			
		}
		?>
        <br class="clear" />
        <div class="form_row textarea">            
            
		    <label for="contact_message"><?php esc_html_e('Message', 'Current'); ?> <em> (<?php esc_html_e('Required', 'Current'); ?></em>):</label><br />
		    <textarea id="contact_message" name="contact_message" rows="6" class="inputtextarea required  <?php if(isset($contact_messageError)) echo "formerror";?>" style="width:100%"><?php echo isset($contact_message)?esc_attr($contact_message):""; ?></textarea>
        <?php if(isset($contact_messageError)) { ?>
        	<label for="contact_message" generated="true" class="formerror"><?php echo $contact_messageError;?></label>
        <?php } ?>            
		</div>

                   
            
		   
<?php		if ( get_post_meta($mcurrentpage, $args['id'].'_recaptcha', true) == 'yes' ) : ?>
		    <script type="text/javascript">var RecaptchaOptions = {theme : '<?php echo get_post_meta($mcurrentpage, $args['id'].'_recaptcha_theme', true); ?>', lang : '<?php echo get_post_meta($mcurrentpage, $args['id'].'_recaptcha_lang', true); ?>'};</script>
		    <div class="form_row recaptcha"> 
<?php			echo recaptcha_get_html( $publickey); ?>
		    </div>
        
<?php		endif; ?>

		

        <div class="form_row">
		    <input class="inputbutton" name="submit" class="submit" type="submit" value="<?php esc_attr_e('Submit', 'Current'); ?>"/>
		</div>
        </fieldset>
	    </form>
	
	</div>
			<?php }
?>
