
<?php
function front_simple_search($args = array()) { 
 
global $post, $global_blocks, $Current_options;

$font_size = $Current_options['font_size'];
$line_height = $Current_options['line_height'];
$site_width = $Current_options['site_width'];
$gutter_width = $Current_options['gutter_width'];



$availcols = calc_width($args['column'], $global_blocks[$args['row']]);
$availwidth = $availcols*$site_width/24;

include(dirname(dirname(__FILE__)).'/scripts/ylsy_search_excerpt.php');
query_posts($query_string . '&showposts=10');
	
		if (have_posts()) : ?>
        
        <div class="blogitemcontainer">
<?php		 while (have_posts()) : the_post(); ?>

			<div <?php post_class() ?>>
<?php
				$title = get_the_title();
				$search_term = preg_replace('/\/|\+|\*|\[|\]/iu','',$s);
				$keys= explode(" ",$search_term);
				$title = preg_replace( '/('.implode('|', $keys) .')/iu', '<strong class="search-excerpt">\0</strong>', $title );
?>
				<h3 id="post-<?php the_ID(); ?>"><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php echo $title; ?></a></h3>
				
				<?php
				    $excerpt = new SearchExcerpt();
				    echo $excerpt->the_excerpt( get_the_excerpt() );
				?>
			</div>

		<?php 
		
		echo do_shortcode("[divider]");
		
		endwhile; ?>

            <br class="clear" />
			</div>
<?php			// Pagination
			if(function_exists('wp_pagenavi')) :
			    wp_pagenavi();
			else : ?>
		    <div class="navigation">
			    <div class="custombuttonwrap m alignleft"><?php previous_posts_link("<span>".__("Previous", 'Current')."</span>", 0) ?></div>
				    <div class="custombuttonwrap m alignright"><?php next_posts_link("<span>".__("Next", 'Current')."</span>", 0) ?></div>
                                <br class="clear" />
		    </div>
            
<?php			endif; 

	    else : ?>
		    <p></p><div class="warning">
				<div class="msgbox-icon"><?php _e("No results found. Refine your search!", 'Current'); ?></div>
</div>
            
<?php
	    endif; 
		wp_reset_query();

 }
?>
