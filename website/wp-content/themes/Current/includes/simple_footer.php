
<?php
function front_simple_footer($args = array()) { 

	global $Current_options;
		$rowstuff='<div class="simple_footer_text"><div class="textwidget">';
		
				if( isset($Current_options['simple_footer_cp']) && $Current_options['simple_footer_cp'] == 'yes' ) {
						$rowstuff.= $Current_options['simple_footer_message'];
				}
			    if( isset($Current_options['simple_footer_wp']) && $Current_options['simple_footer_wp'] == 'yes' ) {
					$rowstuff.=__(' is powered by <a href="http://wordpress.org/"><strong>WordPress</strong></a>', 'Current');
				}
				
				if( isset($Current_options['simple_footer_tc']) && $Current_options['simple_footer_tc'] == 'yes' ) {
					$rowstuff.=__(' Current by <a href="http://www.themeshigh.com/">themeshigh<strong></strong></a>', 'Current');
				}
				
			    if( isset($Current_options['simple_footer_entries']) && $Current_options['simple_footer_entries'] == 'yes' ) {
					$rowstuff.= ' | <a href="'.get_bloginfo('rss2_url').'">'.esc_html__('Entries (RSS)', 'Current').'</a>';
				}
			    if( isset($Current_options['simple_footer_comments']) && $Current_options['simple_footer_comments'] == 'yes' ){
					$rowstuff.= ' | <a href="'.get_bloginfo('comments_rss2_url').'">'.esc_html__('Comments (RSS)', 'Current').'</a>';
				}
				if( isset($Current_options['simple_footer_top']) && $Current_options['simple_footer_top'] == 'yes' ) {
					$rowstuff.= '<div class="simple_footer_top">';
					$rowstuff.= '<a href="#top">'.esc_html__('Back to Top', 'Current').'</a></div>';
				
				} 
				$rowstuff.= '</div></div>';
				
			echo $rowstuff;

 }
?>
