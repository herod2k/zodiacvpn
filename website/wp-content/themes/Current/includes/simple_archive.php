
<?php
function front_simple_archive($args = array()) { 
 
global $post, $global_blocks, $Current_options;

$font_size = $Current_options['font_size'];
$line_height = $Current_options['line_height'];
$site_width = $Current_options['site_width'];
$gutter_width = $Current_options['gutter_width'];



$availcols = calc_width($args['column'], $global_blocks[$args['row']]);
$availwidth = $availcols*$site_width/24;

	
		if (have_posts()) : ?>
        
        <div class="blogitemcontainer">
<?php	
		global $more;
		while (have_posts()) : the_post(); 
 
 $postmetainfodisplay = false;
 
if($Current_options['simple_archive_author'] == 'yes' || $Current_options['simple_archive_category'] == 'yes'  || (isset($Current_options['comments_posts']) && $Current_options['comments_posts']=="yes" && comments_open() && $Current_options['simple_archive_comments']=="yes") || $Current_options['simple_archive_tags'] == 'yes') {
	ob_start();
?>

                             <div class="postmetadatawrap">   

                                   <?php  $divid = false;                                 
                                    
									if($Current_options['simple_archive_author'] == 'yes') {
                                           echo "<div class='blogpostauthor".(!$divid?" alpha":"")."'><strong>".__('Author', 'Current').":</strong> ".get_the_author_link()."</div>";
											echo "<div class='blogpostdate'><strong>".__('Posted', 'Current').":</strong> ".get_the_date()."</div>";
											$divid=true;
									}
									
									if($Current_options['simple_archive_category'] == 'yes') {
                                     		echo"<div class='blogpostcategory".(!$divid?" alpha":"")."'>";
											the_category(', ');
											echo "</div>";
											$divid=true;
									}
                                
									if(isset($Current_options['comments_posts']) && $Current_options['comments_posts']=="yes" && comments_open() && $Current_options['simple_archive_comments']=="yes") {
											
											echo "<div class='blogpostcomments".(!$divid?" alpha":"")."'>";
											comments_popup_link( __( 'Leave a comment', 'Current' ), __( '1 Comment', 'Current' ), __( '% Comments', 'Current' ) );
											echo "</div>";
											$divid=true;
									}
																	
									if($Current_options['simple_archive_tags'] == 'yes'){
											echo "<div class='blogposttags".(!$divid?" alpha":"")."'>";
											the_tags(__('Tags: ', 'Current'), ', ', '');
											echo "</div>";
									}
									
									?>
                              </div>

<?php
$postmetainfodisplay=ob_get_contents();
  ob_end_clean();
}
   
?>      
        
		    <div <?php post_class() ?> id="post-<?php the_ID(); ?>">
            
            
			<div class="currentitem">

           <?php 

			if(get_post_meta($post->ID, 'th_post_video', true)) {
				$thumbwidth = get_post_meta($post->ID, 'th_post_video_width', true);
				$thumbheight = get_post_meta($post->ID, 'th_post_video_height', true);
				
				if($thumbwidth>0 && $thumbheight>0)
					$videowrap = "class='postvideowrap' style='height:".$thumbheight."px; width:".$thumbwidth."px; position: relative;margin:auto;'";					
				else
					$videowrap = "class='postvideowrap grid_".$availcols." alpha omega' style='height: 0;padding-bottom: ".(100/get_post_meta($post->ID, 'th_post_video_aspect', true))."%;position: relative;'";
				
				echo "<div ".$videowrap.">";
				th_postvideo();
				echo "</div> <br class=\"clear\" />";
			}
			elseif(has_post_thumbnail())
				th_imagethumb(($availwidth-$gutter_width), (int)(($availwidth-$gutter_width)/($Current_options['simple_archive_aspect']?$Current_options['simple_archive_aspect']:$Current_options['image_aspect'])));
			
 
$readmorebt=false;
if ($Current_options['simple_archive_readmore'] && $Current_options['simple_archive_readmore']!="") {
	$readmorebt=do_shortcode('[button class="blogreadmore" text="'.$Current_options['simple_archive_readmore'].'" title="'.get_the_title().'" url="'.get_permalink().'"]')."<br  class=\"clear\" />";
}
?>                   
             
			<div class="blogposttitle">
             <a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><h3><?php the_title(); ?></h3></a>
             </div>
			 <?php
			 if($postmetainfodisplay) {?>
				 <div class="blogpostmetainfo">
				 
			<?php	echo $postmetainfodisplay;	?>
            	</div>
			 <?php }
			
			
				if($postmetainfodisplay) echo "<div class=\"blogpostcontent\">";
			

			if($Current_options['simple_archive_excerpt']=="yes") {
		   		echo "<p>";
				the_excerpt();
				echo "</p>";
			}
			else
				$more = 0;	the_content('');
		   echo $readmorebt?$readmorebt:"";
			if($postmetainfodisplay) echo "</div> <br class=\"clear\" />";

 ?>
         

			</div>
		    </div>
			
<?php		
			echo do_shortcode("[divider backtotop='1']");
			endwhile; ?>

            <br class="clear" />
			</div>
<?php			// Pagination
			if(function_exists('wp_pagenavi')) :
			    wp_pagenavi();
			else : ?>
		    <div class="navigation">
			    <div class="custombuttonwrap m alignleft"><?php previous_posts_link("<span>".__("Previous", 'Current')."</span>", 0) ?></div>
				    <div class="custombuttonwrap m alignright"><?php next_posts_link("<span>".__("Next", 'Current')."</span>", 0) ?></div>
                                <br class="clear" />
		    </div>
            
<?php			endif; 

	    else : ?>
		    <p></p><div class="warning">
				<div class="msgbox-icon"><?php _e("There are no posts to be displayed", 'Current'); ?></div>
            </div>
            
<?php
	    endif; 
		
		wp_reset_query();

 }
?>
