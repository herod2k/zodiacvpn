
<?php
function front_simple_404($args = array()) { 
 
global $post, $global_blocks, $Current_options;

$font_size = $Current_options['font_size'];
$line_height = $Current_options['line_height'];
$site_width = $Current_options['site_width'];
$gutter_width = $Current_options['gutter_width'];



$availcols = calc_width($args['column'], $global_blocks[$args['row']]);
$availwidth = $availcols*$site_width/24;
?>
<p></p>
<div class="warning" style="margin:auto;"><div class="msgbox-icon"><?php esc_html_e('The page that you are looking for cannot be found. (Error 404)', 'Current'); ?></div></div>
    <p></p>
            <div style="float:left">
            	<?php esc_html_e('You can try to search for it', 'Current'); 
 get_search_form(); ?></div>
<br class="clear" />
			<?php }
?>
