
<?php
function front_simple_sitemap($args = array()) { 

global $post, $global_blocks, $Current_options;
$mcurrentpage = $post->ID;
$font_size = $Current_options['font_size'];
$line_height = $Current_options['line_height'];
$site_width = $Current_options['site_width'];
$gutter_width = $Current_options['gutter_width'];
$postmetainfodisplay = false;
 
 				$availcols = calc_width($args['column'], $global_blocks[$args['row']]);
				$availwidth = $availcols*$site_width/24;


if (have_posts()) : while (have_posts()) : the_post();
if(get_post_meta($post->ID, 'th_post_video', true)) {
				$thumbwidth = get_post_meta($post->ID, 'th_post_video_width', true);
				$thumbheight = get_post_meta($post->ID, 'th_post_video_height', true);
				
				if($thumbwidth>0 && $thumbheight>0)
					$videowrap = "class='postvideowrap' style='height:".$thumbheight."px; width:".$thumbwidth."px; position: relative;margin:auto;'";					
				else
					$videowrap = "class='postvideowrap grid_".$availcols." alpha omega' style='height: 0;padding-bottom: ".(100/get_post_meta($post->ID, 'th_post_video_aspect', true))."%;position: relative;'";
				
				echo "<div class='thumb'><div ".$videowrap.">";
				th_postvideo();
				echo "</div> <br class=\"clear\" /></div>";
			}
			elseif(has_post_thumbnail()) {
				echo "<div class='thumb'>";
				th_imagethumb(($availwidth-$gutter_width), false, true);
				echo "</div>";
			}
		    the_content();
						wp_link_pages(array('before' => '<p><strong>'.__('Pages','Current').':</strong> ', 'after' => '</p>', 'next_or_number' => __('number','Current'))); 
		endwhile; endif; 
		$thispost = $post->ID;
		?>
		
		<div class="grid_<?php echo $availcols/2;?> alpha">
  
			

			<h3><?php esc_html_e('Pages', 'Current'); ?></h3>
			<ul class="list_style1">
				<li><a href="<?php echo home_url(); ?>">Home</a></li>
				<?php wp_list_pages('title_li=&exclude='.str_replace("|", ",", get_post_meta($post->ID, $args['id'].'_pages', true))); ?>
			</ul>
   
   
 <?php $cats_array = get_categories();
 	$excludecats=array();

 ?>
            
            <h3><?php esc_html_e('Categories', 'Current'); ?></h3>
            <ul class="list_style1">
                <?php wp_list_categories('title_li=&exclude='.str_replace("|", ",", get_post_meta($post->ID, $args['id'].'_cats', true))); ?>
            </ul>
			
<?php if(get_post_meta($post->ID, $args['id'].'_show_monthly_archives', true) =="yes") { ?>
			<h3><?php esc_html_e('Monthly Archives', 'Current'); ?></h3>
			<ul class="list_style1">
				<?php wp_get_archives('type=monthly'); ?>
			</ul>
 <?php } 
 if(get_post_meta($post->ID, $args['id'].'_show_site_feeds', true) =="yes") { ?>         
            <h3><?php esc_html_e('Site Feeds', 'Current'); ?></h3>
			<ul class="list_style1">
				<li><a href="<?php bloginfo('rss2_url'); ?>">Main RSS Feed</a></li>
				<li><a href="<?php bloginfo('comments_rss2_url'); ?>">Comments RSS Feed</a></li>
			</ul>
 <?php } 
			if (get_post_meta($post->ID, $args['id'].'_show_tags_sitemap', true)=="yes" && function_exists('wp_tag_cloud') ) : ?>
			    <h3>Tags</h3>
<?php			    echo preg_replace('/class=\"(.*?)\"|class=\'(.*?)\'/', 'class="list_style1"', wp_tag_cloud('smallest=9&largest=9&format=list&echo=0'));
			endif; ?>
		</div>



		<div class="grid_<?php echo $availcols/2;?> omega">
			<h3><?php esc_html_e('All Articles', 'Current'); ?></h3>
			<ul class="list_style1">
<?php			    query_posts('posts_per_page=-1&cat=-'.str_replace("|", ",-", get_post_meta($thispost, $args['id'].'_cats', true)));
			    if (have_posts()) : while (have_posts()) : the_post(); ?>
				
                <li><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title(); ?>"><?php the_title(); ?></a>
                <?php if(get_post_meta($thispost, $args['id'].'_show_date_sitemap', true)=="yes") {?>
                <br /><?php the_time('j-M-Y'); }
				if(get_post_meta($thispost, $args['id'].'_show_author_sitemap', true)=="yes") {
					if(get_post_meta($thispost, $args['id'].'_show_date_sitemap', true)=="yes") echo "-";
					the_author_posts_link();
				}
				if(get_post_meta($thispost, $args['id'].'_show_comments_sitemap', true)=="yes") {
					if(get_post_meta($thispost, $args['id'].'_show_date_sitemap', true)=="yes" || get_post_meta($thispost, $args['id'].'_show_author_sitemap', true)=="yes") echo "-";
					comments_popup_link('0 comments', '1 comment', '% comments'); 
				}
				?></li>
                
                
<?php			    endwhile; endif; 
wp_reset_query();
?>
             </ul>
                      
		</div>
        <br class="clear" />
			<?php }
?>
