
<?php
function front_simple_pagetitle($args = array()) { 

	global $Current_options, $post;
	if(is_front_page()){
		if(isset($Current_options['simple_pagetitle_home_message']) && $Current_options['simple_pagetitle_home_message']!='') 		{
			echo "<div><div class='textwidget'>".do_shortcode($Current_options['simple_pagetitle_home_message'])."</div></div>";	
			
		} else {
	?>
    
    <h1 class="pagetitle"><?php bloginfo('title'); ?></h1>
		
	<?php	
		}
	}
	else {
	if(isset($posts[0]))
						$post = $posts[0]; 
                	if (is_page()) { ?>
                    	<h1 class="pagetitle"><?php the_title(); ?></h1>
    <?php			} 
					elseif ( is_single() ) { ?>
                    <h1 class="pagetitle"><?php the_title(); ?></h1>
                    <?php	if ( get_post_type( $post ) == 'post' ) { 
                        	$categories_names_array = array();
							
							foreach((get_the_category()) as $category) {
								array_push( $categories_names_array, $category->cat_name );
							} 
			    } 
			} 
					
					elseif (is_tax()) { /* If this is a taxonomy archive */
                    	$term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );  ?>
                    	<h1 class="pagetitle"><?php echo ucwords( $term->taxonomy ) . ': ' . $term->name; ?></h1>
    <?php			} 
	
					elseif (is_category()) { /* If this is a category archive */ 
                    if (isset($Current_options['remove_archivefor']) && $Current_options['remove_archivefor'] == 'yes') { ?>
								<h1 class="pagetitle"><?php single_cat_title("", true); ?></h1>
<?php                       } else { ?>
								<h1 class="pagetitle"><?php printf( __('Archive for the &#8216;%s&#8217; Category', 'Current' ), single_cat_title("", false) ); ?></h1>
<?php                       } 
			} 
	
					elseif (is_search()) { /* If this is a search results page */ ?>
                    	<h1 class="pagetitle"><?php printf( __('Search Results for &#8216;<em>%s</em>&#8216;', 'Current' ), get_search_query() ); ?></h1>
    <?php			} 
	
					elseif (is_404()) { /* If this is a 404 page */ ?>
                   		<h1 class="pagetitle"><?php esc_html_e('Page Not Found (Error 404)', 'Current'); ?></h1>
    <?php			} 
				
					elseif( is_tag() ) { /* If this is a tag archive */ ?>
                    	<h1 class="pagetitle"><?php printf( __('Posts Tagged &#8216;%s&#8217;', 'Current' ), single_tag_title("", false) ); ?></h1>
    <?php			} 
					
					elseif (is_day()) { /* If this is a daily archive */ ?>
                    	<h1 class="pagetitle"><?php printf( __('Archive for %s', 'Current' ), get_the_date() ); ?></h1>
    <?php			} 
	
					elseif (is_month()) { /* If this is a monthly archive */ ?>
                    	<h1 class="pagetitle"><?php printf( __('Archive for %s', 'Current' ), get_the_time('F, Y') ); ?></h1>
    <?php			} 
	
					elseif (is_year()) { /* If this is a yearly archive */ ?>
                   		<h1 class="pagetitle"><?php printf( __('Archive for %s', 'Current' ), get_the_time('Y') ); ?></h1>
    <?php			} 
	
					elseif (is_author()) { /* If this is an author archive */ ?>
                    	<h1 class="pagetitle"><?php esc_html_e('Author Archive', 'Current'); ?></h1>
    <?php			} 
	
					elseif (isset($_GET['paged']) && !empty($_GET['paged'])) { /* If this is a paged archive */ ?>
                    	<h1 class="pagetitle"><?php esc_html_e('Blog Archives', 'Current'); ?></h1>
    <?php			} 
	
					else { // the case when a Title is NOT present the height should be maintained ?>
                    	<div class="no-title-present"></div>
    <?php			} 
	}
	
 }
?>
