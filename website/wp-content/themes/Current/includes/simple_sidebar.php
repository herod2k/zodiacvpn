
<?php
function front_simple_sidebar($args = array()) {
				global $post, $Current_options;
				$args['id'] = isset($args['id'])?$args['id']:str_replace(' ','_',$args['name']);
				
				if(is_archive() || is_search() || is_404())
					$thesidebar = $Current_options['simple_sidebar_'.$args['id'].'_archive'];
				else
					$thesidebar = get_post_meta($post->ID, $args['id'].'_val', true);
					
				echo th_sidebar("simple_sidebar_".str_replace(" ", "_", strtolower($thesidebar)));
			}
?>
