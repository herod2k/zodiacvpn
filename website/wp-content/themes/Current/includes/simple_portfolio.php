
<?php
function front_simple_portfolio($args = array()) { 
 
global $post, $global_blocks, $Current_options;
$mcurrentpage = $post->ID;
$currentpfid = get_post_meta($mcurrentpage, $args['id'].'_cats', true);
$portfolio_items_per_page = (get_post_meta($mcurrentpage, $args['id'].'_items_per_page', true)>0)?get_post_meta($mcurrentpage, $args['id'].'_items_per_page', true):6;
$currentcategoryid = isset($_GET['cat'])?mysql_real_escape_string($_GET['cat']):false;
$font_size = $Current_options['font_size'];
$line_height = $Current_options['line_height'];
$site_width = $Current_options['site_width'];
$gutter_width = $Current_options['gutter_width'];



$colsperpage=((get_post_meta($mcurrentpage, $args['id'].'_cols_per_row', true)==1 && ((get_post_meta($mcurrentpage, $args['id'].'_cols_per_row', true)>0 && !isset($args['title_top']) && (get_post_meta($mcurrentpage, $args['id'].'_desc_source', true)>0 && !isset($args['description_hover']))) || (get_post_meta($mcurrentpage, $args['id'].'_desc_source', true)>0 && !isset($args['description_hover']))))?2:get_post_meta($mcurrentpage, $args['id'].'_cols_per_row', true));
																																																																													   
																																																																													   				$availwidth = calc_width($args['column'], $global_blocks[$args['row']])*$site_width/24;
				$grid = calc_width($args['column'], $global_blocks[$args['row']])/$colsperpage;		

if (have_posts()) : while (have_posts()) : the_post();

			if(get_post_meta($post->ID, 'th_post_video', true)) {
				$thumbwidth = get_post_meta($post->ID, 'th_post_video_width', true);
				$thumbheight = get_post_meta($post->ID, 'th_post_video_height', true);
				
				if($thumbwidth>0 && $thumbheight>0)
					$videowrap = "class='postvideowrap' style='height:".$thumbheight."px; width:".$thumbwidth."px; position: relative;margin:auto;'";					
				else
					$videowrap = "class='postvideowrap grid_".$availcols." alpha omega' style='height: 0;padding-bottom: ".(100/get_post_meta($post->ID, 'th_post_video_aspect', true))."%;position: relative;'";
				
				echo "<div class='thumb'><div ".$videowrap.">";
				th_postvideo();
				echo "</div> <br class=\"clear\" /></div>";
			}
			elseif(has_post_thumbnail()) {
				echo "<div class='thumb'>";
				th_imagethumb(($availwidth-$gutter_width), false, true);
				echo "</div>";
			}

		    the_content();
		endwhile; endif; 

if(get_post_meta($mcurrentpage, $args['id'].'_sortable', true)=='yes' && strpos($currentpfid, "|")) { ?>
<div id="portfoliosort">
<?php				$sortcats = array();
					$sortcats[]=do_shortcode("[button size='s' url='".get_permalink($mcurrentpage)."' text='".__("All")."' align='left']");
					foreach(explode("|", $currentpfid) as $categoryitem) { 
							$sortcats[]=do_shortcode("[button size='s' url='".add_query_arg(array('cat' => $categoryitem), get_permalink($mcurrentpage))."' text='".get_cat_name($categoryitem)."' align='left']");

 					} 
					echo implode("  ", $sortcats);
					
					?>
</div>
<br class="clear" />                              

<?php }

	    if( $currentpfid ) :

		if ( !$currentcategoryid )
				$currentcategoryid = $currentpfid;
		
		
	$counter = 0;
	
	if ( get_query_var('paged') ) $paged = get_query_var('paged');
	
	elseif ( get_query_var('page') ) $paged = get_query_var('page');
	
	else  $paged = 1;


	query_posts( array('cat' => str_replace("|", ",", $currentcategoryid), 'posts_per_page' => $portfolio_items_per_page, 'paged' => $paged));
		if (have_posts()) : ?>
        
        <div class="portfolioitemcontainer">
		<?php  
				global $more;
				while (have_posts()) : the_post();
				$title="";


								
				
				if(get_post_meta($mcurrentpage, $args['id'].'_title', true)>0){
					$title="<div class='portfolio_item_title'><".get_post_meta($mcurrentpage, $args['id'].'_title_style', true).">".((get_post_meta($mcurrentpage, $args['id'].'_title', true)==2)?"<a title='".get_the_title()."' href='".get_permalink()."'>":"");
					$title.=(get_the_title()?get_the_title():"&nbsp;");
				$title.=((get_post_meta($mcurrentpage, $args['id'].'_title', true)==2)?"</a>":"")."</".get_post_meta($mcurrentpage, $args['id'].'_title_style', true)."></div>";
				}

				$item_description="";
                if(get_post_meta($mcurrentpage, $args['id'].'_desc_source', true)>0){
					$more = 0;
				$item_description = (get_post_meta($mcurrentpage, $args['id'].'_desc_source', true)==1)?get_the_content(''):"<p>".get_the_excerpt()."</p>";
                    if($item_description){
                        $item_description="<div class='portfolio_item_desc'>".do_shortcode($item_description)."</div>";
                    }
                }


				if( get_post_meta($mcurrentpage, $args['id'].'_author', true) == 'yes' ) {
                	$item_description="<div class='portfolio_item_author'><small>".sprintf( __('By %1$s on %2$s ', 'Current'), get_the_author_link(), get_the_date() )."</small></div>".$item_description;
				}
				
				if(!isset($args['title_top'])) {
					$item_description=$title.$item_description;				
				}
				
				if (get_post_meta($mcurrentpage, $args['id'].'_readmore', true) && get_post_meta($mcurrentpage, $args['id'].'_readmore', true)!="") {
					$item_description=$item_description.do_shortcode('[button text="'.get_post_meta($mcurrentpage, $args['id'].'_readmore', true).'" title="'.get_the_title().'" url="'.get_permalink().'"]')."<br  class=\"clear\" />";
				}
				
				//here																																																																										
				
				
		if(isset($args['title_top']) && get_post_meta($mcurrentpage, $args['id'].'_cols_per_row', true)==1 && (get_post_meta($mcurrentpage, $args['id'].'_desc_source', true)>0 && !isset($args['description_hover']))) {
				echo "<div class='grid_".($grid*2)."'>".$title."</div>";
                }
				?>
			    <div class="portfolioitem grid_<?php echo $grid; echo ($counter==0)?" alpha":((get_post_meta($mcurrentpage, $args['id'].'_cols_per_row', true)==($counter+1))?" omega":"");?>">
                <?php if(isset($args['title_top']) && (get_post_meta($mcurrentpage, $args['id'].'_cols_per_row', true)!=1 || (get_post_meta($mcurrentpage, $args['id'].'_desc_source', true)<1 || isset($args['description_hover'])))) {
				echo $title;
                }
                 if(isset($args['image_frame']) && $grid>1) {

					  $thumbwidth=($availwidth-($gutter_width*$colsperpage))/$colsperpage;
					  if($thumbwidth<ceil($availwidth/2))
					  	$thumbwidth = ceil($availwidth/2);
					  $thumbheight=(int)($thumbwidth/(get_post_meta($mcurrentpage, $args['id'].'_aspect', true)?get_post_meta($mcurrentpage, $args['id'].'_aspect', true):1));		
					//echo '<div class="rhythmize" style="padding-bottom:'.(($line_height*$font_size)-(($thumbheight+42)%($line_height*$font_size)))/$font_size.'em;">';
					?>
                    
                    <span class="image_frame">  
                              <?php
							 
							  }else { 
							  $thumbwidth=($availwidth-($gutter_width*$colsperpage))/$colsperpage;
							  if($thumbwidth<ceil($availwidth/2))
					  			$thumbwidth = ceil($availwidth/2);
								$thumbheight=(int)($thumbwidth/(get_post_meta($mcurrentpage, $args['id'].'_aspect', true)?get_post_meta($mcurrentpage, $args['id'].'_aspect', true):$Current_options['image_aspect']));		

							  //echo '<div class="rhythmize" style="padding-bottom:'.((($line_height*$font_size)-($thumbheight%($line_height*$font_size)))/$font_size).'em;">';
							  
							  }
			
						
						simple_portfolio_imagethumb($thumbwidth, $thumbheight, isset($args['thumb_grey']) );
					
					 if(isset($args['description_hover']) && strlen($item_description)>2) { ?>										 					
     				<div class="portfoliodeschover_wrap">
                        <div class="portfoliodeschover">
                        	<?php echo $item_description;?>
                        </div>
    
                    </div>
             <?php } 
 //echo "</div>";

					if(isset($args['image_frame']) && $grid>1) {?>
			    			</span>
              <?php }
		  	

		if(get_post_meta($mcurrentpage, $args['id'].'_cols_per_row', true)==1){ ?>
				</div>
                 <?php if(!isset($args['description_hover']) && strlen($item_description)>2) { ?>	
				<div class="portfoliodesc grid_<?php echo $grid;?> omega">
           
					<div class="portfoliodescside<?php echo (isset($args['image_frame']) && $grid>1)?" portfoliowithframe":"";?>" >
						<?php echo $item_description;?>
                    </div>

				</div>
                <br class="clear" />
                <?php } 
 }else
				{
 if(!isset($args['description_hover']) && strlen($item_description)>2) { ?>										 					
     				<div class="portfoliodesc">
                        <div class="portfoliodescbelow">
                        	<?php echo $item_description;?>
                        </div>
    
                    </div>
             <?php } ?>

				</div>
              <?php
			  	$counter++;
				if(get_post_meta($mcurrentpage, $args['id'].'_cols_per_row', true)==$counter) { ?>
                <br class="clear" />
			<?php
				$counter=0;
					}
				}
		    endwhile; ?>
            <br class="clear" />
			</div>
<?php			
			echo do_shortcode("[divider backtotop='1']");
		// Pagination
			if(function_exists('wp_pagenavi')) :
			    wp_pagenavi();
			else : 
			?>
		    <div class="navigation">
			    <div class="custombuttonwrap m alignleft"><?php previous_posts_link("<span>".__("Previous", 'Current')."</span>", 0) ?></div>
				    <div class="custombuttonwrap m alignright"><?php next_posts_link("<span>".__("Next", 'Current')."</span>", 0) ?></div>
            <br class="clear" />
		    </div>

<?php			endif; 
		endif;
		//Reset Query
		wp_reset_query();
		 

	    else : ?>
		    <p></p><div class="warning">
				<div class="msgbox-icon"><?php _e("A Category needs to be assigned in the Portfolio section for this page in order to have the portfolio item posts from that category displayed here. Please check the Current setting's Portfolio Section.", 'wpavatar'); ?></div>
            </div>
            
<?php
	    endif; 

 }
?>
