
<?php
function front_mocktail_slider($args = array()) { 

	global $post, $Current_options;
	$args['id'] = isset($args['id'])?$args['id']:str_replace(' ','_',$args['name']);
	$Current_options['mocktail_slider_frontpage']=substr($Current_options['mocktail_slider_frontpage'],0,strlen($Current_options['mocktail_slider_frontpage'])-2).ICL_LANGUAGE_CODE;
	if(is_front_page())
		$slider=md5($Current_options['mocktail_slider_frontpage']);
	elseif(is_archive() || is_search() || is_404())
		$slider=md5($Current_options['mocktail_slider_archive']);
	else
		$slider = md5(get_post_meta($post->ID, $args['id'].'_val', true));
				
?>

<div id="mocktail_slider_wrap">

<div id="mocktail_slider_bg">
<div id="mocktail_slider_corner1" class="mocktail_slider_corner"></div>
	<div id="mocktail_slider_corner2" class="mocktail_slider_corner"></div>
		<div id="mocktail_slider_corner3" class="mocktail_slider_corner"></div>
			<div id="mocktail_slider_corner4" class="mocktail_slider_corner"></div>
</div>          
<div id="mocktail_slider_container">
    <ul id="jcycleslider">
    <?php  
                    $allslides=0;
                    
                    foreach( explode( '|', $Current_options['mocktail_'.$slider] ) as $currentslide ) {
    
     ?>
                        <li <?php if($allslides>0) echo "style='display:none'"; $allslides++; ?>>

                        <div class="slide_component1" >
                            <img src="<?php echo isset($Current_options['mocktail_'.$slider.'_slide'.$currentslide.'_img'])?$Current_options['mocktail_'.$slider.'_slide'.$currentslide.'_img']:"images/default.gif"; ?>" />
                         </div>
    <?php               if (isset($Current_options['mocktail_'.$slider.'_slide'.$currentslide.'_img2']) && $Current_options['mocktail_'.$slider.'_slide'.$currentslide.'_img2']!='') { ?>
                            <div class="slide_component2" >
                                <img src="<?php echo $Current_options['mocktail_'.$slider.'_slide'.$currentslide.'_img2']; ?>"  />
                            </div>
    <?php               }

                        if ( isset($Current_options['mocktail_'.$slider.'_slide'.$currentslide.'_text']) && $Current_options['mocktail_'.$slider.'_slide'.$currentslide.'_text']!='' ) { ?>
                            <div class="slide_component3">
    <?php                   	echo do_shortcode( $Current_options['mocktail_'.$slider.'_slide'.$currentslide.'_text'] ); ?>
                            </div>
    <?php               } ?>
    
                        </li>
    <?php				} ?>
                    </ul>

<div id="jslider_nav"></div>

</div>
			
 </div>
			<?php }
?>
