// JavaScript Document
jQuery.noConflict();

/**
 * CoolInput Plugin
 *
 * @version 1.5 (10/09/2009)
 * @requires jQuery v1.2.6+
 * @author Alex Weber <alexweber.com.br>
 * @author Evan Winslow <ewinslow@cs.stanford.edu> (v1.5)
 * @copyright Copyright (c) 2008-2009, Alex Weber
 * @see http://remysharp.com/2007/01/25/jquery-tutorial-text-box-hints/
 *
 * Distributed under the terms of the GNU General Public License
 * http://www.gnu.org/licenses/gpl-3.0.html
 */
jQuery(document).ready(function($){
    $.fn.coolinput=function(b){
	var c={
	    hint:null,
	    source:"value",
	    blurClass:"blur",
	    iconClass:false,
	    clearOnSubmit:true,
	    clearOnFocus:true,
	    persistent:true
	};if(b&&typeof b=="object")
	    $.extend(c,b);else
	    c.hint=b;return this.each(function(){
	    var d=$(this);var e=c.hint||d.attr(c.source);var f=c.blurClass;function g(){
		if(d.val()=="")
		    d.val(e).addClass(f)
		    }
	    function h(){
		if(d.val()==e&&d.hasClass(f))
		    d.val("").removeClass(f)
		    }
	    if(e){
		if(c.persistent)
		    d.blur(g);if(c.clearOnFocus)
		    d.focus(h);if(c.clearOnSubmit)
		    d.parents("form:first").submit(h);if(c.iconClass)
		    d.addClass(c.iconClass);g()
		}
	    })
	}
    });
jQuery(document).ready(function($){
	$('.search_field').coolinput({
		blurClass: 'blur'
	});

});



// scrolling to the top
jQuery(document).ready(function($){
    $('a[href=#top]').click(function(){
        $('html, body').animate({scrollTop:0}, 'slow');
        return false;
    });
});

// Fix sub page menu
jQuery(document).ready(function($) {
        $('#navigation-menu a').each(function() {
            if ( !$(this).attr("href") ) {
                $(this).addClass("nocursor");
            }
        });
});

// Fix blog metainfo display
jQuery(document).ready(function($) {
		var takeout=false;
        if($("div.blogpostmetainfo").length>0) {

			$("div.blogpostmetainfo > div.postmetadatawrap").each(function () {

				var blogpostmetaicons=$("<div>").addClass("blogpostmetaicons");
				$(this).parent().parent().children("div.blogposttitle").append(blogpostmetaicons);
				
				$(this).children("div").each(function() {
					blogpostmetaicons.append($("<a>").addClass($(this).attr("class").replace(" alpha", "")).bind("mouseenter", function() {

					$(this).parent().children("div.metahoverdisplay").remove();																					
					$(this).parent().append($("<div>").addClass("metahoverdisplay").css("opacity", 0).html($(this).parent().parent().parent().find("div."+$(this).attr("class")).html()).animate({opacity: 1, paddingTop:4}, 200));																												   
																																   }).bind("mouseleave", function() {
		$(this).parent().children("div.metahoverdisplay").bind("mouseenter", function () {takeout=false; $(this).bind("mouseleave", function() {$(this).remove()})});
		
		takeout=true
		removeifnotused($(this).parent().children("div.metahoverdisplay"));																												   		}));									  
				});
			blogpostmetaicons.append($("<br>").addClass("clear"));
			});
			
		}
		
		function removeifnotused(ehnu) {
			ehnu.delay(1000).queue(function () {
			if(takeout) {
				$(this).remove();
				takeout=false;	
			}
																																																			  			});	
		}
});