<?php

$root = dirname(dirname(dirname(dirname(dirname(__FILE__)))));

if ( file_exists( $root.'/wp-load.php' ) ) {
    require_once( $root.'/wp-load.php' );
} elseif ( file_exists( $root.'/wp-config.php' ) ) {
    require_once( $root.'/wp-config.php' );
}

header("content-type: application/x-javascript");
global $mocktail_slider_array;
global $isfrontpage;
global $post;

if($_REQUEST['post']=='fp')
	$isfrontpage=true;
else {
	$isfrontpage=false;
	$post->ID = $_REQUEST['post'];	
}
	
foreach($mocktail_slider_array as $args) {
	if(willrender_mocktail_slider($args)) {
		if($isfrontpage)
			$slider=md5($Current_options['mocktail_slider_frontpage']);
		else
			$slider = md5(get_post_meta($post->ID, $args['id'].'_val', true));
		
		
		$fade=isset($Current_options['mocktail_'.$slider.'_effect']) && $Current_options['mocktail_'.$slider.'_effect']=="fade";
		$mix=isset($Current_options['mocktail_'.$slider.'_effect']) && $Current_options['mocktail_'.$slider.'_effect']=="mix";
		?>
		jQuery(document).ready(function($){
		
			var mtransition = "<?php echo $fade?"fade":"scrollRight";?>";
			var mpace = 500;
			var mnext = 5000;
			var msync = 0;
			var easing = 'easeOutQuad'
		
			$(function(){
				mnext = <?php echo $Current_options['mocktail_'.$slider.'_timeout']?$Current_options['mocktail_'.$slider.'_timeout']:3000;?>;
				
		
				if ( $("#jcycleslider li").size() == 1 ) {
					$('#jcycleslider li:eq(0)').clone().insertAfter('#jcycleslider li:eq(0)');
				}
				
		
				$('#jcycleslider').cycle({
					fx:			mtransition,
					easing:     easing,
					before:		onBefore,
					after:		onAfter,
					speed:		mpace,
					timeout:		mnext,
					sync:		msync,
					randomizeEffects:	0,
					pager:		'#jslider_nav'
				});
		
		<?php if($fade || $mix) { ?>
				
				function onBefore(current, next, options, forward) {
		
						$(next).find('.slide_component2').css({'display':'none', 'opacity':0});
		
						$(next).find('.slide_component3').css({'display':'none', 'opacity':0});
				}
				function onAfter(current, next, options, forward) {
		
						$(this).find('.slide_component2').css({'display':'block'}).delay(50).animate({'left':'0', 'opacity':1}, 250, easing);
		
						$(this).find('.slide_component3').css({'display':'block'}).delay(250).animate({'left':'0', 'opacity':1}, 350, easing);
				}
				
		<?php } else { ?>
		
				function onBefore(current, next, options, forward) {
		
						$(next).find('.slide_component2').css({'display':'none', 'left':'-960px'});
		
						$(next).find('.slide_component3').css({'display':'none', 'left':'<?php echo 960*2;?>px'});
				}
				function onAfter(current, next, options, forward) {
		
						$(this).find('.slide_component2').css({'display':'block'}).delay(50).animate({'left':'0', 'opacity':1}, 250, easing);
		
						$(this).find('.slide_component3').css({'display':'block'}).delay(250).animate({'left':'0', 'opacity':1}, 350, easing);
				}
		<?php } ?>        
			
				$('#jcycleslider').mouseenter(function() {
					$(this).cycle('pause');
					return false;
				});
		
				$('#jcycleslider').mouseleave(function() {
					$(this).cycle('resume');
					return false;
				});
			});
			
			
		});
<?php  	}
 } ?>    