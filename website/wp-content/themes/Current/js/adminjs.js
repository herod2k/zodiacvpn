// JavaScript Document
jQuery.noConflict();

jQuery(document).ready(function($) {
	if($('body.appearance_page_Current_settings_page').length>0) {								
		window.send_to_editor = function(html) {
			imgurl = $('img',html).attr('src');
			imgwidth=$('img',html).attr('width');
			imgheight=$('img',html).attr('height');		
			$('#'+ formfield).val(imgurl);
			
			if(typeof(formwidth) != 'undefined')
				$('#'+ formwidth).val(imgwidth);			
			if(typeof(formheight) != 'undefined')
				$('#'+ formheight).val(imgheight);
			tb_remove();
		}
	}
});


// color picker script
jQuery(document).ready(function($) {
	$('.Current_color_box').each(function(){
		var merabox=$(this);
		$('#'+$(this).attr("id").replace("wrap_", "")).bind("input paste", function(){
			$(this).val($(this).val().replace("#",""));
			merabox.css("background-color", "#"+$(this).val());
		});
		$('#'+$(this).attr("id").replace("wrap_", "")).blur(function(){
			if($(this).val().length	> 6)													 
				$(this).val($(this).val().substr(0, 6));

			merabox.css("background-color", "#"+$(this).val());				
		});
		
		addColorPicker('#'+$(this).attr("id").replace("wrap_", ""), '#'+$(this).attr("id"), rgb2hex( $(this).css('background-color') ));	

	});
	
	function addColorPicker(inputField, colorSelector, defaultColor) {
	$(colorSelector).ColorPicker({
		color: '#000'+defaultColor,
		onShow: function (colpkr) {
			$(colpkr).fadeIn(500);
			return false;
		},
		onHide: function (colpkr) {
			$(colpkr).fadeOut(500);
			return false;
		},
		onChange: function (hsb, hex, rgb) {
			$(colorSelector).css('backgroundColor', '#'+hex);
			$('input'+inputField).attr("value", hex);
		},
		onSubmit: function(hsb, hex, rgb, el) {
			$(el).val(hex);
			$(el).ColorPickerHide();
		}
	});
    }
    // convert rgb to hex color string
    function rgb2hex(rgb) {
	if (  rgb.search("rgb") == -1 ) {
	    return rgb;
	} else {
	    rgb = rgb.match(/^rgba?\((\d+),\s*(\d+),\s*(\d+)(?:,\s*(\d+))?\)$/);
	    function hex(x) {
		return ("0" + parseInt(x).toString(16)).slice(-2);
	    }
	    return hex(rgb[1]) + hex(rgb[2]) + hex(rgb[3]);
	}
    }
});

jQuery(document).ready(function($) {
								
						
			$('body.appearance_page_Current_settings_page div#simple_background_url input.upmediabt').bind('click', function() {
				formfield = $(this).attr('id').replace("upload_", "");
				tb_show('', 'media-upload.php?type=image&amp;TB_iframe=true');
				return false;
			});

});jQuery(document).ready(function($) {
								
						
			$('div#simple_logo_settings_container input.upmediabt').bind('click', function() {
				formfield = $(this).attr('id').replace("upload_", "");
				formwidth = $('#simple_logo_width').attr('id');
				formheight = $('#simple_logo_height').attr('id');
				tb_show('', 'media-upload.php?type=image&amp;TB_iframe=true');
				return false;
			});

});
jQuery(document).ready(function($) {
								
			var themocktail_sliders=$("input#mocktail_slider_list").val().split("|");
			
			for(x in themocktail_sliders){
				if(themocktail_sliders[x]!="")
					$("select.mocktail_slider_collection").append("<option>"+themocktail_sliders[x]+"</option>");	
			}
			
			$("input#add_mocktail_slider").click(function() {
					var exists=false;
					$("select#mocktail_slider_collection option").each(function(){
							if($(this).text()==$("input#mocktail_slider_new_name").val()) {
								alert("slider with the same name already exists");
								exists=true;
							}
						});
					
		
					if(!exists) {
						$("select.mocktail_slider_collection").append("<option>"+$("input#mocktail_slider_new_name").val()+"</option>");
						$("input#mocktail_slider_new_name").val("");
						updateField();
					}
				});
			$("input#remove_mocktail_slider").click(function() {
					if(confirm("Are you sure, you want to remove this mocktail_slider(s)")) {
						$("select#mocktail_slider_collection option:selected").each(function (){
							var toberemoved=$(this).text();
							//$("select#mocktail_slider_collection option:selected").remove();
							$("select.mocktail_slider_collection option").each(function(){
								if($(this).text()==toberemoved)
									$(this).remove();
							});
						});
						updateField();
					}
			});
			function updateField(){
				var mocktail_sliders=new Array();
						
				var i=0;
				$("select.mocktail_slider_collection option").each(function (){
					if($(this).text().replace(" ", "")=="") {
						$(this).remove();
					}
				});
				
				$("select#mocktail_slider_collection option").each(function (){
					if($(this).text().replace(" ", "")!="") {
					mocktail_sliders[i]=$(this).text();
					i++;
					}
				});
		
				$("input#mocktail_slider_list").val(mocktail_sliders.join("|"));
				
				$("select#mocktail_slider_collection option").unbind("dblclick");
				
				$("select#mocktail_slider_collection option").bind("dblclick", function() {
						doubleclick($(this).val());														   
																				   
					});
			}
			
			$("select#mocktail_slider_collection option").bind("dblclick", function() {
						doubleclick($(this).val());														   
																				   
					});
			
			function doubleclick(mval) {
				if(mval!="")
					jQuery.post(
					   ajaxurl, 
					   {
						  'action':'mocktail_slider_settings',
						  'slider':mval
					   }, 
					   function(response){
						  $("div#mocktail_slider_settings_container").html(response);
						  fix_mocktail_slides();
						  $("input#mocktail_slider_current").val(mval);
					   }
					);
			}
			
			function fix_mocktail_slides() {
				
				$("input#add_mocktail_slide").unbind("click");
				$("input#add_mocktail_slide").bind("click", function(){
					add_new_slide();	  
				});
				
				if($("div#mocktail_slider_settings_container ul#mocktail_slides > li").length<1) {
					add_new_slide();
				}
				
				comb_slides();
			}
			
			function add_new_slide() {
				var newitem=$('<li>').append($("div#mocktail_slider_settings_container div#slidetemplate").html());
				$("div#mocktail_slider_settings_container ul#mocktail_slides").append(newitem);
				comb_slides();
			}
			
			function comb_slides() {
				var i=0;
				var mocktail_slides_list =  new Array();
				
						
				$("div#mocktail_slider_settings_container ul#mocktail_slides > li").each(function() {
					
					$(this).find("input, textarea").removeAttr("disabled");																					
					
					
					$(this).find('.remove_mocktail_slide').unbind('click');
					$(this).find('.remove_mocktail_slide').bind('click', function () {
																				
						$(this).parent().parent().remove();	
						comb_slides();
					});
					
					
					i++;
					
					$(this).find("input, textarea").each(function() {
						var name_array = $(this).attr('id').split('_');
						name_array[name_array.length-2] = 'slide'+i;
						$(this).attr('id', name_array.join('_'));
						$(this).attr('name', 'Current_options['+name_array.join('_')+']');
					});
					mocktail_slides_list[i-1]=i;	
				});
				
				$("input#mocktail_slides_list").val(mocktail_slides_list.join('|'));
				
				$('div#mocktail_slider_settings_container input.upmediabt').bind('click', function() {
					formfield = $(this).attr('id').replace("upload_", "");
					tb_show('', 'media-upload.php?type=image&amp;TB_iframe=true');
					return false;
				});
				
				$("div#mocktail_slider_settings_container ul#mocktail_slides").sortable({
				   stop: function(event, ui) { comb_slides(); }
				});
			}
			
			doubleclick($("input#mocktail_slider_current").val());
			
		});


		jQuery(document).ready(function($) {
								
			var thesimple_sidebars=$("input#simple_sidebar_list").val().split("|");
		
			for(x in thesimple_sidebars){
				if(thesimple_sidebars[x]!="")
					$("select.simple_sidebar_collection").append("<option>"+thesimple_sidebars[x]+"</option>");	
			}
			
			$("input#add_simple_sidebar").click(function() {
					var exists=false;
					$("select#simple_sidebar_collection option").each(function(){
							if($(this).text()==$("input#simple_sidebar_new_name").val() || $("input#simple_sidebar_new_name").val()=="none" || $("input#simple_sidebar_new_name").val()=="Default sidebar") {
								alert("sidebar with the same name already exists");
								exists=true;
							}
						});
					
		
					if(!exists) {
						$("select.simple_sidebar_collection").append("<option>"+$("input#simple_sidebar_new_name").val()+"</option>");
						$("input#simple_sidebar_new_name").val("");
						updateField();
					}
				});
			$("input#remove_simple_sidebar").click(function() {
					if(confirm("Are you sure, you want to remove this simple_sidebar(s)")) {
						$("select#simple_sidebar_collection option:selected").each(function (){
							var toberemoved=$(this).text();
							//$("select#simple_sidebar_collection option:selected").remove();
							$("select.simple_sidebar_collection option").each(function(){
								if($(this).text()==toberemoved)
									$(this).remove();
							});
						});
						updateField();
					}
			});
			function updateField(){
				var simple_sidebars=new Array();
						
				var i=0;
				$("select.simple_sidebar_collection option").each(function (){
					if($(this).text().replace(" ", "")=="") {
						$(this).remove();
					}
				});
				
				$("select#simple_sidebar_collection option").each(function (){
					if($(this).text().replace(" ", "")!="") {
					simple_sidebars[i]=$(this).text();
					i++;
					}
				});
		
				$("input#simple_sidebar_list").val(simple_sidebars.join("|"));
				
			}
			
		});
		
