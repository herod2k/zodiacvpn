// JavaScript Document
jQuery.noConflict();



		jQuery(document).ready(function($){

		// Accordion
			$(".accordion").accordion({ header: "h4" });

		// Tabs
			$('.tabs').tabs();

		});
		
jQuery(document).ready(function($) {

			$("div.plugin_latestposts_container").each(function() {
					plugin_latestposts_get($(this), "right");											
																
			});

			function plugin_latestposts_get(caller, direction) {
				//if(caller.find("div.contents").height()>100)
					//caller.find("div.contents").css("height", caller.find("div.contents").height()+"px");
				if(direction=="right")
					caller.find("div.contents").animate({'right':'100%'}, caller.width(), 'swing', function() {$(this).css('right','-100%');});
				else
					caller.find("div.contents").animate({'right':'-100%'}, caller.width(), 'swing', function() {$(this).css('right','100%');});
					jQuery.post(
					   thajaxurl,
					   caller.find("form.plugin_latestposts_form").serialize(),
					   function(response){
						  caller.find("div.contents").html(response);
							load_plugin_latestposts(caller);
								caller.find("div.contents").animate({'right':0}, caller.width(), 'swing');
					   }
					);
			}
			
			function load_plugin_latestposts(caller) {
				caller.find("div.plugin_latestposts_sort_all").html(caller.find("div.contents > div.plugin_latestposts_sort").html());
				caller.find("div.navigation_all").html(caller.find("div.contents > div.navigation").html());	
				
				caller.find("div.navigation_all a").unbind("click");
				
				caller.find("div.navigation_all a").bind("click", function() {
					var splitted = $(this).attr("href").split("=");
					caller.find("form.plugin_latestposts_form > input#paged").val(splitted[splitted.length-1]);
					plugin_latestposts_get(caller, $(this).attr("id").replace("direction", ""));
					return false;
				});
				
				caller.find("div.plugin_latestposts_sort_all a").unbind("click");
				
				caller.find("div.plugin_latestposts_sort_all a").bind("click", function() {
					var splitted = $(this).attr("href").split("=");
					caller.find("form.plugin_latestposts_form > input#currentcategory").val(splitted[splitted.length-1]);
					caller.find("form.plugin_latestposts_form > input#paged").val('');
					plugin_latestposts_get(caller, "left");
					return false;
				});
				
				
				
				
				try {
					initprettyphoto( caller.find("a[rel^='wp-prettyPhoto'], a[rel^='prettyPhoto']"));
				}
				catch (err) {}
				
				
				
			}
		
		});


		// jQuery Validate for simple contact form
		jQuery(document).ready(function($){
			if( $("#simple_contact_form").length>0 ) {
				$.validator.addMethod("phoneUS", function(phone_number, element) {
					phone_number = phone_number.replace(/\s+/g, ""); 
					return this.optional(element) || phone_number.length > 9 &&
						phone_number.match(/^(1-?)?(\([2-9]\d{2}\)|[2-9]\d{2})-?[2-9]\d{2}-?\d{4}$/);
				}, "Please specify a valid phone number");
				$("#simple_contact_form").validate();
		
			}
		});

		jQuery(document).ready(function($){
			$(function() {
     				$("input:checkbox, input:radio").uniform();
     				$("#hear_about_us").uniform();
 			});
		});
