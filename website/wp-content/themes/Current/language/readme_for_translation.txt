You can create your own translation for the Current theme.

copy the Current.pot file and rename it to the language locale string and extension as .po.

Example, rename Current.pot es_ES.po

Edit the newely created file with poedit (http://www.poedit.net) and save the file in the current folder when done. When you save a new file with the same name but extenion .mo will be generated, this .mo file will be used by the WordPress for language.