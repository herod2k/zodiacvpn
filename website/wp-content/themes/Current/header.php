<?php 
/**
 * @package WordPress
 * @subpackage Current
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?>>
<head profile="http://gmpg.org/xfn/11">
<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<title><?php wp_title('&laquo;', true, 'right'); 
 bloginfo('name'); ?></title>

<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
<script type="text/javascript">
var thajaxurl = "<?php echo admin_url('admin-ajax.php'); ?>";
</script>

<?php
if ( is_singular() && get_option( 'thread_comments' ) )
		wp_enqueue_script( 'comment-reply' );
wp_head();
?>
</head>

<body <?php body_class(); ?>>

<?php
th_render_header( array ());


th_render_header( array ());


th_render_header( array ());


th_render_header( array ());


th_render_header( array ());
?>

<div id='page_header' class='page_section'>


<?php
global $global_blocks;


th_render_row( $global_blocks[7]);


th_render_row( $global_blocks[8]);


th_render_row( $global_blocks[9]);
?>


</div>