<?php
    if((isset($_GET['post']) && get_post_meta(mysql_real_escape_string($_GET['post']),'_wp_page_template',true) == 'blogpage.php') || (isset($_POST['post_ID']) && get_post_meta(mysql_real_escape_string($_POST['post_ID']),'_wp_page_template',true) == 'blogpage.php')) {
        add_action( 'load-page.php', 'simple_blog_mbox_setup' );
        add_action('save_post', 'save_simple_blog_mbox');
    }
 function simple_blog_mbox_setup() {
        add_action( 'add_meta_boxes', 'add_simple_blog_mbox_setup' );
    }
 function add_simple_blog_mbox_setup() {

        add_meta_box(
            'simple-blog-mbox',			// Unique ID
            esc_html__( 'Blog Settings', 'Current' ),		// Title
            'simple_blog_mbox',		// Callback function
            'page',					// Admin page (or post type)
            'normal',					// Context
            'core'					// Priority
        );
    }
 function simple_blog_mbox( $object, $box ) {
		global $simple_blog_array, $global_blocks, $post, $Current_options;
		
		foreach($simple_blog_array as $blog) {
			$cats_array=get_categories();
        ?>
        <div class="params_div1"><div class="params_label"><label for="simple_blog_cats"><?php esc_html_e('Assign the Category(s)', 'Current'); ?>:</label></div>
            <div class="params_value">
                <select id="simple_blog_cats" multiple="multiple" name="<?php echo $blog['id'];?>_cats[]">
    <?php			$cats = explode("|", get_post_meta($post->ID, $blog['id'].'_cats', true));
					foreach ($cats_array as $cat_obj) :
						$cur_cat_ID = $cat_obj->term_id; ?>
						<option value="<?php echo $cur_cat_ID;?>" <?php echo in_array($cur_cat_ID, $cats)?"selected='selected'":"";?> ><?php echo $cat_obj->name; ?></option>
    <?php			endforeach; ?>
                </select> 
<span class="legend"><?php esc_html_e('You can select multiple categories by holding down the "ctrl" key', 'Current'); ?> </span> 
             </div>
         </div>
         
         <div class="params_div2"><div class="params_label"><label for="simple_blog_items_sortable"><?php esc_html_e('Display Sort options', 'Current'); ?>:</label></div>
         <div class="params_value">
         <input name="<?php echo $blog['id'];?>_sortable" type="checkbox" id="simple_blog_items_sortable" value="yes" <?php checked('yes', get_post_meta($post->ID, $blog['id'].'_sortable', true));?>  /> <span class="legend"><?php esc_html_e('Note: The sort by categories options will be available only in case of multiple categories assigned to the blog page', 'Current'); ?></span>
         </div>
         </div>
         
		<div class="params_div1">
        	<div class="params_label">
        		<label for="simple_blog_excerpt"><?php esc_html_e('Excerpt', 'Current'); ?>:</label></div>
			<div class="params_value">
                <input name="<?php echo $blog['id'];?>_excerpt" type="checkbox" id="simple_blog_excerpt" value="yes" <?php checked('yes', get_post_meta($post->ID, $blog['id'].'_excerpt', true)); ?> /> <span class="legend"><?php esc_html_e('Display Post\'s excerpt, instead of the Post\'s content', 'Current'); ?> </span> 
			</div>
        </div>

			<div class="params_div2">
            <div class="params_label">
            <label for="simple_blog_readmore">
			<?php esc_html_e('"Read more" text', 'Current'); ?>:
			    </label></div>
                <div class="params_value">
                
				<input name="<?php echo $blog['id'];?>_readmore" type="text" id="simple_blog_readmore" value="<?php echo get_post_meta($post->ID, $blog['id'].'_readmore', true);?>" size="30" maxlength="100" />
				<span class="legend"><?php esc_html_e("Leave blank for turning off read more button", 'Current'); ?></span>
			    </div>
                </div>

			<div class="params_div1">
                <div class="params_label">
               <label for="simple_blog_author">
                <?php esc_html_e('Show Author', 'Current'); ?>:
			    </label>
                </div>
                <div class="params_value">
				<input name="<?php echo $blog['id'];?>_author" type="checkbox" id="simple_blog_author" value="yes" <?php checked('yes', get_post_meta($post->ID, $blog['id'].'_author', true)); ?> /> <span class="legend"><?php esc_html_e('Show Author info for each post', 'Current'); ?> </span> 
			    </div>
            </div>
                
            <div class="params_div2">
            <div class="params_label">
				<label for="simple_blog_category">
            <?php esc_html_e('Show Category(s)', 'Current'); ?>:</label>
            </div>
            
			<div class="params_value">
				
				    <input name="<?php echo $blog['id'];?>_category" type="checkbox" id="simple_blog_category" value="yes" <?php checked('yes', get_post_meta($post->ID, $blog['id'].'_category', true)); ?> /> 
				 <span class="legend"><?php esc_html_e('Show Category (s) info for each post', 'Current'); ?> </span> 
			    </div>
                </div>
                
			<div class="params_div1">
            <div class="params_label">
            	<label for="simple_blog_tags">
            <?php esc_html_e('Show Tags', 'Current'); ?>:</label>
            </div>
            <div class="params_value">
				<input name="<?php echo $blog['id'];?>_tags" type="checkbox" id="simple_blog_tags" value="yes" <?php checked('yes', get_post_meta($post->ID, $blog['id'].'_tags', true)); ?> /> <span class="legend"><?php esc_html_e('Show Tag(s) info for each post', 'Current'); ?> </span> 
				    
			    </div>
                </div> 

			<div class="params_div2">
            <div class="params_label">
            	<label for="simple_blog_comments">
            <?php esc_html_e('Show Comment(s)', 'Current'); ?>:</label>
            </div>
            <div class="params_value">
				<input name="<?php echo $blog['id'];?>_comments" type="checkbox" id="simple_blog_comments" value="yes" <?php checked('yes', get_post_meta($post->ID, $blog['id'].'_comments', true)); ?> />  <span class="legend"><?php esc_html_e('Show Comment(s) info for each post', 'Current'); ?> </span> 
				    
			</div>
            </div>
            
            <div class="params_div1">
                <div class="params_label"><label for="aspect"><?php esc_html_e('Thumbnail aspect ratio', 'Current'); ?>:</label></div>
                <div class="params_value">
                                        <input name="<?php echo $blog['id'];?>_aspect" type="text" id="aspect" value="<?php echo (get_post_meta($post->ID, $blog['id'].'_aspect', true)) ? get_post_meta($post->ID, $blog['id'].'_aspect', true) : $Current_options['image_aspect']; ?>" size="5" maxlength="5" />:1 
                <span class="legend"><?php esc_html_e('use value greater than 1 for landscape mode and lower value for portrait mode', 'Current'); ?> </span>                 
                </div>
            </div>                          
         
    <?php
		}
    }
 function save_simple_blog_mbox($post_id) {
        global $simple_blog_array, $Current_options;
        foreach($simple_blog_array as $blog) {
			$aspect = $Current_options['image_aspect'];
			if (isset($_POST[$blog['id'].'_cats'])) {
                update_post_meta($post_id, $blog['id'].'_cats', implode("|", $_POST[$blog['id'].'_cats']));
            }
            if (isset($_POST[$blog['id'].'_sortable'])) {
                update_post_meta($post_id, $blog['id'].'_sortable', $_POST[$blog['id'].'_sortable']);
            }
			else
				update_post_meta($post_id, $blog['id'].'_sortable', '');
				
			if (isset($_POST[$blog['id'].'_excerpt'])) {
                update_post_meta($post_id, $blog['id'].'_excerpt', $_POST[$blog['id'].'_excerpt']);
            }
			else
				update_post_meta($post_id, $blog['id'].'_excerpt', '');	
			
			if (isset($_POST[$blog['id'].'_author'])) {
                update_post_meta($post_id, $blog['id'].'_author', $_POST[$blog['id'].'_author']);
            }
			else
				update_post_meta($post_id, $blog['id'].'_author', '');	
				
			if (isset($_POST[$blog['id'].'_category'])) {
                update_post_meta($post_id, $blog['id'].'_category', $_POST[$blog['id'].'_category']);
            }
			else
				update_post_meta($post_id, $blog['id'].'_category', '');
				
			if (isset($_POST[$blog['id'].'_tags'])) {
                update_post_meta($post_id, $blog['id'].'_tags', $_POST[$blog['id'].'_tags']);
            }
			else
				update_post_meta($post_id, $blog['id'].'_tags', '');				
				

			if (isset($_POST[$blog['id'].'_readmore'])) {
                update_post_meta($post_id, $blog['id'].'_readmore', $_POST[$blog['id'].'_readmore']);
            }
			
			if (isset($_POST[$blog['id'].'_comments'])) {
                update_post_meta($post_id, $blog['id'].'_comments', $_POST[$blog['id'].'_comments']);
            }
			else
				update_post_meta($post_id, $blog['id'].'_comments', '');
				
			if (isset($_POST[$blog['id'].'_aspect']) && is_numeric($_POST[$blog['id'].'_aspect'])) {
				$aspect = $_POST[$blog['id'].'_aspect'];
				if($aspect <= 0)
					$aspect = $Current_options['image_aspect'];
			}
			update_post_meta($post_id, $blog['id'].'_aspect', $aspect);
        }
    }?>