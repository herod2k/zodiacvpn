<?php
	function render_cpanel_google_analytics( $options ) {
				?>
				<div class="params_div1">   
						<div class="params_label">
							 <label for="simple_logo_url" ><?php esc_html_e('Analytics code:', 'Current'); ?> </label></div>
									<div class="params_value" id="simple_logo_settings_container">
									<textarea class="code" style="width: 98%; font-size: 12px;" id="google_analytics" rows="10" cols="60" name="Current_options[google_analytics]"><?php if( isset($options['google_analytics']) ){ esc_attr_e($options['google_analytics']); } ?></textarea>
			    <br />
			    <span class="legend"><?php esc_html_e('Paste your Analytics tracking code here. It will be inserted just before the closing body tag of each page', 'Current'); ?></span>
									</div>
						 </div>

				<?php
				display_save_changes_button(); 				
			}?>