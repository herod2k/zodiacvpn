<?php

			add_action( 'load-post.php', 'simple_background_mbox_setup' );
			add_action( 'load-post-new.php', 'simple_background_mbox_setup' );
			add_action('save_post', 'save_simple_background_mbox');
 function simple_background_mbox_setup() {
				add_action( 'add_meta_boxes', 'add_simple_background_mbox_setup' );
			}
 function add_simple_background_mbox_setup() {

				add_meta_box(
					'simple-background-mbox',			// Unique ID
					esc_html__( 'Page Background settings override', 'Current' ),		// Title
					'simple_background_mbox',		// Callback function
					'',					// Admin page (or post type)
					'normal',					// Context
					'core'					// Priority
				);
			}
 function simple_background_mbox( $object, $box ) {
				global $simple_background_array, $post, $Current_options;
				foreach($simple_background_array as $simple_background) {
			?>
				<div class="params_div1">
            <div class="params_label"><label for="<?php echo $simple_background['id']; ?>_img"><?php esc_html_e('Enter a URL or upload an image:', 'Current'); ?></label></div>
            <div class="params_value" id="simple_background_url"><input name="<?php echo $simple_background['id']; ?>_img" type="text" id="<?php echo $simple_background['id']; ?>_img" value="<?php echo get_post_meta($post->ID, $simple_background['id'].'_img', true); ?>" size="65" />
                            <input id="upload_<?php echo $simple_background['id']; ?>_img" type="button" value="<?php esc_attr_e('Upload Image', 'Current'); ?>" class="button-secondary upmediabt" /></div>
                            </div>
        <div class="params_div2">
        	<div class="params_label"><label for="<?php echo $simple_background['id']; ?>_img_repeat"><?php esc_html_e('Background Properties:', 'Current'); ?></label></div>
            <div class="params_value"><select name="<?php echo $simple_background['id']; ?>_img_repeat" id="<?php echo $simple_background['id']; ?>_img_repeat">
                                <option value="no-repeat"<?php echo (get_post_meta($post->ID, $simple_background['id'].'_img_repeat', true) == 'no-repeat') ? ' selected="selected"' : ''; ?>><?php esc_attr_e('No Repeat', 'Current'); ?></option>
                                <option value="repeat-x"<?php echo (get_post_meta($post->ID, $simple_background['id'].'_img_repeat', true) == 'repeat-x') ? ' selected="selected"' : ''; ?>><?php esc_attr_e('Repeat only Horizontally', 'Current'); ?></option>
                                <option value="repeat-y"<?php echo (get_post_meta($post->ID, $simple_background['id'].'_img_repeat', true) == 'repeat-y') ? ' selected="selected"' : ''; ?>><?php esc_attr_e('Repeat only Vertically', 'Current'); ?></option>
                                <option value="repeat"<?php echo (get_post_meta($post->ID, $simple_background['id'].'_img_repeat', true) == 'repeat') ? ' selected="selected"' : ''; ?> style="padding-right:10px;"><?php esc_attr_e('Repeat both Vertically and Horizontally', 'Current'); ?></option>
                            </select>
                            <label for="<?php echo $simple_background['id']; ?>_img_position_horizontal"><?php esc_html_e('Horizontal:', 'Current'); ?></label>
                            <select name="<?php echo $simple_background['id']; ?>_img_position_horizontal" id="<?php echo $simple_background['id']; ?>_img_position_horizontal">
                                <option value="left"<?php echo (get_post_meta($post->ID, $simple_background['id'].'_img_position_horizontal', true) == 'left') ? ' selected="selected"' : ''; ?>><?php esc_attr_e('left', 'Current'); ?></option>
                                <option value="right"<?php echo (get_post_meta($post->ID, $simple_background['id'].'_img_position_horizontal', true) == 'right') ? ' selected="selected"' : ''; ?>><?php esc_attr_e('right', 'Current'); ?></option>
                                <option value="center"<?php echo (get_post_meta($post->ID, $simple_background['id'].'_img_position_horizontal', true) == 'center') ? ' selected="selected"' : ''; ?> style="padding-right:10px;"><?php esc_attr_e('center', 'Current'); ?></option>
                            </select>
                            <label for="<?php echo $simple_background['id']; ?>_img_position_vertical"><?php esc_html_e('Vertical:', 'Current'); ?></label>
                            <select name="<?php echo $simple_background['id']; ?>_img_position_vertical" id="<?php echo $simple_background['id']; ?>_img_position_vertical">
                                <option value="top"<?php echo (get_post_meta($post->ID, $simple_background['id'].'_img_position_vertical', true) == 'top') ? ' selected="selected"' : ''; ?>><?php esc_attr_e('top', 'Current'); ?></option>
                                <option value="center"<?php echo (get_post_meta($post->ID, $simple_background['id'].'_img_position_vertical', true) == 'center') ? ' selected="selected"' : ''; ?>><?php esc_attr_e('center', 'Current'); ?></option>
                                <option value="bottom"<?php echo (get_post_meta($post->ID, $simple_background['id'].'_img_position_vertical', true) == 'bottom') ? ' selected="selected"' : ''; ?> style="padding-right:10px;"><?php esc_attr_e('bottom', 'Current'); ?></option>
                            </select></div>
                            </div>
                            
			          
                            
               <div class="params_div1">
              <div class="params_label"><label for="<?php echo $simple_background['id']; ?>_color"><?php esc_html_e('Page background color', 'Current'); ?></label></div>
              <div class="params_value"><div id="wrap_<?php echo $simple_background['id']; ?>_color" class="Current_color_box" style="background-color: #<?php echo get_post_meta($post->ID, $simple_background['id'].'_color', true); ?>;"></div>
                                         #<input name="<?php echo $simple_background['id']; ?>_color" id="<?php echo $simple_background['id']; ?>_color" type="text" maxlength="7" size="6" style="margin:7px 10px 0 0" value="<?php echo get_post_meta($post->ID, $simple_background['id'].'_color', true); ?>" /> <span class="legend"><?php esc_html_e('Accepts hex color code, without the "#"', 'Current'); ?> </span> 
               <br class="clear" />                        
              </div>
              </div>
<script type="text/javascript">
		    //<![CDATA[
			var sendtoeditor;
		    jQuery(document).ready( function($) {
						
				$('input#upload_<?php echo $simple_background['id']; ?>_img.upmediabt').bind('click', function() {
					formfield = $(this).attr('id').replace("upload_", "");
					sendtoeditor = window.send_to_editor; 
					window.send_to_editor = function(html) {
						imgurl = $('img',html).attr('src');
						$('#'+ formfield).val(imgurl);
						tb_remove();
						window.send_to_editor = sendtoeditor;
					};
					
					tb_show('', 'media-upload.php?type=image&amp;TB_iframe=true');
					return false;
				});
				
		    });
		    //]]>
		</script>           
<?php				}
			}
 function save_simple_background_mbox($post_id) {
				global $simple_background_array;
					foreach($simple_background_array as $simple_background) {
						if (isset($_POST[$simple_background['id'].'_img'])) {
							update_post_meta($post_id, $simple_background['id'].'_img', $_POST[$simple_background['id'].'_img']);
						}
						if (isset($_POST[$simple_background['id'].'_img_repeat'])) {
							update_post_meta($post_id, $simple_background['id'].'_img_repeat', $_POST[$simple_background['id'].'_img_repeat']);
						}
						if (isset($_POST[$simple_background['id'].'_img_position_horizontal'])) {
							update_post_meta($post_id, $simple_background['id'].'_img_position_horizontal', $_POST[$simple_background['id'].'_img_position_horizontal']);
						}
						if (isset($_POST[$simple_background['id'].'_img_position_vertical'])) {
							update_post_meta($post_id, $simple_background['id'].'_img_position_vertical', $_POST[$simple_background['id'].'_img_position_vertical']);
						}
						if (isset($_POST[$simple_background['id'].'_color'])) {
							update_post_meta($post_id, $simple_background['id'].'_color', $_POST[$simple_background['id'].'_color']);
						}
						
					}
			}
 function render_cpanel_simple_background( $options ) {
				global $simple_background_array;
				
				foreach($simple_background_array as $simple_background_item) {
?>
            <div class="params_div1">
            <div class="params_label"><label for="<?php echo $simple_background_item['id']; ?>_img"><?php esc_html_e('Enter a URL or upload an image:', 'Current'); ?></label></div>
            <div class="params_value" id="simple_background_url"><input name="Current_options[<?php echo $simple_background_item['id']; ?>_img]" type="text" id="<?php echo $simple_background_item['id']; ?>_img" value="<?php if( isset($options[$simple_background_item['id'].'_img']) && $options[$simple_background_item['id'].'_img']!="" ){ echo esc_url($options[$simple_background_item['id'].'_img']); } ?>" size="65" />
                            <input id="upload_<?php echo $simple_background_item['id']; ?>_img" type="button" value="<?php esc_attr_e('Upload Image', 'Current'); ?>" class="button-secondary upmediabt" /></div>
                            </div>
        <div class="params_div2">
        	<div class="params_label"><label for="<?php echo $simple_background_item['id']; ?>_img_repeat"><?php esc_html_e('Background Properties:', 'Current'); ?></label></div>
            <div class="params_value"><select name="Current_options[<?php echo $simple_background_item['id']; ?>_img_repeat]" id="<?php echo $simple_background_item['id']; ?>_img_repeat">
                                <option value="no-repeat"<?php echo ($options[$simple_background_item['id'].'_img_repeat'] == 'no-repeat') ? ' selected="selected"' : ''; ?>><?php esc_attr_e('No Repeat', 'Current'); ?></option>
                                <option value="repeat-x"<?php echo ($options[$simple_background_item['id'].'_img_repeat'] == 'repeat-x') ? ' selected="selected"' : ''; ?>><?php esc_attr_e('Repeat only Horizontally', 'Current'); ?></option>
                                <option value="repeat-y"<?php echo ($options[$simple_background_item['id'].'_img_repeat'] == 'repeat-y') ? ' selected="selected"' : ''; ?>><?php esc_attr_e('Repeat only Vertically', 'Current'); ?></option>
                                <option value="repeat"<?php echo ($options[$simple_background_item['id'].'_img_repeat'] == 'repeat') ? ' selected="selected"' : ''; ?> style="padding-right:10px;"><?php esc_attr_e('Repeat both Vertically and Horizontally', 'Current'); ?></option>
                            </select>
                            <label for="<?php echo $simple_background_item['id']; ?>_img_position_horizontal"><?php esc_html_e('Horizontal:', 'Current'); ?></label>
                            <select name="Current_options[<?php echo $simple_background_item['id']; ?>_img_position_horizontal]" id="<?php echo $simple_background_item['id']; ?>_img_position_horizontal">
                                <option value="left"<?php echo ($options[$simple_background_item['id'].'_img_position_horizontal'] == 'left') ? ' selected="selected"' : ''; ?>><?php esc_attr_e('left', 'Current'); ?></option>
                                <option value="right"<?php echo ($options[$simple_background_item['id'].'_img_position_horizontal'] == 'right') ? ' selected="selected"' : ''; ?>><?php esc_attr_e('right', 'Current'); ?></option>
                                <option value="center"<?php echo ($options[$simple_background_item['id'].'_img_position_horizontal'] == 'center') ? ' selected="selected"' : ''; ?> style="padding-right:10px;"><?php esc_attr_e('center', 'Current'); ?></option>
                            </select>
                            <label for="<?php echo $simple_background_item['id']; ?>_img_position_vertical"><?php esc_html_e('Vertical:', 'Current'); ?></label>
                            <select name="Current_options[<?php echo $simple_background_item['id']; ?>_img_position_vertical]" id="<?php echo $simple_background_item['id']; ?>_img_position_vertical">
                                <option value="top"<?php echo ($options[$simple_background_item['id'].'_img_position_vertical'] == 'top') ? ' selected="selected"' : ''; ?>><?php esc_attr_e('top', 'Current'); ?></option>
                                <option value="center"<?php echo ($options[$simple_background_item['id'].'_img_position_vertical'] == 'center') ? ' selected="selected"' : ''; ?>><?php esc_attr_e('center', 'Current'); ?></option>
                                <option value="bottom"<?php echo ($options[$simple_background_item['id'].'_img_position_vertical'] == 'bottom') ? ' selected="selected"' : ''; ?> style="padding-right:10px;"><?php esc_attr_e('bottom', 'Current'); ?></option>
                            </select></div>
                            </div>
			<div class="params_div1">
                <div class="params_label">
               <label for="<?php echo $simple_background_item['id']; ?>_fixed">
                <?php esc_html_e('Fixed Background image', 'Current'); ?>:
			    </label>
                </div>
                <div class="params_value">
				<input name="Current_options[<?php echo $simple_background_item['id']; ?>_fixed]" type="checkbox" id="<?php echo $simple_background_item['id']; ?>_fixed" value="yes" <?php if(isset($options[$simple_background_item['id'].'_color'])) checked('yes', $options[$simple_background_item['id'].'_fixed']); ?> /> <span class="legend"><?php esc_html_e('Fix the background image to not scroll with the page', 'Current'); ?> </span> 
			    </div>
            </div>                             
               <div class="params_div2">
              <div class="params_label"><label for="<?php echo $simple_background_item['id']; ?>_color"><?php esc_html_e('Page background color', 'Current'); ?></label></div>
              <div class="params_value"><div id="wrap_<?php echo $simple_background_item['id']; ?>_color" class="Current_color_box" style="background-color: #<?php echo ($options[$simple_background_item['id'].'_color']) ? esc_attr($options[$simple_background_item['id'].'_color']) : 'ffffff'; ?>;"></div>
                                         #<input name="Current_options[<?php echo $simple_background_item['id']; ?>_color]" id="<?php echo $simple_background_item['id']; ?>_color" type="text" maxlength="7" size="6" style="margin:7px 10px 0 0" value="<?php echo ($options[$simple_background_item['id'].'_color']) ? esc_attr($options[$simple_background_item['id'].'_color']) : 'ffffff'; ?>" />
               <br class="clear" />                        
              </div>
              </div>
              
             
				
		<?php display_save_changes_button(1);
				}
}?>