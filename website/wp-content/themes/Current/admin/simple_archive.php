<?php function render_cpanel_simple_archive( $options ) {
	global $Current_options;
		$cats_array=get_categories();
        ?>
                 
		<div class="params_div1">
        	<div class="params_label">
        		<label for="simple_archive_excerpt"><?php esc_html_e('Excerpt', 'Current'); ?>:</label></div>
			<div class="params_value">
                <input name="Current_options[simple_archive_excerpt]" type="checkbox" id="simple_archive_excerpt" value="yes" <?php if(isset($options['simple_archive_excerpt'])) checked('yes', $options['simple_archive_excerpt']); ?> /> <span class="legend"><?php esc_html_e('Display Post\'s excerpt, instead of the Post\'s content', 'Current'); ?> </span> 
			</div>
        </div>

			<div class="params_div2">
            <div class="params_label">
            <label for="simple_archive_readmore">
			<?php esc_html_e('"Read more" text', 'Current'); ?>:
			    </label></div>
                <div class="params_value">
                
				<input name="Current_options[simple_archive_readmore]" type="text" id="simple_archive_readmore" value="<?php echo $options['simple_archive_readmore'];?>" size="30" maxlength="100" />
				<span class="legend"><?php esc_html_e("Leave blank for turning off read more button", 'Current'); ?></span>
			    </div>
                </div>

			<div class="params_div1">
                <div class="params_label">
               <label for="simple_archive_author">
                <?php esc_html_e('Show Author', 'Current'); ?>:
			    </label>
                </div>
                <div class="params_value">
				<input name="Current_options[simple_archive_author]" type="checkbox" id="simple_archive_author" value="yes" <?php if(isset($options['simple_archive_author'])) checked('yes', $options['simple_archive_author']); ?> /> <span class="legend"><?php esc_html_e('Show Author info for each post', 'Current'); ?> </span> 
			    </div>
            </div>
                
            <div class="params_div2">
            <div class="params_label">
				<label for="simple_archive_category">
            <?php esc_html_e('Show Category(s)', 'Current'); ?>:</label>
            </div>
            
			<div class="params_value">
				
				    <input name="Current_options[simple_archive_category]" type="checkbox" id="simple_archive_category" value="yes" <?php if(isset($options['simple_archive_category'])) checked('yes', $options['simple_archive_category']); ?> /> 
				 <span class="legend"><?php esc_html_e('Show Category (s) info for each post', 'Current'); ?> </span> 
			    </div>
                </div>
                
			<div class="params_div1">
            <div class="params_label">
            	<label for="simple_archive_tags">
            <?php esc_html_e('Show Tags', 'Current'); ?>:</label>
            </div>
            <div class="params_value">
				<input name="Current_options[simple_archive_tags]" type="checkbox" id="simple_archive_tags" value="yes" <?php if(isset($options['simple_archive_tags'])) checked('yes', $options['simple_archive_tags']); ?> /> <span class="legend"><?php esc_html_e('Show Tag(s) info for each post', 'Current'); ?> </span> 
				    
			    </div>
                </div> 

			<div class="params_div2">
            <div class="params_label">
            	<label for="simple_archive_comments">
            <?php esc_html_e('Show Comment(s)', 'Current'); ?>:</label>
            </div>
            <div class="params_value">
				<input name="Current_options[simple_archive_comments]" type="checkbox" id="simple_archive_comments" value="yes" <?php if(isset($options['simple_archive_comments'])) checked('yes', $options['simple_archive_comments']); ?> />  <span class="legend"><?php esc_html_e('Show Comment(s) info for each post', 'Current'); ?> </span> 
				    
			</div>
            </div>
            
			<div class="params_div1">
                <div class="params_label">
               <label for="simple_archive_author">
                <?php esc_html_e('Remove "Archive for" title', 'Current'); ?>:
			    </label>
                </div>
                <div class="params_value">
				<input name="Current_options[remove_archivefor]" type="checkbox" id="remove_archivefor" value="yes" <?php if(isset($options['remove_archivefor'])) checked('yes', $options['remove_archivefor']); ?> /> <span class="legend"><?php esc_html_e('Remove the "Archive for" text from the category title', 'Current'); ?> </span> 
			    </div>
            </div>            
            
            <div class="params_div2">
                <div class="params_label"><label for="aspect"><?php esc_html_e('Thumbnail aspect ratio', 'Current'); ?>:</label></div>
                <div class="params_value">
                                        <input name="Current_options[simple_archive_aspect]" type="text" id="aspect" value="<?php echo ($options['simple_archive_aspect']) ? $options['simple_archive_aspect'] : $Current_options['image_aspect']; ?>" size="5" maxlength="5" />:1 
                <span class="legend"><?php esc_html_e('use value greater than 1 for landscape mode and lower value for portrait mode', 'Current'); ?> </span>                 
                </div>
            </div>                          
         
    <?php

				display_save_changes_button(1); 
}


?>