<?php
	function render_cpanel_simple_footer( $options ) {
				?>
				                       
                <div class="params_div2">
                <div class="params_label">        
				<label for="show_cp_in_footer"><?php esc_html_e('Show Copyright text in footer', 'Current'); ?>:</label></div>
                <div class="params_value">
				    <input name="Current_options[simple_footer_cp]" type="checkbox" id="simple_footer_cp" value="yes" <?php if(isset($options['simple_footer_cp'])) checked('yes',  $options['simple_footer_cp']); ?> />
</div>
</div>
				
<div class="params_div1">
<div class="params_label">                
                <label for="simple_footer_wp"><?php esc_html_e('Show WordPress credits link', 'Current'); ?>:</label></div>
                <div class="params_value">
                    <input name="Current_options[simple_footer_wp]" type="checkbox" id="simple_footer_wp" value="yes" <?php if(isset($options['simple_footer_wp'])) checked('yes', $options['simple_footer_wp']); ?> />
                </div>
                </div>
                
<div class="params_div2">
<div class="params_label">                
                <label for="simple_footer_tc"><?php esc_html_e('Show theme credits link', 'Current'); ?>:</label></div>
                <div class="params_value">
                    <input name="Current_options[simple_footer_tc]" type="checkbox" id="simple_footer_tc" value="yes" <?php if(isset($options['simple_footer_tc'])) checked('yes', $options['simple_footer_tc']); ?> />
                </div>
                </div>     
                           
                <div class="params_div1">
                <div class="params_label">
                
                <label for="simple_footer_entries"><?php esc_html_e('Show Entries (RSS) link', 'Current'); ?>:</label></div>
                <div class="params_value">
				    <input name="Current_options[simple_footer_entries]" type="checkbox" id="simple_footer_entries" value="yes" <?php if(isset($options['simple_footer_entries'])) checked('yes', $options['simple_footer_entries']); ?> />
				</div>
                </div>
                
                <div class="params_div2">
                <div class="params_label">
                
                <label for="simple_footer_comments"><?php esc_html_e('Show Comments (RSS) link', 'Current'); ?>:
				   </label></div>
                   <div class="params_value">
				    <input name="Current_options[simple_footer_comments]" type="checkbox" id="simple_footer_comments" value="yes" <?php if(isset($options['simple_footer_comments'])) checked('yes', $options['simple_footer_comments']); ?> />
				</div>
                </div>
                
                <div class="params_div1">
                <div class="params_label">
				<label for="simple_footer_top"><?php esc_html_e('Show "Back to Top" link', 'Current'); ?>:</label>
                </div>
                <div class="params_value">
				    <input name="Current_options[simple_footer_top]" type="checkbox" id="simple_footer_top" value="yes" <?php if(isset($options['simple_footer_top'])) checked('yes', $options['simple_footer_top']); ?> />
				   
				</div>
                </div>
                <div class="params_div2">
                <div class="params_label">           
			<label for="simple_footer_message">
            <?php esc_html_e('Copyright Message', 'Current'); ?>:</label></div>
            <div class="params_value">
				<textarea style="width: 98%; font-size: 12px;" id="simple_footer_message" rows="2" cols="60" name="Current_options[simple_footer_message]"><?php if($options['simple_footer_message']) { echo esc_attr($options['simple_footer_message']); } ?></textarea>
				</div>
                </div>

				<?php
				display_save_changes_button(1); 				
			}?>