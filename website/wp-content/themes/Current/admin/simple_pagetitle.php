<?php
	function render_cpanel_simple_pagetitle( $options ) {
		
		global $simple_pagetitle_array; 
		
		foreach($simple_pagetitle_array as $pagetitle) {
				?>
				<div class="params_div1">   
						<div class="params_label">
							 <label for="simple_pagetitle_enable" ><?php esc_html_e('Display page title:', 'Current'); ?> </label></div>
									<div class="params_value">
									<input type="checkbox"  name="Current_options[simple_pagetitle_enable]" id="simple_pagetitle_enable" value="yes" <?php if(isset($options['simple_pagetitle_enable'])) checked('yes', $options['simple_pagetitle_enable']); ?> />
									</div>
						 </div>
				<?php if(isset($pagetitle['frontpage'])) { ?>
                       <div class="params_div2">
                        <div class="params_label">
                        <label for="simple_pagetitle_home_message">
                    <?php esc_html_e('Home page title alternate', 'Current'); ?>:</label></div>
                        <div class="params_value">
                        <textarea  name="Current_options[simple_pagetitle_home_message]" id="simple_pagetitle_home_message"><?php echo $options['simple_pagetitle_home_message']; ?></textarea>
                      </div>
                      </div>
                      

				<?php
				}
				
				display_save_changes_button(2); 
				
		}
	}?>