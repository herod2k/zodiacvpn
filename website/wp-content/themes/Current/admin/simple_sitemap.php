<?php
    if((isset($_GET['post']) && get_post_meta(mysql_real_escape_string($_GET['post']),'_wp_page_template',true) == 'sitemap.php') || (isset($_POST['post_ID']) && get_post_meta(mysql_real_escape_string($_POST['post_ID']),'_wp_page_template',true) == 'sitemap.php')) {
        add_action( 'load-page.php', 'simple_sitemap_mbox_setup' );
        add_action('save_post', 'save_simple_sitemap_mbox');
    }
 function simple_sitemap_mbox_setup() {
        add_action( 'add_meta_boxes', 'add_simple_sitemap_mbox_setup' );
    }
 function add_simple_sitemap_mbox_setup() {

        add_meta_box(
            'simple-sitemap-mbox',			// Unique ID
            esc_html__( 'Sitemap Settings', 'Current' ),		// Title
            'simple_sitemap_mbox',		// Callback function
            'page',					// Admin page (or post type)
            'normal',					// Context
            'core'					// Priority
        );
    }
 function simple_sitemap_mbox( $object, $box ) {
		global $simple_sitemap_array, $global_blocks, $post, $Current_options;
		
		foreach($simple_sitemap_array as $sitemap) {
        ?>
       <div class="params_div1">
                <div class="params_label"><label for="exclude_page_from_sitemap">
                <?php esc_html_e('Exclude Pages', 'Current'); ?>:</label></div>
                        <div class="params_value">
                        
        <?php				$pages_array = get_pages('hierarchical=0'); ?>

        				<select id="simple_sitemap_pages" multiple="multiple" name="<?php echo $sitemap['id'];?>_pages[]">
           <?php		$pages = explode("|", get_post_meta($post->ID, $sitemap['id'].'_pages', true));
       			    	foreach ($pages_array as $page_obj) :
			                 $cur_page_ID = $page_obj->ID; ?>
                            <option value="<?php echo $cur_page_ID;?>" <?php echo in_array($cur_page_ID, $pages)?"selected='selected'":"";?> ><?php echo $page_obj->post_title; ?></option>
                         <?php	endforeach; ?>

                        </select> 

<span class="legend"><?php esc_html_e('You can select pages by holding down the "ctrl" key. You can also de-select an item by holding down "ctrl" and clicking a selected item.', 'Current'); ?> </span> 


               </div>
         </div>
        
        <div class="params_div2">
        <div class="params_label">
        <label for="exclude_categories">
        			    <?php esc_html_e('Exclude categories', 'Current'); ?>:</label></div>
		<div class="params_value">
        		<?php				$cats_array = get_categories(); ?>
				<select id="simple_sitemap_cats" multiple="multiple" name="<?php echo $sitemap['id'];?>_cats[]">
    <?php			$cats = explode("|", get_post_meta($post->ID, $sitemap['id'].'_cats', true));
					foreach ($cats_array as $cat_obj) :
						$cur_cat_ID = $cat_obj->term_id; ?>
						<option value="<?php echo $cur_cat_ID;?>" <?php echo in_array($cur_cat_ID, $cats)?"selected='selected'":"";?> ><?php echo $cat_obj->name; ?></option>
    <?php			endforeach; ?>
                </select> 
<span class="legend"><?php esc_html_e('You can select multiple categories by holding down the "ctrl" key. You can also de-select an item by holding down "ctrl" and clicking a selected item.', 'Current'); ?> </span>

			    </div>
                </div>
            
<div class="params_div1">
            <div class="params_label">
            	<label for="show_monthly_archives">
            <?php esc_html_e('Show', 'Current'); ?>:</label>
            </div>
            <div class="params_value">
				<label for="show_monthly_archives">
            <?php esc_html_e('Monthly archives', 'Current'); ?>:</label> <input name="<?php echo $sitemap['id'];?>_show_monthly_archives" type="checkbox" id="show_monthly_archives" value="yes" <?php checked('yes',  get_post_meta($post->ID, $sitemap['id'].'_show_monthly_archives', true)); ?> />
            <label for="show_site_feeds">
            <?php esc_html_e('Site feeds', 'Current'); ?>:</label>
				<input name="<?php echo $sitemap['id'];?>_show_site_feeds" type="checkbox" id="show_site_feeds" value="yes" <?php checked('yes', get_post_meta($post->ID, $sitemap['id'].'_show_site_feeds', true)); ?> />
             <label for="show_tags_sitemap">
            <?php esc_html_e('Tags', 'Current'); ?>:</label>
            <input name="<?php echo $sitemap['id'];?>_show_tags_sitemap" type="checkbox" id="show_tags_sitemap" value="yes" <?php  checked('yes', get_post_meta($post->ID, $sitemap['id'].'_show_tags_sitemap', true)); ?> />   
			    </div>
                </div>	
	<div class="params_div2">
            <div class="params_label">
            	<label for="show_date_sitemap">
            <?php esc_html_e('Articles list', 'Current'); ?>:</label>
            </div>
            <div class="params_value">
				<label for="show_date_sitemap">
            <?php esc_html_e('Show date', 'Current'); ?>:</label> <input name="<?php echo $sitemap['id'];?>_show_date_sitemap" type="checkbox" id="show_date_sitemap" value="yes" <?php checked('yes', get_post_meta($post->ID, $sitemap['id'].'_show_date_sitemap', true)); ?> />
            <label for="show_author_sitemap">
            <?php esc_html_e('Show author', 'Current'); ?>:</label>
				<input name="<?php echo $sitemap['id'];?>_show_author_sitemap" type="checkbox" id="show_author_sitemap" value="yes" <?php checked('yes', get_post_meta($post->ID, $sitemap['id'].'_show_author_sitemap', true)); ?> />
             <label for="show_comments_sitemap">
            <?php esc_html_e('Show comments link', 'Current'); ?>:</label>
            <input name="<?php echo $sitemap['id'];?>_show_comments_sitemap" type="checkbox" id="show_comments_sitemap" value="yes" <?php checked('yes', get_post_meta($post->ID, $sitemap['id'].'_show_comments_sitemap', true)); ?> />   
			    </div>
	</div>        
    <?php
		}
    }
 function save_simple_sitemap_mbox($post_id) {
        global $simple_sitemap_array, $Current_options;
        foreach($simple_sitemap_array as $sitemap) {
			$aspect = $Current_options['image_aspect'];
			
			if (isset($_POST[$sitemap['id'].'_cats'])) {
                update_post_meta($post_id, $sitemap['id'].'_cats', implode("|", $_POST[$sitemap['id'].'_cats']));
            }
			else
				update_post_meta($post_id, $sitemap['id'].'_cats', '');
				
			if (isset($_POST[$sitemap['id'].'_pages'])) {
                update_post_meta($post_id, $sitemap['id'].'_pages', implode("|", $_POST[$sitemap['id'].'_pages']));
            }
			else
				update_post_meta($post_id, $sitemap['id'].'_pages', '');
			
            if (isset($_POST[$sitemap['id'].'_show_monthly_archives'])) {
                update_post_meta($post_id, $sitemap['id'].'_show_monthly_archives', $_POST[$sitemap['id'].'_show_monthly_archives']);
            }
			else
				update_post_meta($post_id, $sitemap['id'].'_show_monthly_archives', '');
				
			if (isset($_POST[$sitemap['id'].'_show_site_feeds'])) {
                update_post_meta($post_id, $sitemap['id'].'_show_site_feeds', $_POST[$sitemap['id'].'_show_site_feeds']);
            }
			else
				update_post_meta($post_id, $sitemap['id'].'_show_site_feeds', '');	
			
			if (isset($_POST[$sitemap['id'].'_show_tags_sitemap'])) {
                update_post_meta($post_id, $sitemap['id'].'_show_tags_sitemap', $_POST[$sitemap['id'].'_show_tags_sitemap']);
            }
			else
				update_post_meta($post_id, $sitemap['id'].'_show_tags_sitemap', '');	
				
			if (isset($_POST[$sitemap['id'].'_show_date_sitemap'])) {
                update_post_meta($post_id, $sitemap['id'].'_show_date_sitemap', $_POST[$sitemap['id'].'_show_date_sitemap']);
            }
			else
				update_post_meta($post_id, $sitemap['id'].'_show_date_sitemap', '');
				
			if (isset($_POST[$sitemap['id'].'_show_author_sitemap'])) {
                update_post_meta($post_id, $sitemap['id'].'_show_author_sitemap', $_POST[$sitemap['id'].'_show_author_sitemap']);
            }
			else
				update_post_meta($post_id, $sitemap['id'].'_show_author_sitemap', '');				
				

			if (isset($_POST[$sitemap['id'].'_show_comments_sitemap'])) {
                update_post_meta($post_id, $sitemap['id'].'_show_comments_sitemap', $_POST[$sitemap['id'].'_show_comments_sitemap']);
            }
			else
				update_post_meta($post_id, $sitemap['id'].'_show_comments_sitemap', '');
				
        }
    }?>