<?php
	function render_cpanel_general_settings( $options ) {
				global $general_settings_array;
				
				foreach($general_settings_array as $general_settings_item) {
?><div class="params_title"><?php esc_html_e($general_settings_item['name'], 'Current'); ?></div><?php
					$keyclass=1;
					foreach($general_settings_item as $key => $value) {
						if(strpos($key, ",")>0) {
							$exploded = explode(",", $key);
	?>				
				<div class="params_div<?php echo $keyclass;?>">   
						<div class="params_label">
							 <label><?php esc_html_e($value, 'Current'); ?>: </label></div>
									<div class="params_value">
<?php 
	$paramname="";
	switch($exploded[0]) {
		case "css":
			$paramname = str_replace(" ", "_", str_replace(">","_", str_replace(":","_",str_replace(".","_",str_replace("#", "_", str_replace(",", "_", $key))))));
		break;
		case "param":
			$paramname = $exploded[1];
		break;
	}

		switch($exploded[sizeof($exploded)-1]) {
			case "px":
	?>                                    
									<input name="Current_options[<?php echo $paramname; ?>]" type="text" id="<?php echo $paramname; ?>" value="<?php echo esc_attr($options[$paramname]);?>"  /> px
                                    
<?php
			break;
			case "checkbox":
	?>                                    
									<input name="Current_options[<?php echo $paramname; ?>]" type="checkbox" id="<?php echo $paramname; ?>" value="yes" <?php if(isset($options[$paramname])) checked("yes", esc_attr($options[$paramname]));?> />
                                    
<?php
			break;

			case "color":
			?>
            
            <div id="wrap_<?php echo $paramname; ?>" class="Current_color_box" style="background-color: #<?php echo ($options[$paramname]) ? esc_attr($options[$paramname]) : '000000'; ?>;"></div>
                                         #<input name="Current_options[<?php echo $paramname; ?>]" id="<?php echo $paramname; ?>" type="text" maxlength="7" size="6" style="margin:7px 10px 0 0" value="<?php echo ($options[$paramname]) ? esc_attr($options[$paramname]) : '000000'; ?>" />
               <br class="clear" />
            
            <?php
			break;
			default:
				?>
                <input name="Current_options[<?php echo $paramname; ?>]" type="text" id="<?php echo $paramname; ?>" value="<?php echo esc_attr($options[$paramname]);?>"  />
                
                <?php
		}

?>                                    
                                    
									</div>
						 </div>					
	<?php			
						$keyclass=($keyclass==1)?2:1;
						}
					}
					display_save_changes_button($keyclass); 
				}
				
								
			}?>