<?php
    if((isset($_GET['post']) && get_post_meta(mysql_real_escape_string($_GET['post']),'_wp_page_template',true) == 'portfolio.php') || (isset($_POST['post_ID']) && get_post_meta(mysql_real_escape_string($_POST['post_ID']),'_wp_page_template',true) == 'portfolio.php')) {
        add_action( 'load-page.php', 'simple_portfolio_mbox_setup' );
        add_action('save_post', 'save_simple_portfolio_mbox');
    }
 function simple_portfolio_mbox_setup() {
        add_action( 'add_meta_boxes', 'add_simple_portfolio_mbox_setup' );
    }
 function add_simple_portfolio_mbox_setup() {

        add_meta_box(
            'simple-portfolio-mbox',			// Unique ID
            esc_html__( 'Portfolio Settings', 'Current' ),		// Title
            'simple_portfolio_mbox',		// Callback function
            'page',					// Admin page (or post type)
            'normal',					// Context
            'core'					// Priority
        );
    }
 function simple_portfolio_mbox( $object, $box ) {
		global $simple_portfolio_array, $global_blocks, $post, $Current_options;
		
		foreach($simple_portfolio_array as $portfolio) {
			$cats_array=get_categories();
        ?>
        <div class="params_div1"><div class="params_label"><label for="simple_portfolio_cats"><?php esc_html_e('Assign the Category(s)', 'Current'); ?>:</label></div>
            <div class="params_value">
                <select id="simple_portfolio_cats" multiple="multiple" name="<?php echo $portfolio['id'];?>_cats[]">
    <?php			$cats = explode("|", get_post_meta($post->ID, $portfolio['id'].'_cats', true));
					foreach ($cats_array as $cat_obj) :
						$cur_cat_ID = $cat_obj->term_id; ?>
						<option value="<?php echo $cur_cat_ID;?>" <?php echo in_array($cur_cat_ID, $cats)?"selected='selected'":"";?> ><?php echo $cat_obj->name; ?></option>
    <?php			endforeach; ?>
                </select> <span class="legend"><?php esc_html_e('You can select multiple categories by holding down the "ctrl" key', 'Current'); ?> </span> 
             </div>
         </div>
         
         <div class="params_div2"><div class="params_label"><label for="simple_portfolio_items_sortable"><?php esc_html_e('Display Sort options', 'Current'); ?>:</label></div>
         <div class="params_value">
         <input name="<?php echo $portfolio['id'];?>_sortable" type="checkbox" id="simple_portfolio_items_sortable" value="yes" <?php checked('yes', get_post_meta($post->ID, $portfolio['id'].'_sortable', true));?>  /> <span class="legend"><?php esc_html_e('Note: The sort by categories options will be available only in case of multiple categories assigned to the portfolio page', 'Current'); ?></span>
         </div>
         </div>
         
         
         <div class="params_div1">
         	<div class="params_label"><label for="portfolio_items_per_page"><?php esc_html_e('Portfolio grid', 'Current'); ?>:</label></div>
		<div class="params_value">
 <label for="portfolio_items_per_page"><?php esc_html_e('with', 'Current'); ?> <input name="<?php echo $portfolio['id'];?>_items_per_page" type="text" id="portfolio_items_per_page" value="<?php echo get_post_meta($post->ID, $portfolio['id'].'_items_per_page', true)?get_post_meta($post->ID, $portfolio['id'].'_items_per_page', true):8;?>" size="5" maxlength="5" /> <?php esc_html_e('items per page', 'Current'); ?></label> <label for="portfolio_cols_per_row"> <?php esc_html_e('and', 'Current'); ?> 
                    <select name="<?php echo $portfolio['id'];?>_cols_per_row"  id="portfolio_cols_per_row" >
                    <?php 
					$avail_width = calc_width($portfolio['column'], $global_blocks[$portfolio['row']]);
					for($i=1; $i<=$avail_width; $i++) {
					
						if(($avail_width % $i)==0) {
							echo "<option value='$i'";
							if(get_post_meta($post->ID, $portfolio['id'].'_cols_per_row', true) == $i)
								echo " selected='selected' ";
							echo " >$i</option>";
						}
					}
					
					?>
                    </select>              
               <?php esc_html_e('columns per row', 'Current'); ?>  </label>
               </div>
               </div>
               
               
               <div class="params_div2">
                     <div class="params_label">
                    <label for="portfolio_item_desc_source">
                    	<?php esc_html_e('Item Description Source', 'Current'); ?>:</label>
                     </div>
                     <div class="params_value">

                         <select name="<?php echo $portfolio['id'];?>_desc_source"  id="portfolio_item_desc_source" >
                         <option value="0"<?php echo (get_post_meta($post->ID, $portfolio['id'].'_desc_source', true)==0)?' selected="selected"':''?>>None</option>
                            <option value="1"<?php echo (get_post_meta($post->ID, $portfolio['id'].'_desc_source', true)==1)?' selected="selected"':''?>>Post Content</option>
                            <option value="2"<?php echo (get_post_meta($post->ID, $portfolio['id'].'_desc_source', true)==2)?' selected="selected"':''?>>Post Excerpt</option>
                          </select> 
                     </div>
               </div> 
               
               <div class="params_div1">
                     <div class="params_label">
                    <label for="portfolio_item_title">
                    	<?php esc_html_e('Portfolio Item Title', 'Current'); ?>:</label>
                     </div>
                     <div class="params_value">

                         <select name="<?php echo $portfolio['id'];?>_title"  id="portfolio_item_title" >
                             <option value="0"<?php echo (get_post_meta($post->ID, $portfolio['id'].'_title', true)==0)?' selected="selected"':''?>>Do not Display</option>
                             <option value="1"<?php echo (get_post_meta($post->ID, $portfolio['id'].'_title', true)==1)?' selected="selected"':''?>>Display Normal</option>
                             <option value="2"<?php echo (get_post_meta($post->ID, $portfolio['id'].'_title', true)==2)?' selected="selected"':''?>>Display & Link to Post</option>
                          </select>  
                        <label for="portfolio_item_title_style"> 
                    	<?php esc_html_e('Style', 'Current'); ?>:</label>  
                        <select name="<?php echo $portfolio['id'];?>_title_style"  id="portfolio_item_title_style" >
                             <option value="h1"<?php echo (get_post_meta($post->ID, $portfolio['id'].'_title_style', true)=="h1")?' selected="selected"':''?>>h1</option>
                             <option value="h2"<?php echo (get_post_meta($post->ID, $portfolio['id'].'_title_style', true)=="h2")?' selected="selected"':''?>>h2</option>
                             <option value="h3"<?php echo (get_post_meta($post->ID, $portfolio['id'].'_title_style', true)=="h3")?' selected="selected"':''?>>h3</option>
                             <option value="h4"<?php echo (get_post_meta($post->ID, $portfolio['id'].'_title_style', true)=="h4")?' selected="selected"':''?>>h4</option>                             
                             <option value="h5"<?php echo (get_post_meta($post->ID, $portfolio['id'].'_title_style', true)=="h5")?' selected="selected"':''?>>h5</option>                             
                             <option value="h6"<?php echo (get_post_meta($post->ID, $portfolio['id'].'_title_style', true)=="h6")?' selected="selected"':''?>>h6</option>                             
                          </select>  
                     </div>
               </div> 
               
               
 				               
               
				<div class="params_div2">
                		<div class="params_label">
                               <label for="portfolio_item_author"> <?php esc_html_e('Show Portfolio Item Author', 'Current'); ?>:  
                               </label></div>
                        <div class="params_value">
                               <input name="<?php echo $portfolio['id'];?>_author" type="checkbox" id="portfolio_item_author" value="yes" <?php checked('yes', get_post_meta($post->ID, $portfolio['id'].'_author', true)); ?> /> <span class="legend"><?php esc_html_e('Show Author info for each post', 'Current'); ?> </span> 
                		</div>
                </div>


 							<div class="params_div1">
                               <div class="params_label">
                               <label for="portfolio_item_readmore"> <?php esc_html_e('Show Readmore button', 'Current'); ?>:  
                               </label></div>
                               <div class="params_value">
                               <input name="<?php echo $portfolio['id'];?>_readmore" type="text" id="simple_portfolio_readmore" value="<?php echo get_post_meta($post->ID, $portfolio['id'].'_readmore', true);?>" size="30" maxlength="100" />
				<span class="legend"><?php esc_html_e("Leave blank for turning off read more button", 'Current'); ?></span>
                               </div>
                               </div>


                               
<div class="params_div2">
<div class="params_label"><label for="aspect"><?php esc_html_e('Thumbnail aspect ratio', 'Current'); ?>:</label></div>
<div class="params_value">
						<input name="<?php echo $portfolio['id'];?>_aspect" type="text" id="aspect" value="<?php echo (get_post_meta($post->ID, $portfolio['id'].'_aspect', true)) ? get_post_meta($post->ID, $portfolio['id'].'_aspect', true) : $Current_options['image_aspect']; ?>" size="5" maxlength="5" />:1  <span class="legend"><?php esc_html_e('use value greater than 1 for landscape mode and lower value for portrait mode', 'Current'); ?> </span>                 
				    </div>
                    </div>                           
         
    <?php
		}
    }
 function save_simple_portfolio_mbox($post_id) {
        global $simple_portfolio_array, $Current_options;
        foreach($simple_portfolio_array as $portfolio) {
			$aspect = $Current_options['image_aspect'];
			$itemsperpage =  6;
			
			if (isset($_POST[$portfolio['id'].'_cats'])) {
                update_post_meta($post_id, $portfolio['id'].'_cats', implode("|", $_POST[$portfolio['id'].'_cats']));
            }
            if (isset($_POST[$portfolio['id'].'_sortable'])) {
                update_post_meta($post_id, $portfolio['id'].'_sortable', $_POST[$portfolio['id'].'_sortable']);
            }
			else
				update_post_meta($post_id, $portfolio['id'].'_sortable', '');
			
			
			if (isset($_POST[$portfolio['id'].'_author'])) {
                update_post_meta($post_id, $portfolio['id'].'_author', $_POST[$portfolio['id'].'_author']);
            }
			else
				update_post_meta($post_id, $portfolio['id'].'_author', '');	
				
			if (isset($_POST[$portfolio['id'].'_readmore'])) {
                update_post_meta($post_id, $portfolio['id'].'_readmore', $_POST[$portfolio['id'].'_readmore']);
            }
						
			
			if (isset($_POST[$portfolio['id'].'_aspect']) && is_numeric($_POST[$portfolio['id'].'_aspect'])) {
				$aspect = $_POST[$portfolio['id'].'_aspect'];
				if($aspect <= 0)
					$aspect = $Current_options['image_aspect'];
			}
			update_post_meta($post_id, $portfolio['id'].'_aspect', $aspect);
			
			if (isset($_POST[$portfolio['id'].'_items_per_page'])) {
                update_post_meta($post_id, $portfolio['id'].'_items_per_page', $_POST[$portfolio['id'].'_items_per_page']);
            }
			if (isset($_POST[$portfolio['id'].'_cols_per_row'])) {
                update_post_meta($post_id, $portfolio['id'].'_cols_per_row', $_POST[$portfolio['id'].'_cols_per_row']);
            }
			
			if (isset($_POST[$portfolio['id'].'_title'])) {
                update_post_meta($post_id, $portfolio['id'].'_title', $_POST[$portfolio['id'].'_title']);
            }
			if (isset($_POST[$portfolio['id'].'_title_style'])) {
                update_post_meta($post_id, $portfolio['id'].'_title_style', $_POST[$portfolio['id'].'_title_style']);
            }			
			if (isset($_POST[$portfolio['id'].'_desc_source'])) {
                update_post_meta($post_id, $portfolio['id'].'_desc_source', $_POST[$portfolio['id'].'_desc_source']);
            }
        }
    }?>