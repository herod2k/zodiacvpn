<?php add_action( 'load-post.php', 'simple_sidebar_mbox_setup' );
			add_action( 'load-post-new.php', 'simple_sidebar_mbox_setup' );
			add_action('save_post', 'save_simple_sidebar_mbox'); 
 function simple_sidebar_mbox_setup() {
				add_action( 'add_meta_boxes', 'add_simple_sidebar_mbox_setup' );
			} 
 function add_simple_sidebar_mbox_setup() {

				add_meta_box(
					'simple-sidebar-mbox',			// Unique ID
					esc_html__( 'Page Sidebar(s)', 'Current' ),		// Title
					'simple_sidebar_mbox',		// Callback function
					'',					// Admin page (or post type)
					'normal',					// Context
					'core'					// Priority
				);
			} 
 function simple_sidebar_mbox( $object, $box ) {
				global $simple_sidebar_array, $post, $Current_options;
				foreach($simple_sidebar_array as $sidebar) {
					
			?>
					<div class="params_div" >
                    <?php echo $sidebar['name'];?><br />
						<select name="<?php echo $sidebar['id'].'_val';?>">
						<option value="none" <?php echo (get_post_meta($post->ID, $sidebar['id'].'_val', true) == "none")?"selected='selected'":"";?>><?php _e('None', 'Current'); ?></option>
						<option value="Default sidebar" <?php echo (get_post_meta($post->ID, $sidebar['id'].'_val', true) == "Default sidebar")?"selected='selected'":"";?>>Default sidebar</option>
					<?php foreach(explode("|", $Current_options['simple_sidebar_list']) as $item) { ?>
							<option value="<?php echo $item; ?>" <?php echo (get_post_meta($post->ID, $sidebar['id'].'_val', true) == $item)?"selected='selected'":"";?>><?php echo $item; ?></option>
					<?php } ?>
							

						</select>
				</div>
			<?php

				}
				echo "<br style='clear:both;' />"; ?>
                <span class="legend"><?php printf( __('Select a sidebar(s) to be assigned to this page. You can define these sidebars at the %1$sAppearance -> Theme Options%2$s page', 'Current'), '<a href="themes.php?page=Current_settings_page">', '</a>' ); ?> </span> 
				<?php
			} 
 function save_simple_sidebar_mbox($post_id) {
				global $simple_sidebar_array;
				foreach($simple_sidebar_array as $sidebar) {
					if (isset($_POST[$sidebar['id'].'_val'])) {
						update_post_meta($post_id, $sidebar['id'].'_val', $_POST[$sidebar['id'].'_val']);
					}
				}
			} 
 function render_cpanel_simple_sidebar( $options ) {
				
               global $simple_sidebar_array; ?>
               
               <div class="params_title"><?php esc_html_e('Assign sidebars to the generic pages', 'Current'); ?></div>
				<div class="params_div1">   
			   <?php
				foreach($simple_sidebar_array as $sidebar) {
					
			?>
					
                    <div class="params_div" >
                    <?php echo $sidebar['name'];?><br />
						<select name="Current_options[simple_sidebar_<?php echo $sidebar['id'];?>_archive]">
						<option value="none" <?php echo ($options['simple_sidebar_'.$sidebar['id'].'_archive'] == "none")?"selected='selected'":"";?>><?php _e('None', 'Current'); ?></option>
						<option value="Default sidebar" <?php echo ($options['simple_sidebar_'.$sidebar['id'].'_archive'] == "Default sidebar")?"selected='selected'":"";?>>Default sidebar</option>
					<?php foreach(explode("|",  $options['simple_sidebar_list']) as $item) { ?>
							<option value="<?php echo $item; ?>" <?php echo ($options['simple_sidebar_'.$sidebar['id'].'_archive'] == $item)?"selected='selected'":"";?>><?php echo $item; ?></option>
					<?php } ?>
							

						</select>
				</div>
			<?php

				}
				echo "<br style='clear:both;' />"; ?>
                <span class="legend"><?php  _e('Select sidebar(s) to be assigned to the generic pages. These pages consist of archive, search results and other templates that are not assigned to any of the pages.', 'Current'); ?> </span> 
                 </div>     
				<div class="params_title"><?php esc_html_e('Add / Remove sidebars', 'Current'); ?></div>
				<div class="params_div1">   
						<div class="params_label">
							 <label for="simple_sidebar_new_name" ><?php esc_html_e('New sidebar name:', 'Current'); ?> </label></div>
									<div class="params_value">
									<input name="simple_sidebar_new_name" type="text" id="simple_sidebar_new_name" value="" maxlength="32" />
				
									<input type="button" class="button-secondary" value="<?php esc_html_e('Add new sidebar', 'Current'); ?>" id="add_simple_sidebar" />
									</div>
						 </div>
				
							 <div class="params_div2">
							 <div class="params_label" >      <label for="simple_sidebar_collection" >
										<?php esc_html_e('List of current sidebars: ', 'Current'); ?></label></div>
										<div class="params_value">
										<div style="float:left; width:510px">
										<select name="simple_sidebar_collection[]" id="simple_sidebar_collection" class="simple_sidebar_collection" multiple="multiple" style="height:200px; width:500px;">
				
										 </select></div>
										 <div style="float:left; width:200px"><input type="button" class="button-secondary" value="<?php _e('Remove sidebar(s)', 'Current');?>" id="remove_simple_sidebar" /></div>
									<input type="hidden" id="simple_sidebar_list" name="Current_options[simple_sidebar_list]" value="<?php echo isset($options['simple_sidebar_list'])?$options['simple_sidebar_list']:"";?>" /> 
									<br class="clear" />  
									</div>
									</div>
				<?php
				display_save_changes_button(1); 
			} ?>