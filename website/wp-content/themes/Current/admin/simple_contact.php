<?php
    if((isset($_GET['post']) && get_post_meta(mysql_real_escape_string($_GET['post']),'_wp_page_template',true) == 'contact.php') || (isset($_POST['post_ID']) && get_post_meta(mysql_real_escape_string($_POST['post_ID']),'_wp_page_template',true) == 'contact.php')) {
        add_action( 'load-page.php', 'simple_contact_mbox_setup' );
        add_action('save_post', 'save_simple_contact_mbox');
    }
 function simple_contact_mbox_setup() {
        add_action( 'add_meta_boxes', 'add_simple_contact_mbox_setup' );
    }
 function add_simple_contact_mbox_setup() {

        add_meta_box(
            'simple-contact-mbox',			// Unique ID
            esc_html__( 'Contact page Settings', 'Current' ),		// Title
            'simple_contact_mbox',		// Callback function
            'page',					// Admin page (or post type)
            'normal',					// Context
            'core'					// Priority
        );
    }
 function simple_contact_mbox( $object, $box ) {
		global $simple_contact_array, $global_blocks, $post, $Current_options;
		//get_post_meta($post->ID, $contact['id'].'_cats', true) 
		foreach($simple_contact_array as $contact) {
			$cats_array=get_categories();
        ?>
        
        <div class="params_div1">
        <div class="params_label">
			    <label for="contact_address"><?php esc_html_e('Display Address', 'Current'); ?>:</label>
                </div>
                <div class="params_value">
				<textarea style="width: 98%; font-size: 12px;" id="contact_address" rows="4" cols="60" name="<?php echo $contact['id'];?>_address"><?php echo get_post_meta($post->ID, $contact['id'].'_address', true); ?></textarea><br />
<span class="legend"><?php esc_html_e('This information you provide here will be displayed beside the contact form. You can use it to display your business address here. Accepts html markup.', 'Current'); ?> </span> 
			    </div>
                </div>
              
       
        <div class="params_title"><?php esc_html_e('Google map settings', 'Current'); ?></div>  
        <div class="params_div1">
        <div class="params_label"><label for="contact_google_map"><?php esc_html_e('Address for Google map display:', 'Current') ?>:</label></div>
		<div class="params_value">   
        <input id="contact_google_map" type="text" name="<?php echo $contact['id'];?>_google_map" value="<?php echo get_post_meta($post->ID, $contact['id'].'_google_map', true);?>" /> <span class="legend"><?php esc_html_e('The location you specify here will be displayed as a marker on the map', 'Current'); ?> </span>        
        </div>
        </div>
        
         <div class="params_div2">
                <div class="params_label"><label for="contact_zoom"><?php esc_html_e('Google map Zoom', 'Current'); ?>:</label></div>
                <div class="params_value">
                                        <input name="<?php echo $contact['id'];?>_google_map_zoom" type="text" id="contact_zoom" value="<?php echo (get_post_meta($post->ID, $contact['id'].'_google_map_zoom', true)) ? get_post_meta($post->ID, $contact['id'].'_google_map_zoom', true) : "16"; ?>" size="5" maxlength="5" /> <span class="legend"><?php esc_html_e('set the zoom level for the google map display', 'Current'); ?> </span> 
                </div>
            </div>          
        
         <div class="params_div1">
                <div class="params_label"><label for="contact_aspect"><?php esc_html_e('Google map aspect ratio', 'Current'); ?>:</label></div>
                <div class="params_value">
                                        <input name="<?php echo $contact['id'];?>_google_map_aspect" type="text" id="contact_aspect" value="<?php echo (get_post_meta($post->ID, $contact['id'].'_google_map_aspect', true)) ? get_post_meta($post->ID, $contact['id'].'_google_map_aspect', true) : $Current_options['image_aspect']; ?>" size="5" maxlength="5" />:1  
                <span class="legend"><?php esc_html_e('use value greater than 1 for landscape mode and lower value for portrait mode', 'Current'); ?> </span>                 
                </div>
            </div> 
            
                
        <div class="params_title"><?php esc_html_e('Contact form settings', 'Current'); ?></div>
        
		<div class="params_div1">
        <div class="params_label"><label for="contact_name"><?php esc_html_e('Name field', 'Current') ?>:</label></div>
		<div class="params_value">   
        <label for="contact_name"><?php esc_html_e('Enable', 'Current') ?>:</label><input name="<?php echo $contact['id'];?>_name" type="checkbox" id="contact_name" value="yes" <?php checked('yes', get_post_meta($post->ID, $contact['id'].'_name', true)); ?> /> <label for="contact_name_req"><?php esc_html_e('Required', 'Current') ?>:</label><input name="<?php echo $contact['id'];?>_name_req" type="checkbox" id="contact_name_req" value="yes" <?php checked('yes', get_post_meta($post->ID, $contact['id'].'_name_req', true)); ?> /><br />
   
		<span class="legend"><?php esc_html_e('Configure the sender\'s name field in the contact form.', 'Current'); ?></span>
        
	</div>
    </div>
    
    <div class="params_div2">
        <div class="params_label"><label for="contact_phone"><?php esc_html_e('Phone field', 'Current') ?>:</label></div>
		<div class="params_value">   
        <label for="contact_phone"><?php esc_html_e('Enable', 'Current') ?>:</label><input name="<?php echo $contact['id'];?>_phone" type="checkbox" id="contact_phone" value="yes" <?php checked('yes', get_post_meta($post->ID, $contact['id'].'_phone', true)); ?> /> <label for="contact_phone_req"><?php esc_html_e('Required', 'Current') ?>:</label><input name="<?php echo $contact['id'];?>_phone_req" type="checkbox" id="contact_phone_req" value="yes" <?php checked('yes', get_post_meta($post->ID, $contact['id'].'_phone_req', true)); ?> /> <label for="contact_phone_us"><?php esc_html_e('US phone number validation', 'Current') ?>:</label><input name="<?php echo $contact['id'];?>_phone_us" type="checkbox" id="contact_phone_us" value="yes" <?php checked('yes', get_post_meta($post->ID, $contact['id'].'_phone_us', true)); ?> />    <br />
		<span class="legend"><?php esc_html_e('Configure the sender\'s Phone number field in the contact form.', 'Current'); ?></span>
        
	</div>
    </div>
    
    
		<div class="params_div1">
        <div class="params_label">
			    <label for="contact_receivers"><?php esc_html_e('Email Recipient(s)', 'Current'); ?>:</label>
                </div>
                <div class="params_value">
				<span class="legend"><?php esc_html_e("Enter the email id(s) for the recipients. Multiple email ids should be seperated with comma:", 'Current'); ?></span><br />
				<textarea style="width: 98%; font-size: 12px;" id="contact_receivers" rows="2" cols="60" name="<?php echo $contact['id'];?>_receivers"><?php echo get_post_meta($post->ID, $contact['id'].'_receivers', true); ?></textarea>
			    </div>
                </div>
                
			   <div class="params_div2">
               <div class="params_label">
               <label for="recaptcha">
               <?php esc_html_e('ReCAPTCHA', 'Current'); ?>:</label>
			    </div>
                <div class="params_value">
               <label for="recaptcha"><?php esc_html_e('Enable', 'Current'); ?>:</label>
                <input name="<?php echo $contact['id'];?>_recaptcha" type="checkbox" id="recaptcha" value="yes" <?php checked( 'yes', get_post_meta($post->ID, $contact['id'].'_recaptcha', true)); ?> /> <label for="recaptcha_theme"><?php esc_html_e('Theme', 'Current'); ?>:</label>
                <select name="<?php echo $contact['id'];?>_recaptcha_theme" id="recaptcha_theme">
					    <option value="white"<?php echo (get_post_meta($post->ID, $contact['id'].'_recaptcha_theme', true) == 'white') ? ' selected="selected"' : ''; ?>><?php esc_attr_e('white', 'Current'); ?></option>
					    <option value="red"<?php echo (get_post_meta($post->ID, $contact['id'].'_recaptcha_theme', true) == 'red') ? ' selected="selected"' : ''; ?>><?php esc_attr_e('red', 'Current'); ?></option>
					    <option value="blackglass"<?php echo (get_post_meta($post->ID, $contact['id'].'_recaptcha_theme', true) == 'blackglass') ? ' selected="selected"' : ''; ?>><?php esc_attr_e('blackglass', 'Current'); ?></option>
					    <option value="clean"<?php echo (get_post_meta($post->ID, $contact['id'].'_recaptcha_theme', true) == 'clean') ? ' selected="selected"' : ''; ?>><?php esc_attr_e('clean', 'Current'); ?></option>
					</select>
                <label for="recaptcha_lang"><?php esc_html_e('Language', 'Current'); ?>:</label> 
                <select name="<?php echo $contact['id'];?>_recaptcha_lang" id="recaptcha_lang">
		    <option value="en"<?php echo (get_post_meta($post->ID, $contact['id'].'_recaptcha_lang', true) == 'en') ? ' selected="selected"' : ''; ?>>English</option>
		    <option value="nl"<?php echo (get_post_meta($post->ID, $contact['id'].'_recaptcha_lang', true) == 'nl') ? ' selected="selected"' : ''; ?>>Dutch</option>
		    <option value="fr"<?php echo (get_post_meta($post->ID, $contact['id'].'_recaptcha_lang', true) == 'fr') ? ' selected="selected"' : ''; ?>>French</option>
		    <option value="de"<?php echo (get_post_meta($post->ID, $contact['id'].'_recaptcha_lang', true) == 'de') ? ' selected="selected"' : ''; ?>>German</option>
		    <option value="pt"<?php echo (get_post_meta($post->ID, $contact['id'].'_recaptcha_lang', true) == 'pt') ? ' selected="selected"' : ''; ?>>Portuguese</option>
		    <option value="ru"<?php echo (get_post_meta($post->ID, $contact['id'].'_recaptcha_lang', true) == 'ru') ? ' selected="selected"' : ''; ?>>Russian</option>
		    <option value="es"<?php echo (get_post_meta($post->ID, $contact['id'].'_recaptcha_lang', true) == 'es') ? ' selected="selected"' : ''; ?>>Spanish</option>
		    <option value="tr"<?php echo (get_post_meta($post->ID, $contact['id'].'_recaptcha_lang', true) == 'tr') ? ' selected="selected"' : ''; ?>>Turkish</option>
					</select>
                   <br /><span class="legend"><?php printf( esc_html__('Add ReCAPTCHA to the contact form for extra security ( visit %s for more information)', 'Current'), '<a href="http://www.google.com/recaptcha" target="_blank">http://www.google.com/recaptcha</a>' ); ?>
				</span><br />

				<span class="legend"><?php esc_html_e('Please note: ReCAPTCHA will not work if the two fields below are left blank', 'Current'); ?></span>

			   </div>
               </div>
               
			<div class="params_div1">
            <div class="params_label">
			<label for="recaptcha_pbkey"><?php esc_html_e('ReCAPTCHA Public Key', 'Current'); ?></label>
            </div>
            <div class="params_value">
				<input name="<?php echo $contact['id'];?>_recaptcha_pbkey" type="text" id="recaptcha_pbkey" value="<?php echo esc_attr(get_post_meta($post->ID, $contact['id'].'_recaptcha_pbkey', true)); ?>" size="55" maxlength="100" />
				<br /><span class="legend"><?php esc_html_e('To use reCAPTCHA you must get an API public key from', 'Current'); ?> <a href="http://www.google.com/recaptcha" target="_blank">http://www.google.com/recaptcha</a></span>
			    </div>
                </div>
                
                <div class="params_div2">
                <div class="params_label">
			<label for="recaptcha_prkey"><?php esc_html_e('ReCAPTCHA Private Key', 'Current'); ?>
            </label></div>
            <div class="params_value">
            				<input name="<?php echo $contact['id'];?>_recaptcha_prkey" type="text" id="recaptcha_prkey" value="<?php echo esc_attr(get_post_meta($post->ID, $contact['id'].'_recaptcha_prkey', true));?>" size="55" maxlength="100" />
				<br /><span class="legend"><?php esc_html_e('To use ReCAPTCHA you must get an API private key from', 'Current'); ?> <a href="http://www.google.com/recaptcha" target="_blank">http://www.google.com/recaptcha</a></span><br />
				<span class="legend"><?php esc_html_e('This key is used when communicating between your server and the ReCAPTCHA server. Be sure to keep it a secret.', 'Current'); ?></span>
			    </div>
                </div>                      
         
    <?php
		}
    }
 function save_simple_contact_mbox($post_id) {
        global $simple_contact_array, $Current_options;
        foreach($simple_contact_array as $contact) {
			
			if (isset($_POST[$contact['id'].'_name'])) {
                update_post_meta($post_id, $contact['id'].'_name', $_POST[$contact['id'].'_name']);
            }
			else
				delete_post_meta($post_id, $contact['id'].'_name');
			
			if (isset($_POST[$contact['id'].'_name_req'])) {
                update_post_meta($post_id, $contact['id'].'_name_req', $_POST[$contact['id'].'_name_req']);
            }
			else
				delete_post_meta($post_id, $contact['id'].'_name_req');		

			if (isset($_POST[$contact['id'].'_phone'])) {
                update_post_meta($post_id, $contact['id'].'_phone', $_POST[$contact['id'].'_phone']);
            }
			else
				delete_post_meta($post_id, $contact['id'].'_phone');	
				
			if (isset($_POST[$contact['id'].'_phone_req'])) {
                update_post_meta($post_id, $contact['id'].'_phone_req', $_POST[$contact['id'].'_phone_req']);
            }
			else
				delete_post_meta($post_id, $contact['id'].'_phone_req');
				
			if (isset($_POST[$contact['id'].'_phone_us'])) {
                update_post_meta($post_id, $contact['id'].'_phone_us', $_POST[$contact['id'].'_phone_us']);
            }
			else
				delete_post_meta($post_id, $contact['id'].'_phone_us');		
				
			if (isset($_POST[$contact['id'].'_recaptcha'])) {
                update_post_meta($post_id, $contact['id'].'_recaptcha', $_POST[$contact['id'].'_recaptcha']);
            }
			else
				delete_post_meta($post_id, $contact['id'].'_recaptcha');	
				

			if (isset($_POST[$contact['id'].'_recaptcha_theme'])) {
                update_post_meta($post_id, $contact['id'].'_recaptcha_theme', $_POST[$contact['id'].'_recaptcha_theme']);
            }
				
			if (isset($_POST[$contact['id'].'_recaptcha_lang'])) {
                update_post_meta($post_id, $contact['id'].'_recaptcha_lang', $_POST[$contact['id'].'_recaptcha_lang']);
            }

			if (isset($_POST[$contact['id'].'_receivers'])) {
                update_post_meta($post_id, $contact['id'].'_receivers', $_POST[$contact['id'].'_receivers']);
            }

			if (isset($_POST[$contact['id'].'_recaptcha_prkey'])) {
                update_post_meta($post_id, $contact['id'].'_recaptcha_prkey', $_POST[$contact['id'].'_recaptcha_prkey']);
            }

			if (isset($_POST[$contact['id'].'_recaptcha_pbkey'])) {
                update_post_meta($post_id, $contact['id'].'_recaptcha_pbkey', $_POST[$contact['id'].'_recaptcha_pbkey']);
            }
			
			if (isset($_POST[$contact['id'].'_google_map'])) {
                update_post_meta($post_id, $contact['id'].'_google_map', $_POST[$contact['id'].'_google_map']);
            }
			
			if (isset($_POST[$contact['id'].'_google_map_aspect'])) {
                update_post_meta($post_id, $contact['id'].'_google_map_aspect', $_POST[$contact['id'].'_google_map_aspect']);
            }	
			
			if (isset($_POST[$contact['id'].'_address'])) {
                update_post_meta($post_id, $contact['id'].'_address', $_POST[$contact['id'].'_address']);
            }
			
			$googlezoom=16;
			if(isset($_POST[$contact['id'].'_google_map_zoom']) && is_numeric($_POST[$contact['id'].'_google_map_zoom']))
				$googlezoom = $_POST[$contact['id'].'_google_map_zoom'];
				
			update_post_meta($post_id, $contact['id'].'_google_map_zoom', $googlezoom);

        }
    }?>