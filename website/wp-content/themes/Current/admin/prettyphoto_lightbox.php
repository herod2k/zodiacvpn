<?php
	function render_cpanel_prettyphoto_lightbox( $options ) {
				?>
				 <div class="params_div1">
                <div class="params_label"><label for="prettyphoto"><?php esc_html_e('Prettyphoto Lightbox', 'Current'); ?>:</label></div>
                <div class="params_value">
				<label for="prettyphoto"><?php esc_html_e('Enable', 'Current'); ?>:</label>
				    <input name="Current_options[prettyphoto]" type="checkbox" id="prettyphoto" value="yes" <?php if(isset($options['prettyphoto'])) checked('yes', $options['prettyphoto']); ?> /> 
	<label for=""><?php esc_html_e('Style', 'Current'); ?>:</label>
                    <select name="Current_options[prettyphoto_theme]" id="prettyphoto_theme">
                         <option value="default"<?php echo ($options["prettyphoto_theme"]=="default")? " selected='selected'":"";?>>Default</option>
                         <option value="dark_rounded"<?php echo ($options["prettyphoto_theme"]=="dark_rounded")? " selected='selected'":"";?>>Dark rounded</option>
                         <option value="dark_square"<?php echo ($options["prettyphoto_theme"]=="dark_square")? " selected='selected'":"";?>>Dark square</option>
                         <option value="light_rounded"<?php echo ($options["prettyphoto_theme"]=="light_rounded")? " selected='selected'":"";?>>Light rounded</option>
                         <option value="light_square"<?php echo ($options["prettyphoto_theme"]=="light_square")? " selected='selected'":"";?>>Light square</option>
                         <option value="facebook"<?php echo ($options["prettyphoto_theme"]=="facebook")? " selected='selected'":"";?>>Facebook</option>                         
                         
                    </select>

</div>
</div>

				<?php
				display_save_changes_button(2); 				
			}?>