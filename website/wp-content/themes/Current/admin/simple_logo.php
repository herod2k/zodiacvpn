<?php
	function render_cpanel_simple_logo( $options ) {
				?>
				<div class="params_div1">   
						<div class="params_label">
							 <label for="simple_logo_url" ><?php esc_html_e('Logo:', 'Current'); ?> </label></div>
									<div class="params_value" id="simple_logo_settings_container">
									<input type="text"  name="Current_options[simple_logo_url]" id="simple_logo_url" value="<?php echo $options['simple_logo_url']; ?>" /> <input id="upload_simple_logo_url"  type="button" value="<?php esc_attr_e('Upload Logo', 'Current'); ?>" class="button-secondary upmediabt" />
									</div>
						 </div>

                       <div class="params_div2">
                        <div class="params_label">
                        <label for="simple_logo_width">
                    <?php esc_html_e('Logo Dimensions', 'Current'); ?>:</label></div>
                        <div class="params_value">
                        <label for="simple_logo_width"><?php esc_html_e('Width', 'Current');?>:</label><input name="Current_options[simple_logo_width]" type="text" id="simple_logo_width" value="<?php echo esc_attr($options['simple_logo_width']); ?>" size="5" maxlength="4" /><span class="legend"> px</span> 
                        <label for="simple_logo_height"><?php esc_html_e('Height', 'Current');?>:</label>
                        <input name="Current_options[simple_logo_height]" type="text" id="simple_logo_height" value="<?php echo esc_attr($options['simple_logo_height']); ?>" size="5" maxlength="4" /><span class="legend"> px</span>
                      </div>
                      </div>
                      <div class="params_div1">
                      <div class="params_label">
                  <label for="simple_logo_top"><?php esc_html_e('Logo Position', 'Current'); ?>:</label></div>
                    <div class="params_value">
                        <label for="simple_logo_top"><?php esc_html_e('Top', 'Current');?>:</label><input name="Current_options[simple_logo_top]" type="text" id="simple_logo_top" value="<?php echo esc_attr($options['simple_logo_top']); ?>" size="5" maxlength="3" /><span class="legend"> px</span> 
                        <label for="simple_logo_left"><?php esc_html_e('Left', 'Current');?>:</label><input name="Current_options[simple_logo_left]" type="text" id="simple_logo_left" value="<?php echo esc_attr($options['simple_logo_left']); ?>" size="5" maxlength="3" /><span class="legend"> px </span>
                        </div>
                        </div>
                        <div class="params_div2">
                        <div class="params_label">                     
                    <label for="simple_logo_slogan_top"><?php esc_html_e('Slogan Position', 'Current'); ?>:</label></div>
      <div class="params_value">
                     <label for="simple_logo_slogan_top"><?php esc_html_e('Top', 'Current');?>:</label> 
                        <input name="Current_options[simple_logo_slogan_top]" type="text" id="simple_logo_slogan_top" value="<?php echo esc_attr($options['simple_logo_slogan_top']); ?>" size="5" maxlength="3" /><span class="legend"> px</span> <label for="simple_logo_slogan_left"><?php esc_html_e('Left', 'Current'); ?>:</label> <input name="Current_options[simple_logo_slogan_left]" type="text" id="simple_logo_slogan_left" value="<?php echo esc_attr($options['simple_logo_slogan_left']); ?>" size="5" maxlength="3" /><span class="legend"> px</span>
                       <br />

                        <span class="legend"><?php  printf( __('Please note that the actual Slogan text can be changed or deleted at %1$sSettings -> General%2$s <strong>Tagline</strong> option.', 'Current'), '<a href="options-general.php">', '</a>' ); ?></span>
       </div>
       </div>
       
       <div class="params_div1">
	   <div class="params_label"><label for="simple_logo_slogan_font_size"><?php esc_html_e('Slogan Font Size', 'Current'); ?></label></div>
                        <div class="params_value">
                        
                            <select name="Current_options[simple_logo_slogan_font_size]" id="simple_logo_slogan_font_size">
                                <option value="8"<?php echo ($options['simple_logo_slogan_font_size'] == '8') ? ' selected="selected"' : ''; ?>>8px</option>
                                <option value="9"<?php echo ($options['simple_logo_slogan_font_size'] == '9') ? ' selected="selected"' : ''; ?>>9px</option>
                                <option value="10"<?php echo ($options['simple_logo_slogan_font_size'] == '10') ? ' selected="selected"' : ''; ?>>10px</option>
                                <option value="11"<?php echo ($options['simple_logo_slogan_font_size'] == '11') ? ' selected="selected"' : ''; ?>>11px</option>
                                <option value="12"<?php echo ($options['simple_logo_slogan_font_size'] == '12') ? ' selected="selected"' : ''; ?> style="padding-right:7px;">12px (Default)</option>
                                <option value="13"<?php echo ($options['simple_logo_slogan_font_size'] == '13') ? ' selected="selected"' : ''; ?>>13px</option>
                                <option value="14"<?php echo ($options['simple_logo_slogan_font_size'] == '14') ? ' selected="selected"' : ''; ?>>14px</option>
                                <option value="15"<?php echo ($options['simple_logo_slogan_font_size'] == '15') ? ' selected="selected"' : ''; ?>>15px</option>
                                <option value="16"<?php echo ($options['simple_logo_slogan_font_size'] == '16') ? ' selected="selected"' : ''; ?>>16px</option>
                                <option value="17"<?php echo ($options['simple_logo_slogan_font_size'] == '17') ? ' selected="selected"' : ''; ?>>17px</option>
                                <option value="18"<?php echo ($options['simple_logo_slogan_font_size'] == '18') ? ' selected="selected"' : ''; ?>>18px</option>
                                <option value="19"<?php echo ($options['simple_logo_slogan_font_size'] == '19') ? ' selected="selected"' : ''; ?>>19px</option>
                                <option value="20"<?php echo ($options['simple_logo_slogan_font_size'] == '20') ? ' selected="selected"' : ''; ?>>20px</option>
                                <option value="21"<?php echo ($options['simple_logo_slogan_font_size'] == '21') ? ' selected="selected"' : ''; ?>>21px</option>
                                <option value="22"<?php echo ($options['simple_logo_slogan_font_size'] == '22') ? ' selected="selected"' : ''; ?>>22px</option>
                                <option value="23"<?php echo ($options['simple_logo_slogan_font_size'] == '23') ? ' selected="selected"' : ''; ?>>23px</option>
                                <option value="24"<?php echo ($options['simple_logo_slogan_font_size'] == '24') ? ' selected="selected"' : ''; ?>>24px</option>
                                <option value="25"<?php echo ($options['simple_logo_slogan_font_size'] == '25') ? ' selected="selected"' : ''; ?>>25px</option>
                                <option value="26"<?php echo ($options['simple_logo_slogan_font_size'] == '26') ? ' selected="selected"' : ''; ?>>26px</option>
                                <option value="27"<?php echo ($options['simple_logo_slogan_font_size'] == '27') ? ' selected="selected"' : ''; ?>>27px</option>
                                <option value="28"<?php echo ($options['simple_logo_slogan_font_size'] == '28') ? ' selected="selected"' : ''; ?>>28px</option>
                                <option value="32"<?php echo ($options['simple_logo_slogan_font_size'] == '32') ? ' selected="selected"' : ''; ?>>32px</option>
                                <option value="36"<?php echo ($options['simple_logo_slogan_font_size'] == '36') ? ' selected="selected"' : ''; ?>>36px</option>
                                <option value="48"<?php echo ($options['simple_logo_slogan_font_size'] == '48') ? ' selected="selected"' : ''; ?>>48px</option>
                                <option value="60"<?php echo ($options['simple_logo_slogan_font_size'] == '60') ? ' selected="selected"' : ''; ?>>60px</option>
                                <option value="72"<?php echo ($options['simple_logo_slogan_font_size'] == '72') ? ' selected="selected"' : ''; ?>>72px</option>                                                                                                
                            </select>
                        
                        </div>
                        </div>

				<?php
				display_save_changes_button(2); 				
			}?>