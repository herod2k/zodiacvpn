<?php
global $google_webfonts;
$google_webfonts=array("Abel"  => "Abel:400",
"Abril Fatface"  => "Abril+Fatface:400",
"Aclonica"  => "Aclonica:400",
"Acme"  => "Acme:400",
"Actor"  => "Actor:400",
"Adamina"  => "Adamina:400",
"Advent Pro"  => "Advent+Pro:400",
"Advent Pro Bold"  => "Advent+Pro:700",
"Advent Pro Book"  => "Advent+Pro:300",
"Advent Pro Light"  => "Advent+Pro:200",
"Advent Pro Medium"  => "Advent+Pro:500",
"Advent Pro Semi-Bold"  => "Advent+Pro:600",
"Advent Pro Ultra-Light"  => "Advent+Pro:100",
"Aguafina Script"  => "Aguafina+Script:400",
"Aladin"  => "Aladin:400",
"Aldrich"  => "Aldrich:400",
"Alegreya"  => "Alegreya:400",
"Alegreya Bold"  => "Alegreya:700",
"Alegreya Bold Italic"  => "Alegreya:700Italic",
"Alegreya Italic"  => "Alegreya:400Italic",
"Alegreya SC"  => "Alegreya+SC:400",
"Alegreya SC Bold"  => "Alegreya+SC:700",
"Alegreya SC Bold Italic"  => "Alegreya+SC:700Italic",
"Alegreya SC Italic"  => "Alegreya+SC:400Italic",
"Alegreya SC Ultra-Bold"  => "Alegreya+SC:900",
"Alegreya SC Ultra-Bold Italic"  => "Alegreya+SC:900Italic",
"Alegreya Ultra-Bold"  => "Alegreya:900",
"Alegreya Ultra-Bold Italic"  => "Alegreya:900Italic",
"Alex Brush"  => "Alex+Brush:400",
"Alfa Slab One"  => "Alfa+Slab+One:400",
"Alice"  => "Alice:400",
"Alike"  => "Alike:400",
"Alike Angular"  => "Alike+Angular:400",
"Allan Bold"  => "Allan:700",
"Allerta"  => "Allerta:400",
"Allerta Stencil"  => "Allerta+Stencil:400",
"Allura"  => "Allura:400",
"Almendra"  => "Almendra:400",
"Almendra Bold"  => "Almendra:700",
"Almendra Italic"  => "Almendra:400Italic",
"Almendra SC"  => "Almendra+SC:400",
"Amaranth"  => "Amaranth:400",
"Amaranth Bold"  => "Amaranth:700",
"Amaranth Bold Italic"  => "Amaranth:700Italic",
"Amaranth Italic"  => "Amaranth:400Italic",
"Amatic SC"  => "Amatic+SC:400",
"Amatic SC Bold"  => "Amatic+SC:700",
"Amethysta"  => "Amethysta:400",
"Andada"  => "Andada:400",
"Andika"  => "Andika:400",
"Angkor"  => "Angkor:400",
"Annie Use Your Telescope"  => "Annie+Use+Your+Telescope:400",
"Anonymous Pro"  => "Anonymous+Pro:400",
"Anonymous Pro Bold"  => "Anonymous+Pro:700",
"Anonymous Pro Bold Italic"  => "Anonymous+Pro:700Italic",
"Anonymous Pro Italic"  => "Anonymous+Pro:400Italic",
"Antic"  => "Antic:400",
"Antic Didone"  => "Antic+Didone:400",
"Antic Slab"  => "Antic+Slab:400",
"Anton"  => "Anton:400",
"Arapey"  => "Arapey:400",
"Arapey Italic"  => "Arapey:400Italic",
"Arbutus"  => "Arbutus:400",
"Architects Daughter"  => "Architects+Daughter:400",
"Arimo"  => "Arimo:400",
"Arimo Bold"  => "Arimo:700",
"Arimo Bold Italic"  => "Arimo:700Italic",
"Arimo Italic"  => "Arimo:400Italic",
"Arizonia"  => "Arizonia:400",
"Armata"  => "Armata:400",
"Artifika"  => "Artifika:400",
"Arvo"  => "Arvo:400",
"Arvo Bold"  => "Arvo:700",
"Arvo Bold Italic"  => "Arvo:700Italic",
"Arvo Italic"  => "Arvo:400Italic",
"Asap"  => "Asap:400",
"Asap Bold"  => "Asap:700",
"Asap Bold Italic"  => "Asap:700Italic",
"Asap Italic"  => "Asap:400Italic",
"Asset"  => "Asset:400",
"Astloch"  => "Astloch:400",
"Astloch Bold"  => "Astloch:700",
"Asul"  => "Asul:400",
"Asul Bold"  => "Asul:700",
"Atomic Age"  => "Atomic+Age:400",
"Aubrey"  => "Aubrey:400",
"Audiowide"  => "Audiowide:400",
"Average"  => "Average:400",
"Averia Gruesa Libre"  => "Averia+Gruesa+Libre:400",
"Averia Libre"  => "Averia+Libre:400",
"Averia Libre Bold"  => "Averia+Libre:700",
"Averia Libre Bold Italic"  => "Averia+Libre:700Italic",
"Averia Libre Book"  => "Averia+Libre:300",
"Averia Libre Book Italic"  => "Averia+Libre:300Italic",
"Averia Libre Italic"  => "Averia+Libre:400Italic",
"Averia Sans Libre"  => "Averia+Sans+Libre:400",
"Averia Sans Libre Bold"  => "Averia+Sans+Libre:700",
"Averia Sans Libre Bold Italic"  => "Averia+Sans+Libre:700Italic",
"Averia Sans Libre Book"  => "Averia+Sans+Libre:300",
"Averia Sans Libre Book Italic"  => "Averia+Sans+Libre:300Italic",
"Averia Sans Libre Italic"  => "Averia+Sans+Libre:400Italic",
"Averia Serif Libre"  => "Averia+Serif+Libre:400",
"Averia Serif Libre Bold"  => "Averia+Serif+Libre:700",
"Averia Serif Libre Bold Italic"  => "Averia+Serif+Libre:700Italic",
"Averia Serif Libre Book"  => "Averia+Serif+Libre:300",
"Averia Serif Libre Book Italic"  => "Averia+Serif+Libre:300Italic",
"Averia Serif Libre Italic"  => "Averia+Serif+Libre:400Italic",
"Bad Script"  => "Bad+Script:400",
"Balthazar"  => "Balthazar:400",
"Bangers"  => "Bangers:400",
"Basic"  => "Basic:400",
"Battambang"  => "Battambang:400",
"Battambang Bold"  => "Battambang:700",
"Baumans"  => "Baumans:400",
"Bayon"  => "Bayon:400",
"Belgrano"  => "Belgrano:400",
"Belleza"  => "Belleza:400",
"Bentham"  => "Bentham:400",
"Berkshire Swash"  => "Berkshire+Swash:400",
"Bevan"  => "Bevan:400",
"Bigshot One"  => "Bigshot+One:400",
"Bilbo"  => "Bilbo:400",
"Bilbo Swash Caps"  => "Bilbo+Swash+Caps:400",
"Bitter"  => "Bitter:400",
"Bitter Bold"  => "Bitter:700",
"Bitter Italic"  => "Bitter:400Italic",
"Black Ops One"  => "Black+Ops+One:400",
"Bokor"  => "Bokor:400",
"Bonbon"  => "Bonbon:400",
"Boogaloo"  => "Boogaloo:400",
"Bowlby One"  => "Bowlby+One:400",
"Bowlby One SC"  => "Bowlby+One+SC:400",
"Brawler"  => "Brawler:400",
"Bree Serif"  => "Bree+Serif:400",
"Bubblegum Sans"  => "Bubblegum+Sans:400",
"Buda Book"  => "Buda:300",
"Buenard"  => "Buenard:400",
"Buenard Bold"  => "Buenard:700",
"Butcherman"  => "Butcherman:400",
"Butterfly Kids"  => "Butterfly+Kids:400",
"Cabin"  => "Cabin:400",
"Cabin Bold"  => "Cabin:700",
"Cabin Bold Italic"  => "Cabin:700Italic",
"Cabin Condensed"  => "Cabin+Condensed:400",
"Cabin Condensed Bold"  => "Cabin+Condensed:700",
"Cabin Condensed Medium"  => "Cabin+Condensed:500",
"Cabin Condensed Semi-Bold"  => "Cabin+Condensed:600",
"Cabin Italic"  => "Cabin:400Italic",
"Cabin Medium"  => "Cabin:500",
"Cabin Medium Italic"  => "Cabin:500Italic",
"Cabin Semi-Bold"  => "Cabin:600",
"Cabin Semi-Bold Italic"  => "Cabin:600Italic",
"Cabin Sketch"  => "Cabin+Sketch:400",
"Cabin Sketch Bold"  => "Cabin+Sketch:700",
"Caesar Dressing"  => "Caesar+Dressing:400",
"Cagliostro"  => "Cagliostro:400",
"Calligraffitti"  => "Calligraffitti:400",
"Cambo"  => "Cambo:400",
"Candal"  => "Candal:400",
"Cantarell"  => "Cantarell:400",
"Cantarell Bold"  => "Cantarell:700",
"Cantarell Bold Italic"  => "Cantarell:700Italic",
"Cantarell Italic"  => "Cantarell:400Italic",
"Cantata One"  => "Cantata+One:400",
"Cardo"  => "Cardo:400",
"Cardo Bold"  => "Cardo:700",
"Cardo Italic"  => "Cardo:400Italic",
"Carme"  => "Carme:400",
"Carter One"  => "Carter+One:400",
"Caudex"  => "Caudex:400",
"Caudex Bold"  => "Caudex:700",
"Caudex Bold Italic"  => "Caudex:700Italic",
"Caudex Italic"  => "Caudex:400Italic",
"Cedarville Cursive"  => "Cedarville+Cursive:400",
"Ceviche One"  => "Ceviche+One:400",
"Changa One"  => "Changa+One:400",
"Changa One Italic"  => "Changa+One:400Italic",
"Chango"  => "Chango:400",
"Chau Philomene One"  => "Chau+Philomene+One:400",
"Chau Philomene One Italic"  => "Chau+Philomene+One:400Italic",
"Chelsea Market"  => "Chelsea+Market:400",
"Chenla"  => "Chenla:400",
"Cherry Cream Soda"  => "Cherry+Cream+Soda:400",
"Chewy"  => "Chewy:400",
"Chicle"  => "Chicle:400",
"Chivo"  => "Chivo:400",
"Chivo Italic"  => "Chivo:400Italic",
"Chivo Ultra-Bold"  => "Chivo:900",
"Chivo Ultra-Bold Italic"  => "Chivo:900Italic",
"Coda"  => "Coda:400",
"Coda Caption Extra-Bold"  => "Coda+Caption:800",
"Coda Extra-Bold"  => "Coda:800",
"Codystar"  => "Codystar:400",
"Codystar Book"  => "Codystar:300",
"Comfortaa"  => "Comfortaa:400",
"Comfortaa Bold"  => "Comfortaa:700",
"Comfortaa Book"  => "Comfortaa:300",
"Coming Soon"  => "Coming+Soon:400",
"Concert One"  => "Concert+One:400",
"Condiment"  => "Condiment:400",
"Content"  => "Content:400",
"Content Bold"  => "Content:700",
"Contrail One"  => "Contrail+One:400",
"Convergence"  => "Convergence:400",
"Cookie"  => "Cookie:400",
"Copse"  => "Copse:400",
"Corben"  => "Corben:400",
"Corben Bold"  => "Corben:700",
"Cousine"  => "Cousine:400",
"Cousine Bold"  => "Cousine:700",
"Cousine Bold Italic"  => "Cousine:700Italic",

"Cousine Italic"  => "Cousine:400Italic",
"Coustard"  => "Coustard:400",
"Coustard Ultra-Bold"  => "Coustard:900",
"Covered By Your Grace"  => "Covered+By+Your+Grace:400",
"Crafty Girls"  => "Crafty+Girls:400",
"Creepster"  => "Creepster:400",
"Crete Round"  => "Crete+Round:400",
"Crete Round Italic"  => "Crete+Round:400Italic",
"Crimson Text"  => "Crimson+Text:400",
"Crimson Text Bold"  => "Crimson+Text:700",
"Crimson Text Bold Italic"  => "Crimson+Text:700Italic",
"Crimson Text Italic"  => "Crimson+Text:400Italic",
"Crimson Text Semi-Bold"  => "Crimson+Text:600",
"Crimson Text Semi-Bold Italic"  => "Crimson+Text:600Italic",
"Crushed"  => "Crushed:400",
"Cuprum"  => "Cuprum:400",
"Cuprum Bold"  => "Cuprum:700",
"Cuprum Bold Italic"  => "Cuprum:700Italic",
"Cuprum Italic"  => "Cuprum:400Italic",
"Cutive"  => "Cutive:400",
"Damion"  => "Damion:400",
"Dancing Script"  => "Dancing+Script:400",
"Dancing Script Bold"  => "Dancing+Script:700",
"Dangrek"  => "Dangrek:400",
"Dawning of a New Day"  => "Dawning+of+a+New+Day:400",
"Days One"  => "Days+One:400",
"Delius"  => "Delius:400",
"Delius Swash Caps"  => "Delius+Swash+Caps:400",
"Delius Unicase"  => "Delius+Unicase:400",
"Delius Unicase Bold"  => "Delius+Unicase:700",
"Della Respira"  => "Della+Respira:400",
"Devonshire"  => "Devonshire:400",
"Didact Gothic"  => "Didact+Gothic:400",
"Diplomata"  => "Diplomata:400",
"Diplomata SC"  => "Diplomata+SC:400",
"Doppio One"  => "Doppio+One:400",
"Dorsa"  => "Dorsa:400",
"Dosis"  => "Dosis:400",
"Dosis Bold"  => "Dosis:700",
"Dosis Book"  => "Dosis:300",
"Dosis Extra-Bold"  => "Dosis:800",
"Dosis Light"  => "Dosis:200",
"Dosis Medium"  => "Dosis:500",
"Dosis Semi-Bold"  => "Dosis:600",
"Dr Sugiyama"  => "Dr+Sugiyama:400",
"Droid Sans"  => "Droid+Sans:400",
"Droid Sans Bold"  => "Droid+Sans:700",
"Droid Sans Mono"  => "Droid+Sans+Mono:400",
"Droid Serif"  => "Droid+Serif:400",
"Droid Serif Bold"  => "Droid+Serif:700",
"Droid Serif Bold Italic"  => "Droid+Serif:700Italic",
"Droid Serif Italic"  => "Droid+Serif:400Italic",
"Duru Sans"  => "Duru+Sans:400",
"Dynalight"  => "Dynalight:400",
"Eater"  => "Eater:400",
"EB Garamond"  => "EB+Garamond:400",
"Economica"  => "Economica:400",
"Economica Bold"  => "Economica:700",
"Economica Bold Italic"  => "Economica:700Italic",
"Economica Italic"  => "Economica:400Italic",
"Electrolize"  => "Electrolize:400",
"Emblema One"  => "Emblema+One:400",
"Emilys Candy"  => "Emilys+Candy:400",
"Engagement"  => "Engagement:400",
"Enriqueta"  => "Enriqueta:400",
"Enriqueta Bold"  => "Enriqueta:700",
"Erica One"  => "Erica+One:400",
"Esteban"  => "Esteban:400",
"Euphoria Script"  => "Euphoria+Script:400",
"Ewert"  => "Ewert:400",
"Exo"  => "Exo:400",
"Exo Bold"  => "Exo:700",
"Exo Bold Italic"  => "Exo:700Italic",
"Exo Book"  => "Exo:300",
"Exo Book Italic"  => "Exo:300Italic",
"Exo Extra-Bold"  => "Exo:800",
"Exo Extra-Bold Italic"  => "Exo:800Italic",
"Exo Italic"  => "Exo:400Italic",
"Exo Light"  => "Exo:200",
"Exo Light Italic"  => "Exo:200Italic",
"Exo Medium"  => "Exo:500",
"Exo Medium Italic"  => "Exo:500Italic",
"Exo Semi-Bold"  => "Exo:600",
"Exo Semi-Bold Italic"  => "Exo:600Italic",
"Exo Ultra-Bold"  => "Exo:900",
"Exo Ultra-Bold Italic"  => "Exo:900Italic",
"Exo Ultra-Light"  => "Exo:100",
"Exo Ultra-Light Italic"  => "Exo:100Italic",
"Expletus Sans"  => "Expletus+Sans:400",
"Expletus Sans Bold"  => "Expletus+Sans:700",
"Expletus Sans Bold Italic"  => "Expletus+Sans:700Italic",
"Expletus Sans Italic"  => "Expletus+Sans:400Italic",
"Expletus Sans Medium"  => "Expletus+Sans:500",
"Expletus Sans Medium Italic"  => "Expletus+Sans:500Italic",
"Expletus Sans Semi-Bold"  => "Expletus+Sans:600",
"Expletus Sans Semi-Bold Italic"  => "Expletus+Sans:600Italic",
"Fanwood Text"  => "Fanwood+Text:400",
"Fanwood Text Italic"  => "Fanwood+Text:400Italic",
"Fascinate"  => "Fascinate:400",
"Fascinate Inline"  => "Fascinate+Inline:400",
"Federant"  => "Federant:400",
"Federo"  => "Federo:400",
"Felipa"  => "Felipa:400",
"Fjord One"  => "Fjord+One:400",
"Flamenco"  => "Flamenco:400",
"Flamenco Book"  => "Flamenco:300",
"Flavors"  => "Flavors:400",
"Fondamento"  => "Fondamento:400",
"Fondamento Italic"  => "Fondamento:400Italic",
"Fontdiner Swanky"  => "Fontdiner+Swanky:400",
"Forum"  => "Forum:400",
"Francois One"  => "Francois+One:400",
"Fredericka the Great"  => "Fredericka+the+Great:400",
"Fredoka One"  => "Fredoka+One:400",
"Freehand"  => "Freehand:400",
"Fresca"  => "Fresca:400",
"Frijole"  => "Frijole:400",
"Fugaz One"  => "Fugaz+One:400",
"Galdeano"  => "Galdeano:400",
"Gentium Basic"  => "Gentium+Basic:400",
"Gentium Basic Bold"  => "Gentium+Basic:700",
"Gentium Basic Bold Italic"  => "Gentium+Basic:700Italic",
"Gentium Basic Italic"  => "Gentium+Basic:400Italic",
"Gentium Book Basic"  => "Gentium+Book+Basic:400",
"Gentium Book Basic Bold"  => "Gentium+Book+Basic:700",
"Gentium Book Basic Bold Italic"  => "Gentium+Book+Basic:700Italic",
"Gentium Book Basic Italic"  => "Gentium+Book+Basic:400Italic",
"Geo"  => "Geo:400",
"Geostar"  => "Geostar:400",
"Geostar Fill"  => "Geostar+Fill:400",
"Germania One"  => "Germania+One:400",
"GFS Didot"  => "GFS+Didot:400",
"GFS Neohellenic"  => "GFS+Neohellenic:400",
"GFS Neohellenic Bold"  => "GFS+Neohellenic:700",
"GFS Neohellenic Bold Italic"  => "GFS+Neohellenic:700Italic",
"GFS Neohellenic Italic"  => "GFS+Neohellenic:400Italic",
"Give You Glory"  => "Give+You+Glory:400",
"Glass Antiqua"  => "Glass+Antiqua:400",
"Glegoo"  => "Glegoo:400",
"Gloria Hallelujah"  => "Gloria+Hallelujah:400",
"Goblin One"  => "Goblin+One:400",
"Gochi Hand"  => "Gochi+Hand:400",
"Gorditas"  => "Gorditas:400",
"Gorditas Bold"  => "Gorditas:700",
"Goudy Bookletter 1911"  => "Goudy+Bookletter+1911:400",
"Graduate"  => "Graduate:400",
"Gravitas One"  => "Gravitas+One:400",
"Great Vibes"  => "Great+Vibes:400",
"Gruppo"  => "Gruppo:400",
"Gudea"  => "Gudea:400",
"Gudea Bold"  => "Gudea:700",
"Gudea Italic"  => "Gudea:400Italic",
"Habibi"  => "Habibi:400",
"Hammersmith One"  => "Hammersmith+One:400",
"Handlee"  => "Handlee:400",
"Hanuman"  => "Hanuman:400",
"Hanuman Bold"  => "Hanuman:700",
"Happy Monkey"  => "Happy+Monkey:400",
"Henny Penny"  => "Henny+Penny:400",
"Herr Von Muellerhoff"  => "Herr+Von+Muellerhoff:400",
"Holtwood One SC"  => "Holtwood+One+SC:400",
"Homemade Apple"  => "Homemade+Apple:400",
"Homenaje"  => "Homenaje:400",
"Iceberg"  => "Iceberg:400",
"Iceland"  => "Iceland:400",
"IM Fell Double Pica"  => "IM+Fell+Double+Pica:400",
"IM Fell Double Pica Italic"  => "IM+Fell+Double+Pica:400Italic",
"IM Fell Double Pica SC"  => "IM+Fell+Double+Pica+SC:400",
"IM Fell DW Pica"  => "IM+Fell+DW+Pica:400",
"IM Fell DW Pica Italic"  => "IM+Fell+DW+Pica:400Italic",
"IM Fell DW Pica SC"  => "IM+Fell+DW+Pica+SC:400",
"IM Fell English"  => "IM+Fell+English:400",
"IM Fell English Italic"  => "IM+Fell+English:400Italic",
"IM Fell English SC"  => "IM+Fell+English+SC:400",
"IM Fell French Canon"  => "IM+Fell+French+Canon:400",
"IM Fell French Canon Italic"  => "IM+Fell+French+Canon:400Italic",
"IM Fell French Canon SC"  => "IM+Fell+French+Canon+SC:400",
"IM Fell Great Primer"  => "IM+Fell+Great+Primer:400",
"IM Fell Great Primer Italic"  => "IM+Fell+Great+Primer:400Italic",
"IM Fell Great Primer SC"  => "IM+Fell+Great+Primer+SC:400",
"Imprima"  => "Imprima:400",
"Inconsolata"  => "Inconsolata:400",
"Inder"  => "Inder:400",
"Indie Flower"  => "Indie+Flower:400",
"Inika"  => "Inika:400",
"Inika Bold"  => "Inika:700",
"Irish Grover"  => "Irish+Grover:400",
"Istok Web"  => "Istok+Web:400",
"Istok Web Bold"  => "Istok+Web:700",
"Istok Web Bold Italic"  => "Istok+Web:700Italic",
"Istok Web Italic"  => "Istok+Web:400Italic",
"Italiana"  => "Italiana:400",
"Italianno"  => "Italianno:400",
"Jim Nightshade"  => "Jim+Nightshade:400",
"Jockey One"  => "Jockey+One:400",
"Jolly Lodger"  => "Jolly+Lodger:400",
"Josefin Sans"  => "Josefin+Sans:400",
"Josefin Sans Bold"  => "Josefin+Sans:700",
"Josefin Sans Bold Italic"  => "Josefin+Sans:700Italic",
"Josefin Sans Book"  => "Josefin+Sans:300",
"Josefin Sans Book Italic"  => "Josefin+Sans:300Italic",
"Josefin Sans Italic"  => "Josefin+Sans:400Italic",
"Josefin Sans Semi-Bold"  => "Josefin+Sans:600",
"Josefin Sans Semi-Bold Italic"  => "Josefin+Sans:600Italic",
"Josefin Sans Ultra-Light"  => "Josefin+Sans:100",
"Josefin Sans Ultra-Light Italic"  => "Josefin+Sans:100Italic",
"Josefin Slab"  => "Josefin+Slab:400",
"Josefin Slab Bold"  => "Josefin+Slab:700",
"Josefin Slab Bold Italic"  => "Josefin+Slab:700Italic",
"Josefin Slab Book"  => "Josefin+Slab:300",
"Josefin Slab Book Italic"  => "Josefin+Slab:300Italic",
"Josefin Slab Italic"  => "Josefin+Slab:400Italic",
"Josefin Slab Semi-Bold"  => "Josefin+Slab:600",
"Josefin Slab Semi-Bold Italic"  => "Josefin+Slab:600Italic",
"Josefin Slab Ultra-Light"  => "Josefin+Slab:100",
"Josefin Slab Ultra-Light Italic"  => "Josefin+Slab:100Italic",
"Judson"  => "Judson:400",
"Judson Bold"  => "Judson:700",
"Judson Italic"  => "Judson:400Italic",
"Julee"  => "Julee:400",
"Junge"  => "Junge:400",
"Jura"  => "Jura:400",
"Jura Book"  => "Jura:300",
"Jura Medium"  => "Jura:500",
"Jura Semi-Bold"  => "Jura:600",
"Just Another Hand"  => "Just+Another+Hand:400",
"Just Me Again Down Here"  => "Just+Me+Again+Down+Here:400",
"Kameron"  => "Kameron:400",
"Kameron Bold"  => "Kameron:700",
"Karla"  => "Karla:400",
"Karla Bold"  => "Karla:700",
"Karla Bold Italic"  => "Karla:700Italic",
"Karla Italic"  => "Karla:400Italic",
"Kaushan Script"  => "Kaushan+Script:400",
"Kelly Slab"  => "Kelly+Slab:400",
"Kenia"  => "Kenia:400",
"Khmer"  => "Khmer:400",
"Knewave"  => "Knewave:400",
"Kotta One"  => "Kotta+One:400",
"Koulen"  => "Koulen:400",
"Kranky"  => "Kranky:400",
"Kreon"  => "Kreon:400",
"Kreon Bold"  => "Kreon:700",
"Kreon Book"  => "Kreon:300",
"Kristi"  => "Kristi:400",
"Krona One"  => "Krona+One:400",
"La Belle Aurore"  => "La+Belle+Aurore:400",
"Lancelot"  => "Lancelot:400",
"Lato"  => "Lato:400",
"Lato Bold"  => "Lato:700",
"Lato Bold Italic"  => "Lato:700Italic",
"Lato Book"  => "Lato:300",
"Lato Book Italic"  => "Lato:300Italic",
"Lato Italic"  => "Lato:400Italic",
"Lato Ultra-Bold"  => "Lato:900",
"Lato Ultra-Bold Italic"  => "Lato:900Italic",
"Lato Ultra-Light"  => "Lato:100",
"Lato Ultra-Light Italic"  => "Lato:100Italic",
"League Script"  => "League+Script:400",
"Leckerli One"  => "Leckerli+One:400",
"Ledger"  => "Ledger:400",
"Lekton"  => "Lekton:400",
"Lekton Bold"  => "Lekton:700",
"Lekton Italic"  => "Lekton:400Italic",
"Lemon"  => "Lemon:400",
"Lilita One"  => "Lilita+One:400",
"Limelight"  => "Limelight:400",
"Linden Hill"  => "Linden+Hill:400",
"Linden Hill Italic"  => "Linden+Hill:400Italic",
"Lobster"  => "Lobster:400",
"Lobster Two"  => "Lobster+Two:400",
"Lobster Two Bold"  => "Lobster+Two:700",
"Lobster Two Bold Italic"  => "Lobster+Two:700Italic",
"Lobster Two Italic"  => "Lobster+Two:400Italic",
"Londrina Outline"  => "Londrina+Outline:400",
"Londrina Shadow"  => "Londrina+Shadow:400",
"Londrina Sketch"  => "Londrina+Sketch:400",
"Londrina Solid"  => "Londrina+Solid:400",
"Lora"  => "Lora:400",
"Lora Bold"  => "Lora:700",
"Lora Bold Italic"  => "Lora:700Italic",
"Lora Italic"  => "Lora:400Italic",
"Love Ya Like A Sister"  => "Love+Ya+Like+A+Sister:400",
"Loved by the King"  => "Loved+by+the+King:400",
"Lovers Quarrel"  => "Lovers+Quarrel:400",
"Luckiest Guy"  => "Luckiest+Guy:400",
"Lusitana"  => "Lusitana:400",
"Lusitana Bold"  => "Lusitana:700",
"Lustria"  => "Lustria:400",
"Macondo"  => "Macondo:400",
"Macondo Swash Caps"  => "Macondo+Swash+Caps:400",
"Magra"  => "Magra:400",
"Magra Bold"  => "Magra:700",
"Maiden Orange"  => "Maiden+Orange:400",
"Mako"  => "Mako:400",
"Marck Script"  => "Marck+Script:400",
"Marko One"  => "Marko+One:400",
"Marmelad"  => "Marmelad:400",
"Marvel"  => "Marvel:400",
"Marvel Bold"  => "Marvel:700",
"Marvel Bold Italic"  => "Marvel:700Italic",
"Marvel Italic"  => "Marvel:400Italic",
"Mate"  => "Mate:400",
"Mate Italic"  => "Mate:400Italic",
"Mate SC"  => "Mate+SC:400",
"Maven Pro"  => "Maven+Pro:400",
"Maven Pro Bold"  => "Maven+Pro:700",
"Maven Pro Medium"  => "Maven+Pro:500",
"Maven Pro Ultra-Bold"  => "Maven+Pro:900",
"Meddon"  => "Meddon:400",
"MedievalSharp"  => "MedievalSharp:400",
"Medula One"  => "Medula+One:400",
"Megrim"  => "Megrim:400",
"Merienda One"  => "Merienda+One:400",
"Merriweather"  => "Merriweather:400",
"Merriweather Bold"  => "Merriweather:700",
"Merriweather Book"  => "Merriweather:300",
"Merriweather Ultra-Bold"  => "Merriweather:900",
"Metal"  => "Metal:400",
"Metamorphous"  => "Metamorphous:400",
"Metrophobic"  => "Metrophobic:400",
"Michroma"  => "Michroma:400",
"Miltonian"  => "Miltonian:400",
"Miltonian Tattoo"  => "Miltonian+Tattoo:400",
"Miniver"  => "Miniver:400",
"Miss Fajardose"  => "Miss+Fajardose:400",
"Modern Antiqua"  => "Modern+Antiqua:400",
"Molengo"  => "Molengo:400",
"Monofett"  => "Monofett:400",
"Monoton"  => "Monoton:400",
"Monsieur La Doulaise"  => "Monsieur+La+Doulaise:400",
"Montaga"  => "Montaga:400",
"Montez"  => "Montez:400",
"Montserrat"  => "Montserrat:400",
"Moul"  => "Moul:400",
"Moulpali"  => "Moulpali:400",
"Mountains of Christmas"  => "Mountains+of+Christmas:400",
"Mountains of Christmas Bold"  => "Mountains+of+Christmas:700",
"Mr Bedfort"  => "Mr+Bedfort:400",
"Mr Dafoe"  => "Mr+Dafoe:400",
"Mr De Haviland"  => "Mr+De+Haviland:400",
"Mrs Saint Delafield"  => "Mrs+Saint+Delafield:400",
"Mrs Sheppards"  => "Mrs+Sheppards:400",
"Muli"  => "Muli:400",
"Muli Book"  => "Muli:300",
"Muli Book Italic"  => "Muli:300Italic",
"Muli Italic"  => "Muli:400Italic",
"Mystery Quest"  => "Mystery+Quest:400",
"Neucha"  => "Neucha:400",
"Neuton"  => "Neuton:400",
"Neuton Bold"  => "Neuton:700",
"Neuton Book"  => "Neuton:300",
"Neuton Extra-Bold"  => "Neuton:800",
"Neuton Italic"  => "Neuton:400Italic",
"Neuton Light"  => "Neuton:200",
"News Cycle"  => "News+Cycle:400",
"News Cycle Bold"  => "News+Cycle:700",
"Niconne"  => "Niconne:400",
"Nixie One"  => "Nixie+One:400",
"Nobile"  => "Nobile:400",
"Nobile Bold"  => "Nobile:700",
"Nobile Bold Italic"  => "Nobile:700Italic",
"Nobile Italic"  => "Nobile:400Italic",
"Nokora"  => "Nokora:400",
"Nokora Bold"  => "Nokora:700",
"Norican"  => "Norican:400",
"Nosifer"  => "Nosifer:400",
"Nothing You Could Do"  => "Nothing+You+Could+Do:400",
"Noticia Text"  => "Noticia+Text:400",
"Noticia Text Bold"  => "Noticia+Text:700",
"Noticia Text Bold Italic"  => "Noticia+Text:700Italic",
"Noticia Text Italic"  => "Noticia+Text:400Italic",
"Nova Cut"  => "Nova+Cut:400",
"Nova Flat"  => "Nova+Flat:400",
"Nova Mono"  => "Nova+Mono:400",
"Nova Oval"  => "Nova+Oval:400",
"Nova Round"  => "Nova+Round:400",
"Nova Script"  => "Nova+Script:400",
"Nova Slim"  => "Nova+Slim:400",
"Nova Square"  => "Nova+Square:400",
"Numans"  => "Numans:400",
"Nunito"  => "Nunito:400",
"Nunito Bold"  => "Nunito:700",
"Nunito Book"  => "Nunito:300",
"Odor Mean Chey"  => "Odor+Mean+Chey:400",
"Old Standard TT"  => "Old+Standard+TT:400",
"Old Standard TT Bold"  => "Old+Standard+TT:700",
"Old Standard TT Italic"  => "Old+Standard+TT:400Italic",
"Oldenburg"  => "Oldenburg:400",
"Oleo Script"  => "Oleo+Script:400",
"Oleo Script Bold"  => "Oleo+Script:700",
"Open Sans"  => "Open+Sans:400",
"Open Sans Bold"  => "Open+Sans:700",
"Open Sans Bold Italic"  => "Open+Sans:700Italic",
"Open Sans Book"  => "Open+Sans:300",
"Open Sans Book Italic"  => "Open+Sans:300Italic",
"Open Sans Condensed Bold"  => "Open+Sans+Condensed:700",
"Open Sans Condensed Book"  => "Open+Sans+Condensed:300",
"Open Sans Condensed Book Italic"  => "Open+Sans+Condensed:300Italic",
"Open Sans Extra-Bold"  => "Open+Sans:800",
"Open Sans Extra-Bold Italic"  => "Open+Sans:800Italic",
"Open Sans Italic"  => "Open+Sans:400Italic",
"Open Sans Semi-Bold"  => "Open+Sans:600",
"Open Sans Semi-Bold Italic"  => "Open+Sans:600Italic",
"Orbitron"  => "Orbitron:400",
"Orbitron Bold"  => "Orbitron:700",
"Orbitron Medium"  => "Orbitron:500",
"Orbitron Ultra-Bold"  => "Orbitron:900",
"Original Surfer"  => "Original+Surfer:400",
"Oswald"  => "Oswald:400",
"Oswald Bold"  => "Oswald:700",
"Oswald Book"  => "Oswald:300",
"Over the Rainbow"  => "Over+the+Rainbow:400",
"Overlock"  => "Overlock:400",
"Overlock Bold"  => "Overlock:700",
"Overlock Bold Italic"  => "Overlock:700Italic",
"Overlock Italic"  => "Overlock:400Italic",
"Overlock SC"  => "Overlock+SC:400",
"Overlock Ultra-Bold"  => "Overlock:900",
"Overlock Ultra-Bold Italic"  => "Overlock:900Italic",
"Ovo"  => "Ovo:400",
"Oxygen"  => "Oxygen:400",
"Pacifico"  => "Pacifico:400",
"Parisienne"  => "Parisienne:400",
"Passero One"  => "Passero+One:400",
"Passion One"  => "Passion+One:400",
"Passion One Bold"  => "Passion+One:700",
"Passion One Ultra-Bold"  => "Passion+One:900",
"Patrick Hand"  => "Patrick+Hand:400",
"Patua One"  => "Patua+One:400",
"Paytone One"  => "Paytone+One:400",
"Permanent Marker"  => "Permanent+Marker:400",
"Petrona"  => "Petrona:400",
"Philosopher"  => "Philosopher:400",
"Philosopher Bold"  => "Philosopher:700",
"Philosopher Bold Italic"  => "Philosopher:700Italic",
"Philosopher Italic"  => "Philosopher:400Italic",
"Piedra"  => "Piedra:400",
"Pinyon Script"  => "Pinyon+Script:400",
"Plaster"  => "Plaster:400",
"Play"  => "Play:400",
"Play Bold"  => "Play:700",
"Playball"  => "Playball:400",
"Playfair Display"  => "Playfair+Display:400",
"Playfair Display Italic"  => "Playfair+Display:400Italic",
"Podkova"  => "Podkova:400",
"Podkova Bold"  => "Podkova:700",
"Poiret One"  => "Poiret+One:400",
"Poller One"  => "Poller+One:400",
"Poly"  => "Poly:400",
"Poly Italic"  => "Poly:400Italic",
"Pompiere"  => "Pompiere:400",
"Pontano Sans"  => "Pontano+Sans:400",
"Port Lligat Sans"  => "Port+Lligat+Sans:400",
"Port Lligat Slab"  => "Port+Lligat+Slab:400",
"Prata"  => "Prata:400",
"Preahvihear"  => "Preahvihear:400",
"Press Start 2P"  => "Press+Start+2P:400",
"Princess Sofia"  => "Princess+Sofia:400",
"Prociono"  => "Prociono:400",
"Prosto One"  => "Prosto+One:400",
"PT Mono"  => "PT+Mono:400",
"PT Sans"  => "PT+Sans:400",
"PT Sans Bold"  => "PT+Sans:700",
"PT Sans Bold Italic"  => "PT+Sans:700Italic",
"PT Sans Caption"  => "PT+Sans+Caption:400",
"PT Sans Caption Bold"  => "PT+Sans+Caption:700",
"PT Sans Italic"  => "PT+Sans:400Italic",
"PT Sans Narrow"  => "PT+Sans+Narrow:400",
"PT Sans Narrow Bold"  => "PT+Sans+Narrow:700",
"PT Serif"  => "PT+Serif:400",
"PT Serif Bold"  => "PT+Serif:700",
"PT Serif Bold Italic"  => "PT+Serif:700Italic",
"PT Serif Caption"  => "PT+Serif+Caption:400",
"PT Serif Caption Italic"  => "PT+Serif+Caption:400Italic",
"PT Serif Italic"  => "PT+Serif:400Italic",
"Puritan"  => "Puritan:400",
"Puritan Bold"  => "Puritan:700",
"Puritan Bold Italic"  => "Puritan:700Italic",
"Puritan Italic"  => "Puritan:400Italic",
"Quantico"  => "Quantico:400",
"Quantico Bold"  => "Quantico:700",
"Quantico Bold Italic"  => "Quantico:700Italic",
"Quantico Italic"  => "Quantico:400Italic",
"Quattrocento"  => "Quattrocento:400",
"Quattrocento Bold"  => "Quattrocento:700",
"Quattrocento Sans"  => "Quattrocento+Sans:400",
"Quattrocento Sans Bold"  => "Quattrocento+Sans:700",
"Quattrocento Sans Bold Italic"  => "Quattrocento+Sans:700Italic",
"Quattrocento Sans Italic"  => "Quattrocento+Sans:400Italic",
"Questrial"  => "Questrial:400",
"Quicksand"  => "Quicksand:400",
"Quicksand Bold"  => "Quicksand:700",
"Quicksand Book"  => "Quicksand:300",
"Qwigley"  => "Qwigley:400",
"Radley"  => "Radley:400",
"Radley Italic"  => "Radley:400Italic",
"Raleway Ultra-Light"  => "Raleway:100",
"Rammetto One"  => "Rammetto+One:400",
"Rancho"  => "Rancho:400",
"Rationale"  => "Rationale:400",
"Redressed"  => "Redressed:400",
"Reenie Beanie"  => "Reenie+Beanie:400",
"Revalia"  => "Revalia:400",
"Ribeye"  => "Ribeye:400",
"Ribeye Marrow"  => "Ribeye+Marrow:400",
"Righteous"  => "Righteous:400",
"Rochester"  => "Rochester:400",
"Rock Salt"  => "Rock+Salt:400",
"Rokkitt"  => "Rokkitt:400",
"Rokkitt Bold"  => "Rokkitt:700",
"Ropa Sans"  => "Ropa+Sans:400",
"Ropa Sans Italic"  => "Ropa+Sans:400Italic",
"Rosario"  => "Rosario:400",
"Rosario Bold"  => "Rosario:700",
"Rosario Bold Italic"  => "Rosario:700Italic",
"Rosario Italic"  => "Rosario:400Italic",
"Rosarivo"  => "Rosarivo:400",
"Rosarivo Italic"  => "Rosarivo:400Italic",
"Rouge Script"  => "Rouge+Script:400",
"Ruda"  => "Ruda:400",
"Ruda Bold"  => "Ruda:700",
"Ruda Ultra-Bold"  => "Ruda:900",
"Ruge Boogie"  => "Ruge+Boogie:400",
"Ruluko"  => "Ruluko:400",
"Ruslan Display"  => "Ruslan+Display:400",
"Russo One"  => "Russo+One:400",
"Ruthie"  => "Ruthie:400",
"Sail"  => "Sail:400",
"Salsa"  => "Salsa:400",
"Sancreek"  => "Sancreek:400",
"Sansita One"  => "Sansita+One:400",
"Sarina"  => "Sarina:400",
"Satisfy"  => "Satisfy:400",
"Schoolbell"  => "Schoolbell:400",
"Seaweed Script"  => "Seaweed+Script:400",
"Sevillana"  => "Sevillana:400",
"Shadows Into Light"  => "Shadows+Into+Light:400",
"Shadows Into Light Two"  => "Shadows+Into+Light+Two:400",
"Shanti"  => "Shanti:400",
"Share"  => "Share:400",
"Share Bold"  => "Share:700",
"Share Bold Italic"  => "Share:700Italic",
"Share Italic"  => "Share:400Italic",
"Shojumaru"  => "Shojumaru:400",
"Short Stack"  => "Short+Stack:400",
"Siemreap"  => "Siemreap:400",
"Sigmar One"  => "Sigmar+One:400",
"Signika"  => "Signika:400",
"Signika Bold"  => "Signika:700",
"Signika Book"  => "Signika:300",
"Signika Negative"  => "Signika+Negative:400",
"Signika Negative Bold"  => "Signika+Negative:700",
"Signika Negative Book"  => "Signika+Negative:300",
"Signika Negative Semi-Bold"  => "Signika+Negative:600",
"Signika Semi-Bold"  => "Signika:600",
"Simonetta"  => "Simonetta:400",
"Simonetta Italic"  => "Simonetta:400Italic",
"Simonetta Ultra-Bold"  => "Simonetta:900",
"Simonetta Ultra-Bold Italic"  => "Simonetta:900Italic",
"Sirin Stencil"  => "Sirin+Stencil:400",
"Six Caps"  => "Six+Caps:400",
"Slackey"  => "Slackey:400",
"Smokum"  => "Smokum:400",
"Smythe"  => "Smythe:400",
"Sniglet Extra-Bold"  => "Sniglet:800",
"Snippet"  => "Snippet:400",
"Sofia"  => "Sofia:400",
"Sonsie One"  => "Sonsie+One:400",
"Sorts Mill Goudy"  => "Sorts+Mill+Goudy:400",
"Sorts Mill Goudy Italic"  => "Sorts+Mill+Goudy:400Italic",
"Special Elite"  => "Special+Elite:400",
"Spicy Rice"  => "Spicy+Rice:400",
"Spinnaker"  => "Spinnaker:400",
"Spirax"  => "Spirax:400",
"Squada One"  => "Squada+One:400",
"Stardos Stencil"  => "Stardos+Stencil:400",
"Stardos Stencil Bold"  => "Stardos+Stencil:700",
"Stint Ultra Condensed"  => "Stint+Ultra+Condensed:400",
"Stint Ultra Expanded"  => "Stint+Ultra+Expanded:400",
"Stoke"  => "Stoke:400",
"Sue Ellen Francisco"  => "Sue+Ellen+Francisco:400",
"Sunshiney"  => "Sunshiney:400",
"Supermercado One"  => "Supermercado+One:400",
"Suwannaphum"  => "Suwannaphum:400",
"Swanky and Moo Moo"  => "Swanky+and+Moo+Moo:400",
"Syncopate"  => "Syncopate:400",
"Syncopate Bold"  => "Syncopate:700",
"Tangerine"  => "Tangerine:400",
"Tangerine Bold"  => "Tangerine:700",
"Taprom"  => "Taprom:400",
"Telex"  => "Telex:400",
"Tenor Sans"  => "Tenor+Sans:400",
"The Girl Next Door"  => "The+Girl+Next+Door:400",
"Tienne"  => "Tienne:400",
"Tienne Bold"  => "Tienne:700",
"Tienne Ultra-Bold"  => "Tienne:900",
"Tinos"  => "Tinos:400",
"Tinos Bold"  => "Tinos:700",
"Tinos Bold Italic"  => "Tinos:700Italic",
"Tinos Italic"  => "Tinos:400Italic",
"Titan One"  => "Titan+One:400",
"Trade Winds"  => "Trade+Winds:400",
"Trocchi"  => "Trocchi:400",
"Trochut"  => "Trochut:400",
"Trochut Bold"  => "Trochut:700",
"Trochut Italic"  => "Trochut:400Italic",
"Trykker"  => "Trykker:400",
"Tulpen One"  => "Tulpen+One:400",
"Ubuntu"  => "Ubuntu:400",
"Ubuntu Bold"  => "Ubuntu:700",
"Ubuntu Bold Italic"  => "Ubuntu:700Italic",
"Ubuntu Book"  => "Ubuntu:300",
"Ubuntu Book Italic"  => "Ubuntu:300Italic",
"Ubuntu Condensed"  => "Ubuntu+Condensed:400",
"Ubuntu Italic"  => "Ubuntu:400Italic",
"Ubuntu Medium"  => "Ubuntu:500",
"Ubuntu Medium Italic"  => "Ubuntu:500Italic",
"Ubuntu Mono"  => "Ubuntu+Mono:400",
"Ubuntu Mono Bold"  => "Ubuntu+Mono:700",
"Ubuntu Mono Bold Italic"  => "Ubuntu+Mono:700Italic",
"Ubuntu Mono Italic"  => "Ubuntu+Mono:400Italic",
"Ultra"  => "Ultra:400",
"Uncial Antiqua"  => "Uncial+Antiqua:400",
"UnifrakturCook Bold"  => "UnifrakturCook:700",
"UnifrakturMaguntia"  => "UnifrakturMaguntia:400",
"Unkempt"  => "Unkempt:400",
"Unkempt Bold"  => "Unkempt:700",
"Unlock"  => "Unlock:400",
"Unna"  => "Unna:400",
"Varela"  => "Varela:400",
"Varela Round"  => "Varela+Round:400",
"Vast Shadow"  => "Vast+Shadow:400",
"Vibur"  => "Vibur:400",
"Vidaloka"  => "Vidaloka:400",
"Viga"  => "Viga:400",
"Voces"  => "Voces:400",
"Volkhov"  => "Volkhov:400",
"Volkhov Bold"  => "Volkhov:700",
"Volkhov Bold Italic"  => "Volkhov:700Italic",
"Volkhov Italic"  => "Volkhov:400Italic",
"Vollkorn"  => "Vollkorn:400",
"Vollkorn Bold"  => "Vollkorn:700",
"Vollkorn Bold Italic"  => "Vollkorn:700Italic",
"Vollkorn Italic"  => "Vollkorn:400Italic",
"Voltaire"  => "Voltaire:400",
"VT323"  => "VT323:400",
"Waiting for the Sunrise"  => "Waiting+for+the+Sunrise:400",
"Wallpoet"  => "Wallpoet:400",
"Walter Turncoat"  => "Walter+Turncoat:400",
"Wellfleet"  => "Wellfleet:400",
"Wire One"  => "Wire+One:400",
"Yanone Kaffeesatz"  => "Yanone+Kaffeesatz:400",
"Yanone Kaffeesatz Bold"  => "Yanone+Kaffeesatz:700",
"Yanone Kaffeesatz Book"  => "Yanone+Kaffeesatz:300",
"Yanone Kaffeesatz Light"  => "Yanone+Kaffeesatz:200",
"Yellowtail"  => "Yellowtail:400",
"Yeseva One"  => "Yeseva+One:400",
"Yesteryear"  => "Yesteryear:400",
"Zeyada"  => "Zeyada:400");



	function render_cpanel_google_webfonts( $options ) {
				global $google_webfonts_array, $google_webfonts;
				$generic_fonts = array("Arial,'DejaVu Sans','Liberation Sans',Freesans,sans-serif",
			"Arial Narrow','Nimbus Sans L',sans-serif",
			"Arial Black',Gadget,sans-serif",
			"Bookman Old Style',Bookman,'URW Bookman L','Palatino Linotype',serif",
			"Century Gothic',futura,'URW Gothic L',Verdana,sans-serif",
			"Comic Sans MS',cursive",
			"Consolas,'Lucida Console','DejaVu Sans Mono',monospace",
			"Courier New',Courier,'Nimbus Mono L',monospace",
			"Constantina,Georgia,'Nimbus Roman No9 L',serif",
			"Helvetica,Arial,'DejaVu Sans','Liberation Sans',Freesans,sans-serif",
			"Impact, Haettenschweiler, 'Arial Narrow Bold', sans-serif",
			"Lucida Sans Unicode','Lucida Grande','Lucida Sans','DejaVu Sans Condensed',sans-serif",
			"Cambria,'Palatino Linotype','Book Antiqua','URW Palladio L',serif",
			"symbol,'Standard Symbols L'",
			"Tahoma',sans-serif",
			"Cambria,'Times New Roman','Nimbus Roman No9 L','Freeserif',Times,serif",
			"Trebuchet MS',sans-serif",
			"Verdana,Geneva,'DejaVu Sans',sans-serif",
			"Webdings,fantasy",
			"Wingdings,fantasy",
			"Monotype Corsiva','Apple Chancery','ITC Zapf Chancery','URW Chancery L',cursive",
			"Monotype Sorts',dingbats,'ITC Zapf Dingbats',fantasy");			
				foreach($google_webfonts_array as $google_webfonts_item) {
?>
    		
		                
                <div class="params_div1">
                <div class="params_label">
                
                <?php esc_html_e('General Font Settings', 'Current'); ?>:</div>
			    
                <div class="params_value">
                
				<label for="font_family" style="float:left; width:220px;">
					<?php esc_html_e('Font Family: ', 'Current'); ?><br />
					<select name="Current_options[font_family]" id="font_family" style="width:200px;">					<?php 
						foreach($generic_fonts as $generic_font) { ?>
					    <option value="<?php echo $generic_font;?>"<?php echo ($options['font_family'] == $generic_font) ? ' selected="selected"' : ''; ?>><?php echo str_replace("'", "", $generic_font);?></option>

<?php					  }

						echo '<optgroup label="Google Web Fonts:">';
						foreach ($google_webfonts as $the_display_font_name=>$web_font_name) {
						    $make_current_font_selected = ($options['font_family'] == $web_font_name) ? ' selected="selected"' : '';
						    echo '<option value="'.$web_font_name.'"'.$make_current_font_selected.'>'.$the_display_font_name.'</option>';
						}
						echo '</optgroup>';
					   ?>
					</select>
				</label>
				<label for="font_size" style="float:left; width:100px;">
					<?php esc_html_e('Font Size: ', 'Current'); ?><br />
					<select name="Current_options[font_size]" id="font_size" style="padding-right:5px;">
					    <option value="8"<?php echo ($options['font_size'] == '8') ? ' selected="selected"' : ''; ?>>8px</option>
					    <option value="9"<?php echo ($options['font_size'] == '9') ? ' selected="selected"' : ''; ?>>9px</option>
					    <option value="10"<?php echo ($options['font_size'] == '10') ? ' selected="selected"' : ''; ?>>10px</option>
					    <option value="11"<?php echo ($options['font_size'] == '11') ? ' selected="selected"' : ''; ?>>11px</option>
					    <option value="12"<?php echo ($options['font_size'] == '12') ? ' selected="selected"' : ''; ?> style="padding-right:7px;">12px (Default)</option>
					    <option value="13"<?php echo ($options['font_size'] == '13') ? ' selected="selected"' : ''; ?>>13px</option>
					    <option value="14"<?php echo ($options['font_size'] == '14') ? ' selected="selected"' : ''; ?>>14px</option>
					    <option value="15"<?php echo ($options['font_size'] == '15') ? ' selected="selected"' : ''; ?>>15px</option>
					    <option value="16"<?php echo ($options['font_size'] == '16') ? ' selected="selected"' : ''; ?>>16px</option>
					    <option value="17"<?php echo ($options['font_size'] == '17') ? ' selected="selected"' : ''; ?>>17px</option>
					    <option value="18"<?php echo ($options['font_size'] == '18') ? ' selected="selected"' : ''; ?>>18px</option>
					    <option value="19"<?php echo ($options['font_size'] == '19') ? ' selected="selected"' : ''; ?>>19px</option>
					    <option value="20"<?php echo ($options['font_size'] == '20') ? ' selected="selected"' : ''; ?>>20px</option>
					    <option value="21"<?php echo ($options['font_size'] == '21') ? ' selected="selected"' : ''; ?>>21px</option>
					    <option value="22"<?php echo ($options['font_size'] == '22') ? ' selected="selected"' : ''; ?>>22px</option>
					    <option value="23"<?php echo ($options['font_size'] == '23') ? ' selected="selected"' : ''; ?>>23px</option>
					    <option value="24"<?php echo ($options['font_size'] == '24') ? ' selected="selected"' : ''; ?>>24px</option>
					    <option value="25"<?php echo ($options['font_size'] == '25') ? ' selected="selected"' : ''; ?>>25px</option>
					    <option value="26"<?php echo ($options['font_size'] == '26') ? ' selected="selected"' : ''; ?>>26px</option>
					    <option value="27"<?php echo ($options['font_size'] == '27') ? ' selected="selected"' : ''; ?>>27px</option>
					    <option value="28"<?php echo ($options['font_size'] == '28') ? ' selected="selected"' : ''; ?>>28px</option>
					    <option value="32"<?php echo ($options['font_size'] == '32') ? ' selected="selected"' : ''; ?>>32px</option>
					    <option value="36"<?php echo ($options['font_size'] == '36') ? ' selected="selected"' : ''; ?>>36px</option>
                        <option value="48"<?php echo ($options['font_size'] == '48') ? ' selected="selected"' : ''; ?>>48px</option>
                        <option value="60"<?php echo ($options['font_size'] == '60') ? ' selected="selected"' : ''; ?>>60px</option>
                        <option value="72"<?php echo ($options['font_size'] == '72') ? ' selected="selected"' : ''; ?>>72px</option>
					</select>
				</label> 
                <label for="line_height" style="float:left; width:100px; margin-left:30px;">
					<?php esc_html_e('Line Height: ', 'Current'); ?><br />
					<select name="Current_options[line_height]" id="line_height" style="padding-right:5px;">
					    <option value="1"<?php echo ($options['line_height'] == '1') ? ' selected="selected"' : ''; ?>>1</option>
					    <option value="1.1"<?php echo ($options['line_height'] == '1.1') ? ' selected="selected"' : ''; ?>>1.1</option>
					    <option value="1.2"<?php echo ($options['line_height'] == '1.2') ? ' selected="selected"' : ''; ?>>1.2</option>
					    <option value="1.3"<?php echo ($options['line_height'] == '1.3') ? ' selected="selected"' : ''; ?>>1.3</option>
					    <option value="1.4"<?php echo ($options['line_height'] == '1.4') ? ' selected="selected"' : ''; ?>>1.4</option>
					    <option value="1.5"<?php echo ($options['line_height'] == '1.5') ? ' selected="selected"' : ''; ?>>1.5</option>
					    <option value="1.6"<?php echo ($options['line_height'] == '1.6') ? ' selected="selected"' : ''; ?>>1.6</option>
					    <option value="1.7"<?php echo ($options['line_height'] == '1.7') ? ' selected="selected"' : ''; ?> style="padding-right:7px;">1.7 (Default)</option>
					    <option value="1.8"<?php echo ($options['line_height'] == '1.8') ? ' selected="selected"' : ''; ?>>1.8</option>
					    <option value="1.9"<?php echo ($options['line_height'] == '1.9') ? ' selected="selected"' : ''; ?>>1.9</option>
					    <option value="2"<?php echo ($options['line_height'] == '2') ? ' selected="selected"' : ''; ?>>2</option>
					    <option value="2.1"<?php echo ($options['line_height'] == '2.1') ? ' selected="selected"' : ''; ?>>2.1</option>
					    <option value="2.2"<?php echo ($options['line_height'] == '2.2') ? ' selected="selected"' : ''; ?>>2.2</option>
					    <option value="2.3"<?php echo ($options['line_height'] == '2.3') ? ' selected="selected"' : ''; ?>>2.3</option>
					    <option value="2.4"<?php echo ($options['line_height'] == '2.4') ? ' selected="selected"' : ''; ?>>2.4</option>
					    <option value="2.5"<?php echo ($options['line_height'] == '2.5') ? ' selected="selected"' : ''; ?>>2.5</option>
					    <option value="2.6"<?php echo ($options['line_height'] == '2.6') ? ' selected="selected"' : ''; ?>>2.6</option>
					    <option value="2.7"<?php echo ($options['line_height'] == '2.7') ? ' selected="selected"' : ''; ?>>2.7</option>
					    <option value="2.8"<?php echo ($options['line_height'] == '2.8') ? ' selected="selected"' : ''; ?>>2.8</option>
					    <option value="2.8"<?php echo ($options['line_height'] == '2.9') ? ' selected="selected"' : ''; ?>>2.9</option>
					    <option value="3"<?php echo ($options['line_height'] == '3') ? ' selected="selected"' : ''; ?>>3</option>

					</select>
				</label>
                <br class="clear" />
			    </div>
                </div>
                <div class="params_div2">
                <div class="params_label">
                <?php esc_html_e('Top Menu Font Settings', 'Current'); ?>:</div>
                <div class="params_value">
			    
                
				<label for="top_nav_font_family" style="float:left; width:220px;">
					<?php esc_html_e('Font Family: ', 'Current'); ?><br />
					<select name="Current_options[top_nav_font_family]" id="top_nav_font_family" style="width:200px;">
                    <?php
					    foreach($generic_fonts as $generic_font) { ?>
					    <option value="<?php echo $generic_font;?>"<?php echo ($options['top_nav_font_family'] == $generic_font) ? ' selected="selected"' : ''; ?>><?php echo str_replace("'", "", $generic_font);?></option>

<?php					  }
						echo '<optgroup label="Google Web Fonts:">';
						foreach ($google_webfonts as $the_display_font_name=>$web_font_name) {
						    $make_current_font_selected = ($options['top_nav_font_family'] == $web_font_name) ? ' selected="selected"' : '';
						    echo '<option value="'.$web_font_name.'"'.$make_current_font_selected.'>'.$the_display_font_name.'</option>';
						}
						echo '</optgroup>';
					     ?>
					</select>
				</label>
				<label for="top_nav_font_size" style="float:left; width:100px;">
					<?php esc_html_e('Font Size: ', 'Current'); ?>
					<select name="Current_options[top_nav_font_size]" id="top_nav_font_size">
					    <option value="8"<?php echo ($options['top_nav_font_size'] == '8') ? ' selected="selected"' : ''; ?>>8px</option>
					    <option value="9"<?php echo ($options['top_nav_font_size'] == '9') ? ' selected="selected"' : ''; ?>>9px</option>
					    <option value="10"<?php echo ($options['top_nav_font_size'] == '10') ? ' selected="selected"' : ''; ?>>10px</option>
					    <option value="11"<?php echo ($options['top_nav_font_size'] == '11') ? ' selected="selected"' : ''; ?>>11px</option>
					    <option value="12"<?php echo ($options['top_nav_font_size'] == '12') ? ' selected="selected"' : ''; ?>>12px</option>
					    <option value="13"<?php echo ($options['top_nav_font_size'] == '13') ? ' selected="selected"' : ''; ?>>13px</option>
					    <option value="14"<?php echo ($options['top_nav_font_size'] == '14') ? ' selected="selected"' : ''; ?> style="padding-right:7px;">14px (Default)</option>
					    <option value="15"<?php echo ($options['top_nav_font_size'] == '15') ? ' selected="selected"' : ''; ?>>15px</option>
					    <option value="16"<?php echo ($options['top_nav_font_size'] == '16') ? ' selected="selected"' : ''; ?>>16px</option>
					    <option value="17"<?php echo ($options['top_nav_font_size'] == '17') ? ' selected="selected"' : ''; ?>>17px</option>
					    <option value="18"<?php echo ($options['top_nav_font_size'] == '18') ? ' selected="selected"' : ''; ?>>18px</option>
					    <option value="19"<?php echo ($options['top_nav_font_size'] == '19') ? ' selected="selected"' : ''; ?>>19px</option>
					    <option value="20"<?php echo ($options['top_nav_font_size'] == '20') ? ' selected="selected"' : ''; ?>>20px</option>
					    <option value="21"<?php echo ($options['top_nav_font_size'] == '21') ? ' selected="selected"' : ''; ?>>21px</option>
					    <option value="22"<?php echo ($options['top_nav_font_size'] == '22') ? ' selected="selected"' : ''; ?>>22px</option>
					    <option value="23"<?php echo ($options['top_nav_font_size'] == '23') ? ' selected="selected"' : ''; ?>>23px</option>
					    <option value="24"<?php echo ($options['top_nav_font_size'] == '24') ? ' selected="selected"' : ''; ?>>24px</option>
					    <option value="25"<?php echo ($options['top_nav_font_size'] == '25') ? ' selected="selected"' : ''; ?>>25px</option>
					    <option value="26"<?php echo ($options['top_nav_font_size'] == '26') ? ' selected="selected"' : ''; ?>>26px</option>
					    <option value="27"<?php echo ($options['top_nav_font_size'] == '27') ? ' selected="selected"' : ''; ?>>27px</option>
					    <option value="28"<?php echo ($options['top_nav_font_size'] == '28') ? ' selected="selected"' : ''; ?>>28px</option>
					    <option value="32"<?php echo ($options['top_nav_font_size'] == '32') ? ' selected="selected"' : ''; ?>>32px</option>
					    <option value="36"<?php echo ($options['top_nav_font_size'] == '36') ? ' selected="selected"' : ''; ?>>36px</option>
                        <option value="48"<?php echo ($options['top_nav_font_size'] == '48') ? ' selected="selected"' : ''; ?>>48px</option>
                        <option value="60"<?php echo ($options['top_nav_font_size'] == '60') ? ' selected="selected"' : ''; ?>>60px</option>
                        <option value="72"<?php echo ($options['top_nav_font_size'] == '72') ? ' selected="selected"' : ''; ?>>72px</option>
					</select>
				</label>
                <label for="top_nav_sub_font_size" style="float:left; width:150px; margin-left:30px; ">
					<?php esc_html_e('Sub Menu Font Size: ', 'Current'); ?><br />
					<select name="Current_options[top_nav_sub_font_size]" id="top_nav_sub_font_size" style="padding-right:5px;">
					    <option value="1"<?php echo ($options['top_nav_sub_font_size'] == '1') ? ' selected="selected"' : ''; ?>>1 em</option>
					    <option value="0.95"<?php echo ($options['top_nav_sub_font_size'] == '0.95') ? ' selected="selected"' : ''; ?>>0.95 em</option>
					    <option value="0.90"<?php echo ($options['top_nav_sub_font_size'] == '0.90') ? ' selected="selected"' : ''; ?>>0.90 em</option>
					    <option value="0.85"<?php echo ($options['top_nav_sub_font_size'] == '0.85') ? ' selected="selected"' : ''; ?>>0.85 em</option>
					    <option value="0.80"<?php echo ($options['top_nav_sub_font_size'] == '0.80') ? ' selected="selected"' : ''; ?>>0.80 em</option>
					    <option value="0.75"<?php echo ($options['top_nav_sub_font_size'] == '0.75') ? ' selected="selected"' : ''; ?>>0.75 em</option>
					    <option value="0.70"<?php echo ($options['top_nav_sub_font_size'] == '0.70') ? ' selected="selected"' : ''; ?>>0.70 em</option>
					    <option value="0.65"<?php echo ($options['top_nav_sub_font_size'] == '0.65') ? ' selected="selected"' : ''; ?>>0.65 em</option>
					    <option value="0.60"<?php echo ($options['top_nav_sub_font_size'] == '0.60') ? ' selected="selected"' : ''; ?>>0.60 em</option>
					    <option value="0.55"<?php echo ($options['top_nav_sub_font_size'] == '0.55') ? ' selected="selected"' : ''; ?>>0.55 em</option>
					    <option value="0.50"<?php echo ($options['top_nav_sub_font_size'] == '0.50') ? ' selected="selected"' : ''; ?>>0.50 em</option>
					    <option value="0.45"<?php echo ($options['top_nav_sub_font_size'] == '0.45') ? ' selected="selected"' : ''; ?>>0.45 em</option>
					    <option value="0.40"<?php echo ($options['top_nav_sub_font_size'] == '0.40') ? ' selected="selected"' : ''; ?>>0.40 em</option>
					    <option value="0.35"<?php echo ($options['top_nav_sub_font_size'] == '0.35') ? ' selected="selected"' : ''; ?>>0.35 em</option>
					    <option value="0.30"<?php echo ($options['top_nav_sub_font_size'] == '0.30') ? ' selected="selected"' : ''; ?>>0.30 em</option>
					    <option value="0.25"<?php echo ($options['top_nav_sub_font_size'] == '0.25') ? ' selected="selected"' : ''; ?>>0.25 em</option>
					    <option value="0.20"<?php echo ($options['top_nav_sub_font_size'] == '0.20') ? ' selected="selected"' : ''; ?>>0.20 em</option>
					    <option value="0.15"<?php echo ($options['top_nav_sub_font_size'] == '0.15') ? ' selected="selected"' : ''; ?>>0.15 em</option>
					    <option value="0.10"<?php echo ($options['top_nav_sub_font_size'] == '0.10') ? ' selected="selected"' : ''; ?>>0.10 em</option>
	

					</select>
				</label>
                
                 <label for="top_nav_font_transform" style="float:left; width:150px; margin-left:30px; ">
					<?php esc_html_e('Transform: ', 'Current'); ?><br />
					<select name="Current_options[top_nav_font_transform]" id="top_nav_font_transform" style="padding-right:5px;">
					    <option value="none"<?php echo ($options['top_nav_font_transform'] == 'none') ? ' selected="selected"' : ''; ?>>None</option>
					    <option value="lowercase"<?php echo ($options['top_nav_font_transform'] == 'lowercase') ? ' selected="selected"' : ''; ?>>Lowercase</option>
					    <option value="uppercase"<?php echo ($options['top_nav_font_transform'] == 'uppercase') ? ' selected="selected"' : ''; ?>>Uppercase</option>                        					    
					    <option value="capitalize"<?php echo ($options['top_nav_font_transform'] == 'capitalize') ? ' selected="selected"' : ''; ?>>Capitalize</option>	

					</select>
				</label>
                <br class="clear" />
			    </div>
                </div>
                
			<div class="params_div1">
            <div class="params_label">
            
            <?php esc_html_e('Title Headings Font Settings', 'Current'); ?>
            </div>
			<div class="params_value">
            
				<label for="title_headings_font_family" style="float:left; width:220px;">
				    <?php esc_html_e('Font Family: ', 'Current'); ?><br />
				    <select name="Current_options[title_headings_font_family]" id="title_headings_font_family" style="width:200px;">
					    <?php
					    foreach($generic_fonts as $generic_font) { ?>
					    <option value="<?php echo $generic_font;?>"<?php echo ($options['title_headings_font_family'] == $generic_font) ? ' selected="selected"' : ''; ?>><?php echo str_replace("'", "", $generic_font);?></option>

<?php					  }
						echo '<optgroup label="Google Web Fonts:">';
						foreach ($google_webfonts as $the_display_font_name=>$web_font_name) {
						    $make_current_font_selected = ($options['title_headings_font_family'] == $web_font_name) ? ' selected="selected"' : '';
						    echo '<option value="'.$web_font_name.'"'.$make_current_font_selected.'>'.$the_display_font_name.'</option>';
						}
						echo '</optgroup>';
					    ?>
				    </select>
				</label>
				<label for="heading_font_size_coefficient" style="float:left; width:130px;">
					<?php esc_html_e('Font Size Coefficient: ', 'Current'); ?><br />
					<select name="Current_options[heading_font_size_coefficient]" id="heading_font_size_coefficient">
					    <option value="0.2"<?php echo ($options['heading_font_size_coefficient'] == '0.2') ? ' selected="selected"' : ''; ?>>0.2</option>
					    <option value="0.4"<?php echo ($options['heading_font_size_coefficient'] == '0.4') ? ' selected="selected"' : ''; ?>>0.4</option>
					    <option value="0.6"<?php echo ($options['heading_font_size_coefficient'] == '0.6') ? ' selected="selected"' : ''; ?>>0.6</option>
					    <option value="0.8"<?php echo ($options['heading_font_size_coefficient'] == '0.8') ? ' selected="selected"' : ''; ?>>0.8</option>
					    <option value="1.0"<?php echo ($options['heading_font_size_coefficient'] == '1.0') ? ' selected="selected"' : ''; ?> style="padding-right:7px;">1.0 (Default)</option>
					    <option value="1.2"<?php echo ($options['heading_font_size_coefficient'] == '1.2') ? ' selected="selected"' : ''; ?>>1.2</option>
					    <option value="1.4"<?php echo ($options['heading_font_size_coefficient'] == '1.4') ? ' selected="selected"' : ''; ?>>1.4</option>
					    <option value="1.6"<?php echo ($options['heading_font_size_coefficient'] == '1.6') ? ' selected="selected"' : ''; ?>>1.6</option>
					    <option value="1.8"<?php echo ($options['heading_font_size_coefficient'] == '1.8') ? ' selected="selected"' : ''; ?>>1.8</option>
					    <option value="2.0"<?php echo ($options['heading_font_size_coefficient'] == '2.0') ? ' selected="selected"' : ''; ?>>2.0</option>
					    <option value="2.2"<?php echo ($options['heading_font_size_coefficient'] == '2.2') ? ' selected="selected"' : ''; ?>>2.2</option>
					    <option value="2.4"<?php echo ($options['heading_font_size_coefficient'] == '2.4') ? ' selected="selected"' : ''; ?>>2.4</option>
					    <option value="2.6"<?php echo ($options['heading_font_size_coefficient'] == '2.6') ? ' selected="selected"' : ''; ?>>2.6</option>
					    <option value="2.8"<?php echo ($options['heading_font_size_coefficient'] == '2.8') ? ' selected="selected"' : ''; ?>>2.8</option>
					</select>
				</label>
                  <label for="heading_font_transform" style="float:left; width:150px; margin-left:30px; ">
					<?php esc_html_e('Transform: ', 'Current'); ?><br />
					<select name="Current_options[heading_font_transform]" id="heading_font_transform" style="padding-right:5px;">
					    <option value="none"<?php echo ($options['heading_font_transform'] == 'none') ? ' selected="selected"' : ''; ?>>None</option>
					    <option value="lowercase"<?php echo ($options['heading_font_transform'] == 'lowercase') ? ' selected="selected"' : ''; ?>>Lowercase</option>
					    <option value="uppercase"<?php echo ($options['heading_font_transform'] == 'uppercase') ? ' selected="selected"' : ''; ?>>Uppercase</option>                        					    
					    <option value="capitalize"<?php echo ($options['heading_font_transform'] == 'capitalize') ? ' selected="selected"' : ''; ?>>Capitalize</option>	

					</select>
				</label>               
				
                <br class="clear" />
			</div>
            </div>
					
<?php				}
				
				display_save_changes_button(2); 				
			}?>