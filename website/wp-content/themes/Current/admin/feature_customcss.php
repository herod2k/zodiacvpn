<?php

global $widgetcss_options;
if((!$widgetcss_options = get_option('widget_css')) || !is_array($widgetcss_options) ) $widgetcss_options = array();

if (is_admin()) {
	add_action( 'sidebar_admin_setup', 'widget_css_expand_control');
	add_filter( 'widget_update_callback', 'widget_css_widget_update_callback', 10, 3); 
}

function widget_css_expand_control() {
	global $wp_registered_widgets, $wp_registered_widget_controls, $widgetcss_options;

	if ( 'post' == strtolower($_SERVER['REQUEST_METHOD']) )
	{	foreach ( (array) $_POST['widget-id'] as $widget_number => $widget_id )
			if (isset($_POST[$widget_id.'-widget_css']))
				$widgetcss_options[$widget_id]=$_POST[$widget_id.'-widget_css'];
	}
	


	update_option('widget_css', $widgetcss_options);

	
	foreach ( $wp_registered_widgets as $id => $widget )
	{	
		if (!$wp_registered_widget_controls[$id])
			wp_register_widget_control($id,$widget['name'], 'widget_css_empty_control');
		$wp_registered_widget_controls[$id]['callback_widget_css_redirect']=$wp_registered_widget_controls[$id]['callback'];
		$wp_registered_widget_controls[$id]['callback']='widget_css_extra_control';
		array_push($wp_registered_widget_controls[$id]['params'],$id);	
	}	
	
	
}


function widget_css_extra_control()
{	global $wp_registered_widget_controls, $widgetcss_options;

	$params=func_get_args();
	$id=array_pop($params);


	$callback=$wp_registered_widget_controls[$id]['callback_widget_css_redirect'];
	if (is_callable($callback))
		call_user_func_array($callback, $params);		
	
	$value = !empty( $widgetcss_options[$id ] ) ? htmlspecialchars( stripslashes( $widgetcss_options[$id ] ),ENT_QUOTES ) : '';


	if(isset($params[0]['number']))
		$number=$params[0]['number'];
	if (isset($number) && $number==-1) {$number="%i%"; $value="";}
	$id_disp=$id;
	if (isset($number)) $id_disp=$wp_registered_widget_controls[$id]['id_base'].'-'.$number;


	echo "<p><label for='".$id_disp."-widget_css'>".__('CSS class', 'Current')." <input type='text' name='".$id_disp."-widget_css' id='".$id_disp."-widget_css' value='".$value."' /></label></p>";
}

function widget_css_widget_update_callback($instance, $new_instance, $this_widget)
{	
	global $widgetcss_options;

	$widget_id=$this_widget->id;
	if ( isset($_POST[$widget_id.'-widget_css']))
	{	$widgetcss_options[$widget_id]=$_POST[$widget_id.'-widget_css'];
		update_option('widget_css', $widgetcss_options);
	}
	return $instance;
}
?>