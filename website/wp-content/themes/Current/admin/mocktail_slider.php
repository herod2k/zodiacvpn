<?php add_action( 'load-post.php', 'mocktail_slider_mbox_setup' );
			add_action( 'load-post-new.php', 'mocktail_slider_mbox_setup' );
			add_action('save_post', 'save_mocktail_slider_mbox');
 function mocktail_slider_mbox_setup() {
				add_action( 'add_meta_boxes', 'add_mocktail_slider_mbox_setup' );
			}
 function add_mocktail_slider_mbox_setup() {

				add_meta_box(
					'mocktail-slider-mbox',			// Unique ID
					esc_html__( 'Page Slider', 'Current' ),		// Title
					'mocktail_slider_mbox',		// Callback function
					'',					// Admin page (or post type)
					'normal',					// Context
					'core'					// Priority
				);
			}
 function mocktail_slider_mbox( $object, $box ) {
				global $mocktail_slider_array, $post, $Current_options;
				foreach($mocktail_slider_array as $mocktail_slider) {
			?>
            	<div class="params_div">
				<select name="<?php echo $mocktail_slider['id'].'_val';?>">
                <option value="none" <?php echo (get_post_meta($post->ID, $mocktail_slider['id'].'_val', true) == "none")?"selected='selected'":"";?>><?php _e('None', 'Current'); ?></option>
					<?php
					if(isset($Current_options['mocktail_slider_list'])) {
						foreach(explode("|", $Current_options['mocktail_slider_list']) as $item) { ?>
							<option value="<?php echo $item; ?>" <?php echo (get_post_meta($post->ID, $mocktail_slider['id'].'_val', true) == $item)?"selected='selected'":"";?>><?php echo $item; ?></option>
					<?php }
					} ?>
					
				</select>
                </div>
			<?php
				echo "<br style='clear:both;' />"; ?>
                <span class="legend"><?php printf( __('Select a slider to be assigned to this page. You can define these sliders at the %1$sAppearance -> Theme Options%2$s page.<br />Note: To Select a slider for the front page, configure the slider settings found here %1$sAppearance -> Theme Options%2$s', 'Current'), '<a href="themes.php?page=Current_settings_page">', '</a>' ); ?> </span> 
                <?php 
				}
			}
 function save_mocktail_slider_mbox($post_id) {
				global $mocktail_slider_array;
					foreach($mocktail_slider_array as $mocktail_slider) {
						if (isset($_REQUEST[$mocktail_slider['id'].'_val'])) {
							update_post_meta($post_id, $mocktail_slider['id'].'_val', $_REQUEST[$mocktail_slider['id'].'_val']);
						}
					}
			}
 function render_cpanel_mocktail_slider( $options ) {
				?>
                <div class="params_div1">   
						<div class="params_label">
							 <label for="mocktail_slider_frontpage" ><?php esc_html_e('Slider assigned to front page:', 'Current'); ?> </label></div>
									<div class="params_value">
									<select name="Current_options[mocktail_slider_frontpage]">
										<?php 
                                        if(isset($options['mocktail_slider_list'])) {
                                            foreach(explode("|", $options['mocktail_slider_list']) as $item) { ?>
                                                <option value="<?php echo $item; ?>" <?php echo ($options['mocktail_slider_frontpage'] == $item)?"selected='selected'":"";?>><?php echo $item; ?></option>
                                        <?php }
                                        } ?>
                                        <option value="none" <?php echo ($options['mocktail_slider_frontpage'] == "none")?"selected='selected'":"";?>><?php _e('None', 'Current'); ?></option>
                                    </select>
									</div>
						 </div>
                         
				<div class="params_div2">   
						<div class="params_label">
							 <label for="mocktail_slider_archive" ><?php esc_html_e('Slider assigned to generic pages:', 'Current'); ?> </label></div>
									<div class="params_value">
									<select name="Current_options[mocktail_slider_archive]">
										<?php 
                                        if(isset($options['mocktail_slider_list'])) {
                                            foreach(explode("|", $options['mocktail_slider_list']) as $item) { ?>
                                                <option value="<?php echo $item; ?>" <?php echo ($options['mocktail_slider_archive'] == $item)?"selected='selected'":"";?>><?php echo $item; ?></option>
                                        <?php }
                                        } ?>
                                        <option value="none" <?php echo ($options['mocktail_slider_archive'] == "none")?"selected='selected'":"";?>><?php _e('None', 'Current'); ?></option>
                                    </select>
                                    <span class="legend"><?php  _e('Select slider to be assigned to the generic pages. These pages consist of archive, search results and other templates that are not assigned to any of the pages.', 'Current'); ?></span>
									</div>
						 </div>
                         
				<div class="params_title"><?php esc_html_e('Add / Remove sliders', 'Current'); ?></div>
				<div class="params_div1">   
						<div class="params_label">
							 <label for="mocktail_slider_new_name" ><?php esc_html_e('New slider name:', 'Current'); ?> </label></div>
									<div class="params_value">
									<input name="mocktail_slider_new_name" type="text" id="mocktail_slider_new_name" value="" maxlength="32" />
				
									<input type="button" class="button-secondary" value="<?php esc_html_e('Add new slider', 'Current'); ?>" id="add_mocktail_slider" />
									</div>
						 </div>
				
							 <div class="params_div2">
							 <div class="params_label" > </div>
										<div class="params_value">
										<div style="float:left; width:510px">
										<select name="mocktail_slider_collection[]" id="mocktail_slider_collection" class="mocktail_slider_collection" multiple="multiple" style="height:200px; width:500px;">
				
										 </select></div>
										 <div style="float:left; width:200px"><input type="button" class="button-secondary" value="<?php _e('Remove slider(s)', 'Current');?>" id="remove_mocktail_slider" /></div>
									<input type="hidden" id="mocktail_slider_list" name="Current_options[mocktail_slider_list]" value="<?php echo isset($options['mocktail_slider_list'])?$options['mocktail_slider_list']:"";?>" />
                                    <input type="hidden" id="mocktail_slider_current" name="Current_options[mocktail_slider_current]" value="<?php echo isset($options['mocktail_slider_current'])?$options['mocktail_slider_current']:"";?>" />
									<br class="clear" />  
									</div>
									</div>
				<?php
				display_save_changes_button(); ?>
				<div class="params_title"><?php esc_html_e('Individual Slider Settings', 'Current'); ?></div>
				<div class="params_div1">
					<div class="params_all">
						<span class="legend"><?php esc_html_e('Double click on any of the slider instance above to add/remove slides and configure other settings here.', 'Current'); ?></span>
						<div  id="mocktail_slider_settings_container">
						
						</div>
					</div>
				</div>
				
				<?php
				display_save_changes_button(2); 				
			}
 add_action('wp_ajax_mocktail_slider_settings', 'mocktail_slider_settings');
 
			 function mocktail_slider_settings() {
				global $Current_options;
				ob_start();
				?>
				<div>
					<table width="100%" border="0">
					  <tr>
						<td><?php esc_html_e('Slide Image url', 'Current'); ?>:</td>
						<td><input type="text" disabled="disabled" name="Current_options[<?php echo 'mocktail_'.md5($_POST['slider']).'_slide999_img';?>]" id="<?php echo 'mocktail_'.md5($_POST['slider']).'_slide999_img';?>" value="param1" style="width:400px;" /> <input id="upload_<?php echo 'mocktail_'.md5($_POST['slider']).'_slide999_img';?>"  type="button" value="<?php esc_attr_e('Upload Image', 'Current'); ?>" class="button-secondary upmediabt" /></td>
					  </tr>
					  <tr>
						<td><?php esc_html_e('Slide Image 2 url', 'Current'); ?>:</td>
						<td><input type="text" disabled="disabled" name="Current_options[<?php echo 'mocktail_'.md5($_POST['slider']).'_slide999_img2';?>]" id="<?php echo 'mocktail_'.md5($_POST['slider']).'_slide999_img2';?>" value="param2" style="width:400px;" /> <input id="upload_<?php echo 'mocktail_'.md5($_POST['slider']).'_slide999_img2';?>"  type="button" value="<?php esc_attr_e('Upload Image', 'Current'); ?>" class="button-secondary upmediabt" /></td>
					  </tr>
					  <tr>
						<td><?php esc_html_e('Slide Text', 'Current'); ?>:</td>
						<td><textarea disabled="disabled" name="Current_options[<?php echo 'mocktail_'.md5($_POST['slider']).'_slide999_text';?>]" id="<?php echo 'mocktail_'.md5($_POST['slider']).'_slide999_text';?>" style="width:400px;">param3</textarea></td>
					  </tr>
					</table>
                    <div class="remove_mocktail_slide"></div>
				</div>
				<?php
				$slidetemplate = ob_get_clean();
				?>
                <div class="params_title"><?php esc_html_e('Settings for', 'Current'); echo " ".$_POST['slider']; ?></div>
				<div id="slidetemplate" style="display:none;">
					<?php echo str_replace("param3", "", str_replace("param2", "", str_replace("param1", "", $slidetemplate))) ;?>
				</div>
				<div class="params_div1">
					<div class="params_label">
					<label for="mocktail_<?php echo md5($_POST['slider']);?>_height"><?php esc_html_e('Height', 'Current'); ?>:</label>
					</div>
					<div class="params_value">
					 <input name="Current_options[mocktail_<?php echo md5($_POST['slider']);?>_height]" type="text" id="mocktail_<?php echo md5($_POST['slider']);?>_height" value="<?php echo (isset($Current_options['mocktail_'.md5($_POST['slider']).'_height'])?$Current_options['mocktail_'.md5($_POST['slider']).'_height']:'360');?>" /> px
					
					</div>
				</div>
				<div class="params_div2">
					<div class="params_label">
					<label for="mocktail_<?php echo md5($_POST['slider']);?>_effect"><?php esc_html_e('Effect', 'Current'); ?>:</label>
					</div>
					<div class="params_value">
					 <select name="Current_options[mocktail_<?php echo md5($_POST['slider']);?>_effect]" id="mocktail_<?php echo md5($_POST['slider']);?>_effect">
						    <option value="slide"<?php echo ($Current_options['mocktail_'.md5($_POST['slider']).'_effect'] == 'slide') ? ' selected="selected"' : ''; ?> style="padding-right:9px;">Slide</option>
						    <option value="fade"<?php echo ($Current_options['mocktail_'.md5($_POST['slider']).'_effect'] == 'fade') ? ' selected="selected"' : ''; ?>>Fade</option>
                            <option value="mix"<?php echo ($Current_options['mocktail_'.md5($_POST['slider']).'_effect'] == 'mix') ? ' selected="selected"' : ''; ?>>Mix</option>
						</select>
					
					</div>
				</div>
				<div class="params_div1">
					<div class="params_label">
					<label for="mocktail_<?php echo md5($_POST['slider']);?>_timeout"><?php esc_html_e('Slide timeout', 'Current'); ?>:</label>
					</div>
					<div class="params_value">
					 <input name="Current_options[mocktail_<?php echo md5($_POST['slider']);?>_timeout]" type="text" id="mocktail_<?php echo md5($_POST['slider']);?>_timeout" value="<?php echo (isset($Current_options['mocktail_'.md5($_POST['slider']).'_timeout'])?$Current_options['mocktail_'.md5($_POST['slider']).'_timeout']:'5000');?>" /> px
					
					</div>
				</div>
                <div class="params_div2">				
				<input type="button" class="button-secondary" value="<?php esc_html_e('Add Slide', 'Current'); ?>" id="add_mocktail_slide" /> <br />
<br />
<span class="legend"><?php esc_html_e('You can also drag the slides below to edit the order of appearance.', 'Current'); ?></span>
                </div>
				<input type="hidden" id="mocktail_slides_list" name="Current_options[mocktail_<?php echo md5($_POST['slider']);?>]" value="<?php echo isset($Current_options['mocktail_'.md5($_POST['slider'])])?$Current_options['mocktail_'.md5($_POST['slider'])]:""; ?>" />
				
				<ul id="mocktail_slides">
				<?php
				if(isset($Current_options['mocktail_'.md5($_POST['slider'])]) && $Current_options['mocktail_'.md5($_POST['slider'])]!="") {
					foreach(explode("|", $Current_options['mocktail_'.md5($_POST['slider'])]) as $slide) {
						echo '<li>'.str_replace("param3", (isset($Current_options['mocktail_'.md5($_POST['slider']).'_slide'.$slide.'_text'])?$Current_options['mocktail_'.md5($_POST['slider']).'_slide'.$slide.'_text']:''), str_replace("param2", (isset($Current_options['mocktail_'.md5($_POST['slider']).'_slide'.$slide.'_img2'])?$Current_options['mocktail_'.md5($_POST['slider']).'_slide'.$slide.'_img2']:''), str_replace("param1", (isset($Current_options['mocktail_'.md5($_POST['slider']).'_slide'.$slide.'_img'])?$Current_options['mocktail_'.md5($_POST['slider']).'_slide'.$slide.'_img']:''), str_replace("slide999", "slide".$slide, $slidetemplate)))).'</li>';
					}
				}?>
				</ul>
				<?php
				die();
			 }
			 ?>